package ru.xlv.trade.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@RequiredArgsConstructor
public class SimpleTradeRequest<T> {

    private final T initiator, target;

    private boolean initiatorConfirmedTrade, targetConfirmedTrade;

    private int initiatorCurrency, targetCurrency;

    private List<ItemStack> initiatorTradeItems = new ArrayList<>();
    private List<ItemStack> targetTradeItems = new ArrayList<>();
}
