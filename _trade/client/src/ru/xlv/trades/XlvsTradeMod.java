package ru.xlv.trades;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.util.KeyBindingsExecutive;
import ru.xlv.core.util.Utils;
import ru.xlv.mochar.XlvsMainMod;
import ru.xlv.trades.handle.TradeHandler;
import ru.xlv.trades.network.PacketTradeCancel;
import ru.xlv.trades.network.PacketTradeConfirm;
import ru.xlv.trades.network.PacketTradeInvite;
import ru.xlv.trades.network.PacketTradeSync;

import static ru.xlv.trades.XlvsTradeMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsTradeMod {

    static final String MODID = "xlvstrades";

    @Mod.Instance(MODID)
    public static XlvsTradeMod INSTANCE;

    @Getter
    private final TradeHandler tradeHandler = new TradeHandler();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsMainMod.INSTANCE.getKeyRegistry().register(new KeyBindingsExecutive("Send trade invite", Keyboard.KEY_G, "Trade", () -> {
            EntityLivingBase closestEntityLookedAt = Utils.getClosestEntityLookedAt(4D, 0);
            if(closestEntityLookedAt instanceof EntityPlayer) {
                getTradeHandler().invitePlayer(closestEntityLookedAt.getCommandSenderName()).thenAcceptSync(result -> {
                    //todo оповещение
                });
            }
        }));

        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketTradeCancel(),
                new PacketTradeConfirm(),
                new PacketTradeInvite(),
                new PacketTradeSync()
        );
    }
}
