package ru.xlv.trades.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.trade.common.SimpleTradeRequest;
import ru.xlv.trades.XlvsTradeMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketTradeSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        SimpleTradeRequest<String> currentTradeRequest = new SimpleTradeRequest<>(bbis.readUTF(), bbis.readUTF());
        currentTradeRequest.setInitiatorConfirmedTrade(bbis.readBoolean());
        currentTradeRequest.setTargetConfirmedTrade(bbis.readBoolean());
        currentTradeRequest.setInitiatorCurrency(bbis.readInt());
        currentTradeRequest.setTargetCurrency(bbis.readInt());
        int c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            currentTradeRequest.getInitiatorTradeItems().add(ByteBufUtils.readItemStack(bbis.getBuffer()));
        }
        c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            currentTradeRequest.getTargetTradeItems().add(ByteBufUtils.readItemStack(bbis.getBuffer()));
        }
        XlvsTradeMod.INSTANCE.getTradeHandler().setCurrentTradeRequest(currentTradeRequest);
    }
}
