package ru.xlv.trades.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.trades.handle.TradeRequest;

import java.io.IOException;

@NoArgsConstructor
public class PacketTradeSync implements IPacketOutServer {

    private TradeRequest tradeRequest;

    public PacketTradeSync(TradeRequest tradeRequest) {
        this.tradeRequest = tradeRequest;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(tradeRequest.getInitiator().getPlayerName());
        bbos.writeUTF(tradeRequest.getTarget().getPlayerName());
        bbos.writeBoolean(tradeRequest.isInitiatorConfirmedTrade());
        bbos.writeBoolean(tradeRequest.isTargetConfirmedTrade());
        bbos.writeInt(tradeRequest.getInitiatorCurrency());
        bbos.writeInt(tradeRequest.getTargetCurrency());
        bbos.writeInt(tradeRequest.getInitiatorTradeItems().size());
        for (ItemStack itemStack : tradeRequest.getInitiatorTradeItems()) {
            ByteBufUtils.writeItemStack(bbos.buffer(), itemStack);
        }
        bbos.writeInt(tradeRequest.getTargetTradeItems().size());
        for (ItemStack itemStack : tradeRequest.getTargetTradeItems()) {
            ByteBufUtils.writeItemStack(bbos.buffer(), itemStack);
        }
    }
}
