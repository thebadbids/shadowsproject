package ru.xlv.mgame.arena.duel;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class DuelConfig implements IConfigGson {

    @Getter
    @RequiredArgsConstructor
    public static class SpawnPositions {
        @Configurable
        private final String serverAddress;
        @Configurable
        private final WorldPosition firstPosition;
        @Configurable
        private final WorldPosition secondsPosition;
        @Setter
        private boolean reserved;
    }

    private final List<SpawnPositions> worldPositions = new ArrayList<>();

    @Override
    public File getConfigFile() {
        return new File("config/mini_games/duel/config.json");
    }
}
