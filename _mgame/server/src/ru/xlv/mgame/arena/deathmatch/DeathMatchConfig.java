package ru.xlv.mgame.arena.deathmatch;

import lombok.Getter;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.ItemStackModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class DeathMatchConfig implements IConfigGson {

    private final int maxActiveArenaAmount = 2;
    private final int maxPlayersPerArena = 20;
    private final int scorePerKill = 1;
    private final float creditsPerScore = 1F;
    private final int ratingPerScore = 1;

    private final List<ItemStackModel> itemsPerMatch = new ArrayList<>();
    private final List<WorldPosition> spawnWorldPosition = new ArrayList<>();

    @Override
    public File getConfigFile() {
        return new File("config/mini_games/death_match/config.json");
    }
}
