package ru.xlv.mgame.arena.teambattle;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerDamagePlayerEvent;
import ru.xlv.core.event.PlayerKilledEvent;
import ru.xlv.core.event.ServerPlayerLogoutEvent;
import ru.xlv.core.event.ServerPlayerRespawnEvent;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.JsonUtils;
import ru.xlv.mgame.arena.Arena;
import ru.xlv.mgame.arena.ArenaState;
import ru.xlv.mgame.arena.ArenaTeam;
import ru.xlv.mgame.util.ArenaLocalization;
import ru.xlv.mochar.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public class ArenaTeamBattle extends Arena {

    private final int registrationTimer = 0, preparationTimer = 3, matchProcessTimer = 120, postMatchTimer = 5;

    private final TObjectIntMap<ServerPlayer> playerScoreMap = new TObjectIntHashMap<>();
    private final Map<ArenaTeam, List<ServerPlayer>> playerMap = new HashMap<>();

    private final TeamBattleConfig config;
    private final ArenaLocalization arenaLocalization;
    private final TeamBattleLocalization localization;

    private int teamScoreBlue, teamScoreRed;

    {
        playerMap.put(ArenaTeam.RED, new ArrayList<>());
        playerMap.put(ArenaTeam.BLUE, new ArrayList<>());
    }

    @Override
    public void start() {
        super.start();
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void stop() {
        super.stop();
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @Override
    public void updatePerTick() {
        super.updatePerTick();
        switch (getArenaState()) {
            case REGISTRATION:
                if(getNumberOfPlayers() >= config.getMinPlayersLimit()) {
                    setArenaState(ArenaState.MATCH_PROCESS);
                }
                break;
            case MATCH_PROCESS:
                if(teamScoreBlue >= config.getTeamScoreToWin()) {
                    processWin(ArenaTeam.BLUE);
                } else if(teamScoreRed >= config.getTeamScoreToWin()) {
                    processWin(ArenaTeam.RED);
                }
                controlStateTimeout(() -> {
                    if(teamScoreRed == teamScoreBlue) {
                        processEndWithDraw();
                    } else if(teamScoreBlue > teamScoreRed) {
                        processWin(ArenaTeam.BLUE);
                    } else {
                        processWin(ArenaTeam.RED);
                    }
                });
                break;
            case POST_MATCH:
                controlStateTimeout();
                break;
            case DONE:
                stop();
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerDamagePlayerEvent event) {
        if(getPlayerTeam(event.getAttacker()) == getPlayerTeam(event.getTarget())) {
            event.setCanceled(true);
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(ServerPlayerLogoutEvent event) {
        synchronized (playerMap) {
            playerMap.keySet().forEach(arenaTeam -> playerMap.get(arenaTeam).removeIf(serverPlayer -> serverPlayer == event.getServerPlayer()));
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerKilledEvent event) {
        if(event.getKillerServerPlayer() == event.getTargetServerPlayer()) return;
        ServerPlayer killer = null;
        ServerPlayer target = null;
        for (ServerPlayer serverPlayer : getPlayerList()) {
            if (serverPlayer == event.getTargetServerPlayer()) {
                target = serverPlayer;
            } else if (serverPlayer == event.getKillerServerPlayer()) {
                killer = serverPlayer;
            }
        }
        if(killer != null && target != null) {
            int score = config.getScorePerKill();
            playerScoreMap.adjustOrPutValue(killer, score, score);
            if (getPlayerTeam(killer) == ArenaTeam.BLUE) {
                teamScoreBlue += score;
            } else {
                teamScoreRed += score;
            }
            sendMessageToAllPlayers(arenaLocalization.getFormatted(arenaLocalization.getPlayerKilledPlayer(), killer.getPlayerName(), target.getPlayerName()));
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(ServerPlayerRespawnEvent event) {
        synchronized (getPlayerList()) {
            if(getPlayerList().contains(event.getServerPlayer())) {
                respawnPlayer(event.getServerPlayer());
            }
        }
    }

    private void processWin(ArenaTeam arenaTeam) {
        playerMap.get(getOppositeTeam(arenaTeam)).forEach(serverPlayer -> Utils.sendMessage(serverPlayer, localization.getFormatted(localization.getTeamWinMessage(), arenaTeam)));
        playerMap.get(arenaTeam).forEach(serverPlayer -> Utils.sendMessage(serverPlayer, localization.getYourTeamWonMessage()));
        setArenaState(ArenaState.POST_MATCH);
    }

    private void processEndWithDraw() {
        playerMap.forEach((arenaTeam, serverPlayers) -> serverPlayers.forEach(serverPlayer -> Utils.sendMessage(serverPlayer, localization.getMatchEndedWithDrawMessage())));
        setArenaState(ArenaState.POST_MATCH);
    }

    private void respawnPlayer(ServerPlayer serverPlayer) {
        movePlayer(serverPlayer, getPlayerTeam(serverPlayer) == ArenaTeam.BLUE ? config.getBlueTeamSpawnWorldPosition() : config.getRedTeamSpawnWorldPosition());
        config.getItemsPerMatch().forEach(itemStackModel -> {
            ItemStack itemStack = ItemStackFactory.create(Item.getItemById(itemStackModel.getId()), itemStackModel.getCount(), itemStackModel.getDamage(), null);
            itemStack.setTagCompound(JsonUtils.jsonToNBT(itemStackModel.getStringedNbtTagCompound()));
            giveArenaItem(serverPlayer, itemStack);
        });
    }

    public ArenaTeam getPlayerTeam(ServerPlayer serverPlayer) {
        for (ArenaTeam arenaTeam : playerMap.keySet()) {
            if (playerMap.get(arenaTeam).contains(serverPlayer)) {
                return arenaTeam;
            }
        }
        return null;
    }

    public void sendMessageToAllPlayers(String message, Object... params) {
        String message1 = String.format(message, params);
        playerMap.forEach((arenaTeam, serverPlayers) -> serverPlayers.forEach(serverPlayer -> Utils.sendMessage(serverPlayer, message1)));
    }

    public boolean addPlayers(List<ServerPlayer> players) {
        if(getNumberOfPlayers() + 1 > config.getMaxPlayersLimit()) {
            return false;
        }
        int blue = playerMap.get(ArenaTeam.BLUE).size();
        int red = playerMap.get(ArenaTeam.RED).size();
        ArenaTeam arenaTeam = blue > red ? ArenaTeam.RED : ArenaTeam.BLUE;
        players.forEach(serverPlayer -> addPlayer(serverPlayer, arenaTeam, true));
        return true;
    }

    public boolean addPlayer(ServerPlayer serverPlayer, ArenaTeam arenaTeam, boolean forceJoin) {
        if(getNumberOfPlayers() + 1 > config.getMaxPlayersLimit()) {
            return false;
        }
        List<ServerPlayer> list = playerMap.get(arenaTeam);
        if(!forceJoin) {
            int blue = playerMap.get(ArenaTeam.BLUE).size();
            int red = playerMap.get(ArenaTeam.RED).size();
            if (Math.abs(blue - red) >= config.getTeamSizeDifferenceValue()) {
                if ((blue > red && arenaTeam == ArenaTeam.BLUE) || (blue < red && arenaTeam == ArenaTeam.RED)) {
                    return false;
                }
            }
        }
        list.add(serverPlayer);
        return true;
    }

    public boolean addPlayer(ServerPlayer serverPlayer) {
        return addPlayer(serverPlayer, ArenaTeam.BLUE, false) || addPlayer(serverPlayer, ArenaTeam.RED, false);
    }

    public boolean removePlayer(ServerPlayer serverPlayer) {
        preRemoveLogic(serverPlayer);
        for (ArenaTeam arenaTeam : playerMap.keySet()) {
            if(playerMap.get(arenaTeam).removeIf(serverPlayer1 -> serverPlayer == serverPlayer1)) {
                return true;
            }
        }
        return false;
    }

    private ArenaTeam getOppositeTeam(ArenaTeam arenaTeam) {
        return arenaTeam == ArenaTeam.BLUE ? ArenaTeam.RED : ArenaTeam.BLUE;
    }

    @Override
    public int getNumberOfPlayers() {
        int i = 0;
        for (ArenaTeam arenaTeam : playerMap.keySet()) {
            i += playerMap.get(arenaTeam).size();
        }
        return i;
    }
}
