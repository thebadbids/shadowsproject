package ru.xlv.mgame.arena.teambattle;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class TeamBattleLocalization extends Localization {

    private final String responseJoinResultSuccessMessage = "";
    private final String responseJoinResultNoAvailableArenaFoundMessage = "";
    private final String responseJoinResultUnknownMessage = "";

    private final String teamWinMessage = "{0} team won!";
    private final String yourTeamWonMessage = "Your team won!";
    private final String matchEndedWithDrawMessage = "Match ended with a draw!";

    @Override
    public File getConfigFile() {
        return new File("config/mini_games/team_battle/localization.json");
    }
}
