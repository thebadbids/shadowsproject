package ru.xlv.mgame.arena.teambattle;

import lombok.Getter;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.common.util.config.ConfigComment;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.ItemStackModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class TeamBattleConfig implements IConfigGson {

    @ConfigComment("Допустимая разница количества игроков между командами")
    private final int teamSizeDifferenceValue = 2;
    private final int maxPlayersLimit = 20;
    private final int minPlayersLimit = 2;
    private final int scorePerKill = 1;
    private final int creditsPerScore = 1;
    private final int teamScoreToWin = 100;

    private final WorldPosition redTeamSpawnWorldPosition = new WorldPosition(0, 0, 0, 0);
    private final WorldPosition blueTeamSpawnWorldPosition = new WorldPosition(0, 0, 0, 0);

    private final List<ItemStackModel> itemsPerMatch = new ArrayList<>();

    @Override
    public File getConfigFile() {
        return new File("config/mini_games/team_battle/config.json");
    }
}
