package ru.xlv.mgame.arena;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public abstract class Arena {

    protected static final String arenaItemSpecNBTTag = "isArenaItem";

    private final Map<ServerPlayer, WorldPosition> playerSourcePositions = new HashMap<>();
    private final List<ServerPlayer> playerList = new ArrayList<>();

    private ArenaState arenaState = ArenaState.REGISTRATION;

    private boolean isActive;

    private int timer;

    public void start() {
        isActive = true;
    }
    public void stop() {
        isActive = false;
    }

    public void updatePerSecond() {
        timer--;
    }

    public void updatePerTick() {}

    public boolean addPlayer(ServerPlayer serverPlayer) {
        return playerList.add(serverPlayer);
    }

    public boolean removePlayer(ServerPlayer serverPlayer) {
        preRemoveLogic(serverPlayer);
        return playerList.remove(serverPlayer);
    }

    protected void preRemoveLogic(ServerPlayer serverPlayer) {
        WorldPosition worldPosition = getPlayerSourcePositions().remove(serverPlayer);
        if (worldPosition != null) {
            movePlayer(serverPlayer, worldPosition);
        }
        serverPlayer.getSelectedCharacter().getMatrixInventory().removeItems(itemStack -> {
            NBTTagCompound tagCompound = itemStack.getTagCompound();
            return tagCompound != null && tagCompound.hasKey(arenaItemSpecNBTTag) && tagCompound.getBoolean(arenaItemSpecNBTTag);
        });
    }

    protected void giveArenaItem(ServerPlayer serverPlayer, ItemStack itemStack) {
        NBTTagCompound nbtTagCompound = itemStack.getTagCompound() == null ? new NBTTagCompound() : itemStack.getTagCompound();
        nbtTagCompound.setBoolean(arenaItemSpecNBTTag, true);
        itemStack.setTagCompound(nbtTagCompound);
        serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(itemStack);
    }

    protected void movePlayer(ServerPlayer serverPlayer, WorldPosition worldPosition) {
        XlvsCore.INSTANCE.getPlayerManager().movePlayer(serverPlayer, worldPosition);
    }

    protected void savePlayerSourcePosition(ServerPlayer serverPlayer) {
        playerSourcePositions.put(serverPlayer, new WorldPosition(
                serverPlayer.getEntityPlayer().dimension,
                serverPlayer.getEntityPlayer().posX,
                serverPlayer.getEntityPlayer().posY,
                serverPlayer.getEntityPlayer().posZ
        ));
    }

    public void sendMessageToAllPlayers(String message, Object... params) {
        String message1 = String.format(message, params);
        getPlayerList().forEach(serverPlayer -> Utils.sendMessage(serverPlayer, message1));
    }

    protected void processInitState(Runnable runnable) {
        if (getTimer() == getMaxTimer(arenaState)) {
            runnable.run();
        }
    }

    protected boolean controlStateTimeout() {
        if (getTimer() <= 0) {
            setArenaState(getNextArenaState());
            return true;
        }
        return false;
    }

    protected boolean controlStateTimeout(Runnable runnable) {
        if(controlStateTimeout()) {
            runnable.run();
        }
        return false;
    }

    public void setArenaState(ArenaState arenaState) {
        this.arenaState = arenaState;
        timer = getMaxTimer(arenaState);
    }

    public int getMaxTimer(ArenaState arenaState) {
        switch (arenaState) {
            case REGISTRATION: return getRegistrationTimer();
            case PREPARATION: return getPreparationTimer();
            case MATCH_PROCESS: return getMatchProcessTimer();
            case POST_MATCH: return getPostMatchTimer();
        }
        return 0;
    }

    public ArenaState getNextArenaState() {
        switch (arenaState) {
            case REGISTRATION: return ArenaState.PREPARATION;
            case PREPARATION: return ArenaState.MATCH_PROCESS;
            case MATCH_PROCESS: return ArenaState.POST_MATCH;
            default: return ArenaState.DONE;
        }
    }

    public int getNumberOfPlayers() {
        return getPlayerList().size();
    }

    protected abstract int getRegistrationTimer();
    protected abstract int getPreparationTimer();
    protected abstract int getMatchProcessTimer();
    protected abstract int getPostMatchTimer();
}
