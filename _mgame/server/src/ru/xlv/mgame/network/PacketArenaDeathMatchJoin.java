package ru.xlv.mgame.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mgame.XlvsMGameMod;
import ru.xlv.mgame.arena.deathmatch.DeathMatchJoinResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketArenaDeathMatchJoin implements IPacketCallbackOnServer {

    private DeathMatchJoinResult deathMatchJoinResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        deathMatchJoinResult = XlvsMGameMod.INSTANCE.getArenaDeathMatchManager().joinArena(entityPlayer.getCommandSenderName());
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(deathMatchJoinResult == DeathMatchJoinResult.SUCCESS);
        bbos.writeUTF(deathMatchJoinResult.getResponseMessage());
    }
}
