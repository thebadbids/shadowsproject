package ru.xlv.mgame.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mgame.XlvsMGameMod;
import ru.xlv.mgame.arena.teambattle.JoinTeamBattleResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketArenaTeamBattleJoin implements IPacketCallbackOnServer {

    private JoinTeamBattleResult joinTeamBattleResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        joinTeamBattleResult = XlvsMGameMod.INSTANCE.getArenaTeamBattleManager().joinArena(entityPlayer.getCommandSenderName());
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(joinTeamBattleResult == JoinTeamBattleResult.SUCCESS);
        bbos.writeUTF(joinTeamBattleResult.getResponseMessage());
    }
}
