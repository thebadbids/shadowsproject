package net.minecraft.src;

import net.minecraft.client.resources.AbstractResourcePack;

import java.io.File;

public class ResourceUtils
{

    public static File getResourcePackFile(AbstractResourcePack arp)
    {
        return arp.resourcePackFile;
    }
}
