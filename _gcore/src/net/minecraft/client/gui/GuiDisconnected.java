package net.minecraft.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.Utils;
import ru.xlv.core.gui.GuiScreenDelegate;
import ru.xlv.customfont.FontType;

import java.util.Iterator;
import java.util.List;

@SideOnly(Side.CLIENT)
public class GuiDisconnected extends AbstractGuiScreenAdvanced
{
    private String field_146306_a;
    private IChatComponent field_146304_f;
    private List field_146305_g;
    private final GuiScreen field_146307_h;

    public GuiDisconnected(GuiScreen p_i45020_1_, String p_i45020_2_, IChatComponent p_i45020_3_)
    {
        this.field_146307_h = p_i45020_1_;
        this.field_146306_a = I18n.format(p_i45020_2_, new Object[0]);
        this.field_146304_f = p_i45020_3_;
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key) {}

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.buttonList.clear();

        float buttonWidth = 271;
        float buttonHeight = 40;
        float x = 960;
        float y = 680;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), I18n.format("gui.toMenu").toUpperCase());
        button.setTexture(new ResourceLocation("escmenu", "textures/button.png"));
        button.setTextureHover(new ResourceLocation("escmenu", "textures/button_hover.png"));
        button.setMaskTexture(new ResourceLocation("escmenu", "textures/button_mask.png"));
        this.buttonList.add(button);
        this.field_146305_g = this.fontRendererObj.listFormattedStringToWidth(this.field_146304_f.getFormattedText(), this.width - 50);
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        if (guiButton.id == 0)
        {
            this.mc.displayGuiScreen(this.field_146307_h);
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        super.drawScreen(mouseX, mouseY, partialTick);
        float fontScale = 2.5f;
        Utils.bindTexture(GuiScreenDelegate.background, false);
        GuiDrawUtils.drawRect(0, 0, ScaleGui.screenWidth, ScaleGui.screenHeight, 1f, 1f, 1f, 1f);
        float y = 540 - 250;
        GuiDrawUtils.drawCenteredStringCenter(FontType.FUTURA_PT_MEDIUM, this.field_146306_a, 960, y, fontScale, 11184810);
        y += 40;

        if (this.field_146305_g != null)
        {
            for (Iterator iterator = this.field_146305_g.iterator(); iterator.hasNext(); y += FontType.FUTURA_PT_MEDIUM.getFontContainer().height() * fontScale)
            {
                String s = (String)iterator.next();
                GuiDrawUtils.drawCenteredStringCenter(FontType.FUTURA_PT_MEDIUM, s, 960, y, fontScale, 16777215);
            }
        }

        drawButtons(mouseX, mouseY, partialTick);
    }
}