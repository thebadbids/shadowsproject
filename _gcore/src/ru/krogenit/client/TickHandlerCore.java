package ru.krogenit.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import ru.krogenit.lighting.LightManagerClient;
import ru.krogenit.utils.AnimationHelper;

public class TickHandlerCore {

    @SubscribeEvent
    public void event(TickEvent.RenderTickEvent event) {
        if(event.phase == Phase.START) {
            AnimationHelper.updateAnimationSpeed();
        }
    }

    @SubscribeEvent
    public void event(TickEvent.ClientTickEvent event) {
        if(event.phase == Phase.START) {
            LightManagerClient.update();
        }
    }
}
