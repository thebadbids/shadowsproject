package ru.krogenit.client.gui.api;

import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.customfont.StringCache;

public class GuiTextFieldAdvanced extends GuiTextField {

	protected static final Minecraft mc = Minecraft.getMinecraft();
    protected final float fontScale;
    protected FontType fontType;
    protected FontContainer fontContainer;
    protected final float baseX, baseY;
    @Setter protected boolean canEdit;

    public GuiTextFieldAdvanced(FontType fontType, float x, float y, float width, float height, float fontScale, int textColor, int stringLength) {
        this(fontType, x, y, width, height, fontScale);
        this.enabledColor = textColor;
        setMaxStringLength(stringLength);
    }

    public GuiTextFieldAdvanced(FontType fontType, float x, float y, float width, float height, float fontScale) {
        super(mc.fontRenderer, x, y, width, height);
        this.fontScale = fontScale;
        this.fontType = fontType;
        this.fontContainer = fontType.getFontContainer();
        this.baseX = x;
        this.baseY = y;
        this.canEdit = true;
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if(canEdit) {
            mouseX = Mouse.getEventX();
            mouseY = mc.displayHeight - Mouse.getEventY() - 1;
            boolean flag = mouseX >= this.xPosition && mouseX < this.xPosition + this.width && mouseY >= this.yPosition && mouseY < this.yPosition + this.height;

            if (this.canLoseFocus) {
                this.setFocused(flag);
            }

            if (this.isFocused && mouseButton == 0) {
                float l = (mouseX - xPosition) / fontScale - (this.getEnableBackgroundDrawing() ? ScaleGui.get(6) : 0);
                StringCache stringCache = fontContainer.getTextFont();
                String s = stringCache.trimStringToWidth(this.text.substring(this.lineScrollOffset), getWidth() / fontScale, false);
                this.setCursorPosition(stringCache.trimStringToWidth(s, l, false).length() + this.lineScrollOffset);
            }
        }
    }

    public float getWidth() {
        return this.getEnableBackgroundDrawing() ? this.width - ScaleGui.get(8) : this.width;
    }

    protected void drawBackground() {
        drawRect(this.xPosition - 1, this.yPosition - 1, this.xPosition + this.width + 1, this.yPosition + this.height + 1, -6250336);
        drawRect(this.xPosition, this.yPosition, this.xPosition + this.width, this.yPosition + this.height, -16777216);
    }
    
    @Override
    public void drawTextBox() {
    	if (this.getVisible())
        {
            if (this.getEnableBackgroundDrawing()) {
                drawBackground();
            }

            int textColor = this.isEnabled ? this.enabledColor : this.disabledColor;
            int j = this.cursorPosition - this.lineScrollOffset;
            int k = this.selectionEnd - this.lineScrollOffset;
            String s = fontContainer.getTextFont().trimStringToWidth(this.text.substring(this.lineScrollOffset), this.getWidth() / fontScale, false);
            boolean flag = j >= 0 && j <= s.length();
            boolean flag1 = this.isFocused && this.cursorCounter / 6 % 2 == 0 && flag;
            float x = this.enableBackgroundDrawing ? this.xPosition + ScaleGui.get(4) : this.xPosition;
            float y = this.enableBackgroundDrawing ? this.yPosition + height / 2f - ScaleGui.get(3f) * fontScale : this.yPosition;
            float j1 = x;

            if (k > s.length()) {
                k = s.length();
            }

            if (s.length() > 0) {
                String s1 = flag ? s.substring(0, j) : s;
                GL11.glPushMatrix();
                GL11.glTranslatef(x, y, 0f);
                GL11.glScalef(fontScale, fontScale, 1.0f);
                j1 = fontContainer.drawStringWithShadow(s1, 0, 0, textColor) * fontScale + x;
                GL11.glPopMatrix();
            }

            boolean flag2 = this.cursorPosition < this.text.length() || this.text.length() >= this.getMaxStringLength();
            float k1 = j1;

            if (!flag) {
                k1 = j > 0 ? x + this.width : x;
            } else if (flag2) {
                k1 = j1 - 1;
                --j1;
            }

            if (s.length() > 0 && flag && j < s.length()) {
                GL11.glPushMatrix();
                GL11.glTranslatef(j1, y, 0f);
                GL11.glScalef(fontScale, fontScale, 1.0f);
                fontContainer.drawStringWithShadow(s.substring(j), 0, 0, textColor);
                GL11.glPopMatrix();
            }

            if (flag1) {
                if (flag2) {
                    Gui.drawRect(k1, y - height / 12.4f, k1 + 1, y + 16, -3092272);
                } else {
                    GL11.glPushMatrix();
                    GL11.glTranslatef(k1, y, 0f);
                    GL11.glScalef(fontScale, fontScale, 1.0f);
                    fontContainer.drawStringWithShadow("_", 0, 0, textColor);
                    GL11.glPopMatrix();
                }
            }

            if (k != j) {
                float l1 = x + fontContainer.getTextFont().getStringWidth(s.substring(0, k)) * fontScale;
                this.drawCursorVertical(k1, y - 1, l1 - 1, y + height / 2.2f);
            }
        }
    }

    @Override
    public void setSelectionPos(int i) {
        StringCache stringCache = fontContainer.getTextFont();
        int j = this.text.length();

        if (i > j) {
            i = j;
        }

        if (i < 0) {
            i = 0;
        }

        this.selectionEnd = i;

        if (this.fontRenderer != null) {
            if (this.lineScrollOffset > j) {
                this.lineScrollOffset = j;
            }

            float k = this.getWidth() / fontScale;
            String s = stringCache.trimStringToWidth(this.text.substring(this.lineScrollOffset), k, false);
            int l = s.length() + this.lineScrollOffset;

            if (i == this.lineScrollOffset) {
                this.lineScrollOffset -= stringCache.trimStringToWidth(this.text, k, true).length();
            }

            if (i > l) {
                this.lineScrollOffset += i - l;
            } else if (i <= this.lineScrollOffset) {
                this.lineScrollOffset -= this.lineScrollOffset - i;
            }

            if (this.lineScrollOffset < 0) {
                this.lineScrollOffset = 0;
            }

            if (this.lineScrollOffset > j) {
                this.lineScrollOffset = j;
            }
        }
    }
    
    protected void drawCursorVertical(float x1, float y1, float x2, float y2) {
        float i1;

        if (x1 < x2) {
            i1 = x1;
            x1 = x2;
            x2 = i1;
        }

        if (y1 < y2) {
            i1 = y1;
            y1 = y2;
            y2 = i1;
        }

        if (x2 > this.xPosition + this.width) {
            x2 = this.xPosition + this.width;
        }

        if (x1 > this.xPosition + this.width) {
            x1 = this.xPosition + this.width;
        }

        Tessellator tessellator = Tessellator.instance;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(0.0F, 0.1F, 0.2F, 0.25f);
        tessellator.addVertex(x1, y2, 0.0D);
        tessellator.addVertex(x2, y2, 0.0D);
        tessellator.addVertex(x2, y1, 0.0D);
        tessellator.addVertex(x1, y1, 0.0D);
        tessellator.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    protected float getFontHeight() {
        return ScaleGui.get(fontContainer.height());
    }

    public void addXPosition(float x) {
        this.xPosition = baseX + x;
    }

    public void addYPosition(float y) {
        this.yPosition = baseY + y;
    }
}
