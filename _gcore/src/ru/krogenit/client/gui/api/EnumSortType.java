package ru.krogenit.client.gui.api;

public enum EnumSortType {
    NONE, DESCENDING, ASCENDING;
}
