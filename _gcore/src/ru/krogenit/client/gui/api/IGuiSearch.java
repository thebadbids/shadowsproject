package ru.krogenit.client.gui.api;

public interface IGuiSearch {
    void search(String value);
}
