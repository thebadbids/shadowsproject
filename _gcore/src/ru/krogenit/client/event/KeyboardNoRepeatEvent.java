package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class KeyboardNoRepeatEvent extends Event {
    public boolean shouldProcess;
}
