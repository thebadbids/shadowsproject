package ru.krogenit.lighting;

public class TickableLight {
    public PointLight light;
    public int lifetime, maxLifeTime;
    public EnumTickableLightType type;

    public TickableLight(PointLight light, int lifetime, EnumTickableLightType type) {
        this.type = type;
        this.light = light;
        this.maxLifeTime = lifetime;
        this.lifetime = lifetime;
    }
}
