package ru.krogenit.lighting;

import lombok.Getter;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;

@Getter
public class PointLight {
	public Vector3f pos, color;
	public float power;

	protected AxisAlignedBB aabb;
	
	public PointLight(PointLight p) {
		this.pos = new Vector3f(p.getPos());
		this.color = new Vector3f(p.getColor());
		this.power = p.getPower();
		this.aabb = AxisAlignedBB.getBoundingBox(pos.x - power, pos.y - power, pos.z - power, pos.x + power, pos.y + power, pos.z + power);
	}

	public PointLight(Vector3f pos, Vector3f color, float power) {
		this.pos = pos;
		this.color = color;
		this.power = power;
		this.aabb = AxisAlignedBB.getBoundingBox(pos.x - power, pos.y - power, pos.z - power, pos.x + power, pos.y + power, pos.z + power);
	}
	
	public PointLight(float r, float g, float b, float power) {
		this.pos = new Vector3f();
		this.color = new Vector3f(r, g, b);
		this.power = power;
		this.aabb = AxisAlignedBB.getBoundingBox(pos.x - power, pos.y - power, pos.z - power, pos.x + power, pos.y + power, pos.z + power);
	}

	public void setPosition(Vector3f pos) {
		this.pos.x = pos.x;
		this.pos.y = pos.y;
		this.pos.z = pos.z;
	}
	
	public void refreshAABB() {
		this.aabb = AxisAlignedBB.getBoundingBox(pos.x - power, pos.y - power, pos.z - power, pos.x + power, pos.y + power, pos.z + power);
	}

	@Override
	public String toString() {
		return pos.x + ":" + pos.y + ":" + pos.z;
	}
}
