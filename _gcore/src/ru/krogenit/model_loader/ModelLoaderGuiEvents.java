package ru.krogenit.model_loader;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

public class ModelLoaderGuiEvents {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {
            ModelLoader.loadModels();
            FMLCommonHandler.instance().bus().unregister(this);
        }
    }
}
