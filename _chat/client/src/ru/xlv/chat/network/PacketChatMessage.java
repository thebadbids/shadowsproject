package ru.xlv.chat.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.util.ChatComponentText;
import ru.xlv.chat.XlvsChatMod;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketChatMessage implements IPacketIn, IPacketOut {

    private String message;
    private int chatTypeOrdinal;

    public PacketChatMessage(String message, int chatTypeOrdinal) {
        this.message = message;
        this.chatTypeOrdinal = chatTypeOrdinal;
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        int ordinal = bbis.readInt();
        String message = bbis.readUTF();
        ChatComponentText chatComponentText = (ChatComponentText) ChatComponentText.Serializer.func_150699_a(message);
        XlvsChatMod.INSTANCE.getChannelChatManager().printMessage(chatComponentText, ChatChannelType.values()[ordinal]);
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(chatTypeOrdinal);
        bbos.writeUTF(message);
    }
}
