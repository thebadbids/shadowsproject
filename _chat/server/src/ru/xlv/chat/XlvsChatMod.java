package ru.xlv.chat;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import ru.xlv.chat.handle.ChatEventListener;
import ru.xlv.chat.handle.ChatHandler;
import ru.xlv.chat.handle.ChatNotificationService;
import ru.xlv.chat.network.PacketChatMessage;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.CommonUtils;

import static ru.xlv.chat.XlvsChatMod.MODID;

@Mod(
        name = "XlvsChatMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsChatMod {

    static final String MODID = "xlvschat";

    @Mod.Instance(MODID)
    public static XlvsChatMod INSTANCE;

    private final ChatHandler chatHandler = new ChatHandler();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketChatMessage());
        XlvsCore.INSTANCE.setNotificationService(new ChatNotificationService());
        CommonUtils.registerEvents(new ChatEventListener());
    }

    public ChatHandler getChatHandler() {
        return chatHandler;
    }
}
