package ru.xlv.chat.handle;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.ServerChatEvent;
import ru.xlv.chat.XlvsChatMod;
import ru.xlv.chat.common.ChatChannelType;

public class ChatEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(ServerChatEvent event) {
        XlvsChatMod.INSTANCE.getChatHandler().handleMessage(event.player, event.message, ChatChannelType.LOCATION);
    }
}
