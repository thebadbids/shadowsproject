#version 120

varying vec2 textureCoords;

void main() {
	gl_Position = ftransform();
	gl_Position.y = gl_Position.y*0.25-0.75;
	gl_Position.x = gl_Position.x*0.5-0.5;
	textureCoords = gl_MultiTexCoord0.xy;
}
