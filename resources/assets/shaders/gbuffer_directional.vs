//Krogenit 
#version 120

#define DIRECTIONAL_LIGHTS 1

varying vec2 texCoords;
varying vec3 dirPos;

uniform mat4 modelView;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

void main() 
{	
	dirPos = (modelView * vec4(directLight[0].dir, 1.0)).xyz;
	texCoords = gl_MultiTexCoord0.xy;
	gl_Position = gl_Vertex;
}