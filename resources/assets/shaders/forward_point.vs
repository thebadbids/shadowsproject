//Vertex Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330 core

#define POINT_LIGHTS 1

layout (location = 4) in vec3 in_position;
layout (location = 5) in vec2 in_textureCoords;
layout (location = 6) in vec3 in_normal;
layout (location = 11) in vec3 in_tangent;

out vec2 texCoords;
out vec4 color;
out vec3 position;
out vec3 normal;
out vec3 tangent;
out vec3 delta;
out vec3 lightPos;

uniform mat4 projMat, viewMat;
uniform mat4 modelView;
uniform vec4 in_color;

uniform struct PointLight {
   vec3 position;
   float attenuation;
   vec3 color;
} pointLights[POINT_LIGHTS];

void main() {	
	vec4 pos = viewMat * vec4(in_position, 1.0);
	color = in_color;
	normal = (viewMat * vec4(in_normal, 0.0)).xyz;
	texCoords = in_textureCoords;
	position = pos.xyz;
	gl_Position = projMat * pos;
	tangent = (viewMat * vec4(in_tangent, 0.0)).xyz;	
	
	lightPos = (modelView * vec4(pointLights[0].position, 1.0)).xyz;
}