//Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330
layout (location = 0) out vec4 outputColor;
layout (location = 1) out vec4 bloomColor;

in vec2 texCoords;
in vec4 color;

uniform sampler2D diffuse;
uniform bool useTexture;

uniform float bloomThreshold;
uniform float bloomIntensity;

void main() {
	if(useTexture) {
		outputColor = texture(diffuse, texCoords) * color;
	} else {
		outputColor = color;
	}
	
	float bright = (outputColor.r * 0.2126) + (outputColor.g * 0.7152) + (outputColor.b * 0.0722);
	if(bright > bloomThreshold) {
		bloomColor = outputColor * bloomIntensity;
	} else {
		bloomColor = vec4(0.0,0.0,0.0,outputColor.w);
	}
	
}