#version 120

varying vec2 textureCoords;
uniform sampler2D bloomTexture;

uniform vec2 texelSize;

void main() {

	vec2 quarterResTC = (floor(gl_FragCoord.xy)*2.+0.5)*texelSize;

		gl_FragData[0] = texture2D(bloomTexture,quarterResTC-0.5*vec2(texelSize.x,texelSize.y))/4.*0.5;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+2.0*vec2(texelSize.x,texelSize.y))/4.*0.5;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(-0.5*texelSize.x,2.0*texelSize.y))/4.*0.5;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(2.0*texelSize.x,-0.5*texelSize.y))/4.*0.5;
		
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC-1.5*vec2(texelSize.x,texelSize.y))/4.*0.125;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+0.5*vec2(texelSize.x,texelSize.y))*0.125;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(0.5*texelSize.x,-1.5*texelSize.y))/2*0.125;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(-1.5*texelSize.x,0.5*texelSize.y))/2*0.125;

		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(2.5*texelSize.x,-1.5*texelSize.y))/4.*0.125;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(2.5*texelSize.x,0.5*texelSize.y))/2.*0.125;		
		
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(2.5*texelSize.x,2.5*texelSize.y))/4.*0.125;
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(2.5*texelSize.x,0.5*texelSize.y))/2.*0.125;				
		
		gl_FragData[0] += texture2D(bloomTexture,quarterResTC+vec2(2.5*texelSize.x,-1.5*texelSize.y))/2.*0.125;		

		gl_FragData[0].rgb = clamp(gl_FragData[0].rgb,0.0,65000.);

	if (max(gl_FragCoord.x*texelSize.x,gl_FragCoord.y*texelSize.y) > 0.5) gl_FragData[0].rgb = vec3(0.0);


}