//Krogenit 
#version 120

varying vec2 texCoords;

void main() 
{	
	texCoords = gl_MultiTexCoord0.xy;
	gl_Position = gl_Vertex;
}