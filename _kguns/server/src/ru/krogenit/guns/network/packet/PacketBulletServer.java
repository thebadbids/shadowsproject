package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.guns.entity.EntityBullet;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketBulletServer implements IPacketIn, IPacketOut {

    EntityBullet bullet;
    String playerName;
    Vector3f vect;

    public PacketBulletServer(EntityBullet bullet) {
        this.bullet = bullet;
    }

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        playerName = data.readUTF();
        vect = new Vector3f(data.readFloat(), data.readFloat(), data.readFloat());
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeUTF(bullet.thrower.getDisplayName());
        data.writeFloat((float) bullet.motionX);
        data.writeFloat((float) bullet.motionY);
        data.writeFloat((float) bullet.motionZ);
    }
}