package ru.krogenit.guns.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.krogenit.guns.item.ItemMeleeWeapon;
import ru.krogenit.guns.util.GunUtils;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketMeleeSync implements IPacketOutServer {

    private ItemMeleeWeapon itemGun;

    public PacketMeleeSync(ItemMeleeWeapon itemGun) {
        this.itemGun = itemGun;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        GunUtils.writeMelee(itemGun, bbos);
    }
}
