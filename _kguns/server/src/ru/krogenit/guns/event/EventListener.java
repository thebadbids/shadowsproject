package ru.krogenit.guns.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.krogenit.guns.CoreGunsServer;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemMeleeWeapon;
import ru.krogenit.guns.network.PacketGunSync;
import ru.krogenit.guns.network.PacketMeleeSync;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.event.PlayerMoveItemSpecEvent;
import ru.xlv.core.event.ServerPlayerLoginEvent;

public class EventListener {

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(ServerPlayerLoginEvent event) {
        for (ItemGun itemGun : CoreGunsServer.instance.getRegisteredGunList()) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(event.getServerPlayer().getEntityPlayer(), new PacketGunSync(itemGun));
        }

        for (ItemMeleeWeapon itemGun : CoreGunsServer.instance.getRegisteredMeleeList()) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(event.getServerPlayer().getEntityPlayer(), new PacketMeleeSync(itemGun));
        }
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(PlayerMoveItemSpecEvent event) {
        if (!event.isFromSpec() && event.getItemStack().getItem() instanceof ItemGun) {
            switch (event.getSlotType()) {
                case ADD_WEAPON:
                case MAIN_WEAPON:
                case MELEE_WEAPON:
                case SECOND_WEAPON:
                    return;
            }
            event.setCanceled(true);
        }
    }
}
