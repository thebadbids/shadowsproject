package ru.krogenit.guns.network;

import lombok.NoArgsConstructor;
import ru.krogenit.guns.util.GunUtils;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketMeleeSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        GunUtils.readMelee(bbis);
    }
}
