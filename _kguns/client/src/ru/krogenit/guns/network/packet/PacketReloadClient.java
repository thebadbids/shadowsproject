package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketReloadClient implements IPacketIn, IPacketOut {

    @Override
    public void read(ByteBufInputStream data) throws IOException {

    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {

    }
}
