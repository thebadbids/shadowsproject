package ru.krogenit.guns.render;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;

@NoArgsConstructor
@AllArgsConstructor
@Configurable
public class Vector3fConfigurable implements IConfigGson {
    @IgnoreObf
    public float x, y, z;

    @Override
    public File getConfigFile() {
        return null;
    }
}
