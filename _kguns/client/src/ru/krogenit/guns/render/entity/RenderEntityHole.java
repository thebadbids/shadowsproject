package ru.krogenit.guns.render.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.entity.EntityHole;
import ru.krogenit.utils.Utils;

import static org.lwjgl.opengl.GL11.*;

@SideOnly(Side.CLIENT)
public class RenderEntityHole extends Render {
    private static final ResourceLocation resLoc =  new ResourceLocation(CoreGunsCommon.MODID, "textures/hole.png");
    private static final Minecraft mc = Minecraft.getMinecraft();

    public RenderEntityHole() {

    }

    public void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
        EntityHole hole = (EntityHole) entity;
        int type = hole.placeType;
        glPushMatrix();
        glTranslatef((float) x, (float) y, (float) z);

        switch (type) {
            case 0:
                glRotatef(90, 1, 0, 0);
                glTranslatef(0, 0, 0.0075f);
                glRotatef(180, 0, 1, 0);
                break;
            case 1:
                glRotatef(90, 1, 0, 0);
                glTranslatef(0, 0, -0.0075f);
                break;
            case 2:
                glTranslatef(0, 0, -0.01f);
                break;
            case 3:
                glTranslatef(0, 0, 0.0075f);
                glRotatef(180, 0, 1, 0);
                break;
            case 4:
                glRotatef(90, 0, 1, 0);
                glTranslatef(0, 0, -0.0075f);
                break;
            case 5:
                glRotatef(90, 0, 1, 0);
                glTranslatef(0, 0, 0.0075f);
                glRotatef(180, 0, 1, 0);
                break;
        }

        glDepthMask(false);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glAlphaFunc(GL_GREATER, 0.0F);

        float scale = 0.002f;
        glScalef(scale, scale, scale);
        Utils.bindTexture(resLoc);
        glColor4f(1, 1, 1, hole.colorAlpha);
        Tessellator t = Tessellator.instance;
        t.startDrawingQuads();
        t.setNormal(0.0F, 0.0F, 1.0F);
        t.addVertexWithUV(-40, (double) 0 + 40, 1, 0, 1.0F);
        t.addVertexWithUV(40, (double) 0 + 40, 1, 1, 1.0F);
        t.addVertexWithUV(40, -40, 1, 1, 0.0F);
        t.addVertexWithUV(-40, -40, 1, 0, 0.0F);
        t.draw();

        glDisable(GL_BLEND);
        glDepthMask(true);
        glAlphaFunc(GL_GREATER, 0.1F);
        glPopMatrix();
    }

    @Override
    public void doRenderPost(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {

    }

    public ResourceLocation getEntityTexture(Entity entity) {
        return TextureMap.locationItemsTexture;
    }
}
