package ru.krogenit.guns.render;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum EnumRenderObjectType {

    FIRST_PERSON("firstPerson"), EQUIPPED("equipped"), ENTITY("entity"), INVENTORY("inventory"), LEFT_HAND("leftHand"), RIGHT_HAND("rightHand");

    @Getter private final String name;

}
