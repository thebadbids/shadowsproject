package ru.krogenit.guns.render;

import lombok.Getter;
import lombok.Setter;
import org.lwjgl.util.vector.Vector3f;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;

@Getter
public class AnimationNode implements IConfigGson {
    @Configurable @IgnoreObf
    private Vector3fConfigurable[] weights;
    private Vector3f animation;
    @Configurable(comment = "speed") @IgnoreObf
    private final Vector3fConfigurable speed = new Vector3fConfigurable(0.1f, 0.1f, 0.1f);
    @Configurable(comment = "id") @IgnoreObf
    @Setter private int id;
    @Configurable(comment = "type") @IgnoreObf
    private int typeOrdinal;
    private EnumAnimationType type;

    public AnimationNode(EnumAnimationType type, int id) {
        this.type = type;
        this.id = id;
        this.animation = new Vector3f();
        this.weights = new Vector3fConfigurable[15];
        for (int i = 0; i < weights.length; i++) {
            weights[i] = new Vector3fConfigurable(0,0,0);
        }
    }

    public void init() {
        type = EnumAnimationType.values()[typeOrdinal];
        animation = new Vector3f();
        if(weights == null) weights = new Vector3fConfigurable[15];
        for (int i = 0; i < weights.length; i++) {
            if(weights[i] == null) weights[i] = new Vector3fConfigurable(0,0,0);
        }
    }

    public void setType(EnumAnimationType type) {
        this.type = type;
        this.typeOrdinal = type.ordinal();
    }

    @Override
    public File getConfigFile() {
        return null;
    }

}
