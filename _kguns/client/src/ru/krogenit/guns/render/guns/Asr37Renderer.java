package ru.krogenit.guns.render.guns;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class Asr37Renderer extends AbstractItemGunRenderer {

    private final Model scope = new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/attachments/ASR-37_Scope.obj"));

    private final TextureDDS resourceLocationStatefulBackDiffuse;
    private final TextureDDS resourceLocationStatefulBackNormal;
    private final TextureDDS resourceLocationStatefulBackSpecular;
    private final TextureDDS resourceLocationStatefulBackGlassMap;
    private final TextureDDS resourceLocationStatefulBarrelDiffuse;
    private final TextureDDS resourceLocationStatefulBarrelNormal;
    private final TextureDDS resourceLocationStatefulBarrelSpecular;
    private final TextureDDS resourceLocationStatefulBarrelGlassMap;
    private final TextureDDS resourceLocationStatefulCarryHandleDiffuse;
    private final TextureDDS resourceLocationStatefulCarryHandleNormal;
    private final TextureDDS resourceLocationStatefulCarryHandleSpecular;
    private final TextureDDS resourceLocationStatefulCarryHandleGlassMap;
    private final TextureDDS resourceLocationStatefulFrontDiffuse;
    private final TextureDDS resourceLocationStatefulFrontNormal;
    private final TextureDDS resourceLocationStatefulFrontSpecular;
    private final TextureDDS resourceLocationStatefulFrontGlassMap;
    @Getter private final TextureDDS resourceLocationStatefulMagazineDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulMagazineNormal;
    @Getter private final TextureDDS resourceLocationStatefulMagazineSpecular;
    @Getter private final TextureDDS resourceLocationStatefulMagazineGlassMap;
    private final TextureDDS resourceLocationStatefulScopeDiffuse;
    private final TextureDDS resourceLocationStatefulScopeNormal;
    private final TextureDDS resourceLocationStatefulScopeSpecular;
    private final TextureDDS resourceLocationStatefulScopeGlassMap;
    private final TextureDDS resourceLocationStatefulScopeLensDiffuse;
    private final TextureDDS resourceLocationStatefulScopeLensNormal;
    private final TextureDDS resourceLocationStatefulScopeLensSpecular;
    private final TextureDDS resourceLocationStatefulScopeLensGlassMap;
    private final TextureDDS scopeD;

    public Asr37Renderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/weapons/ASR-37.obj")), gun);
        resourceLocationStatefulBackDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Back_D.dds"));
        resourceLocationStatefulBackNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Back_N.dds"));
        resourceLocationStatefulBackSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Back_S.dds"));
        resourceLocationStatefulBackGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Back_G.dds"));
        resourceLocationStatefulBarrelDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Barrel_D.dds"));
        resourceLocationStatefulBarrelNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Barrel_N.dds"));
        resourceLocationStatefulBarrelSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Barrel_S.dds"));
        resourceLocationStatefulBarrelGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Barrel_G.dds"));
        resourceLocationStatefulCarryHandleDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_CarryHandle_D.dds"));
        resourceLocationStatefulCarryHandleNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_CarryHandle_N.dds"));
        resourceLocationStatefulCarryHandleSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_CarryHandle_S.dds"));
        resourceLocationStatefulCarryHandleGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_CarryHandle_G.dds"));
        resourceLocationStatefulFrontDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Front_D.dds"));
        resourceLocationStatefulFrontNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Front_N.dds"));
        resourceLocationStatefulFrontSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Front_S.dds"));
        resourceLocationStatefulFrontGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Front_G.dds"));
        resourceLocationStatefulMagazineDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Magazine_D.dds"));
        resourceLocationStatefulMagazineNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Magazine_N.dds"));
        resourceLocationStatefulMagazineSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Magazine_S.dds"));
        resourceLocationStatefulMagazineGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Magazine_G.dds"));
        resourceLocationStatefulScopeDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Scope_D.dds"));
        resourceLocationStatefulScopeNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Scope_N.dds"));
        resourceLocationStatefulScopeSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Scope_S.dds"));
        resourceLocationStatefulScopeGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_Scope_G.dds"));
        resourceLocationStatefulScopeLensDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_ScopeLens_D.dds"));
        resourceLocationStatefulScopeLensNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_ScopeLens_N.dds"));
        resourceLocationStatefulScopeLensSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_ScopeLens_S.dds"));
        resourceLocationStatefulScopeLensGlassMap = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/ASR-37_ScopeLens_G.dds"));
        scopeD = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/asr37/asr37_scope_d.dds"));
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.15f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);

        int lensTexture = 0;
        boolean renderScopeDynamic = false;
        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED) {
            if (type == ItemRenderType.EQUIPPED_FIRST_PERSON && aim.x > 0) {
                lensTexture = prepareLensTexture(aim.x * -6f, -0.15f, 1.15f, 0, 1);
                renderScopeDynamic = true;
            }
        }

        TextureLoaderDDS.bindTexture(resourceLocationStatefulBackDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulBackNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulBackSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulBackGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        gunModel.renderPart("Back", shader);
        gunModel.renderPart("Zatvor-Back", shader);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulBarrelDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulBarrelNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulBarrelSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulBarrelGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        gunModel.renderPart("Barrel_HB_LP_meshId0_name", shader);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulCarryHandleDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulCarryHandleNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulCarryHandleSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulCarryHandleGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        gunModel.renderPart("CarryHandle_Carry_handle_LP_meshId1_name", shader);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulFrontDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulFrontNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulFrontSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulFrontGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        gunModel.renderPart("Front_Front_LP_meshId3_name", shader);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulMagazineDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulMagazineNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulMagazineSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulMagazineGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        gunModel.renderPart("Magazine_Mag_LP_meshId4_name", shader);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulScopeDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulScopeNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulScopeSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulScopeGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        scope.renderPart("scope", shader);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulScopeLensDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulScopeLensNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulScopeLensSpecular, shader);
        TextureLoaderDDS.bindGlossMap(resourceLocationStatefulScopeLensGlassMap, shader);
        glActiveTexture(GL_TEXTURE0);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);

        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED && renderScopeDynamic) {
            glDepthMask(false);
            glBindTexture(GL_TEXTURE_2D, lensTexture);
            shader.setColor(aim.x, aim.x, aim.x, 1f);
            shader.setLightMapping(false);
            scope.renderPart("glass", shader);
            shader.setColor(1, 1, 1, 1);
            glDepthMask(true);

            glDisable(GL_ALPHA_TEST);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glAlphaFunc(GL_GREATER, 0.000001f);
            TextureLoaderDDS.bindTexture(scopeD);
            scope.renderPart("glass", shader);
            shader.setLightMapping(true);
            glAlphaFunc(GL_GREATER, 0.5f);
            glDisable(GL_BLEND);
            glEnable(GL_ALPHA_TEST);
        } else {
            if(!KrogenitShaders.interfacePass) shader.setUseTexture(false);
            shader.setColor(0, 0, 0, 1f);
            scope.renderPart("glass", shader);
            shader.setColor(1f, 1f, 1f, 1f);
            if(!KrogenitShaders.interfacePass) shader.setUseTexture(true);
        }

        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
