package ru.krogenit.guns.render.guns;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class WaspRenderer extends AbstractItemGunRenderer {

    private final Model scope = new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/wasp/wasp_scope.obj"));

    @Getter private final TextureDDS resourceLocationStatefulDiffuse;
    @Getter private final TextureDDS resourceLocationStatefulNormal;
    @Getter private final TextureDDS resourceLocationStatefulSpecular;
    @Getter private final TextureDDS resourceLocationStatefulEmission;
    @Getter private final TextureDDS resourceLocationStatefulDisplayDiffuse;
    private final TextureDDS scopeD;

    public WaspRenderer(ItemGun gun) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/wasp/Wasp.obj")), gun);
        resourceLocationStatefulDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/wasp/Trek47_diff.dds"));
        resourceLocationStatefulNormal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/wasp/Trek47_normal.dds"));
        resourceLocationStatefulSpecular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/wasp/Trek47_specular.dds"));
        resourceLocationStatefulEmission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/wasp/Trek47_emmissive.dds"));
        resourceLocationStatefulDisplayDiffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/wasp/Trek_13_Display.dds"));
        scopeD = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/wasp/wasp_scope_d.dds"));
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.15f;
        glScalef(scale, scale, scale);
        renderGun(ItemRenderType.INVENTORY);
        renderComponents(itemStack, ItemRenderType.INVENTORY);
    }

    @Override
    public void renderComponents(ItemStack item, ItemRenderType type) {

    }

    @Override
    protected void renderGun(ItemRenderType type) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulDiffuse);
        TextureLoaderDDS.bindNormalMap(resourceLocationStatefulNormal, shader);
        TextureLoaderDDS.bindSpecularMap(resourceLocationStatefulSpecular, shader);
        TextureLoaderDDS.bindEmissionMap(resourceLocationStatefulEmission, shader, 5f);
        gunModel.renderPart("TREK-47_Automatic_Rifle.002", shader);
        gunModel.renderPart("MAG", shader);
        scope.renderPart("SCOPE_SCOPE.003", shader);
        glActiveTexture(GL_TEXTURE0);
        TextureLoaderDDS.bindTexture(resourceLocationStatefulDisplayDiffuse);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);

        if (!KrogenitShaders.lightPass && !KrogenitShaders.gBufferPass && type != ItemRenderType.EQUIPPED) {
            TextureLoaderDDS.bindTexture(scopeD);
            TextureLoaderDDS.bindEmissionMap(scopeD, shader, 5f);
            if(!KrogenitShaders.interfacePass) {
                glDepthMask(false);
                GL11.glAlphaFunc(GL_GREATER, 0.0001f);
                GL11.glEnable(GL_BLEND);
            }
            scope.renderPart("Display_Display.003", shader);
            if(!KrogenitShaders.interfacePass) {
                GL11.glDisable(GL_BLEND);
                GL11.glAlphaFunc(GL_GREATER, 0.5f);
                glDepthMask(true);
            }
        }
        shader.setEmissionMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void createParticles(ItemStack item, ItemRenderType type, EntityPlayer p) {
        boolean shouldSpawnSmokde = true;

        Vector3f smokeVec;
        Vector3f sleeveSpawn;
        if (type == ItemRenderType.EQUIPPED || mc.gameSettings.thirdPersonView > 0) {
            smokeVec = new Vector3f(-0.3f+0.02f + (aim.x / 4f), -0.1f-0.08f, 0.4f + 0.08f);
            sleeveSpawn = new Vector3f(1.2f + 0.5f, aim.x/10f, -0.6f + aim.x/1.7f + 0.45f);
        }
        else {
            smokeVec = new Vector3f(-0.3f + (aim.x / 4f), -0.1f, 0.4f);
            sleeveSpawn = new Vector3f(1.2f, aim.x/10f, -0.6f + aim.x/1.7f);
        }

        super.createBasePaticles(item, type, sleeveSpawn, smokeVec, true, shouldSpawnSmokde, 10, p);
    }

    @Override
    public void updatePostShootAnimation() {
        isPlayPostShootAnim = false;
    }

    @Override
    public boolean isNeedPreAnimation() {
        return false;
    }
}
