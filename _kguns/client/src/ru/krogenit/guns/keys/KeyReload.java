package ru.krogenit.guns.keys;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.krogenit.client.key.AbstractKey;
import ru.krogenit.guns.item.ItemGunClient;

public class KeyReload extends AbstractKey {
    private boolean keyDown = false;
    private boolean keyUp = true;
    private Minecraft mc = Minecraft.getMinecraft();

    public KeyReload(KeyBinding keyBindings) {
        super(keyBindings);
    }

    @Override
    public void keyDown() {
        EntityPlayer p = mc.thePlayer;
        if (mc.currentScreen == null && !keyDown && p != null) {
            ItemStack item = p.getCurrentEquippedItem();
            if (item != null && item.getItem() instanceof ItemGunClient) {
                ItemGunClient weapon = (ItemGunClient) item.getItem();
                weapon.onReload(item, p.worldObj, p, false);
                keyDown = true;
                keyUp = false;
            }
        }
    }

    @Override
    public void keyUp() {
        if (!keyUp) {
            keyDown = false;
            keyUp = true;
        }
    }
}
