package ru.krogenit.guns.item.ammo;

import ru.krogenit.guns.CoreGunsClient;
import ru.krogenit.guns.item.ItemAmmo;

public class ItemAmmoClient extends ItemAmmo {
    public ItemAmmoClient(String name, int maxAmmo) {
        super(name, maxAmmo);
        setCreativeTab(CoreGunsClient.tabGuns);
    }

    @Override
    public ItemAmmoClient setNeedTexture() {
        super.setNeedTexture();
        return this;
    }

    public ItemAmmoClient setItemDisplayName(String name) {
        setDisplayName(name);
        return this;
    }
}
