package ru.krogenit.guns.item.medicine;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.krogenit.guns.CoreGunsClient;
import ru.krogenit.guns.item.ItemMedicine;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.item.ItemUseResult;
import ru.xlv.core.network.PacketItemUse;
import ru.xlv.mochar.XlvsMainMod;

public class ItemMedicineClient extends ItemMedicine {

    private final float useTimer;

    public ItemMedicineClient(String name, float useTimer) {
        super(name);
        setCreativeTab(CoreGunsClient.tabGuns);
        this.useTimer = useTimer;
        this.iconString = name;
    }

    @Override
    public ItemUseResult useItem(EntityPlayer entityPlayer, ItemStack itemStack, int slotIndex) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        if(tagCompound == null) {
            tagCompound = new NBTTagCompound();
            itemStack.setTagCompound(tagCompound);
        }

        tagCompound.setInteger(SLOT_INDEX, slotIndex);
        tagCompound.setFloat(TIMER, useTimer);

        return new ItemUseResult(ItemUseResult.Type.SUCCESS);
    }

    @Override
    public void update(ItemStack itemStack) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        if(tagCompound == null) {
            tagCompound = new NBTTagCompound();
            itemStack.setTagCompound(tagCompound);
        }

        float usingTimer = tagCompound.getFloat(TIMER);
        if(usingTimer <= 0f) {
            XlvsMainMod.INSTANCE.getClientMainPlayer().setCurrentUsableItem(null);
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketItemUse(tagCompound.getInteger(SLOT_INDEX)));
        } else {
            tagCompound.setFloat(TIMER, usingTimer - AnimationHelper.getAnimationSpeed());
        }
    }

    @Override
    public float getAnimation(ItemStack itemStack) {
        NBTTagCompound tagCompound = itemStack.getTagCompound();
        return 1f - tagCompound.getFloat(TIMER) / useTimer;
    }

    @Override
    public void registerIcons(IIconRegister p_94581_1_) {

    }


}
