package ru.xlv.mochar.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.result.SkillExecuteResult;
import ru.xlv.mochar.util.Utils;

public class CommandSkill extends CommandBase {

    @Override
    public String getCommandName() {
        return "skill";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
        int id = Integer.parseInt(p_71515_2_[0]);
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer((EntityPlayer) p_71515_1_);
        Skill skill = serverPlayer.getSelectedCharacter().getSkillManager().getLearnedSkill(id);
        if(skill != null) {
            SkillExecuteResult skillExecuteResult = skill.execute(serverPlayer);
            switch (skillExecuteResult) {
                case SUCCESS:
                    Utils.sendMessage(serverPlayer, "Способность активирована.");
                    break;
                case UNKNOWN_ERROR:
                    Utils.sendMessage(serverPlayer, ":(");
                    break;
                case IN_COOLDOWN:
                    Utils.sendMessage(serverPlayer, "Способность перезаряжается.");
                    break;
                case ALREADY_ACTIVE:
                    Utils.sendMessage(serverPlayer, "Способность уже активна.");
                    break;
                case CONDITIONS_NOT_FULFILLED:
                    Utils.sendMessage(serverPlayer, "Условия не выполнены.");
            }
        } else {
            Utils.sendMessage(serverPlayer, "Скил еще не изучен или не существует.");
        }
    }
}
