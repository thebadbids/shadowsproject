package ru.xlv.mochar.network.matrix;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventorySpecSpecItemMove implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int slotTypeFrom = bbis.readInt();
        int slotTypeTo = bbis.readInt();
        if(slotTypeFrom < 0 || slotTypeFrom >= MatrixInventory.SlotType.values().length || slotTypeTo < 0 || slotTypeTo >= MatrixInventory.SlotType.values().length) {
            return;
        }
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(serverPlayer.getSelectedCharacter());
            if (matrixInventoryTransaction.moveItemSpecSpec(serverPlayer, MatrixInventory.SlotType.values()[slotTypeFrom], MatrixInventory.SlotType.values()[slotTypeTo])) {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
            }
        }
    }
}
