package ru.xlv.mochar.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryController;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionItemMultiMove implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private boolean success;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if(REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            boolean fromMyInventory = bbis.readBoolean();
            boolean toMyInventory = bbis.readBoolean();
            ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
            if (serverPlayer != null) {
                IMatrixInventoryProvider transactionMatrixInventoryProvider = MatrixInventoryController.INSTANCE.getTransactionMatrixInventoryProvider(entityPlayer);
                if (transactionMatrixInventoryProvider == null) {
                    transactionMatrixInventoryProvider = serverPlayer.getSelectedCharacter();
                }
                IMatrixInventoryProvider inventoryFrom = fromMyInventory ? serverPlayer.getSelectedCharacter() : transactionMatrixInventoryProvider;
                IMatrixInventoryProvider inventoryTo = toMyInventory ? serverPlayer.getSelectedCharacter() : transactionMatrixInventoryProvider;
                MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(inventoryTo, inventoryFrom);
                int c = bbis.readInt();
                for (int i = 0; i < c; i++) {
                    int x = bbis.readInt();
                    int y = bbis.readInt();
                    matrixInventoryTransaction.moveItem(x, y);
                }
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixInventoryTransactionSync(transactionMatrixInventoryProvider.getMatrixInventory()));
                success = true;
            }
            packetCallbackSender.send();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(success);
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
