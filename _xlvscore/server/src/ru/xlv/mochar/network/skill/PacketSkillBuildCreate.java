package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.result.SkillBuildCreateResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildCreate implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(500L);

    private SkillBuildCreateResult skillBuildCreateResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if(REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            int buildIndex = bbis.readInt();
            int c = bbis.readInt();
            int[] ii = new int[c];
            for (int i = 0; i < c; i++) {
                ii[i] = bbis.readInt();
            }
            ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
            skillBuildCreateResult = serverPlayer.getSelectedCharacter().getSkillManager().createSkillBuild(buildIndex, ii);
            packetCallbackSender.send();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(skillBuildCreateResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
