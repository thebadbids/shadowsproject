package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.flex.FlexPlayer;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketSkillSync implements IPacketOutServer {

    private ServerPlayer serverPlayer;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        FlexPlayer.of(serverPlayer)
                .notNull()
                .apply(serverPlayer -> serverPlayer.getSelectedCharacter().getSkillManager().getSelectedActiveSkills())
                .accept((flexPlayer, activableSkills) -> {
                    try {
                        writeCollection(bbos, activableSkills, this::writeComposable);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }
}
