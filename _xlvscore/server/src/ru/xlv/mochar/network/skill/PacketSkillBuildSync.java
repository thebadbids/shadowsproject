package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.SkillManager;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildSync implements IPacketOutServer {

    private ServerPlayer serverPlayer;

    public PacketSkillBuildSync(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        SkillManager skillManager = serverPlayer.getSelectedCharacter().getSkillManager();
        bbos.writeInt(skillManager.getSkillBuildIndex());
        writeSkillBuilds(skillManager, bbos);
    }

    public static void writeSkillBuilds(SkillManager skillManager, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(skillManager.getAvailableSkillBuildAmount());
        for (int skillBuildIndex = 0; skillBuildIndex < skillManager.getAvailableSkillBuildAmount(); skillBuildIndex++) {
            int[] activeSkills = skillManager.getSkillListProvider().getSkillBuildActivesMap().get(skillBuildIndex);
            int[] passiveSkills = skillManager.getSkillListProvider().getSkillBuildPassivesMap().get(skillBuildIndex);
            bbos.writeInt(skillBuildIndex);
            bbos.writeInt(activeSkills.length);
            for (int activeSkill : activeSkills) {
                bbos.writeInt(activeSkill);
            }
            bbos.writeInt(passiveSkills.length);
            for (int passiveSkill : passiveSkills) {
                bbos.writeInt(passiveSkill);
            }
        }
        bbos.writeInt(skillManager.getAvailableActiveSkillSlotAmount());
        bbos.writeInt(skillManager.getAvailablePassiveSkillSlotAmount());
    }
}
