package ru.xlv.mochar.player.stat;

import lombok.ToString;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.player.stat.IStatProvider;
import ru.xlv.core.common.player.stat.StatDoubleProvider;
import ru.xlv.core.common.player.stat.StatIntProvider;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;

import java.util.HashMap;
import java.util.Map;

@ToString
public class StatManager implements ISavableNBT {

    /*private StatIntProvider totalOnlineTimeSeconds = new StatIntProvider(0);
    private StatIntProvider totalAchievementAmount = new StatIntProvider(0);
    private StatIntProvider totalLevelAmount = new StatIntProvider(0);//???
    private StatDoubleProvider speedRecord = new StatDoubleProvider(0D);//native
    private StatIntProvider totalDrugUsed = new StatIntProvider(0);//???
    private StatIntProvider totalStimulantUsed = new StatIntProvider(0);//???
    private StatIntProvider totalMedicUsed = new StatIntProvider(0);//???
    private StatIntProvider totalGroupedTimeSeconds = new StatIntProvider(0);//???-???
    private StatIntProvider totalSkillLearned = new StatIntProvider(0);//???
    private StatIntProvider totalLocationDiscovered = new StatIntProvider(0);
    private StatIntProvider totalShoots = new StatIntProvider(0);
    private StatIntProvider totalHeadshots = new StatIntProvider(0);
    private StatIntProvider totalGrenadesThrown = new StatIntProvider(0);//???
    private StatIntProvider totalSkillUsed = new StatIntProvider(0);
    private StatIntProvider totalPlayersKilled = new StatIntProvider(0);
    private StatIntProvider totalKillSeriesAmount = new StatIntProvider(0);
    private StatIntProvider totalNpcsKilled = new StatIntProvider(0);
    private StatIntProvider totalDeaths = new StatIntProvider(0);//native
    private StatIntProvider totalDeathsFromPlayer = new StatIntProvider(0);//native
    private StatIntProvider totalDeathsFromNpc = new StatIntProvider(0);
    private StatIntProvider totalDeathsFromFall = new StatIntProvider(0);//native
    private StatIntProvider totalDeathsFromSuffocation = new StatIntProvider(0);
    private StatIntProvider totalSuicides = new StatIntProvider(0);//???-???
    private StatIntProvider totalCreditReceived = new StatIntProvider(0);
    private StatIntProvider totalCreditFromQuestsReceived = new StatIntProvider(0);
    private StatIntProvider totalCreditFromNpcKillsReceived = new StatIntProvider(0);
    private StatIntProvider totalCreditFromTradingReceived = new StatIntProvider(0);
    private StatIntProvider totalCreditFromAuctionReceived = new StatIntProvider(0);
    private StatIntProvider totalCreditOnAuctionSpent = new StatIntProvider(0);
    private StatIntProvider totalCreditOnTradersSpent = new StatIntProvider(0);
    private StatIntProvider totalCreditOnTradingSpent = new StatIntProvider(0);
    private StatIntProvider totalTradesAmount = new StatIntProvider(0);
    private StatIntProvider totalItemsPurchasedFromNpcs = new StatIntProvider(0);
    private StatIntProvider totalItemsPurchasedFromAuction = new StatIntProvider(0);
    private StatIntProvider totalItemsSalesToNpcs = new StatIntProvider(0);
    private StatIntProvider totalItemsSalesOnAuction = new StatIntProvider(0);
    private StatIntProvider totalItemsCreated = new StatIntProvider(0);
    private StatIntProvider totalItemsBroken = new StatIntProvider(0);//native
    private StatIntProvider totalPostSent = new StatIntProvider(0);
    private StatIntProvider totalFriendAdded = new StatIntProvider(0);
    private StatIntProvider totalGroupsCreated = new StatIntProvider(0);
    private StatIntProvider totalBlockedPlayers = new StatIntProvider(0);
    private StatIntProvider totalTradesMade = new StatIntProvider(0);
    private StatIntProvider totalRevenges = new StatIntProvider(0);*/

    @DatabaseValue
    private final Map<StatType, IStatProvider> statProviderMap = new HashMap<>();

    public StatManager() {
        int defIntValue = 0;
        double defDoubleValue = 0D;
        for (StatType value : StatType.values()) {
            IStatProvider statProvider;
            switch (value.getValueType()) {
                case INT:
                    statProvider = new StatIntProvider(defIntValue);
                    break;
                case DOUBLE:
                    statProvider = new StatDoubleProvider(defDoubleValue);
                    break;
                default: continue;
            }
            statProviderMap.put(value, statProvider);
        }
    }

    public IStatProvider getStatProvider(StatType type) {
        synchronized (statProviderMap) {
            return statProviderMap.get(type);
        }
    }

    public synchronized Map<StatType, IStatProvider> getStatProviderMap() {
        return statProviderMap;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        statProviderMap.forEach((type, statProvider) -> {
            Object object;
            switch (type.getValueType()) {
                case INT:
                    object = ((StatIntProvider) statProvider).getStatValue();
                    break;
                case DOUBLE:
                    object = ((StatDoubleProvider) statProvider).getStatValue();
                    break;
                default: return;
            }
            nbtLoader.putObject(type.name(), object, nbtTagCompound);
        });
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        for (StatType type : statProviderMap.keySet()) {
            IStatProvider statProvider;
            switch (type.getValueType()) {
                case INT:
                    statProvider = new StatIntProvider(nbtLoader.getObject(type.name(), Integer.class, nbtTagCompound));
                    break;
                case DOUBLE:
                    statProvider = new StatDoubleProvider(nbtLoader.getObject(type.name(), Double.class, nbtTagCompound));
                    break;
                default: continue;
            }
            statProviderMap.put(type, statProvider);
        }
    }
}
