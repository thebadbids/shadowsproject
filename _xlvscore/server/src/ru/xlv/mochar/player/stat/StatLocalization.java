package ru.xlv.mochar.player.stat;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class StatLocalization extends Localization {

    private final String totalOnlineTimeSecondsName = "totalOnlineTimeSecondsName";
    private final String totalAchievementAmountName = "totalAchievementAmountName";
    private final String totalLevelAmountName = "totalLevelAmountName";
    private final String speedRecordName = "speedRecordName";
    private final String totalDrugUsedName = "totalDrugUsedName";
    private final String totalStimulantUsedName = "totalStimulantUsedName";
    private final String totalMedicUsedName = "totalMedicUsedName";
    private final String totalGroupedTimeSecondsName = "totalGroupedTimeSecondsName";
    private final String totalSkillLearnedName = "totalSkillLearnedName";
    private final String totalLocationDiscoveredName = "totalLocationDiscoveredName";
    private final String totalShootsName = "totalShootsName";
    private final String totalHeadshotsName = "totalHeadshotsName";
    private final String totalGrenadesThrownName = "totalGrenadesThrownName";
    private final String totalSkillUsedName = "totalSkillUsedName";
    private final String totalPlayersKilledName = "totalPlayersKilledName";
    private final String totalKillSeriesAmountName = "totalKillSeriesAmountName";
    private final String totalNpcsKilledName = "totalNpcsKilledName";
    private final String totalDeathsName = "totalDeathsName";
    private final String totalDeathsFromPlayerName = "totalDeathsFromPlayerName";
    private final String totalDeathsFromNpcName = "totalDeathsFromNpcName";
    private final String totalDeathsFromFallName = "totalDeathsFromFallName";
    private final String totalDeathsFromSuffocationName = "totalDeathsFromSuffocationName";
    private final String totalSuicidesName = "totalSuicidesName";
    private final String totalCreditReceivedName = "totalCreditReceivedName";
    private final String totalCreditFromQuestsReceivedName = "totalCreditFromQuestsReceivedName";
    private final String totalCreditFromNpcKillsReceivedName = "totalCreditFromNpcKillsReceivedName";
    private final String totalCreditFromTradingReceivedName = "totalCreditFromTradingReceivedName";
    private final String totalCreditFromAuctionReceivedName = "totalCreditFromAuctionReceivedName";
    private final String totalCreditOnAuctionSpentName = "totalCreditOnAuctionSpentName";
    private final String totalCreditOnTradersSpentName = "totalCreditOnTradersSpentName";
    private final String totalCreditOnTradingSpentName = "totalCreditOnTradingSpentName";
    private final String totalTradesAmountName = "totalTradesAmountName";
    private final String totalItemsPurchasedFromNpcsName = "totalItemsPurchasedFromNpcsName";
    private final String totalItemsPurchasedFromAuctionName = "totalItemsPurchasedFromAuctionName";
    private final String totalItemsSalesToNpcsName = "totalItemsSalesToNpcsName";
    private final String totalItemsSalesOnAuctionName = "totalItemsSalesOnAuctionName";
    private final String totalItemsCreatedName = "totalItemsCreatedName";
    private final String totalPostSentName = "totalPostSentName";
    private final String totalFriendAddedName = "totalFriendAddedName";
    private final String totalBlockedPlayersName = "totalBlockedPlayersName";
    private final String totalTradesMadeName = "totalTradesMadeName";
    private final String totalRevengesName = "totalRevengesName";
    private final String totalQuestCompletedCombatName = "";
    private final String totalQuestCompletedExploreName = "";
    private final String totalQuestCompletedSurviveName = "";

    @Override
    public File getConfigFile() {
        return new File("config/stat/localization.json");
    }
}
