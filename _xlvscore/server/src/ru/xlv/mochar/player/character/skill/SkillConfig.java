package ru.xlv.mochar.player.character.skill;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Getter
@Configurable
public class SkillConfig implements IConfigGson {

    private final int activeSkillThirdSlotOpenAfterLevel = 5;
    private final int activeSkillFourthSlotOpenAfterLevel = 10;

    private final int passiveSkillThirdSlotOpenAfterLevel = 1;
    private final int passiveSkillFourthSlotOpenAfterLevel = 1;
    private final int passiveSkillFifthSlotOpenAfterLevel = 1;
    private final int passiveSkillSixthSlotOpenAfterLevel = 1;
    private final int passiveSkillSeventhSlotOpenAfterLevel = 1;
    private final int passiveSkillEighthSlotOpenAfterLevel = 1;
    private final int passiveSkillNinthSlotOpenAfterLevel = 1;
    private final int passiveSkillTenthSlotOpenAfterLevel = 1;

    private final int skillBuildThirdSlotOpenAfterLevel = 5;
    private final int skillBuildFourthSlotOpenAfterLevel = 10;

    @Override
    public File getConfigFile() {
        return new File("config/skill/config.json");
    }
}
