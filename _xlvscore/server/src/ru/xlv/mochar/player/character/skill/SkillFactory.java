package ru.xlv.mochar.player.character.skill;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import lombok.experimental.UtilityClass;

import java.lang.reflect.InvocationTargetException;

@UtilityClass
public class SkillFactory {

    private final TIntObjectMap<Object> REGISTRY = new TIntObjectHashMap<>();

    public Skill createSkill(SkillType skillType) {
        Object skill = REGISTRY.get(skillType.getSkillId());
        if(skill == null) {
            try {
                REGISTRY.put(skillType.getSkillId(), skill = Class.forName(skillType.getImplClassPath()).getConstructor(SkillType.class).newInstance(skillType));
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return ((Skill) skill).clone();
    }
}
