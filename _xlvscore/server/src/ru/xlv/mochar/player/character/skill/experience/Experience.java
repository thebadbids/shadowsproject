package ru.xlv.mochar.player.character.skill.experience;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.player.character.ExperienceType;

@Setter
@Getter
@DatabaseValue
public class Experience {

    private final ExperienceType type;
    private double value;

    public Experience(ExperienceType type, double value) {
        this.type = type;
        this.value = value;
    }
}
