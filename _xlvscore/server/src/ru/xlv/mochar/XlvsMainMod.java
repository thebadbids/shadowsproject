package ru.xlv.mochar;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.mochar.command.*;
import ru.xlv.mochar.event.TickListener;
import ru.xlv.mochar.location.LocationHandler;
import ru.xlv.mochar.location.LocationManager;
import ru.xlv.mochar.player.character.CharacterLoader;

@Mod(
        name = XlvsMainMod.NAME,
        modid = XlvsMainMod.MODID,
        version = "1.0"
)
public class XlvsMainMod {

    public static final String MODID = "xlvsmochar";
    static final String NAME = "More Characters Mod by Xlv";

    @Mod.Instance(MODID)
    public static XlvsMainMod INSTANCE;

    private CharacterLoader characterLoader;
    private LocationManager locationManager;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        locationManager = new LocationManager();

        characterLoader = new CharacterLoader();
        characterLoader.loadAllInDir();
        CommonUtils.registerAllEvents(LocationHandler.class);

        TickListener object = new TickListener();
        CommonUtils.registerFMLEvents(object);
        XlvsCoreCommon.EVENT_BUS.register(object);
    }

    @Mod.EventHandler
    public void event(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandSkill());
        event.registerServerCommand(new CommandLearnSkill());
        event.registerServerCommand(new CommandExp());
        event.registerServerCommand(new CommandMana());
        event.registerServerCommand(new CommandCreateArea());
    }

    public CharacterLoader getCharacterLoader() {
        return characterLoader;
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }
}
