package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.player.ServerPlayer;

@RequiredArgsConstructor
public class ServerPlayerUpdateEvent extends Event {

    @Getter
    private final ServerPlayer serverPlayer;
}
