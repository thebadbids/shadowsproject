package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.location.Location;

@Getter
@RequiredArgsConstructor
public class PlayerEnterLocationEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final Location location;
}
