package ru.xlv.core.database.mariadb;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Getter
@RequiredArgsConstructor
public class ConfigMariaDB implements IConfigGson {

    @Configurable
    private String host;
    @Configurable
    private String username;
    @Configurable
    private String password;
    @Configurable
    private String database;
    @Configurable
    private int port;

    private final String filePath;

    @Override
    public File getConfigFile() {
        return new File(filePath);
    }
}
