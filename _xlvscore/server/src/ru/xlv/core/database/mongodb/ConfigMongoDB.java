package ru.xlv.core.database.mongodb;

import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.common.util.config.IConfigJson;

import java.io.File;

public class ConfigMongoDB implements IConfigGson {

    @Configurable
    public String host = "";

    @Configurable
    public int port;

    @Configurable
    public String database_name = "";

    @Configurable
    public String username = "";

    @Configurable
    public String password = "";

    private String configPath;

    public ConfigMongoDB(String configPath) {
        this.configPath = configPath;
    }

    @Override
    public File getConfigFile() {
        return new File(configPath);
    }
}
