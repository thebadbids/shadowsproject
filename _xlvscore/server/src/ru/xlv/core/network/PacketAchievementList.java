package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.Achievement;
import ru.xlv.core.achievement.AchievementUtils;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketAchievementList implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private List<Achievement> achievements;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if (REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            String username = bbis.readUTF();
            achievements = XlvsCore.INSTANCE.getAchievementHandler().getAchievements(username);
            packetCallbackSender.send();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        if (achievements != null) {
            AchievementUtils.writeAchievementList(achievements, bbos);
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
