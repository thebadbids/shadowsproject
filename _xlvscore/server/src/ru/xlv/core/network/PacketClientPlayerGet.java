package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.mochar.network.PacketClientPlayerSync;

import java.io.IOException;

@ControllablePacket(period = 200L)
@NoArgsConstructor
public class PacketClientPlayerGet implements IPacketCallbackOnServer {

    private ServerPlayer serverPlayer;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String username = bbis.readUTF();
        boolean completedSuccessfully = FlexPlayer.of(username)
                .notNull()
                .accept(serverPlayer -> {
                    this.serverPlayer = serverPlayer;
                    packetCallbackSender.send();
                })
                .isCompletedSuccessfully();
        if (!completedSuccessfully) {
            XlvsCore.INSTANCE.getDatabaseManager().getPlayerAsync(username).handle((serverPlayer1, throwable) -> {
                if(throwable != null) {
                    throwable.printStackTrace();
                } else if(serverPlayer1 != null) {
                    this.serverPlayer = serverPlayer1;
                    packetCallbackSender.send();
                }
                return null;
            });
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(serverPlayer != null);
        if(serverPlayer != null) {
            PacketClientPlayerSync.writeClientPlayer(serverPlayer, bbos);
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
