package ru.xlv.core.util.flex;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class FlexContainer<T> extends Container<T> {

    protected FlexContainer(T object) {
        super(object);
    }

    @Override
    public FlexContainer<T> test(Predicate<T> predicate) {
        return (FlexContainer<T>) super.test(predicate);
    }

    @Override
    public FlexContainer<T> isNull() {
        return (FlexContainer<T>) super.isNull();
    }

    @Override
    public FlexContainer<T> notNull() {
        return (FlexContainer<T>) super.notNull();
    }

    @Override
    public FlexContainer<T> log(Function<T, String> function) {
        return (FlexContainer<T>) super.log(function);
    }

    @Override
    public FlexContainer<T> log(String message, Object... params) {
        return (FlexContainer<T>) super.log(message, params);
    }

    @Override
    public FlexContainer<T> accept(Consumer<T> consumer) {
        return (FlexContainer<T>) super.accept(consumer);
    }

    @Override
    public <R> BiFlex<T, R> apply(Function<T, R> function) {
        return createBiContainer(function.apply(object));
    }

    @Override
    protected FlexContainer<T> createContainer(T t) {
        return new FlexContainer<>(t);
    }

    @Override
    protected <A> BiFlex<T, A> createBiContainer(A a) {
        return new BiFlex<>(createContainer(object), a, chain);
    }

    public static <T> FlexContainer<T> of(T object) {
        return new FlexContainer<>(object);
    }

    public static class BiFlex<T, V> extends BiContainer<FlexContainer<T>, T, V> {
        public BiFlex(FlexContainer<T> tFlexContainer, V v, boolean chain) {
            super(tFlexContainer, v, chain);
        }

        @Override
        public BiFlex<T, V> notNull() {
            if(chain) {
                chain = this.param != null;
            }
            return this;
        }

        @Override
        public BiFlex<T, V> log(Function<FlexContainer<T>, String> function) {
            return (BiFlex<T, V>) super.log(function);
        }

        @Override
        public BiFlex<T, V> log(String message, Object... params) {
            return (BiFlex<T, V>) super.log(message, params);
        }

        public <R> BiFlex<T, R> applyBi(Function<T, R> function) {
            return new BiFlex<>(object, function.apply(object.object), chain);
        }

        @Override
        public <R> BiFlex<T, R> apply(Function<FlexContainer<T>, R> function) {
            return (BiFlex<T, R>) super.apply(function);
        }

        @Override
        public BiFlex<T, V> accept(BiConsumer<FlexContainer<T>, V> consumer) {
            return (BiFlex<T, V>) super.accept(consumer);
        }

        @Override
        public BiFlex<T, V> acceptBi(BiConsumer<T, V> consumer) {
            return (BiFlex<T, V>) super.acceptBi(consumer);
        }

        @Override
        public BiFlex<T, V> test(Predicate<FlexContainer<T>> predicate) {
            return (BiFlex<T, V>) super.test(predicate);
        }
    }
}
