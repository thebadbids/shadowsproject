package ru.xlv.core.util.flex;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.Character;
import ru.xlv.mochar.util.Utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.*;

public class FlexPlayer extends Container<ServerPlayer> {

    public FlexPlayer(ServerPlayer serverPlayer) {
        super(serverPlayer);
    }

    @Override
    public FlexPlayer test(Predicate<ServerPlayer> predicate) {
        return (FlexPlayer) super.test(predicate);
    }

    @Override
    public FlexPlayer isNull() {
        return (FlexPlayer) super.isNull();
    }

    @Override
    public FlexPlayer notNull() {
        return (FlexPlayer) super.notNull();
    }

    @Override
    public FlexPlayer log(Function<ServerPlayer, String> function) {
        return (FlexPlayer) super.log(function);
    }

    @Override
    public FlexPlayer log(String message, Object... params) {
        return (FlexPlayer) super.log(message, params);
    }

    @Override
    public FlexPlayer accept(Consumer<ServerPlayer> consumer) {
        return (FlexPlayer) super.accept(consumer);
    }

    @Override
    public FlexPlayer orAccept(Consumer<ServerPlayer> consumer) {
        return (FlexPlayer) super.orAccept(consumer);
    }

    @Override
    public <V> BiFlex<V> apply(Function<ServerPlayer, V> function) {
        return createBiContainer(function.apply(object));
    }

    @Override
    protected FlexPlayer createContainer(ServerPlayer serverPlayer) {
        return new FlexPlayer(serverPlayer);
    }

    @Override
    protected <A> BiFlex<A> createBiContainer(A a) {
        return new BiFlex<>(createContainer(object), a, chain);
    }

    public FlexPlayer sendPacket(IPacketOutServer packet) {
        if(chain) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(object.getEntityPlayer(), packet);
        }
        return this;
    }

    public FlexPlayer sendPacket(Function<ServerPlayer, IPacketOutServer> function) {
        if(chain) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(object.getEntityPlayer(), function.apply(object));
        }
        return this;
    }

    public FlexPlayer sendMessage(String message, Object... params) {
        if(chain) {
            Utils.sendMessage(object, message, params);
        }
        return this;
    }

    public FlexPlayer sendNotification(String message, Object... params) {
        if(chain) {
            if (XlvsCore.INSTANCE.getNotificationService() != null) {
                XlvsCore.INSTANCE.getNotificationService().sendNotification(object, String.format(message, params));
            }
        }
        return this;
    }

    public FlexPlayer character(Consumer<Character> consumer) {
        if (chain) {
            if (object.getSelectedCharacter() != null) {
                consumer.accept(object.getSelectedCharacter());
            }
        }
        return this;
    }

    public FlexPlayer movePlayer(WorldPosition worldPosition) {
        if(chain) {
            XlvsCore.INSTANCE.getPlayerManager().movePlayer(object, worldPosition);
        }
        return this;
    }

    public ServerPlayer getPlayer() {
        return object;
    }

    @Nonnull
    public static FlexPlayer of(@Nullable String username) {
        return username == null ? new FlexPlayer(null) : new FlexPlayer(XlvsCore.INSTANCE.getPlayerManager().getPlayer(username));
    }

    @Nonnull
    public static FlexPlayer of(@Nullable EntityPlayer entityPlayer) {
        return entityPlayer == null ? new FlexPlayer(null) : new FlexPlayer(XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer));
    }

    @Nonnull
    public static FlexPlayer of(@Nullable ServerPlayer serverPlayer) {
        return serverPlayer == null ? new FlexPlayer(null) : new FlexPlayer(serverPlayer);
    }

    public static class BiFlex<V> extends BiContainer<FlexPlayer, ServerPlayer, V> {
        public BiFlex(FlexPlayer flexPlayer, V v, boolean chain) {
            super(flexPlayer, v, chain);
        }

        @Override
        public BiFlex<V> notNull() {
            if(chain) {
                chain = this.param != null;
            }
            return this;
        }

        @Override
        public BiFlex<V> log(Function<FlexPlayer, String> function) {
            return (BiFlex<V>) super.log(function);
        }

        @Override
        public BiFlex<V> log(String message, Object... params) {
            return (BiFlex<V>) super.log(message, params);
        }

        public <R> BiFlex<R> applyBi(Function<ServerPlayer, R> function) {
            return new BiFlex<>(object, function.apply(object.object), chain);
        }

        @Override
        public <R> BiFlex<R> apply(Function<FlexPlayer, R> function) {
            return (BiFlex<R>) super.apply(function);
        }

        @Override
        public BiFlex<V> accept(BiConsumer<FlexPlayer, V> consumer) {
            return (BiFlex<V>) super.accept(consumer);
        }

        @Override
        public BiFlex<V> test(Predicate<FlexPlayer> predicate) {
            return (BiFlex<V>) super.test(predicate);
        }

        @Override
        public BiFlex<V> test(BiPredicate<FlexPlayer, V> predicate) {
            return (BiFlex<V>) super.test(predicate);
        }
    }
}
