package ru.xlv.core.util;

import gnu.trove.map.TObjectDoubleMap;
import lombok.experimental.UtilityClass;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import javax.annotation.Nullable;

@UtilityClass
public class DamageUtils {

    public void damageEntity(EntityLivingBase attacker, EntityLivingBase target, TObjectDoubleMap<CharacterAttributeType> damageMap) {
        DamageSourceAttr damageSourceAttr = new DamageSourceAttr(attacker);
        damageSourceAttr.getDamageMap().putAll(damageMap);
        target.attackEntityFrom(damageSourceAttr, 0F);
    }

    public void damageEntity(EntityLivingBase attacker, EntityLivingBase target, CharacterAttributeType characterAttributeType, double value) {
        DamageSourceAttr damageSourceAttr = new DamageSourceAttr(attacker);
        damageSourceAttr.addAttrDamage(characterAttributeType, value);
        target.attackEntityFrom(damageSourceAttr, 0F);
    }

    public void damageEntity(EntityLivingBase attacker, EntityLivingBase target, float amount, boolean ignoreEvents) {
        DamageSourceAttr damageSourceAttr = new DamageSourceAttr(attacker);
        damageSourceAttr.ignoreEvents(ignoreEvents);
        target.attackEntityFrom(damageSourceAttr, amount);
    }

    @Nullable
    public TObjectDoubleMap<CharacterAttributeType> getAttrDamageMap(DamageSource damageSource) {
        if(damageSource instanceof DamageSourceAttr) {
            return ((DamageSourceAttr) damageSource).getDamageMap();
        }
        return null;
    }
}
