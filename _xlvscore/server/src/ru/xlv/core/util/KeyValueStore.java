package ru.xlv.core.util;

import ru.xlv.core.common.database.DatabaseValue;

import java.util.HashMap;
import java.util.Map;

public class KeyValueStore {

    public static class Key {

        private final int key;

        private Key(int key) {
            this.key = key;
        }
    }

    @DatabaseValue
    private final Map<Integer, Object> map = new HashMap<>();

    public synchronized void set(Key key, Object value) {
        map.put(key.key, value);
    }

    public synchronized Object get(Key key) {
        return map.get(key.key);
    }

    public synchronized <T> T get(Key key, Class<T> tClass) {
        Object obj = map.get(key.key);
        return obj != null ? tClass.cast(obj) : null;
    }

    public Key genKey() {
        int i = 0;
        while(map.containsKey(i)) i++;
        return new Key(i);
    }
}
