package ru.xlv.core.util;

import ru.xlv.core.common.util.config.IConfigGson;

import java.util.Arrays;

public abstract class Localization implements IConfigGson {

    public static String getFormattedString(String s, Object... objects) {
        for (int i = 0; i < objects.length; i++) {
            Object o = objects[i];
            s = s.replace("{" + i + "}", String.valueOf(o));
        }
        return s;
    }

    public String getFormatted(String s, Object... objects) {
        return getFormattedString(s, objects);
    }

    public String[] getFormatted(String[] ss, Object... params) {
        String[] ss1 = Arrays.copyOf(ss, ss.length);
        for (int i = 0; i < ss1.length; i++) {
            ss1[i] = getFormatted(ss1[i], params);
        }
        return ss1;
    }
}
