package ru.xlv.core.player;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.event.GiveInsuredItemsPostEvent;
import ru.xlv.mochar.util.Utils;

import java.util.*;

public class InsuranceManager {

    public static final InsuranceManager INSTANCE = new InsuranceManager();

    private final Map<String, List<ItemStack>> insured = new HashMap<>();

    public void insureInventoryTo(EntityPlayer entityPlayer) {
        List<ItemStack> list = new ArrayList<>();
        insureInventoryTo(entityPlayer.inventory.mainInventory, list);
        insureInventoryTo(entityPlayer.inventory.armorInventory, list);
        insureInventoryTo(entityPlayer.getCommandSenderName(), list);
    }

    private void insureInventoryTo(ItemStack[] itemStacks, List<ItemStack> dest) {
        for (ItemStack itemStack : itemStacks) {
            if(itemStack != null) {
                List<EnumItemTag> enumItemTags = EnumItemTag.getItemTags(itemStack);
                if(enumItemTags != null && EnumItemTag.areTagsCanSaveItem(enumItemTags)) {
                    dest.add(itemStack);
                }
            }
        }
    }

    public void insure(EntityPlayer entityPlayer, ItemStack... itemStack) {
        insureInventoryTo(entityPlayer.getCommandSenderName(), Arrays.asList(itemStack));
    }

    public void insure(EntityPlayer entityPlayer, List<ItemStack> itemStack) {
        insureInventoryTo(entityPlayer.getCommandSenderName(), itemStack);
    }

    public void giveInsured(EntityPlayer entityPlayer, MatrixInventory matrixInventory) {
        synchronized (insured) {
            List<ItemStack> list = insured.get(entityPlayer.getCommandSenderName());
            if (list != null) {
                list.removeIf(itemStack -> matrixInventory.addItem(itemStack).isSuccess());
                GiveInsuredItemsPostEvent event = new GiveInsuredItemsPostEvent(entityPlayer, list);
                if (XlvsCoreCommon.EVENT_BUS.post(event) || list.isEmpty()) {
                    return;
                }
                Utils.sendMessage(entityPlayer, "THIS IS A BUG! Some of your insured items were not moved to your inventory, bcs you have not enough inventory space . You will get them after next death.");
            }
        }
    }

    private void insureInventoryTo(String key, List<ItemStack> value) {
        synchronized (insured) {
            List<ItemStack> itemStacks = insured.get(key);
            if (itemStacks != null) {
                itemStacks.addAll(value);
            } else {
                itemStacks = new LinkedList<>(value);
            }
            insured.put(key, itemStacks);
        }
    }
}
