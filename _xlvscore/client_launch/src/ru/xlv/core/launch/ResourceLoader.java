//package ru.xlv.core.launch;
//
//import net.minecraft.client.resources.IResourcePack;
//import net.minecraft.client.resources.data.IMetadataSection;
//import net.minecraft.client.resources.data.IMetadataSerializer;
//import net.minecraft.util.ResourceLocation;
//
//import java.awt.image.BufferedImage;
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.util.Collections;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//public class ResourceLoader implements IResourcePack {
//
//    private final Path assets;
//
//    public ResourceLoader(Path assets) {
//        this.assets = assets;
//    }
//
//    private Path getResourcePath(ResourceLocation rl) {
//        String p = rl.getResourcePath();
//        return assets.resolve(rl.getResourceDomain()).resolve(p.substring(0, p.lastIndexOf('.')));
//    }
//
//    @Override
//    public InputStream getInputStream(ResourceLocation rl) throws IOException {
//        if (resourceExists(rl)) {
////            try {
////                return (InputStream) Tweaker.DCD.getClass().getMethod("d", InputStream.class).invoke(Tweaker.DCD, Files.newInputStream(getResourcePath(rl)));
////            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
////                e.printStackTrace();
////            }
//            return Files.newInputStream(getResourcePath(rl));
//        }
//        return null;
//    }
//
//    @Override
//    public boolean resourceExists(ResourceLocation rl) {
//        Path p = getResourcePath(rl);
//        return Files.exists(p) && Files.isRegularFile(p);
//    }
//
//    @Override
//    public Set getResourceDomains() {
//        try {
//            return Files.list(assets).filter(Files::isDirectory).map(p -> p.getFileName().toString()).collect(Collectors.toSet());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return Collections.emptySet();
//    }
//
//    @Override
//    public IMetadataSection getPackMetadata(IMetadataSerializer p_135058_1_, String p_135058_2_) {
//        return null;
//    }
//
//    @Override
//    public BufferedImage getPackImage() {
//        return null;
//    }
//
//    @Override
//    public String getPackName() {
//        return "CustomResources";
//    }
//}