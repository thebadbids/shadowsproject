package ru.xlv.core.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResource;
import net.minecraft.util.ResourceLocation;
import ru.xlv.core.api.IResourceLoader;

import java.io.IOException;
import java.io.InputStream;

public class MinecraftResourceLoader implements IResourceLoader {

    @Override
    public InputStream getResourceInputStream(String path) throws IOException {
        // some/dir/file.png to some:dir/file.png
        if(!path.contains(":") && path.contains("/")) {
            String s = path.substring(path.indexOf("/") + 1);
            String s1 = path.substring(0, path.indexOf("/"));
            path = s1 + ":" + s;
        }
        ResourceLocation resourceLocation = new ResourceLocation(path);
        IResource resource = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
        if (resource != null) {
            return resource.getInputStream();
        }
        return null;
    }
}
