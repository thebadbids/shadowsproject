package ru.xlv.mochar;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import lombok.Getter;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.event.EventListener;
import ru.xlv.core.event.KeyRegistry;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.util.Config;
import ru.xlv.core.util.data.News;
import ru.xlv.core.util.data.RequestHandler;
import ru.xlv.core.util.obf.IgnoreObf;

import java.util.ArrayList;
import java.util.List;

@Mod(
        name = "More Characters Mod by Xlv",
        modid = XlvsMainMod.MODID,
        version = "1.0"
)
@IgnoreObf
public class XlvsMainMod {

    public static final String MODID = "xlvsmochar";

    @IgnoreObf
    @Mod.Instance(MODID)
    public static XlvsMainMod INSTANCE;

    @Getter
    private Config config;

    @Getter
    private ClientMainPlayer clientMainPlayer;
    @Getter
    private KeyRegistry keyRegistry;

    @Getter
    private List<News> news;

    private int selectedCharacterIndex;

    @Mod.EventHandler
    public void event(FMLPreInitializationEvent event) {
        config = new Config();
        config.load();
        CommonUtils.registerFMLEvents(keyRegistry = new KeyRegistry());
    }

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        CommonUtils.registerAllEvents(EventListener.class);

        clientMainPlayer = new ClientMainPlayer();

        RequestHandler requestHandler = new RequestHandler();
        news = new ArrayList<>();
        requestHandler.loadNews(newsList -> news.addAll(newsList.getNews()));
    }

    @IgnoreObf
    public int getSelectedCharacterIndex() {
        return selectedCharacterIndex;
    }

    public void setSelectedCharacterIndex(int selectedCharacterIndex) {
        this.selectedCharacterIndex = selectedCharacterIndex;
    }

    public CharacterType getCurrentCharacterType() {
        return CharacterType.values()[getSelectedCharacterIndex()];
    }

    public void resetClientPlayer() {
        this.clientMainPlayer = new ClientMainPlayer();
    }
}
