package ru.xlv.core.event;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.MovingObjectPosition;
import org.lwjgl.input.Keyboard;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.network.skill.PacketSkillUse;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.skill.Skill;
import ru.xlv.core.util.AbstractKeyExecutive;
import ru.xlv.core.util.KeyBindingsExecutive;
import ru.xlv.core.util.KeyUseHotSlot;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class KeyRegistry {

    private static final String KEY_SKILL_CATEGORY = "Skills";
    private static final String KEY_HOT_ITEM_CATEGORY = "Hot items";
    public static final String KEY_INVENTORY_CATEGORY = "World";

    private final List<AbstractKeyExecutive> KEY_BINDINGS = new ArrayList<>();

    public KeyRegistry() {
        Consumer<Integer> skillUseConsumer = slotIndex -> {
            if(ClientMainPlayer.HOT_SLOT_AMOUNT > slotIndex) {
                Skill skill = XlvsMainMod.INSTANCE.getClientMainPlayer().getHotActiveSkill(slotIndex);
                if (skill != null) {
                    int id = skill.getId();
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketCallback(new PacketSkillUse(id));
                }
            }
        };
        register(new KeyBindingsExecutive("Use skill #1", Keyboard.KEY_F1, KEY_SKILL_CATEGORY, () -> skillUseConsumer.accept(0)));
        register(new KeyBindingsExecutive("Use skill #2", Keyboard.KEY_F2, KEY_SKILL_CATEGORY, () -> skillUseConsumer.accept(1)));
        register(new KeyBindingsExecutive("Use skill #3", Keyboard.KEY_F3, KEY_SKILL_CATEGORY, () -> skillUseConsumer.accept(2)));
        register(new KeyBindingsExecutive("Use skill #4", Keyboard.KEY_F4, KEY_SKILL_CATEGORY, () -> skillUseConsumer.accept(3)));
        register(new KeyBindingsExecutive("Interact", Keyboard.KEY_F, KEY_INVENTORY_CATEGORY, () -> {
            Minecraft mc = Minecraft.getMinecraft();
            MovingObjectPosition objectMouseOver = mc.objectMouseOver;
            if (objectMouseOver != null) {
//                if(objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.ENTITY) {
                    Entity entityHit = objectMouseOver.entityHit;
                    XlvsCoreCommon.EVENT_BUS.post(new PlayerInteractWorldObjectEvent(
                            entityHit,
                            Minecraft.getMinecraft().objectMouseOver
                    ));
//                }
            }
        }));
        register(new KeyUseHotSlot("Use item #1", Keyboard.KEY_5, KEY_HOT_ITEM_CATEGORY, MatrixInventory.SlotType.HOT_SLOT0.getAssociatedSlotIndex()));
        register(new KeyUseHotSlot("Use item #2", Keyboard.KEY_6, KEY_HOT_ITEM_CATEGORY, MatrixInventory.SlotType.HOT_SLOT1.getAssociatedSlotIndex()));
        register(new KeyUseHotSlot("Use item #3", Keyboard.KEY_7, KEY_HOT_ITEM_CATEGORY, MatrixInventory.SlotType.HOT_SLOT2.getAssociatedSlotIndex()));
        register(new KeyUseHotSlot("Use item #4", Keyboard.KEY_8, KEY_HOT_ITEM_CATEGORY, MatrixInventory.SlotType.HOT_SLOT3.getAssociatedSlotIndex()));
        register(new KeyUseHotSlot("Use item #5", Keyboard.KEY_9, KEY_HOT_ITEM_CATEGORY, MatrixInventory.SlotType.HOT_SLOT4.getAssociatedSlotIndex()));
    }

    public void register(AbstractKeyExecutive keyBindingsExecutive) {
        register(keyBindingsExecutive, false);
    }

    public void register(AbstractKeyExecutive keyBindingsExecutive, boolean ignoreDuplicateVerification) {
        if(!ignoreDuplicateVerification) {
            for (AbstractKeyExecutive key_binding : KEY_BINDINGS) {
                if (key_binding.getKeyCode() == keyBindingsExecutive.getKeyCode()) {
                    throw new RuntimeException("Already registered!");
                }
            }
        }
        KEY_BINDINGS.add(keyBindingsExecutive);
        ClientRegistry.registerKeyBinding(keyBindingsExecutive);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(InputEvent.KeyInputEvent event) {
        for (AbstractKeyExecutive keyBinding : KEY_BINDINGS) {
            if(keyBinding.isPressed()) {
                keyBinding.execute();
                break;
            }
        }
    }
}
