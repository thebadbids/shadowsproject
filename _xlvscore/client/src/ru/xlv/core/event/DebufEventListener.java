package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.client.event.MouseEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import ru.xlv.core.common.player.character.CharacterDebuff;
import ru.xlv.core.common.player.character.CharacterDebuffType;
import ru.xlv.mochar.XlvsMainMod;

public class DebufEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(MouseEvent event) {
        CharacterDebuff characterDebuff = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterDebuffMap().get(CharacterDebuffType.STUN);
        if(characterDebuff != null && System.currentTimeMillis() - characterDebuff.getCreationTimeMills() < characterDebuff.getPeriod()) {
            if(Mouse.getEventButtonState()) {
                event.setCanceled(true);
            }
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(CancellableKeyboardInputEvent event) {
        CharacterDebuff characterDebuff = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterDebuffMap().get(CharacterDebuffType.STUN);
        if(characterDebuff != null && System.currentTimeMillis() - characterDebuff.getCreationTimeMills() < characterDebuff.getPeriod()) {
            if(Keyboard.getEventKeyState()) {
                event.setCanceled(true);
            }
        }
        characterDebuff = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterDebuffMap().get(CharacterDebuffType.BLOCK_MOVE);
        if(characterDebuff != null && System.currentTimeMillis() - characterDebuff.getCreationTimeMills() < characterDebuff.getPeriod()) {
            if(Keyboard.getEventKeyState()) {
                event.setCanceled(true);
            }
        }
    }
}
