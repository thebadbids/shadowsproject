package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.network.PacketDoorOpen;

public class OpenDoorEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerInteractEvent event) {
        if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) {
            Block block = event.world.getBlock(event.x, event.y, event.z);
            if(block == Blocks.wooden_door) {
                event.setResult(Event.Result.DENY);
                event.setCanceled(true);
            } else if(block == Blocks.trapdoor) {
                event.setResult(Event.Result.DENY);
                event.setCanceled(true);
            }
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerInteractWorldObjectEvent event) {
        Block block = Minecraft.getMinecraft().theWorld.getBlock(event.getMovingObjectPosition().blockX, event.getMovingObjectPosition().blockY, event.getMovingObjectPosition().blockZ);
        if(block == Blocks.wooden_door) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketDoorOpen(event.getMovingObjectPosition().blockX, event.getMovingObjectPosition().blockY, event.getMovingObjectPosition().blockZ));
        }
    }
}
