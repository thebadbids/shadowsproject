package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.network.PacketItemPickUp;

public class ItemPickUpEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void event(PlayerInteractWorldObjectEvent event) {
        Entity entity1 = event.getEntity();
        if(entity1 instanceof EntityItem) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketItemPickUp(entity1.getEntityId()));
            event.setCanceled(true);
        }
    }
}
