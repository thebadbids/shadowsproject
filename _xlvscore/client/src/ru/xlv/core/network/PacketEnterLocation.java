package ru.xlv.core.network;

import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.gui.overlay.GameOverlayEnterArea;

import java.io.IOException;

public class PacketEnterLocation implements IPacketIn {

    public PacketEnterLocation() {
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        String message = bbis.readUTF();
        GameOverlayEnterArea overlayElement = XlvsCore.INSTANCE.getGameOverlayManager().getOverlayElement(GameOverlayEnterArea.class);
        if (overlayElement != null) {
            overlayElement.setMessage(message);
        }
    }
}
