package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketItemPickUp implements IPacketOut {

    private int entityId;

    public PacketItemPickUp(int entityId) {
        this.entityId = entityId;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityId);
    }
}
