package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.item.ItemIOUtils;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketItemBaseSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        ItemIOUtils.read(bbis);
    }
}
