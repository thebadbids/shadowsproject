package ru.xlv.core.network.skill;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillList implements IPacketIn {

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        XlvsMainMod.INSTANCE.getClientMainPlayer().updateLearnedSkills(readList(bbis, ByteBufInputStream::readInt));
        PacketSkillBuildSync.readSkillBuilds(bbis);
    }
}
