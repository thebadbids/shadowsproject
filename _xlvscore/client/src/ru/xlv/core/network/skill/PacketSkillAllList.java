package ru.xlv.core.network.skill;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.skill.Skill;
import ru.xlv.core.skill.SkillIO;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketSkillAllList implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        List<Skill> skills = SkillIO.readSkillList(bbis);
        XlvsMainMod.INSTANCE.getClientMainPlayer().updateCharacterSkills(skills);
    }
}
