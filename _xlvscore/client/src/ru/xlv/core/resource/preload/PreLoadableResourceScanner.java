package ru.xlv.core.resource.preload;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import java.lang.reflect.Field;
import java.util.Set;

public class PreLoadableResourceScanner {

    public Set<Field> scanFields() {
        Reflections reflections = new Reflections("ru", new FieldAnnotationsScanner());
        return reflections.getFieldsAnnotatedWith(PreLoadableResource.class);
    }

    public Set<Class<?>> scanClasses() {
        Reflections reflections = new Reflections("ru", new TypeAnnotationsScanner(), new SubTypesScanner());
        return reflections.getTypesAnnotatedWith(PreLoadableResource.class);
    }
}
