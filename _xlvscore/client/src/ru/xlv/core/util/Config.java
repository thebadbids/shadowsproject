package ru.xlv.core.util;

import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;

@Configurable
public class Config implements IConfigGson {

    @IgnoreObf
    public int lastCharacterTypeIndex = 0;

    @Override
    public File getConfigFile() {
        return new File("config/mochars.out");
    }
}
