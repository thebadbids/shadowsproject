package ru.xlv.core.util.data;

import java.util.List;

public class NewsList {

    private final List<News> news;

    public NewsList(List<News> news) {
        this.news = news;
    }

    public List<News> getNews() {
        return news;
    }
}
