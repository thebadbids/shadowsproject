package ru.xlv.core.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.gui.button.ButtonCharacterItem;
import ru.xlv.core.resource.preload.PreLoadableResource;
import ru.xlv.core.util.ConfigLastCharacterData;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import static org.lwjgl.opengl.GL11.*;

public class GuiCharacterChoose extends GuiModule {

    private CharacterType characterType;

    @PreLoadableResource public static final ResourceLocation resLocNewsScroll = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/character_chose_scroll.png");
    @PreLoadableResource public static final ResourceLocation resLocNewsScrollBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/character_chose_scroll_bg.png");
    @PreLoadableResource public static final ResourceLocation resLocBntBig = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn_big.png");
    @PreLoadableResource public static final ResourceLocation resLocBntBigHover = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn_big_hover.png");
    @PreLoadableResource public static final ResourceLocation resLocCharacterChooseBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/character_choose_bg.png");

    private float scroll, scrollAnim, lastScroll;
    private float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    private float scrollX, scrollY, scrollWidth, scrollHeight;
    private boolean mouseScrolling;
    private float mouseStartY;
    private float animation;

    public GuiCharacterChoose(GuiScreen parent, int width, int height) {
        super(parent, width, height);
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        scrollViewHeight = 744;
        float intendX = 16f;

        int i = 0;
        float x = 1342;
        float y = 101;
        float yStart = 80;
        float buttonWidth = 545;
        float buttonHeight = 81;
        for (ConfigLastCharacterData.CharacterData characterData : XlvsCore.INSTANCE.getConfigLastCharacterData().getCharacterDataList()) {
            ButtonCharacterItem buttonCharacterItem = new ButtonCharacterItem(i, ScaleGui.getRight(x, 0), ScaleGui.getCenterY(y, 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),
                    characterData.getCharacterType(), characterData.getCharacterLevel());
            addButton(buttonCharacterItem);
            i++;
            y += 96;
        }

        scrollTotalHeight = y - yStart;

        y = 852;
        x = 1348;
        buttonWidth = 559;
        y += 112;
        GuiButtonAdvanced texturedButton = new GuiButtonAdvanced(-2, ScaleGui.getRight(x + intendX, 0), ScaleGui.getCenterY(y, 0),
                ScaleGui.get(buttonWidth - intendX * 2), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        texturedButton.setTexture(resLocBntBig);
        texturedButton.setTextureHover(resLocBntBigHover);
        addButton(texturedButton);
    }

    private void renderScroll(float offsetX) {
        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = 1904 + offsetX;
        float y = 104;
        float iconWidth = 7;
        scrollTextureHeight = 716;
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        float iconHeight = scrollTextureHeight * scrollValue;
        y += iconHeight / 2f + scrollAnim * scrollYValue;
        scrollX = ScaleGui.getRight(x, iconWidth);
        scrollY = ScaleGui.getCenterY(y, iconHeight);
        scrollWidth = ScaleGui.get(iconWidth);
        scrollHeight = ScaleGui.get(iconHeight);
        GuiDrawUtils.drawCenterYRightCentered(resLocNewsScroll, x, y, iconWidth, iconHeight);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        animation = AnimationHelper.updateSlowEndAnim(animation, 1.0f, 0.25f, 0.003f);
        float offsetX = 592 * (1.0f - animation);

        for (Object button : buttonList) {
            if (button instanceof GuiButtonAdvanced) {
                GuiButtonAdvanced texButton = (GuiButtonAdvanced) button;
                texButton.addXPosition(ScaleGui.get(offsetX));
            }
        }

        for (int i=0;i<buttonList.size()-2;i++) {
            Object button = buttonList.get(i);
            if(button instanceof GuiButtonAdvanced) {
                GuiButtonAdvanced texButton = (GuiButtonAdvanced) button;
                float by = 102 + i * 96f;
                texButton.setYPosition(ScaleGui.getCenterY(by - scrollAnim, 0));
            }
        }

        drawRect(0, 0, width, height, Integer.MIN_VALUE);
        GL11.glColor4f(1, 1, 1, 1);
        GuiDrawUtils.drawCenterYRightCentered(resLocCharacterChooseBg, 1624 + offsetX, 540, 592, 1080);

        float fontScale = 40 / 22f;
        float x = 1347;
        float y = 30;
        GuiDrawUtils.drawStringCenterYRight(FontType.FUTURA_PT_DEMI, "Выбор персонажа", x + offsetX, y, fontScale, 0xffffff);
        GuiDrawUtils.drawCenterYRightCentered(resLocNewsScrollBg, 1904 + offsetX, 463, 13, 723);

        renderScroll(offsetX);
        drawButtons(mouseX, mouseY, partialTick);
        drawModules(mouseX, mouseY, partialTick);
    }
    
    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
    	super.mouseClicked(mouseX, mouseY, mouseButton);

        if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
            mouseStartY = mouseY;
            mouseScrolling = true;
            lastScroll = scroll;
        }
    }
    
    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
    	super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);

        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = ScaleGui.get(scrollTextureHeight) / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }
    
    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
    	super.mouseMovedOrUp(mouseX, mouseY, which);
        mouseScrolling = false;
    }
    
    public void drawButtons(int mx, int my, float ticks) {
        glEnable(GL_SCISSOR_TEST);
        int sx = (int) ScaleGui.getRight(1328, 0);
        int sy = (int) ScaleGui.getCenterY(258, 0);
        int swidth = (int) ScaleGui.get(592);
        int sheight = (int) ScaleGui.get(753);
        glScissor(sx, sy, swidth, sheight);
    	for (int i=0;i<buttonList.size()-1;i++) {
            GuiButtonAdvanced guiButton = (GuiButtonAdvanced) buttonList.get(i);
            guiButton.drawButton(mx, my);
        }
        glDisable(GL_SCISSOR_TEST);
    	buttonList.get(buttonList.size()-1).drawButton(mc, mx, my);
    }
    
    
    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        if (d != 0) {
            float diff = scrollTotalHeight - scrollViewHeight;
            if(diff > 0) {
                scroll -= d / 2f;
                if(scroll < 0) {
                    scroll = 0;
                } else if(scroll > diff) {
                    scroll = diff;
                }
            }
        }
    }

    @Override
    public void actionPerformed(GuiButton guiButton) {
        if(!buttonList.contains(guiButton)) {
            return;
        }

        if(guiButton instanceof ButtonCharacterItem) {
            characterType = ((ButtonCharacterItem) guiButton).getCharacterType();
            for (Object o : buttonList) {
                if(o instanceof ButtonCharacterItem) {
                    ((ButtonCharacterItem) o).setActive(false);
                }
            }
            ((ButtonCharacterItem) guiButton).setActive(true);
            ((GuiCharacterMainMenu) getParentScreen()).setSelectedCharacter(characterType);
            mc.displayGuiScreen(getParentScreen());
        } else {
            switch (guiButton.id) {
                case -1:
                    if(characterType != null) {
                        ((GuiCharacterMainMenu) getParentScreen()).setSelectedCharacter(characterType);
                        mc.displayGuiScreen(getParentScreen());
                    }
                    break;
                case -2:
                    mc.displayGuiScreen(getParentScreen());
                    break;
                case -3:
                	//TODO: char creation
                	 break;
            }
        }
    }
}
