//package ru.xlv.core.gui;
//
//import net.minecraft.util.ResourceLocation;
//import org.lwjgl.input.Mouse;
//import org.lwjgl.opengl.GL11;
//import ru.xlv.core.XlvsCore;
//import ru.xlv.core.network.skill.PacketSkillSelect;
//import ru.xlv.core.player.ClientMainPlayer;
//import ru.xlv.core.skill.Skill;
//import ru.xlv.core.skill.SkillTextureManager;
//import ru.xlv.core.util.DrawUtils;
//import ru.xlv.core.util.Utils;
//import ru.xlv.mochar.XlvsMainMod;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class GuiSkillManager extends GuiExtended {
//
//    private final ResourceLocation TEXTURE_BACKGROUND, TEXTURE_SKILL_SLOT;
//
//    private int skillListStartX, skillListStartY, skillListWidth, skillListHeight;
//    private int skillIconSize;
//    private int hotSkillsStartX, hotSkillsStartY;
//
//    private int draggedSkillId = -1;
//
//    private int lastMouseX, lastMouseY;
//    private float scroller;
//
//    private List<ItemSkill> itemSkills = new ArrayList<>();
//    private List<ItemSkillSlot> skillSlots = new ArrayList<>();
//
//    public GuiSkillManager() {
////        TEXTURE_BACKGROUND = loadTexture(XlvsMainMod.MODID, "textures/gui/skill_manager/background.png");
////        TEXTURE_SKILL_SLOT = loadTexture(XlvsMainMod.MODID, "textures/gui/overlay/skill/slot.png");
//        TEXTURE_BACKGROUND = null;
//        TEXTURE_SKILL_SLOT = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/overlay/skill/slot.png");
//        Utils.bindTexture(TEXTURE_SKILL_SLOT);
//    }
//
//    @Override
//    public void initGui() {
//        super.initGui();
//
//        skillIconSize = 16;
//        skillListWidth = width / 4;
//        skillListHeight = height / 2;
//        skillListStartX = (width - skillListWidth) / 2;
//        skillListStartY = (height - skillListHeight) / 2;
//
//        initSkills();
//    }
//
//    private void initSkills() {
//        itemSkills.clear();
//        skillSlots.clear();
//        for (int i = 0; i < XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterSkills().size(); i++) {
//            itemSkills.add(new ItemSkill(XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterSkills().get(i).getId(), skillListStartX, skillListStartY + skillIconSize * i));
//        }
//
//        hotSkillsStartX = (width - ClientMainPlayer.HOT_SLOT_AMOUNT * (skillIconSize + 2)) / 2;
//        hotSkillsStartY = height - skillIconSize - 10;
//
//        for (int i = 0; i < ClientMainPlayer.HOT_SLOT_AMOUNT; i++) {
//            Skill hotSkill = XlvsMainMod.INSTANCE.getClientMainPlayer().getHotActiveSkill(i);
//            skillSlots.add(new ItemSkillSlot(hotSkillsStartX + (skillIconSize + 2) * i, hotSkillsStartY, i, hotSkill == null ? -1 : hotSkill.getId()));
//        }
//    }
//
//    @Override
//    public void drawScreen(int mouseX, int mouseY, float partialTick) {
//        lastMouseX = mouseX;
//        lastMouseY = mouseY;
//
////        mc.getTextureManager().bindTexture(TEXTURE_BACKGROUND);
////        DrawUtils.drawTexturedModalRect(0, 0, width, height);
//        drawRect(0, 0, width, height, Integer.MIN_VALUE);
//        GL11.glColor4f(1, 1, 1, 1);
//
//        drawRect(skillListStartX, skillListStartY, skillListStartX + skillListWidth, skillListStartY + skillListHeight, Integer.MAX_VALUE);
//        GL11.glColor4f(1, 1, 1, 1);
//
//        GL11.glPushMatrix();
//        GL11.glEnable(GL11.GL_SCISSOR_TEST);
//        DrawUtils.glScissor(skillListStartX, skillListStartY, skillListWidth, skillListHeight);
//        itemSkills.forEach(ItemSkill::draw);
//        GL11.glDisable(GL11.GL_SCISSOR_TEST);
//        GL11.glPopMatrix();
//
//        skillSlots.forEach(ItemSkillSlot::draw);
//
//        super.drawScreen(mouseX, mouseY, partialTick);
//        if(draggedSkillId != -1) {
//            SkillTextureManager.INSTANCE.bindTexture(draggedSkillId);
//            DrawUtils.drawTexturedModalRect(lastMouseX - skillIconSize / 2, lastMouseY - skillIconSize / 2, skillIconSize, skillIconSize);
//        }
//    }
//
//    @Override
//    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
//        if(mouseButton == 0) {
//            if(draggedSkillId != -1) {
//                for (ItemSkillSlot skillSlot : skillSlots) {
//                    if(skillSlot.isMouseOver(mouseX, mouseY)) {
//                        skillSlot.skillContainedId = draggedSkillId;
//                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketSkillSelect(skillSlot.skillContainedId, skillSlot.slotIndex));
//                        draggedSkillId = -1;
//                        initSkills();
//                        return;
//                    }
//                }
//            } else {
//                for (ItemSkill itemSkill : itemSkills) {
//                    if (itemSkill.isMouseOver(mouseX, mouseY)) {
//                        draggedSkillId = itemSkill.id;
//                        initSkills();
//                        return;
//                    }
//                }
//            }
//        }
//        draggedSkillId = -1;
//        super.mouseClicked(mouseX, mouseY, mouseButton);
//    }
//
//    @Override
//    public void handleMouseInput() {
//        super.handleMouseInput();
//        int d = Mouse.getEventDWheel();
//        if (d != 0) {
//            if (d > 0) {
//                d = -1 * d / 120;
//            } else {
//                d = -1 * d / 120;
//            }
//            float amountScrolled = (float) (d * 4);
//            if (lastMouseY >= skillListStartY && lastMouseY <= skillListStartY + skillListHeight) {
//                if (lastMouseX >= skillListStartX && lastMouseX <= skillListStartX + skillListWidth) {
//                    if (scroller + amountScrolled > 0) {
//                        scroller += amountScrolled;
//                    } else {
//                        scroller = 0;
//                    }
//                }
//            }
//        }
//    }
//
//    private class ItemSkillSlot {
//        final int x, y, slotIndex;
//        int skillContainedId;
//
//        public ItemSkillSlot(int x, int y, int slotIndex, int skillContainedId) {
//            this.x = x;
//            this.y = y;
//            this.slotIndex = slotIndex;
//            this.skillContainedId = skillContainedId;
//        }
//
//        public void draw() {
//            Utils.bindTexture(TEXTURE_SKILL_SLOT);
//            DrawUtils.drawTexturedModalRect(x, y, skillIconSize, skillIconSize);
//        }
//
//        public boolean isMouseOver(int mx, int my) {
//            return mx >= x && mx <= x + skillIconSize && my >= y && my <= y + skillIconSize;
//        }
//    }
//
//    private class ItemSkill {
//        final int id, x, y;
//
//        public ItemSkill(int id, int x, int y) {
//            this.id = id;
//            this.x = x;
//            this.y = y;
//        }
//
//        public void draw() {
//            SkillTextureManager.INSTANCE.bindTexture(id);
//            DrawUtils.drawTexturedModalRect(x, (int) (y - scroller), skillIconSize, skillIconSize);
//
//            if(isMouseOver(lastMouseX, lastMouseY)) {
//                //todo
//            }
//        }
//
//        public boolean isMouseOver(int mx, int my) {
//            return mx >= x && mx <= x + skillIconSize && my >= y && my <= y + skillIconSize;
//        }
//    }
//}
