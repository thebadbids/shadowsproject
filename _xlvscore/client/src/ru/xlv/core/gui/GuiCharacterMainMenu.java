package ru.xlv.core.gui;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.gui.*;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.client.event.EventRenderArmorMainMenu;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.gui.button.ButtonCharacterSelection;
import ru.xlv.core.gui.button.ButtonNewsElement;
import ru.xlv.core.gui.button.TexturedButtonIcon;
import ru.xlv.core.resource.preload.PreLoadableResource;
import ru.xlv.core.util.ConfigLastCharacterData;
import ru.xlv.core.util.CoreUtils;
import ru.xlv.core.util.Utils;
import ru.xlv.core.util.data.News;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.awt.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiCharacterMainMenu extends GuiExtended {

    @PreLoadableResource public static final ResourceLocation resLocBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/bg.png");
    @PreLoadableResource public static final ResourceLocation resLocNewsBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/news_bg.png");
    @PreLoadableResource public static final ResourceLocation resLocPlayerBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/player_bg.png");
    @PreLoadableResource public static final ResourceLocation resLocCharacterBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/character_bg.png");
    @PreLoadableResource public static final ResourceLocation resLocCopyRightBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/copyright_bg.png");
    @PreLoadableResource public static final ResourceLocation resLocLogoBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/logo.png");
    @PreLoadableResource public static final ResourceLocation resLocBtn = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn.png");
    @PreLoadableResource public static final ResourceLocation resLocBtnBig = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn_big.png");
    @PreLoadableResource public static final ResourceLocation resLocBtnMain = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/main_btn.png");
    @PreLoadableResource public static final ResourceLocation resLocBtnHover = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn_hover.png");
    @PreLoadableResource public static final ResourceLocation resLocBtnBigHover = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn_big_hover.png");
    @PreLoadableResource public static final ResourceLocation resLocBtnMainHover = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/main_btn_hover.png");
    @PreLoadableResource public static final ResourceLocation resLocIconSettings = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/settings_icon.png");
    @PreLoadableResource public static final ResourceLocation resLocIconLogout = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/logout_icon.png");
    @PreLoadableResource public static final ResourceLocation resLocIconForum = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/forum_icon.png");
    @PreLoadableResource public static final ResourceLocation resLocIconCabinet = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/cabinet_icon.png");
    @PreLoadableResource public static final ResourceLocation resLocIconPlay = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/play_icon.png");
    @PreLoadableResource public static final ResourceLocation resLocNewsScroll = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/news_scroll.png");
    @PreLoadableResource public static final ResourceLocation resLocNewsScrollBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/news_scroll_bg.png");
    @PreLoadableResource public static final ResourceLocation resLocEllipse = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/ellipse.png");
    @PreLoadableResource public static final ResourceLocation resLocCharacterItemBg = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/character_item_bg.png");
    @PreLoadableResource public static final ResourceLocation textureCharacterTank = new ResourceLocation("pda", "textures/group/icon_tank.png");
    @PreLoadableResource public static final ResourceLocation textureCharacterStormTrooper = new ResourceLocation("pda", "textures/group/icon_stormtrooper.png");
    @PreLoadableResource public static final ResourceLocation textureCharacterMedic = new ResourceLocation("pda", "textures/group/icon_medic.png");

    private float intendX, intendY;
    private float scroll, scrollAnim, lastScroll;
    private float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    private float scrollX, scrollY, scrollWidth, scrollHeight;
    private boolean mouseScrolling;
    private float mouseStartY;
    private int lastMouseX, lastMouseY;
    private boolean rotateCharacter = false;
    private float xRotate, xRotateAdd, xRotateAnimation;
    private float mouseStartX;
    private float leftPartHeight, rightPartHeight0, rightPartHeight1, rightPartHeight2;
    private int expandedNewsID;

    private CharacterType characterType;
    private final List<News> addedNews = new ArrayList<>();
    private final Vector3f lightPosition = new Vector3f(0f, -50f, 100f);
    private final Vector3f lightColor = new Vector3f(1.5f, 1.5f, 2f);

    public GuiCharacterMainMenu() {
        super();
        FMLClientHandler.instance().setupServerList();
        this.characterType = CharacterType.values()[XlvsMainMod.INSTANCE.getSelectedCharacterIndex()];
    }

    @Override
    public void initGui() {
        super.initGui();
        addedNews.clear();
        
        XlvsMainMod.INSTANCE.resetClientPlayer();

        setSelectedCharacter(characterType = CharacterType.values()[XlvsMainMod.INSTANCE.getConfig().lastCharacterTypeIndex]);

        intendX = 27;
        intendY = 32;

        leftPartHeight = 1014;
        rightPartHeight2 = 725;
        rightPartHeight1 = 206;
        rightPartHeight0 = 44;

        scrollViewHeight = 955;

        updateButtons();

        activateModule(-1);
        addModule(new GuiCharacterChoose(this, width, height));
    }

    private void renderScroll() {
        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float x = 566;
        float y = 94;
        float iconWidth = 7;
        scrollTextureHeight = 937;
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        float iconHeight = scrollTextureHeight * scrollValue;
        y += iconHeight / 2f + scrollAnim * scrollYValue;
        scrollX = ScaleGui.getCenterX(x, iconWidth);
        scrollY = ScaleGui.getCenterY(y, iconHeight);
        scrollWidth = ScaleGui.get(iconWidth);
        scrollHeight = ScaleGui.get(iconHeight);
        GuiDrawUtils.drawCenterCentered(resLocNewsScroll, x, y, iconWidth, iconHeight);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);

        lastMouseX = mouseX;
        lastMouseY = mouseY;
        updateNews();
        GuiDrawUtils.drawRect(resLocBg, 0, 0, width, height);
        GuiDrawUtils.drawCenterCentered(resLocNewsBg, 306, 540, 558, leftPartHeight);
        GuiDrawUtils.drawCenterCentered(resLocNewsScrollBg, 566, 562, 13, 945);
        
        float totalNewsBlockHeight = 0;
        for (Object o : buttonList) {
            if(o instanceof ButtonNewsElement) {
            	ButtonNewsElement buttonNewsElement = ((ButtonNewsElement) o);
                totalNewsBlockHeight += buttonNewsElement.getElementHeight();
            }
        }

        scrollTotalHeight = totalNewsBlockHeight / (ScaleGui.scaleValue / ScaleGui.DEFAULT_HEIGHT) + 90;

        renderScroll();

        float x = 1607;
        float y = 395;
        float elementWidth = 558;
        GuiDrawUtils.drawCenterCentered(resLocPlayerBg, x, y, elementWidth, rightPartHeight2);
        y += 20 + rightPartHeight2 / 2f + rightPartHeight1 / 2f;
        GuiDrawUtils.drawCenterCentered(resLocCharacterBg, x, y, elementWidth, rightPartHeight1);
        y += 20 + rightPartHeight1 / 2f + rightPartHeight0 / 2f;
        GuiDrawUtils.drawCenterCentered(resLocCopyRightBg, x, y, elementWidth, rightPartHeight0);
        GuiDrawUtils.drawCenterCentered(resLocLogoBg, 959, 305, 469, 124);
        GuiDrawUtils.drawPreAlpha(652, 400, 610, 100, 2.0f);

        float fs = 40 / 22f;
        y = 50;
        GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_DEMI, "Новости", 45, y, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_DEMI, "Ваш аккаунт", 1346, y, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_DEMI, "Ваши персонажи", 1346, 792, fs, 0xffffff);
        fs = 25 / 22f;
        GuiDrawUtils.drawStringCenter(FontType.ROBOTO_REGULAR, "ShadowsProject.Ru © 2020 | Права на игру принадлежат Mojang и Microsoft               V2.0.1", 1346, 1021, fs, 0x6f6f6f);

        x = ScaleGui.getCenterX(x, 0);
        y = ScaleGui.getCenterY(360, 0);
        float scale = ScaleGui.get(250f);
        renderPlayer(x, y, scale, xRotate + xRotateAdd + xRotateAnimation + 180);

        x = 1607;
        y = 121;
        fs = 80 / 32f;
        String playerNickname = mc.getSession().getUsername();
        float nickNameHalfOffsetX = FontType.FUTURA_PT_BOLD.getFontContainer().width(playerNickname) / 2.0f * fs;
        GuiDrawUtils.drawStringCenter(FontType.FUTURA_PT_BOLD, playerNickname, x - nickNameHalfOffsetX - 39 / 2f, y, fs, 0xffffff);

        float ellipseWidth =  41;
        float ellipseHeight = 41;
        GuiDrawUtils.drawCenterCentered(resLocEllipse, x + nickNameHalfOffsetX + 15, y + 10, ellipseWidth, ellipseHeight);
        GuiDrawUtils.drawCenteredStringCenter(FontType.FUTURA_PT_BOLD, "" +  XlvsCore.INSTANCE.getConfigLastCharacterData().getCharacterData(characterType).getCharacterLevel(), x + nickNameHalfOffsetX + 14, y, fs, 0xe79839);

        glColor4f(1f, 1f, 1f, 1f);

        glEnable(GL_SCISSOR_TEST);
        int sx = (int) ScaleGui.getCenterX(27);
        int sy = (int) ScaleGui.getCenterY(46);
        int swidth = (int) ScaleGui.get(558);
        int sheight = (int) ScaleGui.get(955);
        glScissor(sx, sy, swidth, sheight);
        ButtonNewsElement preButton = null;
        float shiftY = 0;
        for (GuiButton button : buttonList) {
            if(button instanceof ButtonNewsElement) {
                if(preButton != null) {
                    shiftY += preButton.getElementHeight();
                }
                ((ButtonNewsElement) button).drawButton(button.xPosition, shiftY + ScaleGui.getCenterY(intendY + 70 - scrollAnim, 0), mouseX, mouseY);
                preButton = (ButtonNewsElement) button;
            }
        }
        glDisable(GL_SCISSOR_TEST);

        for (Object o : buttonList) {
            GuiButtonAdvanced button = (GuiButtonAdvanced) o;
            if(!(button instanceof ButtonNewsElement)) {
                button.drawButton(mouseX, mouseY);
            }
        }

        glPushMatrix();
        glTranslatef(0, 0, 100f);
        drawModules(mouseX, mouseY, partialTick);
        glPopMatrix();
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        super.actionPerformed(guiButton);

        expandedNewsID = -1;

        if (guiButton.id < 0) {
            switch (guiButton.id) {
                case -1:
                    mc.displayGuiScreen(new GuiOptions(this, mc.gameSettings));
                    break;
                case -2:
                    mc.shutdown();
                    break;
                case -3:
                    Desktop desktop = java.awt.Desktop.getDesktop();
                    try {
                        URI oURL = new URI("https://shadowsproject.ru/forum");
                        desktop.browse(oURL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case -4:
                    desktop = java.awt.Desktop.getDesktop();
                    try {
                        URI oURL = new URI("https://shadowsproject.ru/profile");
                        desktop.browse(oURL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case -5:
                    ScaledResolution.isDefaultRescale = true;
                    if(characterType != null) {
                        XlvsMainMod.INSTANCE.getConfig().save();
                        XlvsMainMod.INSTANCE.setSelectedCharacterIndex(characterType.ordinal());
                    }

                    if(CoreUtils.isStartedFromGradle()) {
                        mc.displayGuiScreen(new GuiMultiplayer(this));
                    } else {
                        FMLClientHandler.instance().connectToServer(new GuiCharacterMainMenu(), new ServerData("I <3 XLV", CoreUtils.getConnectionGameServerAddress()));
                    }
                    break;
                case -6:
                    activateModule(0);
                    break;
                case -7:
                    mc.displayGuiScreen(new GuiSelectWorld(this));
                    break;
            }
        } else if(guiButton instanceof ButtonNewsElement) {
            if(((ButtonNewsElement) guiButton).isExpanded()) {
                Utils.openBrowser(((ButtonNewsElement) guiButton).getNews().getNewsURL());
            } else {
                expandedNewsID = guiButton.id;
            }
        }

        for (Object o : buttonList) {
            if(o instanceof ButtonNewsElement) {
                ((ButtonNewsElement) o).setExpanded(false);
            }
        }
        
        if(expandedNewsID == guiButton.id && guiButton instanceof ButtonNewsElement) {
            ((ButtonNewsElement) guiButton).setExpanded(true);
        }
    }

    private void updateNews() {
        List<News> allNews = XlvsMainMod.INSTANCE.getNews();
        float x = intendX + 19;
        float y = intendY - scrollAnim + 70;
        float yStart = y;
        float buttonWidth = 501;
        float buttonHeight = 360;
//        for(int i=0;i<5;i++) {
//            ButtonNewsElement button = new ButtonNewsElement(i, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y, 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
//            button.setNews(new News("ARTICLE", "Faf[ a]sfp [asfp sflgl sdflkg jlkdg jklag dkg p[alg asgl; ]as;g [asg aopsga psgla[psgl a[pslg a[gldf g", null, "shadowsproject.ru"));
//            addButton(button);
//            newsAdded = true;
//            y += buttonHeight;
//        }

        for (int i=0;i<allNews.size();i++) {
            News news = allNews.get(i);
            if(!addedNews.contains(news)) {
                ButtonNewsElement button = new ButtonNewsElement(i, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y, 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
                button.setNews(news);
                if(expandedNewsID == i) {
                    button.setExpanded(true);
                }
                addButton(button);
                addedNews.add(news);
            }
            y += buttonHeight;
        }
        scrollTotalHeight = y - yStart;
    }

    private void updateButtons() {
        buttonList.clear();
        updateNews();
        float x = 656;
        float y = 963;
        float buttonWidth = 297;
        float buttonHeight = 82;
        float yOffset = 112;
        float iconPosX = ScaleGui.get(buttonHeight / 2f - buttonHeight / 3f / 4f);
        float iconPosY = ScaleGui.get(buttonHeight / 2f - buttonHeight / 5.2f);
        float iconSize = ScaleGui.get(buttonHeight / 2.5f);
        TexturedButtonIcon button = new TexturedButtonIcon(-1, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАСТРОЙКИ");
        button.setTextOffsetX(ScaleGui.get(16));
        button.setTexture(resLocBtn);
        button.setTextureHover(resLocBtnHover);
        button.setIconTexture(resLocIconSettings);
        button.setIconSize(iconSize);
        button.setIconX(iconPosX);
        button.setIconY(iconPosY);
        addButton(button);
        x += 311;
        button = new TexturedButtonIcon(-2, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЫЙТИ");
        button.setTextOffsetX(ScaleGui.get(16));
        button.setTexture(resLocBtn);
        button.setTextureHover(resLocBtnHover);
        button.setIconTexture(resLocIconLogout);
        button.setIconSize(iconSize);
        button.setIconX(iconPosX);
        button.setIconY(iconPosY);
        addButton(button);
        y -= yOffset;
        x -= 311;
        buttonWidth = 608;
        button = new TexturedButtonIcon(-3, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ФОРУМ");
        button.setTexture(resLocBtnBig);
        button.setTextureHover(resLocBtnBigHover);
        button.setIconTexture(resLocIconForum);
        button.setIconSize(iconSize);
        button.setIconX(iconPosX);
        button.setIconY(iconPosY);
        addButton(button);
        y -= yOffset;
        button = new TexturedButtonIcon(-4, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ЛИЧНЫЙ КАБИНЕТ");
        button.setTexture(resLocBtnBig);
        button.setTextureHover(resLocBtnBigHover);
        button.setIconTexture(resLocIconCabinet);
        button.setIconSize(iconSize);
        button.setIconX(iconPosX);
        button.setIconY(iconPosY);
        addButton(button);
        y -= yOffset;
        button = new TexturedButtonIcon(-5, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПРИСОЕДИНИТЬСЯ К ИГРЕ");
        button.setTexture(resLocBtnMain);
        button.setTextureHover(resLocBtnMainHover);
        button.setIconTexture(resLocIconPlay);
        button.setIconSize(iconSize);
        button.setIconX(iconPosX);
        button.setIconY(iconPosY);
        addButton(button);
        if(CoreUtils.isStartedFromGradle()) {
            y -= yOffset;
            button = new TexturedButtonIcon(-7, ScaleGui.getCenterX(x, 0), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Одиночная игра");
            button.setTexture(resLocBtnMain);
            button.setTextureHover(resLocBtnMainHover);
            button.setIconTexture(resLocIconPlay);
            button.setIconSize(iconSize);
            button.setIconX(iconPosX);
            button.setIconY(iconPosY);
            addButton(button);
        }

        x = 1607;
        y = 939;
        buttonWidth = 189;
        buttonHeight = 29;
        ButtonCharacterSelection buttonCharacterItem = new ButtonCharacterSelection(-6, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y , 0), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight),
                characterType, XlvsCore.INSTANCE.getConfigLastCharacterData().getCharacterData(characterType).getCharacterLevel());
        addButton(buttonCharacterItem);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
    	super.mouseClicked(mouseX, mouseY, mouseButton);
    	
    	float minX = ScaleGui.getCenterX(1328, 0);
    	float minY = ScaleGui.getCenterY(80, 0);
    	float maxX = minX + ScaleGui.get(558);
    	float maxY = minY + ScaleGui.get(677);
    	
    	if(mouseX > minX && mouseY > minY && mouseX < maxX && mouseY < maxY) {
    		mouseStartX = mouseX;
    		mouseStartY = mouseY;
    		rotateCharacter = true;
    	}

        if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
            mouseStartY = mouseY;
            mouseScrolling = true;
            lastScroll = scroll;
        }
    }
    
    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
    	super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
    	
    	if(rotateCharacter) {
    		xRotateAdd = -(mouseX - mouseStartX) / (width / 1000f);
    	}

        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = ScaleGui.get(scrollTextureHeight) / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }
    
    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
    	super.mouseMovedOrUp(mouseX, mouseY, which);
    	
    	xRotate += xRotateAdd;
    	
    	if(xRotate > 360f) {
    		xRotate -= 360f;
    	} else if(xRotate < 0f){
    		xRotate += 360f;
    	}
    	
    	xRotateAdd = 0f;
    	rotateCharacter = false;
    	mouseScrolling = false;
    }
    
    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        if (d != 0) {
            float diff = scrollTotalHeight - scrollViewHeight;
            float x = ScaleGui.getCenterX(27, 0);
            float y = ScaleGui.getCenterY(79, 0);
            float width = ScaleGui.get(558);
            float height = ScaleGui.get(967);
            if(lastMouseX > x && lastMouseY > y && lastMouseX <= x + width && lastMouseY <= y + height) {
                if(diff > 0) {
                    scroll -= d / 2f;
                    if(scroll < 0) {
                        scroll = 0;
                    } else if(scroll > diff) {
                        scroll = diff;
                    }
                }
            }
        }
    }

    public void setSelectedCharacter(CharacterType characterType) {
        this.characterType = characterType;
        XlvsMainMod.INSTANCE.getConfig().lastCharacterTypeIndex = characterType.ordinal();
        XlvsMainMod.INSTANCE.getConfig().save();
    }

    private void renderPlayer(float x, float y, float scale, float rotateX) {
        ConfigLastCharacterData configLastCharacterData = XlvsCore.INSTANCE.getConfigLastCharacterData();
        ConfigLastCharacterData.CharacterData characterData = configLastCharacterData.getCharacterData(XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterType());
        GuiDrawUtils.drawPlayer(mc.thePlayer, x, y, -10f, scale, rotateX, 1f, 1f, 1f, lightPosition, lightColor,
                new EventRenderArmorMainMenu(mc.thePlayer, (RenderPlayer) RenderManager.instance.getEntityClassRenderObject(EntityPlayer.class),
                        characterData.getBodyItemName(), characterData.getHeadItemName(), characterData.getBracersItemName(), characterData.getPantsItemName(), characterData.getBotsItemName()));
        xRotateAnimation += AnimationHelper.getAnimationSpeed() * 0.2f;
    }
}
