package ru.xlv.core.skill;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.renderer.texture.Texture;
import ru.xlv.core.renderer.texture.TextureManager;
import ru.xlv.mochar.XlvsMainMod;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SkillTextureManager extends TextureManager<Integer> {

    public static final SkillTextureManager INSTANCE = new SkillTextureManager();

    private static final String SKILL_TEXTURE_FORMAT = XlvsMainMod.MODID + ":textures/gui/skills/%s.png";

    private static final String SKILL_TEXTURE_EMPTY = "empty";

    private final Map<Integer, ResourceLocation> locationMap = Collections.synchronizedMap(new HashMap<>());

    private SkillTextureManager() {}

    @Override
    public boolean loadTexture0(Integer key, ITextureObject texture) {
        ResourceLocation resourceLocation = getLocation(key);
        if(Minecraft.getMinecraft().getTextureManager().loadTexture(resourceLocation, texture)) {
            locationMap.put(key, resourceLocation);
            return true;
        }
        locationMap.put(key, getLocation(SKILL_TEXTURE_EMPTY));
        return false;
    }

    @Override
    public void unloadTexture0(Integer key, ITextureObject texture) {
        locationMap.remove(key);
        GL11.glDeleteTextures(texture.getGlTextureId());
    }

    @Override
    public void bindTexture(Integer key) {
        ResourceLocation resourceLocation = locationMap.get(key);
        if(resourceLocation == null) {
            loadTexture(key, new Texture(resourceLocation = getLocation(key)));
        }
        Minecraft.getMinecraft().getTextureManager().bindTexture(resourceLocation);
    }

    public void unloadAllIgnoreSelected() {
        label:
        for (Integer integer : getRegister().keySet()) {
            for (int i = 0; i < ClientMainPlayer.HOT_SLOT_AMOUNT; i++) {
                Skill skill = XlvsMainMod.INSTANCE.getClientMainPlayer().getHotActiveSkill(i);
                if (skill != null && skill.getId() == integer) {
                    continue label;
                }
            }
            unload(integer);
        }
    }

    private ResourceLocation getLocation(Object key) {
        return new ResourceLocation(String.format(SKILL_TEXTURE_FORMAT, key));
    }
}
