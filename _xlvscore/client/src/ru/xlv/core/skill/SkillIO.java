package ru.xlv.core.skill;

import net.minecraft.item.Item;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SkillIO {

    public static List<Skill> readSkillList(ByteBufInputStream bbis) throws IOException {
        List<Skill> skills = new ArrayList<>();
        int c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            int type = bbis.readInt();
            int id = bbis.readInt();
            int familyId = bbis.readInt();
            int parentIds = bbis.readInt();
            int[] parents = new int[parentIds];
            for (int j = 0; j < parentIds; j++) {
                parents[j] = bbis.readInt();
            }
            String name = bbis.readUTF();
            String description = bbis.readUTF();

            List<Skill.SkillCost> skillCosts = new ArrayList<>();
            int costCount = bbis.readInt();
            for (int j = 0; j < costCount; j++) {
                int costType = bbis.readInt();
                switch (costType) {
                    case 0:
                        skillCosts.add(new Skill.SkillCostMana(bbis.readInt()));
                        break;
                    case 1:
                        skillCosts.add(new Skill.SkillCostItem(bbis.readInt(), Item.getItemById(bbis.readInt())));
                }
            }

            List<Skill.SkillLearnRule> skillRules = new ArrayList<>();
            int rulesCount = bbis.readInt();
            for (int j = 0; j < rulesCount; j++) {
                int ruleType = bbis.readInt();
                Skill.SkillLearnRule skillLearnRule;
                switch (ruleType) {
                    case 0:
                        skillLearnRule = new Skill.SkillLearnRuleExp(ExperienceType.values()[bbis.readInt()], bbis.readDouble());
                        break;
                    case 1:
                        skillLearnRule = new Skill.SKillLearnRuleItem(Item.getItemById(bbis.readInt()), bbis.readInt());
                        break;
                    case 2:
                        int c1 = bbis.readInt();
                        int[] ii = new int[c1];
                        for (int k = 0; k < c1; k++) {
                            ii[k] = bbis.readInt();
                        }
                        skillLearnRule = new Skill.SkillLearnRuleAnother(ii);
                        break;
                    default: continue;
                }
                skillRules.add(skillLearnRule);
            }
            List<String> tags = new ArrayList<>();
            int tagCount = bbis.readInt();
            for (int j = 0; j < tagCount; j++) {
                tags.add(bbis.readUTF());
            }
            float iconX = bbis.readFloat();
            float iconY = bbis.readFloat();
            String textureName = bbis.readUTF();
            Skill skill = null;
            if(type == 1) {
                long cooldown = bbis.readLong();
                long maxCooldown = bbis.readLong();
                long activationPeriod = bbis.readLong();
                boolean isActive = bbis.readBoolean();
                skill = new Skill(id, familyId, parents, name, description, iconX, iconY, textureName, skillCosts, skillRules, tags, isActive, cooldown, maxCooldown, activationPeriod);
            } else if(type == 0) {
                skill = new Skill(id, familyId, parents, name, description, iconX, iconY, textureName, skillCosts, skillRules, tags);
            }
            if(skill != null) {
                skills.add(skill);
            }
        }
        return skills;
    }
}
