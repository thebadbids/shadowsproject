package ru.xlv.core.common.network;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.util.ThrBiConsumer;
import ru.xlv.core.util.ThrCallable;
import ru.xlv.core.util.ThrFunction;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public interface IPacket {

    default <T> void writeCollection(ByteBufOutputStream byteBufOutputStream, Collection<T> collection, ThrBiConsumer<ByteBufOutputStream, T> consumer) throws IOException {
        if(collection == null) {
            byteBufOutputStream.writeInt(0);
            return;
        }
        byteBufOutputStream.writeInt(collection.size());
        for (T t : collection) {
            consumer.accept(byteBufOutputStream, t);
        }
    }

    default <T> List<T> readList(ByteBufInputStream byteBufInputStream, ThrFunction<ByteBufInputStream, T> function) throws IOException {
        List<T> list = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            list.add(function.apply(byteBufInputStream));
        }
        return list;
    }

    default <T> List<T> readList(ByteBufInputStream byteBufInputStream, ThrCallable<T> callable) throws IOException {
        List<T> list = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            list.add(callable.call());
        }
        return list;
    }

    default <T extends IPacketComposable> void writeComposable(ByteBufOutputStream byteBufOutputStream, T object) throws IOException {
        List<Object> list = new LinkedList<>();
        object.writeDataToPacket(list, byteBufOutputStream);
        for (Object object1 : list) {
            if(object1 instanceof Serializable) {
                writeObject(byteBufOutputStream, (Serializable) object1);
            } else {
                throw new IOException("Cannot write data into ByteBufOutputStream. " + object1 + " isn't serializable!");
            }
        }
        object.writeDataToPacketPost(byteBufOutputStream);
    }

    default void writeObject(ByteBufOutputStream byteBufOutputStream, Serializable serializable) throws IOException {
        if(serializable instanceof String) {
            byteBufOutputStream.writeUTF((String) serializable);
        } else if(serializable instanceof Integer) {
            byteBufOutputStream.writeInt((Integer) serializable);
        } else if(serializable instanceof Byte) {
            byteBufOutputStream.writeByte((Byte) serializable);
        } else if(serializable instanceof Boolean) {
            byteBufOutputStream.writeBoolean((Boolean) serializable);
        } else if(serializable instanceof Long) {
            byteBufOutputStream.writeLong((Long) serializable);
        } else if(serializable instanceof Float) {
            byteBufOutputStream.writeFloat((Float) serializable);
        } else if(serializable instanceof Double) {
            byteBufOutputStream.writeDouble((Double) serializable);
        } else if(serializable instanceof Character) {
            byteBufOutputStream.writeChar((Character) serializable);
        } else if(serializable instanceof Short) {
            byteBufOutputStream.writeShort((Short) serializable);
        } else if(serializable instanceof byte[]) {
            byteBufOutputStream.writeBytes(new String((byte[]) serializable));
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(serializable);
            objectOutputStream.flush();
            byteBufOutputStream.write(byteArrayOutputStream.toByteArray());
            objectOutputStream.close();
        }
    }
}
