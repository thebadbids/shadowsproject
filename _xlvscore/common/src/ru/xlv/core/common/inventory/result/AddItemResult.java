package ru.xlv.core.common.inventory.result;

import ru.xlv.core.common.util.IResult;

public enum AddItemResult implements IResult {

    NO_FREE_SPACE,
    SUCCESS,
    UNKNOWN;

    @Override
    public boolean isSuccess() {
        return this == SUCCESS;
    }
}
