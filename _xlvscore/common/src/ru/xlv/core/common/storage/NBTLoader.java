package ru.xlv.core.common.storage;

import lombok.RequiredArgsConstructor;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class NBTLoader {

    public static final String ARRAY_DELIMITER = ",";

    private final NBTTagCompound nbtTagCompound;

    public void writeToNBT(ISavableNBT savableNBT) {
        if(savableNBT.getTagName() != null) {
            NBTTagCompound tag = new NBTTagCompound();
            savableNBT.writeToNBT(tag, this);
            nbtTagCompound.setTag(savableNBT.getTagName(), tag);
        }
    }

    public void readFromNBT(ISavableNBT savableNBT) {
        if (savableNBT.getTagName() != null) {
            NBTTagCompound tag = nbtTagCompound.getCompoundTag(savableNBT.getTagName());
            if (tag != null) {
                savableNBT.readFromNBT(tag, this);
            }
        }
    }

    public void writeToNBT(NBTTagCompound nbtTagCompound, ISavableNBT... savableNBTS) {
        for (ISavableNBT iSavableNBT : savableNBTS) {
            NBTTagCompound nbtTagCompound1 = new NBTTagCompound();
            iSavableNBT.writeToNBT(nbtTagCompound1, this);
            nbtTagCompound.setTag(iSavableNBT.getTagName(), nbtTagCompound1);
        }
    }

    public void readFromNBT(NBTTagCompound nbtTagCompound, ISavableNBT... savableNBTs) {
        for (ISavableNBT iSavableNBT : savableNBTs) {
            NBTTagCompound nbtTagCompound1 = (NBTTagCompound) nbtTagCompound.getTag(iSavableNBT.getTagName());
            if (nbtTagCompound1 == null) {
                nbtTagCompound1 = new NBTTagCompound();
            }
            iSavableNBT.readFromNBT(nbtTagCompound1, this);
        }
    }

    public <T> void readListFromNBT(NBTTagCompound nbtTagCompound, String tagName, List<T> list, Function<String, T> function) {
        String string = nbtTagCompound.getString(tagName);
        String[] split = string.split(ARRAY_DELIMITER);
        for (String s : split) {
            T apply = function.apply(s);
            if (apply != null) {
                list.add(apply);
            }
        }
    }

    public <T> void writeListToNBT(NBTTagCompound nbtTagCompound, String tagName, List<T> list, Function<T, String> function) {
        if(!list.isEmpty()) {
            StringBuilder string = new StringBuilder();
            for (T t : list) {
                string.append(function.apply(t)).append(ARRAY_DELIMITER);
            }
            if (string.length() > 0) {
                string = new StringBuilder(string.substring(0, string.length() - 1));
            }
            nbtTagCompound.setString(tagName, string.toString());
        }
    }

    public int getInteger(NBTTagCompound nbtTagCompound, String key, int defaultValue) {
        if(nbtTagCompound.hasKey(key)) {
            return nbtTagCompound.getInteger(key);
        }
        return defaultValue;
    }

    public long getLong(NBTTagCompound nbtTagCompound, String key, long defaultValue) {
        if(nbtTagCompound.hasKey(key)) {
            return nbtTagCompound.getLong(key);
        }
        return defaultValue;
    }

    public void putObject(String key, Object value, NBTTagCompound nbtTagCompound) {
        if(value instanceof String) {
            nbtTagCompound.setString(key, (String) value);
        } else if(value instanceof Integer) {
            nbtTagCompound.setInteger(key, (Integer) value);
        } else if(value instanceof Double) {
            nbtTagCompound.setDouble(key, (Double) value);
        } else if(value instanceof Float) {
            nbtTagCompound.setFloat(key, (Float) value);
        } else if(value instanceof Boolean) {
            nbtTagCompound.setBoolean(key, (Boolean) value);
        } else if(value instanceof Byte) {
            nbtTagCompound.setByte(key, (Byte) value);
        } else if(value instanceof Short) {
            nbtTagCompound.setShort(key, (Short) value);
        } else if(value instanceof Long) {
            nbtTagCompound.setLong(key, (Long) value);
        } else if(value instanceof NBTBase) {
            nbtTagCompound.setTag(key, (NBTBase) value);
        }
    }

    public <T> T getObject(String key, Class<T> tClass, NBTTagCompound nbtTagCompound) {
        if(tClass == String.class) {
            return tClass.cast(nbtTagCompound.getString(key));
        } else if(tClass == Integer.class || tClass == int.class) {
            return tClass.cast(nbtTagCompound.getInteger(key));
        } else if(tClass == Double.class || tClass == double.class) {
            return tClass.cast(nbtTagCompound.getDouble(key));
        } else if(tClass == Float.class || tClass == float.class) {
            return tClass.cast(nbtTagCompound.getFloat(key));
        } else if(tClass == Boolean.class || tClass == boolean.class) {
            return tClass.cast(nbtTagCompound.getBoolean(key));
        } else if(tClass == Byte.class || tClass == byte.class) {
            return tClass.cast(nbtTagCompound.getByte(key));
        } else if(tClass == Short.class || tClass == short.class) {
            return tClass.cast(nbtTagCompound.getShort(key));
        } else if(tClass == Long.class || tClass == long.class) {
            return tClass.cast(nbtTagCompound.getLong(key));
        } else if(tClass == NBTBase.class) {
            return tClass.cast(nbtTagCompound.getTag(key));
        } else if(tClass == NBTTagCompound.class) {
            return tClass.cast(nbtTagCompound.getCompoundTag(key));
        }
        return null;
    }
}
