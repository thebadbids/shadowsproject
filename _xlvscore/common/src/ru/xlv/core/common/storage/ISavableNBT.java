package ru.xlv.core.common.storage;

import net.minecraft.nbt.NBTTagCompound;

public interface ISavableNBT {

    void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader);

    void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader);

    default String getTagName() {
        return this.getClass().getSimpleName();
    }
}
