package ru.xlv.core.common.module;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ru.xlv.core.common.util.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

@SuppressWarnings("unchecked cast")
public class ModuleManager {

    @RequiredArgsConstructor
    private static class ModuleMethod {
        private final ConstructType constructType;
        private final Method method;
    }

    private final Map<Class<? extends Module<?>>, Module<?>> instances = new HashMap<>();
    private final List<ModuleMethod> moduleMethods = new ArrayList<>();

    private final Logger logger = new Logger(getClass().getSimpleName());

    @SneakyThrows
    protected void register(Class<? extends Module<?>> moduleClass, Object... params) {
        Module<?> module;
        if(params == null || params.length == 0) {
            module = moduleClass.newInstance();
        } else {
            Class<?>[] classes = Arrays.stream(params).map(Object::getClass).toArray(Class[]::new);
            Constructor<? extends Module<?>> constructor = moduleClass.getConstructor(classes);
            module = constructor.newInstance(params);
        }
        Arrays.stream(moduleClass.getDeclaredFields())
                .filter(field -> field.getAnnotation(InstanceHolder.class) != null)
                .filter(field -> Modifier.isStatic(field.getModifiers()))
                .filter(field -> Modifier.isPublic(field.getModifiers()))
                .forEach(field -> {
                    try {
                        field.set(null, module);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
        Arrays.stream(moduleClass.getDeclaredMethods())
                .filter(method -> method.getAnnotation(Construct.class) != null)
                .map(method -> new ModuleMethod(method.getAnnotation(Construct.class).type(), method))
                .forEach(moduleMethods::add);
        instances.put((Class<? extends Module<?>>) module.getClass(), module);
        logger.info("registered a new module " + moduleClass);
    }

    public void preInit() {
        callConstructs(ConstructType.PRE);
    }

    public void init() {
        callConstructs(ConstructType.INIT);
    }

    public void postInit() {
        callConstructs(ConstructType.POST);
    }

    public void constructed() {
        callConstructs(ConstructType.CONSTRUCTED);
    }

    private void callConstructs(ConstructType constructType) {
        moduleMethods.stream()
                .filter(moduleMethod -> moduleMethod.constructType == constructType)
                .forEach(moduleMethod -> {
                    try {
                        moduleMethod.method.invoke(instances.get(moduleMethod.method.getDeclaringClass()));
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });
    }

    public <T extends Module<?>> T getInstance(Class<T> tClass) {
        synchronized (instances) {
            return (T) instances.get(tClass);
        }
    }

    public Collection<Module<?>> getInstances() {
        return instances.values();
    }
}
