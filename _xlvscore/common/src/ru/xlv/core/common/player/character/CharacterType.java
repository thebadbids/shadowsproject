package ru.xlv.core.common.player.character;

import lombok.Getter;

@Getter
public enum CharacterType {

    MEDIC("Поддержка"),
    TANK("Защита"),
    ASSASSIN("Штурмовик");

    private final String displayName;

    CharacterType(String displayName) {
        this.displayName = displayName;
    }
}
