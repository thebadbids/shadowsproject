package ru.xlv.core.common.util;

import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Configurable
public class ConfigAttributes implements IConfigGson {

    public transient static final ConfigAttributes INSTANCE = new ConfigAttributes();
    static {
        INSTANCE.load();
    }

    public double DEFAULT_VALUE_HEALTH_ATTRIBUTE = 100D;
    public double DEFAULT_VALUE_STAMINA_ATTRIBUTE = 100D;
    public double DEFAULT_VALUE_MANA_ATTRIBUTE = 100D;
    public double DEFAULT_VALUE_WEIGHT_ATTRIBUTE = 50D;
    public double DEFAULT_VALUE_HEALTH_REGEN_ATTRIBUTE = 0.5D;
    public double DEFAULT_VALUE_STAMINA_REGEN_ATTRIBUTE = 0D;
    public double DEFAULT_VALUE_MANA_REGEN_ATTRIBUTE = 1D;
    public double DEFAULT_VALUE_MANA_MODIF_ATTRIBUTE = 0D;
    public double DEFAULT_VALUE_DAMAGE_BASE_PROTECTION_ATTRIBUTE = 0D;
    public double DEFAULT_VALUE_MOVE_SPEED_ATTRIBUTE = 0.10000000149011612D;

    @Override
    public File getConfigFile() {
        return new File("config/xlvscore/base_attributes.json");
    }
}
