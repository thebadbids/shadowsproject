package ru.xlv.parser;

public interface IJsonParser<T> {

    void parse();

    T getResult();
}
