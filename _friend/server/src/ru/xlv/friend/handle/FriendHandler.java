package ru.xlv.friend.handle;

import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementType;
import ru.xlv.core.util.Flex;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.common.FriendRelation;
import ru.xlv.friend.handle.result.FriendInviteResult;
import ru.xlv.friend.handle.result.FriendRemoveResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class FriendHandler {

    @Getter
    private final List<FriendRelation> relations = Collections.synchronizedList(new ArrayList<>());

    public void init() {
        XlvsFriendMod.INSTANCE.getDatabaseManager().getAllFriends().thenAccept(relations::addAll);
    }

    public CompletableFuture<FriendInviteResult> handleRelation(String callerName, String targetName) {
        FriendRelation friendRelation = getRelation(callerName, targetName);
        if (friendRelation == null) {
            friendRelation = new FriendRelation(callerName, targetName);
            friendRelation.setState(FriendRelation.State.INVITATION);
            synchronized (relations) {
                relations.add(friendRelation);
            }
            return XlvsFriendMod.INSTANCE.getDatabaseManager().updateFriendRelation(friendRelation).handle((aBoolean, throwable) -> {
                if(throwable != null) {
                    throwable.printStackTrace();
                }
                if (aBoolean) {
                    return FriendInviteResult.SUCCESSFULLY_INVITED;
                }
                return FriendInviteResult.DATABASE_ERROR;
            });
        } else if (friendRelation.getInitiator().equals(targetName)) {
            friendRelation.setState(FriendRelation.State.FRIENDS);
            XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(FlexPlayer.of(callerName).getPlayer(), AchievementType.FIRST_FRIENDS);
            XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(FlexPlayer.of(targetName).getPlayer(), AchievementType.FIRST_FRIENDS);
            return XlvsFriendMod.INSTANCE.getDatabaseManager().updateFriendRelation(friendRelation).handle((aBoolean, throwable) -> {
                if(throwable != null) {
                    throwable.printStackTrace();
                }
                if (aBoolean) {
                    return FriendInviteResult.SUCCESSFULLY_ACCEPTED;
                }
                return FriendInviteResult.DATABASE_ERROR;
            });
        } else {
            return CompletableFuture.completedFuture(FriendInviteResult.ALREADY_INVITED);
        }
    }

    public CompletableFuture<FriendRemoveResult> removeFriend(String initiator, String target) {
        FriendRelation friendRelation = getRelation(initiator, target);
        if (friendRelation != null) {
            return XlvsFriendMod.INSTANCE.getDatabaseManager().removeFriendRelation(friendRelation).thenApply(aBoolean -> {
                if(aBoolean) {
                    return FriendRemoveResult.SUCCESS;
                } else {
                    return FriendRemoveResult.NOT_FOUND;
                }
            });
        }
        return CompletableFuture.completedFuture(FriendRemoveResult.NOT_FOUND);
    }

    @Nullable
    public FriendRelation getRelation(String left, String right) {
        synchronized (relations) {
            return Flex.getCollectionElement(relations, friendRelation -> (friendRelation.getInitiator().equals(left) && friendRelation.getTarget().equals(right)) || friendRelation.getInitiator().equals(right) && friendRelation.getTarget().equals(left));
        }
    }

    @Nonnull
    public List<FriendRelation> getAllRelations(@Nullable String key) {
        synchronized (relations) {
            return relations.stream()
                    .filter(friendRelation -> friendRelation.getTarget().equals(key) || friendRelation.getInitiator().equals(key))
                    .collect(Collectors.toList());
        }
    }

    @Nonnull
    public List<String> getAllFriendNames(String username) {
        return getAllRelations(username)
                .stream()
                .filter(friendRelation -> friendRelation.getState() == FriendRelation.State.FRIENDS)
                .map(friendRelation -> friendRelation.getInitiator().equals(username) ? friendRelation.getTarget() : friendRelation.getInitiator())
                .collect(Collectors.toList());
    }
}
