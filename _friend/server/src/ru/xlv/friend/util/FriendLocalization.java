package ru.xlv.friend.util;

import ru.xlv.core.util.Localization;
import ru.xlv.core.common.util.config.Configurable;

import java.io.File;

@Configurable
public class FriendLocalization extends Localization {

    public String responseFriendInviteSuccessMessage = "responseFriendInviteSuccess";
    public String responseFriendInviteAcceptedMessage = "responseFriendInviteAccepted";
    public String responseFriendInviteDatabaseErrorMessage = "responseFriendInviteDatabaseError";
    public String responseFriendInviteAlreadyInvitedMessage = "responseFriendInviteUAlreadyInvited";

    public String responseFriendRemoveSuccessMessage = "responseFriendRemoveSuccessMessage";
    public String responseFriendRemoveNotFoundMessage = "responseFriendRemoveNotFoundMessage";

    @Override
    public File getConfigFile() {
        return new File("config/friend/localization.json");
    }
}
