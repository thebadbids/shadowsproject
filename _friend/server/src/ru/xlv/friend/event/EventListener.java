package ru.xlv.friend.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.event.ServerPlayerLoginEvent;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.friend.network.PacketFriendSyncServer;

public class EventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(ServerPlayerLoginEvent event) {
        FlexPlayer.of(event.getServerPlayer())
                .sendPacket(new PacketFriendSyncServer());
    }
}
