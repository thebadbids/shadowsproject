package ru.xlv.friend;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.friend.handle.FriendHandler;
import ru.xlv.friend.network.*;

import static ru.xlv.friend.XlvsFriendMod.MODID;

@Mod(
        name = "XlvsFriendMod",
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsFriendMod {

    static final String MODID = "xlvsfriend";

    @Mod.Instance(MODID)
    public static XlvsFriendMod INSTANCE;

    private final FriendHandler friendHandler = new FriendHandler();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketFriendSync(),
                new PacketFriendInvite(),
                new PacketFriendInviteRequest(),
                new PacketFriendRemove(),
                new PacketFriendPlayerListGet(),
                new PacketFriendSyncServer()
        );
    }
}
