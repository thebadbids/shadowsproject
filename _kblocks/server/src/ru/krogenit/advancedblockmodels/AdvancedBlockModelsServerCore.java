package ru.krogenit.advancedblockmodels;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import ru.krogenit.advancedblockmodels.block.BlockModelsServer;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelRemoveServer;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelServer;
import ru.krogenit.advancedblockmodels.network.packet.PacketTEModelStaticServer;
import ru.xlv.core.XlvsCore;

@Mod(modid = CoreAdvancedBlockModelsCommon.MODID, name = "Advanced Block Models Server", version = "1.0.0")
public class AdvancedBlockModelsServerCore {

	@Instance(CoreAdvancedBlockModelsCommon.MODID)
	@SuppressWarnings("unused")
	public static AdvancedBlockModelsServerCore instance;

	@EventHandler
	@SuppressWarnings("unused")
	public void event(FMLInitializationEvent e) {
		XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(CoreAdvancedBlockModelsCommon.MODID,
				new PacketTEModelRemoveServer(),
				new PacketTEModelStaticServer(),
				new PacketTEModelServer());

		FMLCommonHandler.instance().bus().register(new ServerTickHandler());
		BlockModelsServer.registerTileEntities();
		BlockModelsServer.registerModelBlocks();
	}
}