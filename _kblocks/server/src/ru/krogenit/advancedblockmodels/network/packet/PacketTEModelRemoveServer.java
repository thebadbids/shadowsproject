package ru.krogenit.advancedblockmodels.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class PacketTEModelRemoveServer implements IPacketInOnServer, IPacketOut {

    private int x, y, z, id;

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeInt(x);
        data.writeInt(y);
        data.writeInt(z);
        data.writeInt(id);
    }

    @Override
    public void read(EntityPlayer p, ByteBufInputStream data) throws IOException {
        x = data.readInt();
        y = data.readInt();
        z = data.readInt();
        id = data.readInt();
        handleMessageOnServerSide(p);
    }

    public void handleMessageOnServerSide(EntityPlayer p) {
        World w = p.worldObj;
        TileEntity tile = w.getTileEntity(x, y, z);
        if (tile instanceof TEBlockModel) {
            TEBlockModel te = (TEBlockModel) tile;
            if (id >= 0) {
                TEModel partToRemove = null;
                List<TEModel> parts = te.getParts();
                if (id < parts.size()) {
                    TEModel part = parts.get(id);
                    parts.remove(id);
                    partToRemove = part;
                }

                List<ServerPlayer> playerList = XlvsCore.INSTANCE.getPlayerManager().getPlayerList();
                for(ServerPlayer serverPlayer : playerList) {
                    if(serverPlayer.isOnline()) {
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), this);
                    }
                }

                if (parts.size() == 0 && !BlockModel.isTEHaveObject(te)) w.setBlockToAir(te.xCoord, te.yCoord, te.zCoord);

                if (partToRemove != null && partToRemove.getAabb() != null) {
                    AxisAlignedBB aabb = partToRemove.getAabb();
                    Vector3f position = partToRemove.getPosition();
                    float newX = position.x + te.xCoord;
                    float newY = position.y + te.yCoord;
                    float newZ = position.z + te.zCoord;

                    int minx = MathHelper.floor_double(aabb.minX + newX);
                    int miny = MathHelper.floor_double(aabb.minY + newY);
                    int minz = MathHelper.floor_double(aabb.minZ + newZ);
                    int maxx = MathHelper.floor_double(aabb.maxX + newX) + 1;
                    int maxy = MathHelper.floor_double(aabb.maxY + newY) + 1;
                    int maxz = MathHelper.floor_double(aabb.maxZ + newZ) + 1;

                    for (int x1 = minx; x1 < maxx; x1++)
                        for (int y1 = miny; y1 < maxy; y1++)
                            for (int z1 = minz; z1 < maxz; z1++) {
                                if (x1 != te.xCoord || y1 != te.yCoord || z1 != te.zCoord) {
                                    TileEntity tile1 = w.getTileEntity(x1, y1, z1);
                                    if (tile1 instanceof TEBlockModel) {
                                        TEBlockModel te1 = (TEBlockModel) tile1;
                                        te1.getChildParts().remove(partToRemove);
                                        if (te1.getParts().size() == 0 && !BlockModel.isTEHaveObject(te1)) {
                                            w.setBlockToAir(te1.xCoord, te1.yCoord, te1.zCoord);
                                        }
                                    }
                                }
                            }
                }
            }
        }
    }
}
