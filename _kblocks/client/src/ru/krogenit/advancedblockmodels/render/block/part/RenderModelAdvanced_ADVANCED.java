package ru.krogenit.advancedblockmodels.render.block.part;

import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.advancedblockmodels.render.block.IBlockModelRenderer;
import ru.krogenit.advancedblockmodels.render.block.ITypeRenderer;

import static org.lwjgl.opengl.GL11.*;
import static ru.krogenit.advancedblockmodels.render.block.TERenderModel.*;

public class RenderModelAdvanced_ADVANCED implements ITypeRenderer {
	@Override
	public void render(TEModelClient part) {
		Vector3f position = part.getPosition();
		Vector3f rotation = part.getRotation();
		Vector3f scale = part.getScale();
		glPushMatrix();
		glTranslatef(position.x, position.y, position.z);
		IBlockModelRenderer renderer = part.getRenderer();
		Vector3f scale1 = renderer.getScale();
		glRotatef(rotation.y, 0, 1, 0);
		glRotatef(rotation.x, 1, 0, 0);
		glRotatef(rotation.z, 0, 0, 1);
		glScalef(scale1.x * scale.x, scale1.y * scale.y, scale1.z * scale.z);
		renderer.renderTileEntity(part);
		glPopMatrix();
	}

	@Override
	public void renderUtils(TEModelClient part) {
		IBlockModelRenderer renderer = part.getRenderer();
		Vector3f position = part.getPosition();
		Vector3f rotation = part.getRotation();
		Vector3f scale = part.getScale();
		glPushMatrix();
		glTranslatef(position.x, position.y, position.z);
		glPushMatrix();
		glRotatef(rotation.y, 0, 1, 0);
		glRotatef(rotation.x, 1, 0, 0);
		glRotatef(rotation.z, 0, 0, 1);
		glScalef(scale.x, scale.y, scale.z);
		renderDebug(renderer.getModel());
		glPopMatrix();
		renderAABB(renderer.getModel(), rotation.x, rotation.y, rotation.z, scale);
		glPopMatrix();
		renderBaseAABB(part);
	}

	@Override
	public void renderPost(TEModelClient part) {
		Vector3f position = part.getPosition();
		Vector3f rotation = part.getRotation();
		Vector3f scale = part.getScale();
		glPushMatrix();
		glTranslatef(position.x, position.y, position.z);
		IBlockModelRenderer renderer = part.getRenderer();
		Vector3f scale1 = renderer.getScale();
		glRotatef(rotation.y, 0, 1, 0);
		glRotatef(rotation.x, 1, 0, 0);
		glRotatef(rotation.z, 0, 0, 1);
		glScalef(scale1.x * scale.x, scale1.y * scale.y, scale1.z * scale.z);
		renderer.renderTileEntityPost(part);
		glPopMatrix();
	}
}
