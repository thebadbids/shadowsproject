package ru.krogenit.advancedblockmodels.render.block;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.BlockModel.ModelCollideType;
import ru.krogenit.advancedblockmodels.block.BlocksModelsClient;
import ru.krogenit.advancedblockmodels.key.KeyModelPlaceType;
import ru.krogenit.shaders.GL;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;

@Getter
@Setter
public class RenderMetroModelPreview {
	private IBlockModelRenderer renderer;
	private ModelCollideType type;

	private Vector3f rotation = new Vector3f();
	private Vector3f addPos = new Vector3f();
	private Vector3f scale = new Vector3f(1, 1, 1);
	private final Vector3f vec_zero = new Vector3f(0, 0, 0);
	private final Vector3f vec_single = new Vector3f(1, 1, 1);

	public RenderMetroModelPreview(String model) {
		this.renderer = BlocksModelsClient.getRenderer(model);
	}

	public void render(double d1, double d2, double d3, IPBR shader) {
		glPushMatrix();
		glTranslated(d1, d2, d3);
		render(shader);
		glPopMatrix();
	}

	private void render(IPBR shader) {
		if(renderer != null) {
			Vector3f scale = renderer.getScale();
			BlockModel.ModelRenderType type = renderer.getType();
			switch (KeyModelPlaceType.placeType) {
				case ADVANCED:
				case EMPTY:
				case ADVANCED_NO_COLLISION:
					switch (type) {
						case ADVANCED:
							glPushMatrix();
							glRotatef(rotation.y, 0, 1, 0);
							glRotatef(rotation.x, 1, 0, 0);
							glRotatef(rotation.z, 0, 0, 1);
							glScalef(this.scale.x, this.scale.y, this.scale.z);
							glPushMatrix();
							glScalef(scale.x, scale.y, scale.z);
							renderer.renderPreview(shader);
							glPopMatrix();
							if (renderer.isModelsLoaded()) {
								renderDebug();
								glPopMatrix();
								renderAABB();
								KrogenitShaders.getCurrentPBRShader(false);
							} else
								glPopMatrix();
							break;
						case STANDARD:
							glPushMatrix();
							glRotatef(rotation.y, 0, 1, 0);
							glRotatef(rotation.x, 1, 0, 0);
							glRotatef(rotation.z, 0, 0, 1);
							glScalef(this.scale.x, this.scale.y, this.scale.z);
							glPushMatrix();
							glScalef(scale.x, scale.y, scale.z);
							renderer.renderPreview(shader);
							glPopMatrix();
							if (renderer.isModelsLoaded()) {
								renderDebug();
								glPopMatrix();
								glPushMatrix();
								renderAABB();
								glPopMatrix();
								KrogenitShaders.getCurrentPBRShader(false);
							} else
								glPopMatrix();
							break;
					}
					break;
				case STANDARD:
					Vector3f translate = renderer.getTranslate();
					switch (type) {
						case STANDARD:
						case ADVANCED:
							glTranslatef(0.5f, 0.5f, 0.5f);
							glRotatef(rotation.y, 0, 1, 0);
							glRotatef(rotation.x, 1, 0, 0);
							glRotatef(rotation.z, 0, 0, 1);
							glTranslatef(translate.x, translate.y, translate.z);
							glScalef(this.scale.x, this.scale.y, this.scale.z);
							glPushMatrix();
							glScalef(scale.x, scale.y, scale.z);
							renderer.renderPreview(shader);
							glPopMatrix();
							if (renderer.isModelsLoaded()) {
								renderDebug();
								KrogenitShaders.getCurrentPBRShader(false);
							}
							break;
					}
					break;
			}
		}
	}

	private void renderAABB() {
		Tessellator t = Tessellator.instance;
		t.startDrawing(GL_LINES);
		AxisAlignedBB aabb = renderer.getAABB(rotation, scale);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.draw();
		IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
		shader.setUseTexture(true);
		shader.setLightMapping(true);
	}

	private void renderDebug() {
		IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
		shader.setUseTexture(false);
		shader.setLightMapping(false);
		GL.color(1f, 0.25f, 0.25f, 1f);
		glLineWidth(2f);
		Tessellator t = Tessellator.instance;
		t.startDrawing(GL_LINES);
		AxisAlignedBB aabb = renderer.getAABB(vec_zero, vec_single);

		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.minY, aabb.minZ);
		t.addVertex(aabb.minX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.maxZ);
		t.addVertex(aabb.maxX, aabb.maxY, aabb.minZ);
		t.addVertex(aabb.maxX, aabb.minY, aabb.minZ);
		t.draw();
	}
}