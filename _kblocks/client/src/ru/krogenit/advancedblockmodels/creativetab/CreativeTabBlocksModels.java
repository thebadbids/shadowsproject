package ru.krogenit.advancedblockmodels.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class CreativeTabBlocksModels extends CreativeTabs {

	public CreativeTabBlocksModels() {
		super(CreativeTabs.getNextID(), "Блоки с моделями");
	}

	@Override
	public Item getTabIconItem() {
		return Items.netherbrick;
	}

}
