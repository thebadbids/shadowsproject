package ru.krogenit.advancedblockmodels.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class PacketTEModelRemoveClient implements IPacketIn, IPacketOut {

    private int x, y, z, id;

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeInt(x);
        data.writeInt(y);
        data.writeInt(z);
        data.writeInt(id);
    }

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        x = data.readInt();
        y = data.readInt();
        z = data.readInt();
        id = data.readInt();
        handleMessageOnClientSide();
    }

    public void handleMessageOnClientSide() {
        EntityPlayer p = Minecraft.getMinecraft().thePlayer;
        World w = p.worldObj;
        TileEntity tile = w.getTileEntity(x, y, z);
        if (tile instanceof TEBlockModel) {
            TEBlockModel te = (TEBlockModel) tile;
            if (id >= 0) {
                List<TEModel> parts = te.getParts();
                if (id < parts.size()) {
                    TEModel part = parts.remove(id);
                    Vector3f position = part.getPosition();

                    float newX = position.x + te.xCoord;
                    float newY = position.y + te.yCoord;
                    float newZ = position.z + te.zCoord;

                    int minx = MathHelper.floor_double(newX);
                    int miny = MathHelper.floor_double(newY);
                    int minz = MathHelper.floor_double(newZ);
                    int maxx = MathHelper.floor_double(newX) + 1;
                    int maxy = MathHelper.floor_double(newY) + 1;
                    int maxz = MathHelper.floor_double(newZ) + 1;

                    AxisAlignedBB axisAlignedBB = part.getAabb();
                    if(axisAlignedBB != null) {
                        minx = MathHelper.floor_double(axisAlignedBB.minX + newX);
                        miny = MathHelper.floor_double(axisAlignedBB.minY + newY);
                        minz = MathHelper.floor_double(axisAlignedBB.minZ + newZ);
                        maxx = MathHelper.floor_double(axisAlignedBB.maxX + newX) + 1;
                        maxy = MathHelper.floor_double(axisAlignedBB.maxY + newY) + 1;
                        maxz = MathHelper.floor_double(axisAlignedBB.maxZ + newZ) + 1;
                    }

                    for (int x1 = minx; x1 < maxx; x1++)
                        for (int y1 = miny; y1 < maxy; y1++)
                            for (int z1 = minz; z1 < maxz; z1++) {
                                if (x1 != te.xCoord || y1 != te.yCoord || z1 != te.zCoord) {
                                    TileEntity tile1 = w.getTileEntity(x1, y1, z1);
                                    if (tile1 instanceof TEBlockModel) {
                                        TEBlockModel te1 = (TEBlockModel) tile1;
                                        te1.getChildParts().remove(part);
                                    }
                                }
                            }
                }
            }
        }
    }
}
