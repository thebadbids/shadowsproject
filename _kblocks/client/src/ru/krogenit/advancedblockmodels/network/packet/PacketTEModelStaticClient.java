package ru.krogenit.advancedblockmodels.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.lwjgl.util.vector.Vector3f;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketTEModelStaticClient implements IPacketOut {

    private int x, y, z, side;
    private float hitX, hitY, hitZ;
    private Vector3f scale;

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeInt(x);
        data.writeInt(y);
        data.writeInt(z);
        data.writeInt(side);
        data.writeFloat(hitX);
        data.writeFloat(hitY);
        data.writeFloat(hitZ);
        data.writeFloat(scale.x);
        data.writeFloat(scale.y);
        data.writeFloat(scale.z);
    }
}
