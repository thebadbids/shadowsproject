package ru.krogenit.advancedblockmodels.key;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

public class KeyBindings {

	private final List<IKey> keys = new ArrayList<>();

	public KeyBindings init() {
		registerKey(new KeyModelPlaceType(new KeyBinding("key.modelplacetype", Keyboard.KEY_NUMPAD5, "key.categories.build").setOnlyForCreative(true)));
		return this;
	}

	private void registerKey(IKey key) {
		ClientRegistry.registerKeyBinding(key.getKeyBinding());
		keys.add(key);
	}

	@SubscribeEvent
	@SuppressWarnings("unused")
	public void event(InputEvent.KeyInputEvent e) {
		for (IKey key : keys) {
			KeyBinding bind = key.getKeyBinding();
			if (bind.isPressed()) {
				key.keyDown();
			} else if (!Keyboard.isKeyDown(key.getKeyBinding().getKeyCode())) {
				key.keyUp();
			}
		}
	}
}
