package ru.krogenit.advancedblockmodels;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel.VectorAABB;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;

import java.util.ArrayList;

public class ClientTickHandlerBlocks {
	private final Minecraft mc = Minecraft.getMinecraft();
	
	@SubscribeEvent
	public void event(TickEvent.ClientTickEvent event) {
		if(event.phase == Phase.END) {
			if(mc.theWorld != null) {
				checkTEModels();
			}
		}
	}
	
	private void checkTEModels() {
		synchronized (TEBlockModel.posToAdd) {
			if (TEBlockModel.posToAdd.size() > 0) {
				World w = mc.theWorld;
				for (int i = 0; i < TEBlockModel.posToAdd.size(); i++) {
					VectorAABB vect = TEBlockModel.posToAdd.get(i);
					if (TEBlockModel.placeTEHelp(w, vect.aabb, (int) vect.vect.x, (int) vect.vect.y, (int) vect.vect.z, i))
						vect.complete = true;
				}
				for (int i = 0; i < TEBlockModel.posToAdd.size(); i++) {
					VectorAABB vect = TEBlockModel.posToAdd.get(i);
					if (vect.complete) {
						TEBlockModel.posToAdd.remove(i);
						i--;
					}
				}
			}
		}
		synchronized (TEBlockModel.partsToLoad) {
			if (TEBlockModel.partsToLoad.size() > 0) {
				ArrayList<String> keys = new ArrayList<>(TEBlockModel.partsToLoad.keySet());
				for (String key : keys) {
					ArrayList<TEModel> list = TEBlockModel.partsToLoad.get(key);
					boolean modelLoaded = true;
					for (TEModel part : list) {
						if (!((TEModelClient) part).checkModelLoad()) {
							modelLoaded = false;
							break;
						}
					}
					if (modelLoaded) {
						TEBlockModel.partsToLoad.remove(key);
					}
				}
			}
		}
	}
}
