package noppes.npcs.containers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import noppes.npcs.NoppesUtilServer;
import noppes.npcs.controllers.Quest;
import noppes.npcs.quests.QuestItem;

public class ContainerNpcQuestTypeItem extends Container
{
    public ContainerNpcQuestTypeItem(EntityPlayer player)
    {
        Quest quest = NoppesUtilServer.getEditingQuest(player);

        int j1;

        int y = 39;
        int x = 44 - 36;
        for (j1 = 0; j1 < QuestItem.SIZE; ++j1)
        {
            this.addSlotToContainer(new Slot(((QuestItem)quest.questInterface).items, j1, x, y));
            y += 25;
            if(y > 39 + 25 * 2) {
                y = 39;
                x += 18;
            }
        }

        for (j1 = 0; j1 < 3; ++j1)
        {
            for (int l1 = 0; l1 < 9; ++l1)
            {
                this.addSlotToContainer(new Slot(player.inventory, l1 + j1 * 9 + 9, 8 + l1 * 18, 113 + j1 * 18));
            }
        }

        for (j1 = 0; j1 < 9; ++j1)
        {
            this.addSlotToContainer(new Slot(player.inventory, j1, 8 + j1 * 18, 171));
        }
    }

    /**
     * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
     */
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int i)
    {
        return null;
    }

    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return true;
    }
}
