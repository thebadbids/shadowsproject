package noppes.npcs.containers;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

class SlotNPCArmor extends Slot
{
    final int armorType;

    SlotNPCArmor(IInventory iinventory, int i, int j, int k, int l)
    {
        super(iinventory, i, j, k);
        this.armorType = l;
    }

    /**
     * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case
     * of armor slots)
     */
    public int getSlotStackLimit()
    {
        return 1;
    }

    /**
     * Returns the icon index on items.png that is used as background image of the slot.
     */
    public IIcon getBackgroundIconIndex()
    {
        return ItemArmor.func_94602_b(this.armorType);
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack itemstack)
    {
        return itemstack.getItem() instanceof ItemArmor ? ((ItemArmor)itemstack.getItem()).armorType == this.armorType : (itemstack.getItem() instanceof ItemBlock ? this.armorType == 0 : false);
    }
}
