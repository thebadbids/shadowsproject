package noppes.npcs.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.play.server.S27PacketExplosion;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.*;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import noppes.npcs.DataStats;
import noppes.npcs.constants.EnumParticleType;
import noppes.npcs.constants.EnumPotionType;
import noppes.npcs.util.IProjectileCallback;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class EntityProjectile extends EntityThrowable
{
    private int xTile = -1;
    private int yTile = -1;
    private int zTile = -1;
    private Block inTile;
    protected boolean inGround = false;
    private int inData = 0;
    public int throwableShake = 0;
    public int arrowShake = 0;
    public boolean canBePickedUp = false;
    public boolean destroyedOnEntityHit = true;
    private EntityLivingBase thrower;
    private EntityNPCInterface npc;
    public EntityItem entityitem;
    private String throwerName = null;
    private int ticksInGround;
    public int ticksInAir = 0;
    private double accelerationX;
    private double accelerationY;
    private double accelerationZ;
    public float damage = 5.0F;
    public int punch = 0;
    public boolean accelerate = false;
    public boolean explosive = false;
    public boolean explosiveDamage = true;
    public int explosiveRadius = 0;
    public EnumPotionType effect;
    public int duration;
    public int amplify;
    public IProjectileCallback callback;
    public ItemStack callbackItem;

    public EntityProjectile(World par1World)
    {
        super(par1World);
        this.effect = EnumPotionType.None;
        this.duration = 5;
        this.amplify = 0;
        this.setSize(0.25F, 0.25F);
    }

    protected void entityInit()
    {
        this.dataWatcher.addObjectByDataType(21, 5);
        this.dataWatcher.addObject(22, String.valueOf(""));
        this.dataWatcher.addObject(23, Integer.valueOf(5));
        this.dataWatcher.addObject(24, Byte.valueOf((byte)0));
        this.dataWatcher.addObject(25, Integer.valueOf(10));
        this.dataWatcher.addObject(26, Byte.valueOf((byte)0));
        this.dataWatcher.addObject(27, Byte.valueOf((byte)0));
        this.dataWatcher.addObject(28, Byte.valueOf((byte)0));
        this.dataWatcher.addObject(29, Byte.valueOf((byte)0));
        this.dataWatcher.addObject(30, Byte.valueOf((byte)0));
        this.dataWatcher.addObject(31, Byte.valueOf((byte)0));
    }

    @SideOnly(Side.CLIENT)

    /**
     * Checks if the entity is in range to render by using the past in distance and comparing it to its average edge
     * length * 64 * renderDistanceWeight Args: distance
     */
    public boolean isInRangeToRenderDist(double par1)
    {
        double d1 = this.boundingBox.getAverageEdgeLength() * 4.0D;
        d1 *= 64.0D;
        return par1 < d1 * d1;
    }

    public EntityProjectile(World par1World, EntityLivingBase par2EntityLiving, ItemStack item, boolean isNPC)
    {
        super(par1World);
        this.effect = EnumPotionType.None;
        this.duration = 5;
        this.amplify = 0;
        this.thrower = par2EntityLiving;

        if (this.thrower != null)
        {
            this.throwerName = this.thrower.getUniqueID().toString();
        }

        this.setThrownItem(item);
        this.dataWatcher.updateObject(27, Byte.valueOf((byte)(this.getItem() == Items.arrow ? 1 : 0)));
        this.setSize((float)(this.dataWatcher.getWatchableObjectInt(23) / 10), (float)(this.dataWatcher.getWatchableObjectInt(23) / 10));
        this.setLocationAndAngles(par2EntityLiving.posX, par2EntityLiving.posY + (double)par2EntityLiving.getEyeHeight(), par2EntityLiving.posZ, par2EntityLiving.rotationYaw, par2EntityLiving.rotationPitch);
        this.posX -= (double)(MathHelper.cos(this.rotationYaw / 180.0F * (float)Math.PI) * 0.1F);
        this.posY -= 0.10000000149011612D;
        this.posZ -= (double)(MathHelper.sin(this.rotationYaw / 180.0F * (float)Math.PI) * 0.1F);
        this.setPosition(this.posX, this.posY, this.posZ);
        this.yOffset = 0.0F;

        if (isNPC)
        {
            this.npc = (EntityNPCInterface)this.thrower;
            this.getStatProperties(this.npc.stats);
        }
    }

    public void setThrownItem(ItemStack item)
    {
        this.dataWatcher.updateObject(21, item);
    }

    /**
     * Similar to setArrowHeading, it's point the throwable entity to a x, y, z direction.
     */
    public void setThrowableHeading(double par1, double par3, double par5, float par7, float par8)
    {
        float f2 = MathHelper.sqrt_double(par1 * par1 + par3 * par3 + par5 * par5);
        float f3 = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
        float yaw = (float)(Math.atan2(par1, par5) * 180.0D / Math.PI);
        float pitch = this.hasGravity() ? par7 : (float)(Math.atan2(par3, (double)f3) * 180.0D / Math.PI);
        this.prevRotationYaw = this.rotationYaw = yaw;
        this.prevRotationPitch = this.rotationPitch = pitch;
        this.motionX = (double)(MathHelper.sin(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI));
        this.motionZ = (double)(MathHelper.cos(yaw / 180.0F * (float)Math.PI) * MathHelper.cos(pitch / 180.0F * (float)Math.PI));
        this.motionY = (double)MathHelper.sin((pitch + 1.0F) / 180.0F * (float)Math.PI);
        this.motionX += this.rand.nextGaussian() * 0.007499999832361937D * (double)par8;
        this.motionZ += this.rand.nextGaussian() * 0.007499999832361937D * (double)par8;
        this.motionY += this.rand.nextGaussian() * 0.007499999832361937D * (double)par8;
        this.motionX *= (double)this.getSpeed();
        this.motionZ *= (double)this.getSpeed();
        this.motionY *= (double)this.getSpeed();
        this.accelerationX = par1 / (double)f2 * 0.1D;
        this.accelerationY = par3 / (double)f2 * 0.1D;
        this.accelerationZ = par5 / (double)f2 * 0.1D;
        this.ticksInGround = 0;
    }

    public float getAngleForXYZ(double varX, double varY, double varZ, double horiDist, boolean arc)
    {
        float g = this.getGravityVelocity();
        float var1 = this.getSpeed() * this.getSpeed();
        double var2 = (double)g * horiDist;
        double var3 = (double)g * horiDist * horiDist + 2.0D * varY * (double)var1;
        double var4 = (double)(var1 * var1) - (double)g * var3;

        if (var4 < 0.0D)
        {
            return 30.0F;
        }
        else
        {
            float var6 = arc ? var1 + MathHelper.sqrt_double(var4) : var1 - MathHelper.sqrt_double(var4);
            float var7 = (float)(Math.atan2((double)var6, var2) * 180.0D / Math.PI);
            return var7;
        }
    }

    public void shoot(float speed)
    {
        double varX = (double)(-MathHelper.sin(this.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float)Math.PI));
        double varZ = (double)(MathHelper.cos(this.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float)Math.PI));
        double varY = (double)(-MathHelper.sin(this.rotationPitch / 180.0F * (float)Math.PI));
        this.setThrowableHeading(varX, varY, varZ, -this.rotationPitch, speed);
    }

    @SideOnly(Side.CLIENT)

    /**
     * Sets the position and rotation. Only difference from the other one is no bounding on the rotation. Args: posX,
     * posY, posZ, yaw, pitch
     */
    public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9)
    {
        if (!this.worldObj.isRemote || !this.inGround)
        {
            this.setPosition(par1, par3, par5);
            this.setRotation(par7, par8);
        }
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        super.onEntityUpdate();

        if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F)
        {
            float block = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
            this.prevRotationYaw = this.rotationYaw = (float)(Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);
            this.prevRotationPitch = this.rotationPitch = (float)(Math.atan2(this.motionY, (double)block) * 180.0D / Math.PI);

            if (this.isRotating())
            {
                this.rotationPitch -= 20.0F;
            }
        }

        if (this.effect == EnumPotionType.Fire && !this.inGround)
        {
            this.setFire(1);
        }

        Block var17 = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);

        if ((this.isArrow() || this.sticksToWalls()) && var17 != null)
        {
            var17.setBlockBoundsBasedOnState(this.worldObj, this.xTile, this.yTile, this.zTile);
            AxisAlignedBB vec3 = var17.getCollisionBoundingBoxFromPool(this.worldObj, this.xTile, this.yTile, this.zTile);

            if (vec3 != null && vec3.isVecInside(Vec3.createVectorHelper(this.posX, this.posY, this.posZ)))
            {
                this.inGround = true;
            }
        }

        if (this.arrowShake > 0)
        {
            --this.arrowShake;
        }

        if (this.inGround)
        {
            int var18 = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);

            if (var17 == this.inTile && var18 == this.inData)
            {
                ++this.ticksInGround;

                if (this.ticksInGround == 1200)
                {
                    this.setDead();
                }
            }
            else
            {
                this.inGround = false;
                this.motionX *= (double)(this.rand.nextFloat() * 0.2F);
                this.motionY *= (double)(this.rand.nextFloat() * 0.2F);
                this.motionZ *= (double)(this.rand.nextFloat() * 0.2F);
                this.ticksInGround = 0;
                this.ticksInAir = 0;
            }
        }
        else
        {
            ++this.ticksInAir;

            if (this.ticksInAir == 1200)
            {
                this.setDead();
            }

            Vec3 var19 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
            Vec3 vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
            MovingObjectPosition movingobjectposition = this.worldObj.func_147447_a(var19, vec31, false, true, false);
            var19 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
            vec31 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);

            if (movingobjectposition != null)
            {
                vec31 = Vec3.createVectorHelper(movingobjectposition.hitVec.xCoord, movingobjectposition.hitVec.yCoord, movingobjectposition.hitVec.zCoord);
            }

            if (!this.worldObj.isRemote)
            {
                Entity f1 = null;
                List f = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
                double f2 = 0.0D;
                EntityLivingBase k = this.getThrower();

                for (int f4 = 0; f4 < f.size(); ++f4)
                {
                    Entity entity1 = (Entity)f.get(f4);

                    if (entity1.canBeCollidedWith() && (!entity1.isEntityEqual(this.thrower) || this.ticksInAir >= 25))
                    {
                        float f3 = 0.3F;
                        AxisAlignedBB axisalignedbb = entity1.boundingBox.expand((double)f3, (double)f3, (double)f3);
                        MovingObjectPosition movingobjectposition1 = axisalignedbb.calculateIntercept(var19, vec31);

                        if (movingobjectposition1 != null)
                        {
                            double d1 = var19.distanceTo(movingobjectposition1.hitVec);

                            if (d1 < f2 || f2 == 0.0D)
                            {
                                f1 = entity1;
                                f2 = d1;
                            }
                        }
                    }
                }

                if (f1 != null)
                {
                    movingobjectposition = new MovingObjectPosition(f1);
                }

                if (this.npc != null && movingobjectposition != null && movingobjectposition.entityHit != null && movingobjectposition.entityHit instanceof EntityPlayer)
                {
                    EntityPlayer var25 = (EntityPlayer)movingobjectposition.entityHit;

                    if (this.npc.faction.isFriendlyToPlayer(var25))
                    {
                        movingobjectposition = null;
                    }
                }
            }

            if (movingobjectposition != null)
            {
                if (movingobjectposition.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && this.worldObj.getBlock(movingobjectposition.blockX, movingobjectposition.blockY, movingobjectposition.blockZ) == Blocks.portal)
                {
                    this.setInPortal();
                }
                else
                {
                    this.dataWatcher.updateObject(29, Byte.valueOf((byte)0));
                    this.onImpact(movingobjectposition);
                }
            }

            this.posX += this.motionX;
            this.posY += this.motionY;
            this.posZ += this.motionZ;
            float var20 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
            this.rotationYaw = (float)(Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);

            for (this.rotationPitch = (float)(Math.atan2(this.motionY, (double)var20) * 180.0D / Math.PI); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F)
            {
                ;
            }

            while (this.rotationPitch - this.prevRotationPitch >= 180.0F)
            {
                this.prevRotationPitch += 360.0F;
            }

            while (this.rotationYaw - this.prevRotationYaw < -180.0F)
            {
                this.prevRotationYaw -= 360.0F;
            }

            while (this.rotationYaw - this.prevRotationYaw >= 180.0F)
            {
                this.prevRotationYaw += 360.0F;
            }

            float var21 = this.isArrow() ? 0.0F : 225.0F;
            this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) + var21 * 0.2F;
            this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;

            if (this.isRotating())
            {
                int var22 = this.isBlock() ? 10 : 20;
                this.rotationPitch -= (float)(this.ticksInAir % 15 * var22) * this.getSpeed();
            }

            float var23 = this.getMotionFactor();
            float f3 = this.getGravityVelocity();

            if (this.isInWater())
            {
                if (this.worldObj.isRemote)
                {
                    for (int var24 = 0; var24 < 4; ++var24)
                    {
                        float var26 = 0.25F;
                        this.worldObj.spawnParticle("bubble", this.posX - this.motionX * (double)var26, this.posY - this.motionY * (double)var26, this.posZ - this.motionZ * (double)var26, this.motionX, this.motionY, this.motionZ);
                    }
                }

                var23 = 0.8F;
            }

            this.motionX *= (double)var23;
            this.motionY *= (double)var23;
            this.motionZ *= (double)var23;

            if (this.hasGravity())
            {
                this.motionY -= (double)f3;
            }

            if (this.accelerate)
            {
                this.motionX += this.accelerationX;
                this.motionY += this.accelerationY;
                this.motionZ += this.accelerationZ;
            }

            if (this.worldObj.isRemote && !this.dataWatcher.getWatchableObjectString(22).equals(""))
            {
                this.worldObj.spawnParticle(this.dataWatcher.getWatchableObjectString(22), this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
            }

            this.setPosition(this.posX, this.posY, this.posZ);
            this.func_145775_I();
        }
    }

    public boolean isBlock()
    {
        ItemStack item = this.getItemDisplay();
        return item == null ? false : item.getItem() instanceof ItemBlock;
    }

    private Item getItem()
    {
        ItemStack item = this.getItemDisplay();
        return item == null ? null : item.getItem();
    }

    protected float getMotionFactor()
    {
        return this.accelerate ? 0.95F : 1.0F;
    }

    /**
     * Called when this EntityThrowable hits a block or entity.
     */
    protected void onImpact(MovingObjectPosition movingobjectposition)
    {
        float axisalignedbb;
        int var12;
        int var15;

        if (movingobjectposition.entityHit != null)
        {
            if (this.callback != null && this.callbackItem != null && movingobjectposition.entityHit instanceof EntityLivingBase && this.callback.onImpact(this, (EntityLivingBase)movingobjectposition.entityHit, this.callbackItem))
            {
                return;
            }

            axisalignedbb = this.damage;

            if (axisalignedbb == 0.0F)
            {
                axisalignedbb = 0.001F;
            }

            if (movingobjectposition.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), axisalignedbb))
            {
                if (movingobjectposition.entityHit instanceof EntityLivingBase && (this.isArrow() || this.sticksToWalls()))
                {
                    EntityLivingBase list1 = (EntityLivingBase)movingobjectposition.entityHit;

                    if (!this.worldObj.isRemote)
                    {
                        list1.setArrowCountInEntity(list1.getArrowCountInEntity() + 1);
                    }

                    if (this.destroyedOnEntityHit && !(movingobjectposition.entityHit instanceof EntityEnderman))
                    {
                        this.setDead();
                    }
                }

                if (this.isBlock())
                {
                    this.worldObj.playAuxSFX(2001, (int)movingobjectposition.entityHit.posX, (int)movingobjectposition.entityHit.posY, (int)movingobjectposition.entityHit.posZ, Item.getIdFromItem(this.getItem()));
                }
                else if (!this.isArrow() && !this.sticksToWalls())
                {
                    for (var15 = 0; var15 < 8; ++var15)
                    {
                        this.worldObj.spawnParticle("iconcrack_" + Item.getIdFromItem(this.getItem()), this.posX, this.posY, this.posZ, this.rand.nextGaussian() * 0.15D, this.rand.nextGaussian() * 0.2D, this.rand.nextGaussian() * 0.15D);
                    }
                }

                if (this.punch > 0)
                {
                    float var16 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);

                    if (var16 > 0.0F)
                    {
                        movingobjectposition.entityHit.addVelocity(this.motionX * (double)this.punch * 0.6000000238418579D / (double)var16, 0.1D, this.motionZ * (double)this.punch * 0.6000000238418579D / (double)var16);
                    }
                }

                if (this.effect != EnumPotionType.None && movingobjectposition.entityHit instanceof EntityLivingBase)
                {
                    if (this.effect != EnumPotionType.Fire)
                    {
                        var15 = this.getPotionEffect(this.effect);
                        ((EntityLivingBase)movingobjectposition.entityHit).addPotionEffect(new PotionEffect(var15, this.duration * 20, this.amplify));
                    }
                    else
                    {
                        movingobjectposition.entityHit.setFire(this.duration);
                    }
                }
            }
            else if (this.hasGravity() && (this.isArrow() || this.sticksToWalls()))
            {
                this.motionX *= -0.10000000149011612D;
                this.motionY *= -0.10000000149011612D;
                this.motionZ *= -0.10000000149011612D;
                this.rotationYaw += 180.0F;
                this.prevRotationYaw += 180.0F;
                this.ticksInAir = 0;
            }
        }
        else if (!this.isArrow() && !this.sticksToWalls())
        {
            if (this.isBlock())
            {
                this.worldObj.playAuxSFX(2001, MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posY), MathHelper.floor_double(this.posZ), Item.getIdFromItem(this.getItem()));
            }
            else
            {
                for (var12 = 0; var12 < 8; ++var12)
                {
                    this.worldObj.spawnParticle("iconcrack_" + Item.getIdFromItem(this.getItem()), this.posX, this.posY, this.posZ, this.rand.nextGaussian() * 0.15D, this.rand.nextGaussian() * 0.2D, this.rand.nextGaussian() * 0.15D);
                }
            }
        }
        else
        {
            this.xTile = movingobjectposition.blockX;
            this.yTile = movingobjectposition.blockY;
            this.zTile = movingobjectposition.blockZ;
            this.inTile = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
            this.inData = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);
            this.motionX = (double)((float)(movingobjectposition.hitVec.xCoord - this.posX));
            this.motionY = (double)((float)(movingobjectposition.hitVec.yCoord - this.posY));
            this.motionZ = (double)((float)(movingobjectposition.hitVec.zCoord - this.posZ));
            axisalignedbb = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
            this.posX -= this.motionX / (double)axisalignedbb * 0.05000000074505806D;
            this.posY -= this.motionY / (double)axisalignedbb * 0.05000000074505806D;
            this.posZ -= this.motionZ / (double)axisalignedbb * 0.05000000074505806D;
            this.inGround = true;

            if (this.isArrow())
            {
                this.playSound("random.bowhit", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
            }
            else
            {
                this.playSound("random.break", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
            }

            this.arrowShake = 7;

            if (!this.hasGravity())
            {
                this.dataWatcher.updateObject(26, Byte.valueOf((byte)1));
            }

            if (this.inTile != null)
            {
                this.inTile.onEntityCollidedWithBlock(this.worldObj, this.xTile, this.yTile, this.zTile, this);
            }
        }

        if (this.explosive)
        {
            Iterator iterator;

            if (this.explosiveRadius == 0 && this.effect != EnumPotionType.None)
            {
                if (this.effect == EnumPotionType.Fire)
                {
                    var12 = movingobjectposition.blockX;
                    var15 = movingobjectposition.blockY;
                    int var19 = movingobjectposition.blockZ;

                    switch (movingobjectposition.sideHit)
                    {
                        case 0:
                            --var15;
                            break;

                        case 1:
                            ++var15;
                            break;

                        case 2:
                            --var19;
                            break;

                        case 3:
                            ++var19;
                            break;

                        case 4:
                            --var12;
                            break;

                        case 5:
                            ++var12;
                    }

                    if (this.worldObj.isAirBlock(var12, var15, var19))
                    {
                        this.worldObj.setBlock(var12, var15, var19, Blocks.fire);
                    }
                }
                else
                {
                    AxisAlignedBB var14 = this.boundingBox.expand(4.0D, 2.0D, 4.0D);
                    List var18 = this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, var14);

                    if (var18 != null && !var18.isEmpty())
                    {
                        iterator = var18.iterator();

                        while (iterator.hasNext())
                        {
                            EntityLivingBase var20 = (EntityLivingBase)iterator.next();
                            double d0 = this.getDistanceSqToEntity(var20);

                            if (d0 < 16.0D)
                            {
                                double d1 = 1.0D - Math.sqrt(d0) / 4.0D;

                                if (var20 == movingobjectposition.entityHit)
                                {
                                    d1 = 1.0D;
                                }

                                int i = this.getPotionEffect(this.effect);

                                if (Potion.potionTypes[i].isInstant())
                                {
                                    Potion.potionTypes[i].affectEntity(this.getThrower(), var20, this.amplify, d1);
                                }
                                else
                                {
                                    int j = (int)(d1 * (double)this.duration + 0.5D);

                                    if (j > 20)
                                    {
                                        var20.addPotionEffect(new PotionEffect(i, j, this.amplify));
                                    }
                                }
                            }
                        }
                    }

                    this.worldObj.playAuxSFX(2002, (int)Math.round(this.posX), (int)Math.round(this.posY), (int)Math.round(this.posZ), this.getPotionColor(this.effect));
                }
            }
            else
            {
                boolean var13 = this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing") && this.explosiveDamage;
                Explosion var17 = new Explosion(this.worldObj, this, this.posX, this.posY, this.posZ, (float)this.explosiveRadius);
                var17.isFlaming = this.effect == EnumPotionType.Fire;
                var17.isSmoking = var13;

                if (var13)
                {
                    var17.doExplosionA();
                }

                var17.doExplosionB(this.worldObj.isRemote);

                if (!this.worldObj.isRemote)
                {
                    iterator = this.worldObj.playerEntities.iterator();

                    while (iterator.hasNext())
                    {
                        EntityPlayer entitylivingbase = (EntityPlayer)iterator.next();

                        if (entitylivingbase.getDistanceSq(this.posX, this.posY, this.posZ) < 4096.0D)
                        {
                            ((EntityPlayerMP)entitylivingbase).playerNetServerHandler.sendPacket(new S27PacketExplosion(this.posX, this.posY, this.posZ, (float)this.explosiveRadius, var17.affectedBlockPositions, (Vec3)var17.func_77277_b().get(entitylivingbase)));
                        }
                    }
                }

                if (this.explosiveRadius != 0 && (this.isArrow() || this.sticksToWalls()))
                {
                    this.setDead();
                }
            }
        }

        if (!this.worldObj.isRemote && !this.isArrow() && !this.sticksToWalls())
        {
            this.setDead();
        }
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        par1NBTTagCompound.setShort("xTile", (short)this.xTile);
        par1NBTTagCompound.setShort("yTile", (short)this.yTile);
        par1NBTTagCompound.setShort("zTile", (short)this.zTile);
        par1NBTTagCompound.setByte("inTile", (byte)Block.getIdFromBlock(this.inTile));
        par1NBTTagCompound.setByte("inData", (byte)this.inData);
        par1NBTTagCompound.setByte("shake", (byte)this.throwableShake);
        par1NBTTagCompound.setByte("inGround", (byte)(this.inGround ? 1 : 0));
        par1NBTTagCompound.setByte("isArrow", (byte)(this.isArrow() ? 1 : 0));
        par1NBTTagCompound.setTag("direction", this.newDoubleNBTList(new double[] {this.motionX, this.motionY, this.motionZ}));
        par1NBTTagCompound.setBoolean("canBePickedUp", this.canBePickedUp);

        if ((this.throwerName == null || this.throwerName.length() == 0) && this.thrower != null && this.thrower instanceof EntityPlayer)
        {
            this.throwerName = this.thrower.getUniqueID().toString();
        }

        par1NBTTagCompound.setString("ownerName", this.throwerName == null ? "" : this.throwerName);

        if (this.getItemDisplay() != null)
        {
            par1NBTTagCompound.setTag("Item", this.getItemDisplay().writeToNBT(new NBTTagCompound()));
        }

        par1NBTTagCompound.setFloat("damagev2", this.damage);
        par1NBTTagCompound.setInteger("punch", this.punch);
        par1NBTTagCompound.setInteger("size", this.dataWatcher.getWatchableObjectInt(23));
        par1NBTTagCompound.setInteger("velocity", this.dataWatcher.getWatchableObjectInt(25));
        par1NBTTagCompound.setInteger("explosiveRadius", this.explosiveRadius);
        par1NBTTagCompound.setInteger("effectDuration", this.duration);
        par1NBTTagCompound.setBoolean("gravity", this.hasGravity());
        par1NBTTagCompound.setBoolean("accelerate", this.accelerate);
        par1NBTTagCompound.setByte("glows", this.dataWatcher.getWatchableObjectByte(24));
        par1NBTTagCompound.setBoolean("explosive", this.explosive);
        par1NBTTagCompound.setInteger("PotionEffect", this.effect.ordinal());
        par1NBTTagCompound.setString("trail", this.dataWatcher.getWatchableObjectString(22));
        par1NBTTagCompound.setByte("Render3D", this.dataWatcher.getWatchableObjectByte(28));
        par1NBTTagCompound.setByte("Spins", this.dataWatcher.getWatchableObjectByte(29));
        par1NBTTagCompound.setByte("Sticks", this.dataWatcher.getWatchableObjectByte(30));
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        this.xTile = par1NBTTagCompound.getShort("xTile");
        this.yTile = par1NBTTagCompound.getShort("yTile");
        this.zTile = par1NBTTagCompound.getShort("zTile");
        this.inTile = Block.getBlockById(par1NBTTagCompound.getByte("inTile") & 255);
        this.inData = par1NBTTagCompound.getByte("inData") & 255;
        this.throwableShake = par1NBTTagCompound.getByte("shake") & 255;
        this.inGround = par1NBTTagCompound.getByte("inGround") == 1;
        this.dataWatcher.updateObject(27, Byte.valueOf(par1NBTTagCompound.getByte("isArrow")));
        this.throwerName = par1NBTTagCompound.getString("ownerName");
        this.canBePickedUp = par1NBTTagCompound.getBoolean("canBePickedUp");
        this.damage = par1NBTTagCompound.getFloat("damagev2");
        this.punch = par1NBTTagCompound.getInteger("punch");
        this.explosiveRadius = par1NBTTagCompound.getInteger("explosiveRadius");
        this.duration = par1NBTTagCompound.getInteger("effectDuration");
        this.accelerate = par1NBTTagCompound.getBoolean("accelerate");
        this.explosive = par1NBTTagCompound.getBoolean("explosive");
        this.effect = EnumPotionType.values()[par1NBTTagCompound.getInteger("PotionEffect") % EnumPotionType.values().length];
        this.dataWatcher.updateObject(22, par1NBTTagCompound.getString("trail"));
        this.dataWatcher.updateObject(23, Integer.valueOf(par1NBTTagCompound.getInteger("size")));
        this.dataWatcher.updateObject(24, Byte.valueOf((byte)(par1NBTTagCompound.getBoolean("glows") ? 1 : 0)));
        this.dataWatcher.updateObject(25, Integer.valueOf(par1NBTTagCompound.getInteger("velocity")));
        this.dataWatcher.updateObject(26, Byte.valueOf((byte)(par1NBTTagCompound.getBoolean("gravity") ? 1 : 0)));
        this.dataWatcher.updateObject(28, Byte.valueOf((byte)(par1NBTTagCompound.getBoolean("Render3D") ? 1 : 0)));
        this.dataWatcher.updateObject(29, Byte.valueOf((byte)(par1NBTTagCompound.getBoolean("Spins") ? 1 : 0)));
        this.dataWatcher.updateObject(30, Byte.valueOf((byte)(par1NBTTagCompound.getBoolean("Sticks") ? 1 : 0)));

        if (this.throwerName != null && this.throwerName.length() == 0)
        {
            this.throwerName = null;
        }

        if (par1NBTTagCompound.hasKey("direction"))
        {
            NBTTagList var2 = par1NBTTagCompound.getTagList("direction", 6);
            this.motionX = var2.func_150309_d(0);
            this.motionY = var2.func_150309_d(1);
            this.motionZ = var2.func_150309_d(2);
        }

        NBTTagCompound var21 = par1NBTTagCompound.getCompoundTag("Item");
        ItemStack item = ItemStack.loadItemStackFromNBT(var21);

        if (item == null)
        {
            this.setDead();
        }
        else
        {
            this.dataWatcher.updateObject(21, item);
        }
    }

    public EntityLivingBase getThrower()
    {
        if (this.throwerName != null && !this.throwerName.isEmpty())
        {
            try
            {
                UUID uuid = UUID.fromString(this.throwerName);

                if (this.thrower == null && uuid != null)
                {
                    this.thrower = this.worldObj.func_152378_a(uuid);
                }
            }
            catch (IllegalArgumentException var2)
            {
                ;
            }

            return this.thrower;
        }
        else
        {
            return null;
        }
    }

    private int getPotionEffect(EnumPotionType p)
    {
        switch (p.ordinal())
        {
            case 1:
                return Potion.poison.id;

            case 2:
                return Potion.hunger.id;

            case 3:
                return Potion.weakness.id;

            case 4:
                return Potion.moveSlowdown.id;

            case 5:
                return Potion.confusion.id;

            case 6:
                return Potion.blindness.id;

            case 7:
                return Potion.wither.id;

            default:
                return 0;
        }
    }

    private int getPotionColor(EnumPotionType p)
    {
        switch (p.ordinal())
        {
            case 1:
                return 32660;

            case 2:
                return 32660;

            case 3:
                return 32696;

            case 4:
                return 32698;

            case 5:
                return 32732;

            case 6:
                return Potion.blindness.id;

            case 7:
                return 32732;

            default:
                return 0;
        }
    }

    public void getStatProperties(DataStats stats)
    {
        this.damage = (float)stats.pDamage;
        this.punch = stats.pImpact;
        this.accelerate = stats.pXlr8;
        this.explosive = stats.pExplode;
        this.explosiveRadius = stats.pArea;
        this.effect = stats.pEffect;
        this.duration = stats.pDur;
        this.amplify = stats.pEffAmp;
        this.setParticleEffect(stats.pTrail);
        this.dataWatcher.updateObject(23, Integer.valueOf(stats.pSize));
        this.dataWatcher.updateObject(24, Byte.valueOf((byte)(stats.pGlows ? 1 : 0)));
        this.setSpeed(stats.pSpeed);
        this.setHasGravity(stats.pPhysics);
        this.setIs3D(stats.pRender3D);
        this.setRotating(stats.pSpin);
        this.setStickInWall(stats.pStick);
    }

    public void setParticleEffect(EnumParticleType type)
    {
        this.dataWatcher.updateObject(22, type.particleName);
    }

    public void setHasGravity(boolean bo)
    {
        this.dataWatcher.updateObject(26, Byte.valueOf((byte)(bo ? 1 : 0)));
    }

    public void setIs3D(boolean bo)
    {
        this.dataWatcher.updateObject(28, Byte.valueOf((byte)(bo ? 1 : 0)));
    }

    public void setStickInWall(boolean bo)
    {
        this.dataWatcher.updateObject(30, Byte.valueOf((byte)(bo ? 1 : 0)));
    }

    public ItemStack getItemDisplay()
    {
        return this.dataWatcher.getWatchableObjectItemStack(21);
    }

    /**
     * Gets how bright this entity is.
     */
    public float getBrightness(float par1)
    {
        return this.dataWatcher.getWatchableObjectByte(24) == 1 ? 1.0F : super.getBrightness(par1);
    }

    @SideOnly(Side.CLIENT)
    public int getBrightnessForRender(float par1)
    {
        return this.dataWatcher.getWatchableObjectByte(24) == 1 ? 15728880 : super.getBrightnessForRender(par1);
    }

    public boolean hasGravity()
    {
        return this.dataWatcher.getWatchableObjectByte(26) == 1;
    }

    public void setSpeed(int speed)
    {
        this.dataWatcher.updateObject(25, Integer.valueOf(speed));
    }

    public float getSpeed()
    {
        return (float)this.dataWatcher.getWatchableObjectInt(25) / 10.0F;
    }

    public boolean isArrow()
    {
        return this.dataWatcher.getWatchableObjectByte(27) == 1;
    }

    public void setRotating(boolean bo)
    {
        this.dataWatcher.updateObject(29, Byte.valueOf((byte)(bo ? 1 : 0)));
    }

    public boolean isRotating()
    {
        return this.dataWatcher.getWatchableObjectByte(29) == 1;
    }

    public boolean glows()
    {
        return this.dataWatcher.getWatchableObjectByte(24) == 1;
    }

    public boolean is3D()
    {
        return this.dataWatcher.getWatchableObjectByte(28) == 1 || this.isBlock();
    }

    public boolean sticksToWalls()
    {
        return this.is3D() && this.dataWatcher.getWatchableObjectByte(30) == 1;
    }

    /**
     * Called by a player entity when they collide with an entity
     */
    public void onCollideWithPlayer(EntityPlayer par1EntityPlayer)
    {
        if (!this.worldObj.isRemote && this.canBePickedUp && this.inGround && this.arrowShake <= 0)
        {
            if (par1EntityPlayer.inventory.addItemStackToInventory(this.getItemDisplay()))
            {
                this.inGround = false;
                this.playSound("random.pop", 0.2F, ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
                par1EntityPlayer.onItemPickup(this, 1);
                this.setDead();
            }
        }
    }

    /**
     * returns if this entity triggers Block.onEntityWalking on the blocks they walk on. used for spiders and wolves to
     * prevent them from trampling crops
     */
    protected boolean canTriggerWalking()
    {
        return false;
    }

    public IChatComponent func_145748_c_()
    {
        return (IChatComponent)(this.getItemDisplay() != null ? new ChatComponentTranslation(this.getItemDisplay().getDisplayName(), new Object[0]) : super.func_145748_c_());
    }
}
