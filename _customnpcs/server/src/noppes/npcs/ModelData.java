package noppes.npcs;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;
import noppes.npcs.controllers.PixelmonHelper;
import noppes.npcs.entity.EntityNPCInterface;

public class ModelData extends ModelDataShared
{
    public EntityLivingBase getEntity(EntityNPCInterface npc)
    {
        if (this.entityClass == null)
        {
            return null;
        }
        else
        {
            if (this.entity == null)
            {
                try
                {
                    this.entity = (EntityLivingBase)this.entityClass.getConstructor(new Class[] {World.class}).newInstance(new Object[] {npc.worldObj});
                    this.entity.readEntityFromNBT(this.extra);

                    if (this.entity instanceof EntityLiving)
                    {
                        EntityLiving living = (EntityLiving)this.entity;
                        living.setCurrentItemOrArmor(0, npc.getHeldItem() != null ? npc.getHeldItem() : npc.getOffHand());
                        living.setCurrentItemOrArmor(1, npc.inventory.armorItemInSlot(3));
                        living.setCurrentItemOrArmor(2, npc.inventory.armorItemInSlot(2));
                        living.setCurrentItemOrArmor(3, npc.inventory.armorItemInSlot(1));
                        living.setCurrentItemOrArmor(4, npc.inventory.armorItemInSlot(0));
                    }

                    if (PixelmonHelper.isPixelmon(this.entity) && npc.worldObj.isRemote)
                    {
                        if (this.extra.hasKey("Name"))
                        {
                            PixelmonHelper.setName(this.entity, this.extra.getString("Name"));
                        }
                        else
                        {
                            PixelmonHelper.setName(this.entity, "Abra");
                        }
                    }
                }
                catch (Exception var3)
                {
                    ;
                }
            }

            return this.entity;
        }
    }

    public ModelData copy()
    {
        ModelData data = new ModelData();
        data.readFromNBT(this.writeToNBT());
        return data;
    }
}
