package noppes.npcs.scripted.roles;

import noppes.npcs.entity.EntityNPCInterface;

public class ScriptRoleMailman extends ScriptRoleInterface
{
    public ScriptRoleMailman(EntityNPCInterface npc)
    {
        super(npc);
    }

    public int getType()
    {
        return 1;
    }
}
