package noppes.npcs.util;

public class JsonException extends Exception
{
    public JsonException(String message, NBTJsonUtil.JsonFile json)
    {
        super(message + ": " + json.getCurrentPos());
    }
}
