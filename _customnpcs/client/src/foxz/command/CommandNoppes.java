package foxz.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;

import java.util.List;

public class CommandNoppes extends CommandBase
{
    public CmdNoppes noppes = new CmdNoppes(this);

    public String getCommandName()
    {
        return this.noppes.commandHelper.name;
    }

    public String getCommandUsage(ICommandSender var1)
    {
        return this.noppes.commandHelper.usage;
    }

    public void processCommand(ICommandSender var1, String[] var2)
    {
        this.noppes.processCommand(var1, var2);
    }

    /**
     * Adds the strings available in this command to the given list of tab completion options.
     */
    public List addTabCompletionOptions(ICommandSender par1, String[] par2)
    {
        return this.noppes.addTabCompletion(par1, par2);
    }

    /**
     * Return the required permission level for this command.
     */
    public int getRequiredPermissionLevel()
    {
        return 2;
    }
}
