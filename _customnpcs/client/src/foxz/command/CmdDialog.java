package foxz.command;

import foxz.commandhelper.ChMcLogger;
import foxz.commandhelper.annotations.Command;
import foxz.commandhelper.annotations.SubCommand;
import foxz.commandhelper.permissions.OpOnly;
import foxz.commandhelper.permissions.ParamCheck;
import net.minecraft.command.CommandBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.NoppesUtilServer;
import noppes.npcs.controllers.Dialog;
import noppes.npcs.controllers.DialogController;
import noppes.npcs.controllers.DialogOption;
import noppes.npcs.controllers.PlayerData;
import noppes.npcs.entity.EntityDialogNpc;
import noppes.npcs.util.EntityUtil;

import java.util.Iterator;
import java.util.List;

@Command(
    name = "dialog",
    desc = "dialog operations",
    usage = "help"
)
public class CmdDialog extends ChMcLogger
{
    public CmdDialog(Object sender)
    {
        super(sender);
    }

    @SubCommand(
        desc = "force read",
        usage = "<player> <dialog>",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public boolean read(String[] args)
    {
        String playername = args[0];
        int diagid;

        try
        {
            diagid = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException var7)
        {
            this.sendmessage("DialogID must be an integer");
            return false;
        }

        List data = this.getPlayersData(playername);

        if (data.isEmpty())
        {
            this.sendmessage(String.format("Unknow player \'%s\'", new Object[] {playername}));
            return false;
        }
        else
        {
            Iterator var5 = data.iterator();

            while (var5.hasNext())
            {
                PlayerData playerdata = (PlayerData)var5.next();
                playerdata.dialogData.dialogsRead.add(Integer.valueOf(diagid));
                playerdata.saveNBTData((NBTTagCompound)null);
            }

            return true;
        }
    }

    @SubCommand(
        desc = "force unread dialog",
        usage = "<player> <dialog>",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public boolean unread(String[] args)
    {
        String playername = args[0];
        int diagid;

        try
        {
            diagid = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException var7)
        {
            this.sendmessage("DialogID must be an integer");
            return false;
        }

        List data = this.getPlayersData(playername);

        if (data.isEmpty())
        {
            this.sendmessage(String.format("Unknow player \'%s\'", new Object[] {playername}));
            return false;
        }
        else
        {
            Iterator var5 = data.iterator();

            while (var5.hasNext())
            {
                PlayerData playerdata = (PlayerData)var5.next();
                playerdata.dialogData.dialogsRead.remove(Integer.valueOf(diagid));
                playerdata.saveNBTData((NBTTagCompound)null);
            }

            return true;
        }
    }

    @SubCommand(
        desc = "reload dialogs from disk",
        permissions = {OpOnly.class}
    )
    public boolean reload(String[] args)
    {
        new DialogController();
        return true;
    }

    @SubCommand(
        desc = "show dialog",
        usage = "<player> <dialog> <name>",
        permissions = {OpOnly.class}
    )
    public void show(String[] args)
    {
        EntityPlayerMP player = CommandBase.getPlayer(this.pcParam, args[0]);

        if (player == null)
        {
            this.sendmessage(String.format("Unknow player \'%s\'", new Object[] {args[0]}));
        }
        else
        {
            int diagid;

            try
            {
                diagid = Integer.parseInt(args[1]);
            }
            catch (NumberFormatException var7)
            {
                this.sendmessage("DialogID must be an integer: " + args[1]);
                return;
            }

            Dialog dialog = (Dialog)DialogController.instance.dialogs.get(Integer.valueOf(diagid));

            if (dialog == null)
            {
                this.sendmessage("Unknown dialog id: " + args[1]);
            }
            else
            {
                EntityDialogNpc npc = new EntityDialogNpc(this.pcParam.getEntityWorld());
                npc.display.name = args[2];
                EntityUtil.Copy(player, npc);
                DialogOption option = new DialogOption();
                option.dialogId = diagid;
                option.title = dialog.title;
                npc.dialogs.put(Integer.valueOf(0), option);
                NoppesUtilServer.openDialog(player, npc, dialog);
            }
        }
    }
}
