package noppes.npcs.config;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.lang.ref.WeakReference;
import java.text.Bidi;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.WeakHashMap;

public class StringCache
{
    private static final int BASELINE_OFFSET = 7;
    private static final int UNDERLINE_OFFSET = 1;
    private static final int UNDERLINE_THICKNESS = 2;
    private static final int STRIKETHROUGH_OFFSET = -6;
    private static final int STRIKETHROUGH_THICKNESS = 2;
    private GlyphCache glyphCache = new GlyphCache();
    private int[] colorTable = new int[32];
    private WeakHashMap<StringCache.Key, StringCache.Entry> stringCache = new WeakHashMap();
    private WeakHashMap<String, StringCache.Key> weakRefCache = new WeakHashMap();
    private StringCache.Key lookupKey = new StringCache.Key(null);
    private StringCache.Glyph[][] digitGlyphs = new StringCache.Glyph[4][];
    private boolean digitGlyphsReady = false;
    private boolean antiAliasEnabled = false;
    private Thread mainThread = Thread.currentThread();
    public int fontHeight;

    public StringCache()
    {
        for (int i = 0; i < 32; ++i)
        {
            int j = (i >> 3 & 1) * 85;
            int k = (i >> 2 & 1) * 170 + j;
            int l = (i >> 1 & 1) * 170 + j;
            int i1 = (i >> 0 & 1) * 170 + j;

            if (i == 6)
            {
                k += 85;
            }

            if (Minecraft.getMinecraft().gameSettings.anaglyph)
            {
                int j1 = (k * 30 + l * 59 + i1 * 11) / 100;
                int k1 = (k * 30 + l * 70) / 100;
                int l1 = (k * 30 + i1 * 70) / 100;
                k = j1;
                l = k1;
                i1 = l1;
            }

            if (i >= 16)
            {
                k /= 4;
                l /= 4;
                i1 /= 4;
            }

            this.colorTable[i] = (k & 255) << 16 | (l & 255) << 8 | i1 & 255;
        }

        this.cacheDightGlyphs();
    }

    public void setDefaultFont(String fontName, int fontSize, boolean antiAlias)
    {
        this.glyphCache.setDefaultFont(fontName, fontSize, antiAlias);
        this.antiAliasEnabled = antiAlias;
        this.weakRefCache.clear();
        this.stringCache.clear();
        this.cacheDightGlyphs();
        this.updateHeight();
    }

    public Font usedFont()
    {
        return (Font)this.glyphCache.usedFonts.get(0);
    }

    public void setCustomFont(ResourceLocation resource, int fontSize, boolean antiAlias) throws Exception
    {
        this.glyphCache.setCustomFont(resource, fontSize, antiAlias);
        this.antiAliasEnabled = antiAlias;
        this.weakRefCache.clear();
        this.stringCache.clear();
        this.cacheDightGlyphs();
        this.updateHeight();
    }

    public void updateHeight()
    {
        int height = 0;
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;
        StringCache.Glyph[] var4 = this.cacheString("AaBbCcDdEeHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz").glyphs;
        int var5 = var4.length;

        for (int var6 = 0; var6 < var5; ++var6)
        {
            StringCache.Glyph g = var4[var6];

            if (g.texture.height > height)
            {
                height = g.texture.height;
            }

            if (g.y < minY)
            {
                minY = g.y;
            }

            if (g.y > maxY)
            {
                maxY = g.y;
            }
        }

        this.fontHeight = (int)((float)(maxY + height) / 2.0F - (float)minY / 2.0F);
    }

    private void cacheDightGlyphs()
    {
        this.digitGlyphsReady = false;
        this.digitGlyphs[0] = this.cacheString("0123456789").glyphs;
        this.digitGlyphs[1] = this.cacheString("\u00a7l0123456789").glyphs;
        this.digitGlyphs[2] = this.cacheString("\u00a7o0123456789").glyphs;
        this.digitGlyphs[3] = this.cacheString("\u00a7l\u00a7o0123456789").glyphs;
        this.digitGlyphsReady = true;
    }

    public int renderString(String str, int startX, int startY, int initialColor, boolean shadowFlag)
    {
        if (str != null && !str.isEmpty())
        {
            StringCache.Entry entry = this.cacheString(str);
            startY += 7;

            if ((initialColor >> 24 & 255) == 0)
            {
                initialColor -= 16777216;
            }

            int color = initialColor;
            int boundTextureName = 0;
            GL11.glTexEnvi(GL11.GL_TEXTURE_ENV, GL11.GL_TEXTURE_ENV_MODE, GL11.GL_MODULATE);
            GL11.glColor4f((float)(initialColor >> 16 & 255) / 255.0F, (float)(initialColor >> 8 & 255) / 255.0F, (float)(initialColor & 255) / 255.0F, 1.0F);

            if (this.antiAliasEnabled)
            {
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            }

            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA(initialColor >> 16 & 255, initialColor >> 8 & 255, initialColor & 255, initialColor >> 24 & 255);
            byte fontStyle = 0;
            int renderStyle = 0;
            int glyphIndex;
            int glyphSpace;
            float y2;
            float var25;
            float var26;

            for (glyphIndex = 0; renderStyle < entry.glyphs.length; ++renderStyle)
            {
                while (glyphIndex < entry.colors.length && entry.glyphs[renderStyle].stringIndex >= entry.colors[glyphIndex].stringIndex)
                {
                    color = this.applyColorCode(entry.colors[glyphIndex].colorCode, initialColor, shadowFlag);
                    fontStyle = entry.colors[glyphIndex].fontStyle;
                    ++glyphIndex;
                }

                StringCache.Glyph colorIndex = entry.glyphs[renderStyle];
                GlyphCache.Entry glyph = colorIndex.texture;
                glyphSpace = colorIndex.x;
                char x1 = str.charAt(colorIndex.stringIndex);

                if (x1 >= 48 && x1 <= 57)
                {
                    int x2 = glyph.width;
                    glyph = this.digitGlyphs[fontStyle][x1 - 48].texture;
                    int y1 = glyph.width;
                    glyphSpace += x2 - y1 >> 1;
                }

                if (boundTextureName != glyph.textureName)
                {
                    tessellator.draw();
                    tessellator.startDrawingQuads();
                    tessellator.setColorRGBA(color >> 16 & 255, color >> 8 & 255, color & 255, color >> 24 & 255);
                    GL11.glBindTexture(GL11.GL_TEXTURE_2D, glyph.textureName);
                    boundTextureName = glyph.textureName;
                }

                var25 = (float)startX + (float)glyphSpace / 2.0F;
                var26 = (float)startX + (float)(glyphSpace + glyph.width) / 2.0F;
                y2 = (float)startY + (float)colorIndex.y / 2.0F;
                float y21 = (float)startY + (float)(colorIndex.y + glyph.height) / 2.0F;
                tessellator.addVertexWithUV((double)var25, (double)y2, 0.0D, (double)glyph.u1, (double)glyph.v1);
                tessellator.addVertexWithUV((double)var25, (double)y21, 0.0D, (double)glyph.u1, (double)glyph.v2);
                tessellator.addVertexWithUV((double)var26, (double)y21, 0.0D, (double)glyph.u2, (double)glyph.v2);
                tessellator.addVertexWithUV((double)var26, (double)y2, 0.0D, (double)glyph.u2, (double)glyph.v1);
            }

            tessellator.draw();

            if (entry.specialRender)
            {
                byte var21 = 0;
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                tessellator.startDrawingQuads();
                tessellator.setColorRGBA(initialColor >> 16 & 255, initialColor >> 8 & 255, initialColor & 255, initialColor >> 24 & 255);
                glyphIndex = 0;

                for (int var22 = 0; glyphIndex < entry.glyphs.length; ++glyphIndex)
                {
                    while (var22 < entry.colors.length && entry.glyphs[glyphIndex].stringIndex >= entry.colors[var22].stringIndex)
                    {
                        this.applyColorCode(entry.colors[var22].colorCode, initialColor, shadowFlag);
                        var21 = entry.colors[var22].renderStyle;
                        ++var22;
                    }

                    StringCache.Glyph var23 = entry.glyphs[glyphIndex];
                    glyphSpace = var23.advance - var23.texture.width;
                    float var24;

                    if ((var21 & 1) != 0)
                    {
                        var24 = (float)startX + (float)(var23.x - glyphSpace) / 2.0F;
                        var25 = (float)startX + (float)(var23.x + var23.advance) / 2.0F;
                        var26 = (float)startY + 0.5F;
                        y2 = (float)startY + 1.5F;
                        tessellator.addVertex((double)var24, (double)var26, 0.0D);
                        tessellator.addVertex((double)var24, (double)y2, 0.0D);
                        tessellator.addVertex((double)var25, (double)y2, 0.0D);
                        tessellator.addVertex((double)var25, (double)var26, 0.0D);
                    }

                    if ((var21 & 2) != 0)
                    {
                        var24 = (float)startX + (float)(var23.x - glyphSpace) / 2.0F;
                        var25 = (float)startX + (float)(var23.x + var23.advance) / 2.0F;
                        var26 = (float)startY + -3.0F;
                        y2 = (float)startY + -2.0F;
                        tessellator.addVertex((double)var24, (double)var26, 0.0D);
                        tessellator.addVertex((double)var24, (double)y2, 0.0D);
                        tessellator.addVertex((double)var25, (double)y2, 0.0D);
                        tessellator.addVertex((double)var25, (double)var26, 0.0D);
                    }
                }

                tessellator.draw();
                GL11.glEnable(GL11.GL_TEXTURE_2D);
            }

            return entry.advance / 2;
        }
        else
        {
            return 0;
        }
    }

    public int getStringWidth(String str)
    {
        if (str != null && !str.isEmpty())
        {
            StringCache.Entry entry = this.cacheString(str);
            return entry.advance / 2;
        }
        else
        {
            return 0;
        }
    }

    private int sizeString(String str, int width, boolean breakAtSpaces)
    {
        if (str != null && !str.isEmpty())
        {
            width += width;
            StringCache.Glyph[] glyphs = this.cacheString(str).glyphs;
            int wsIndex = -1;
            int advance = 0;
            int index;

            for (index = 0; index < glyphs.length && advance <= width; ++index)
            {
                if (breakAtSpaces)
                {
                    char c = str.charAt(glyphs[index].stringIndex);

                    if (c == 32)
                    {
                        wsIndex = index;
                    }
                    else if (c == 10)
                    {
                        wsIndex = index;
                        break;
                    }
                }

                advance += glyphs[index].advance;
            }

            if (index < glyphs.length && wsIndex != -1 && wsIndex < index)
            {
                index = wsIndex;
            }

            return index < glyphs.length ? glyphs[index].stringIndex : str.length();
        }
        else
        {
            return 0;
        }
    }

    public int sizeStringToWidth(String str, int width)
    {
        return this.sizeString(str, width, true);
    }

    public String trimStringToWidth(String str, int width, boolean reverse)
    {
        int length = this.sizeString(str, width, false);
        str = str.substring(0, length);

        if (reverse)
        {
            str = (new StringBuilder(str)).reverse().toString();
        }

        return str;
    }

    private int applyColorCode(int colorCode, int color, boolean shadowFlag)
    {
        if (colorCode != -1)
        {
            colorCode = shadowFlag ? colorCode + 16 : colorCode;
            color = this.colorTable[colorCode] & 16777215 | color & -16777216;
        }

        Tessellator.instance.setColorRGBA(color >> 16 & 255, color >> 8 & 255, color & 255, color >> 24 & 255);
        return color;
    }

    private StringCache.Entry cacheString(String str)
    {
        StringCache.Entry entry = null;

        if (this.mainThread == Thread.currentThread())
        {
            this.lookupKey.str = str;
            entry = (StringCache.Entry)this.stringCache.get(this.lookupKey);
        }

        if (entry == null)
        {
            char[] oldKey = str.toCharArray();
            entry = new StringCache.Entry(null);
            int length = this.stripColorCodes(entry, str, oldKey);
            ArrayList glyphList = new ArrayList();
            entry.advance = this.layoutBidiString(glyphList, oldKey, 0, length, entry.colors);
            entry.glyphs = new StringCache.Glyph[glyphList.size()];
            entry.glyphs = (StringCache.Glyph[])glyphList.toArray(entry.glyphs);
            Arrays.sort(entry.glyphs);
            int colorIndex = 0;
            int shift = 0;

            for (int glyphIndex = 0; glyphIndex < entry.glyphs.length; ++glyphIndex)
            {
                StringCache.Glyph glyph;

                for (glyph = entry.glyphs[glyphIndex]; colorIndex < entry.colors.length && glyph.stringIndex + shift >= entry.colors[colorIndex].stringIndex; ++colorIndex)
                {
                    shift += 2;
                }

                glyph.stringIndex += shift;
            }

            if (this.mainThread == Thread.currentThread())
            {
                StringCache.Key key = new StringCache.Key(null);
                key.str = new String(str);
                entry.keyRef = new WeakReference(key);
                this.stringCache.put(key, entry);
            }
        }

        if (this.mainThread == Thread.currentThread())
        {
            StringCache.Key var11 = (StringCache.Key)entry.keyRef.get();

            if (var11 != null)
            {
                this.weakRefCache.put(str, var11);
            }

            this.lookupKey.str = null;
        }

        return entry;
    }

    private int stripColorCodes(StringCache.Entry cacheEntry, String str, char[] text)
    {
        ArrayList colorList = new ArrayList();
        int start = 0;
        int shift = 0;
        byte fontStyle = 0;
        byte renderStyle = 0;
        int next;

        for (byte colorCode = -1; (next = str.indexOf(167, start)) != -1 && next + 1 < str.length(); shift += 2)
        {
            System.arraycopy(text, next - shift + 2, text, next - shift, text.length - next - 2);
            int code = "0123456789abcdefklmnor".indexOf(Character.toLowerCase(str.charAt(next + 1)));

            switch (code)
            {
                case 16:
                    break;

                case 17:
                    fontStyle = (byte)(fontStyle | 1);
                    break;

                case 18:
                    renderStyle = (byte)(renderStyle | 2);
                    cacheEntry.specialRender = true;
                    break;

                case 19:
                    renderStyle = (byte)(renderStyle | 1);
                    cacheEntry.specialRender = true;
                    break;

                case 20:
                    fontStyle = (byte)(fontStyle | 2);
                    break;

                case 21:
                    fontStyle = 0;
                    renderStyle = 0;
                    colorCode = -1;
                    break;

                default:
                    if (code >= 0 && code <= 15)
                    {
                        colorCode = (byte)code;
                        fontStyle = 0;
                        renderStyle = 0;
                    }
            }

            StringCache.ColorCode entry = new StringCache.ColorCode();
            entry.stringIndex = next;
            entry.stripIndex = next - shift;
            entry.colorCode = colorCode;
            entry.fontStyle = fontStyle;
            entry.renderStyle = renderStyle;
            colorList.add(entry);
            start = next + 2;
        }

        cacheEntry.colors = new StringCache.ColorCode[colorList.size()];
        cacheEntry.colors = (StringCache.ColorCode[])colorList.toArray(cacheEntry.colors);
        return text.length - shift;
    }

    private int layoutBidiString(List<StringCache.Glyph> glyphList, char[] text, int start, int limit, StringCache.ColorCode[] colors)
    {
        int advance = 0;

        if (!Bidi.requiresBidi(text, start, limit))
        {
            return this.layoutStyle(glyphList, text, start, limit, 0, advance, colors);
        }
        else
        {
            Bidi bidi = new Bidi(text, start, (byte[])null, 0, limit - start, -2);

            if (bidi.isRightToLeft())
            {
                return this.layoutStyle(glyphList, text, start, limit, 1, advance, colors);
            }
            else
            {
                int runCount = bidi.getRunCount();
                byte[] levels = new byte[runCount];
                Integer[] ranges = new Integer[runCount];
                int visualIndex;

                for (visualIndex = 0; visualIndex < runCount; ++visualIndex)
                {
                    levels[visualIndex] = (byte)bidi.getRunLevel(visualIndex);
                    ranges[visualIndex] = new Integer(visualIndex);
                }

                Bidi.reorderVisually(levels, 0, ranges, 0, runCount);

                for (visualIndex = 0; visualIndex < runCount; ++visualIndex)
                {
                    int logicalIndex = ranges[visualIndex].intValue();
                    int layoutFlag = (bidi.getRunLevel(logicalIndex) & 1) == 1 ? 1 : 0;
                    advance = this.layoutStyle(glyphList, text, start + bidi.getRunStart(logicalIndex), start + bidi.getRunLimit(logicalIndex), layoutFlag, advance, colors);
                }

                return advance;
            }
        }
    }

    private int layoutStyle(List<StringCache.Glyph> glyphList, char[] text, int start, int limit, int layoutFlags, int advance, StringCache.ColorCode[] colors)
    {
        byte currentFontStyle = 0;
        int colorIndex = Arrays.binarySearch(colors, Integer.valueOf(start));

        if (colorIndex < 0)
        {
            colorIndex = -colorIndex - 2;
        }

        while (start < limit)
        {
            int next;

            for (next = limit; colorIndex >= 0 && colorIndex < colors.length - 1 && colors[colorIndex].stripIndex == colors[colorIndex + 1].stripIndex; ++colorIndex)
            {
                ;
            }

            if (colorIndex >= 0 && colorIndex < colors.length)
            {
                currentFontStyle = colors[colorIndex].fontStyle;
            }

            while (true)
            {
                ++colorIndex;

                if (colorIndex >= colors.length)
                {
                    break;
                }

                if (colors[colorIndex].fontStyle != currentFontStyle)
                {
                    next = colors[colorIndex].stripIndex;
                    break;
                }
            }

            advance = this.layoutString(glyphList, text, start, next, layoutFlags, advance, currentFontStyle);
            start = next;
        }

        return advance;
    }

    private int layoutString(List<StringCache.Glyph> glyphList, char[] text, int start, int limit, int layoutFlags, int advance, int style)
    {
        if (this.digitGlyphsReady)
        {
            for (int font = start; font < limit; ++font)
            {
                if (text[font] >= 48 && text[font] <= 57)
                {
                    text[font] = 48;
                }
            }
        }

        while (start < limit)
        {
            Font var10 = this.glyphCache.lookupFont(text, start, limit, style);
            int next = var10.canDisplayUpTo(text, start, limit);

            if (next == -1)
            {
                next = limit;
            }

            if (next == start)
            {
                ++next;
            }

            advance = this.layoutFont(glyphList, text, start, next, layoutFlags, advance, var10);
            start = next;
        }

        return advance;
    }

    private int layoutFont(List<StringCache.Glyph> glyphList, char[] text, int start, int limit, int layoutFlags, int advance, Font font)
    {
        if (this.mainThread == Thread.currentThread())
        {
            this.glyphCache.cacheGlyphs(font, text, start, limit, layoutFlags);
        }

        GlyphVector vector = this.glyphCache.layoutGlyphVector(font, text, start, limit, layoutFlags);
        StringCache.Glyph glyph = null;
        int numGlyphs = vector.getNumGlyphs();

        for (int index = 0; index < numGlyphs; ++index)
        {
            Point position = vector.getGlyphPixelBounds(index, (FontRenderContext)null, (float)advance, 0.0F).getLocation();

            if (glyph != null)
            {
                glyph.advance = position.x - glyph.x;
            }

            glyph = new StringCache.Glyph();
            glyph.stringIndex = start + vector.getGlyphCharIndex(index);
            glyph.texture = this.glyphCache.lookupGlyph(font, vector.getGlyphCode(index));
            glyph.x = position.x;
            glyph.y = position.y;
            glyphList.add(glyph);
        }

        advance += (int)vector.getGlyphPosition(numGlyphs).getX();

        if (glyph != null)
        {
            glyph.advance = advance - glyph.x;
        }

        return advance;
    }

    private static class ColorCode implements Comparable<Integer>
    {
        public static final byte UNDERLINE = 1;
        public static final byte STRIKETHROUGH = 2;
        public int stringIndex;
        public int stripIndex;
        public byte colorCode;
        public byte fontStyle;
        public byte renderStyle;

        private ColorCode() {}

        public int compareTo(Integer i)
        {
            return this.stringIndex == i.intValue() ? 0 : (this.stringIndex < i.intValue() ? -1 : 1);
        }
    }

    private static class Entry
    {
        public WeakReference<StringCache.Key> keyRef;
        public int advance;
        public StringCache.Glyph[] glyphs;
        public StringCache.ColorCode[] colors;
        public boolean specialRender;

        private Entry() {}

        Entry(Object x0)
        {
            this();
        }
    }

    private static class Glyph implements Comparable<StringCache.Glyph>
    {
        public int stringIndex;
        public GlyphCache.Entry texture;
        public int x;
        public int y;
        public int advance;

        private Glyph() {}

        public int compareTo(StringCache.Glyph o)
        {
            return this.stringIndex == o.stringIndex ? 0 : (this.stringIndex < o.stringIndex ? -1 : 1);
        }
    }

    private static class Key
    {
        public String str;

        private Key() {}

        public int hashCode()
        {
            int code = 0;
            int length = this.str.length();
            boolean colorCode = false;

            for (int index = 0; index < length; ++index)
            {
                char c = this.str.charAt(index);

                if (c >= 48 && c <= 57 && !colorCode)
                {
                    c = 48;
                }

                code = code * 31 + c;
                colorCode = c == 167;
            }

            return code;
        }

        public boolean equals(Object o)
        {
            if (o == null)
            {
                return false;
            }
            else
            {
                String other = o.toString();
                int length = this.str.length();

                if (length != other.length())
                {
                    return false;
                }
                else
                {
                    boolean colorCode = false;

                    for (int index = 0; index < length; ++index)
                    {
                        char c1 = this.str.charAt(index);
                        char c2 = other.charAt(index);

                        if (c1 != c2 && (c1 < 48 || c1 > 57 || c2 < 48 || c2 > 57 || colorCode))
                        {
                            return false;
                        }

                        colorCode = c1 == 167;
                    }

                    return true;
                }
            }
        }

        public String toString()
        {
            return this.str;
        }

        Key(Object x0)
        {
            this();
        }
    }
}
