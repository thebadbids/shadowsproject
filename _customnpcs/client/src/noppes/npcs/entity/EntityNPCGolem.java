package noppes.npcs.entity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.constants.EnumAnimation;

public class EntityNPCGolem extends EntityNPCInterface
{
    public EntityNPCGolem(World world)
    {
        super(world);
        this.display.texture = "customnpcs:textures/entity/golem/Iron Golem.png";
        this.width = 1.4F;
        this.height = 2.5F;
    }

    public void updateHitbox()
    {
        this.currentAnimation = EnumAnimation.values()[this.dataWatcher.getWatchableObjectInt(14)];

        if (this.currentAnimation == EnumAnimation.LYING)
        {
            this.width = this.height = 0.5F;
        }
        else if (this.currentAnimation == EnumAnimation.SITTING)
        {
            this.width = 1.4F;
            this.height = 2.0F;
        }
        else
        {
            this.width = 1.4F;
            this.height = 2.5F;
        }
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.setEntityClass(EntityNPCGolem.class);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
