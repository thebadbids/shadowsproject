package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNPCDwarfMale extends EntityNPCInterface
{
    public EntityNPCDwarfMale(World world)
    {
        super(world);
        this.scaleX = this.scaleZ = 0.85F;
        this.scaleY = 0.6875F;
        this.display.texture = "customnpcs:textures/entity/dwarfmale/Simon.png";
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.legs.setScale(1.1F, 0.7F, 0.9F);
            data.arms.setScale(0.9F, 0.7F);
            data.body.setScale(1.2F, 0.7F, 1.5F);
            data.head.setScale(0.85F, 0.85F);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
