package noppes.npcs.client.gui.player;

import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.ResourceLocation;
import noppes.npcs.NoppesStringUtils;
import noppes.npcs.NoppesUtilPlayer;
import noppes.npcs.client.ClientProxy;
import noppes.npcs.client.NoppesUtil;
import noppes.npcs.client.TextBlockClient;
import noppes.npcs.client.controllers.MusicController;
import noppes.npcs.client.gui.util.GuiNPCInterface;
import noppes.npcs.client.gui.util.IGuiClose;
import noppes.npcs.constants.EnumOptionType;
import noppes.npcs.constants.EnumPlayerPacket;
import noppes.npcs.controllers.Dialog;
import noppes.npcs.controllers.DialogOption;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GuiDialogInteract extends GuiNPCInterface implements IGuiClose
{
    @Getter
    protected Dialog dialog;
    protected int selected = 0;
    protected List<TextBlockClient> lines = new ArrayList();
    protected List<Integer> options = new ArrayList();
    private int rowStart = 0;
    private int rowTotal = 0;
    protected int dialogHeight = 180;
    private ResourceLocation wheel;
    private ResourceLocation[] wheelparts;
    private ResourceLocation indicator;
    protected boolean isGrabbed = false;
    private int selectedX = 0;
    private int selectedY = 0;

    public GuiDialogInteract(EntityNPCInterface npc, Dialog dialog)
    {
        super(npc);
        this.dialog = dialog;
        this.appendDialog(dialog);
        this.ySize = 238;
        this.wheel = this.getResource("wheel.png");
        this.indicator = this.getResource("indicator.png");
        this.wheelparts = new ResourceLocation[] {this.getResource("wheel1.png"), this.getResource("wheel2.png"), this.getResource("wheel3.png"), this.getResource("wheel4.png"), this.getResource("wheel5.png"), this.getResource("wheel6.png")};
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.isGrabbed = false;
        this.grabMouse(this.dialog.showWheel);
        this.guiTop = this.height - this.ySize;
        this.calculateRowHeight();
    }

    public void grabMouse(boolean grab)
    {
        if (grab && !this.isGrabbed)
        {
            Minecraft.getMinecraft().mouseHelper.grabMouseCursor();
            this.isGrabbed = true;
        }
        else if (!grab && this.isGrabbed)
        {
            Minecraft.getMinecraft().mouseHelper.ungrabMouseCursor();
            this.isGrabbed = false;
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        if (!this.dialog.hideNPC)
        {
            float count = (float)(this.guiLeft - 70);
            float i1 = (float)(this.guiTop + this.ySize);
            GL11.glEnable(GL11.GL_COLOR_MATERIAL);
            GL11.glPushMatrix();
            GL11.glTranslatef(count, i1, 50.0F);
            float block = this.npc.height;

            if (this.npc.width * 2.0F > block)
            {
                block = this.npc.width * 2.0F;
            }

            block = 2.0F / block * 40.0F;
            GL11.glScalef(-block, block, block);
            GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
            float size = this.npc.renderYawOffset;
            float f3 = this.npc.rotationYaw;
            float line = this.npc.rotationPitch;
            float f7 = this.npc.rotationYawHead;
            float f5 = count - (float) mouseX;
            float f6 = i1 - 50.0F - (float) mouseY;
            int rotation = this.npc.ai.orientation;
            this.npc.ai.orientation = 0;
            GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
            RenderHelper.enableStandardItemLighting();
            GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(-((float)Math.atan((double)(f6 / 80.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
            this.npc.renderYawOffset = 0.0F;
            this.npc.rotationYaw = (float)Math.atan((double)(f5 / 80.0F)) * 40.0F;
            this.npc.rotationPitch = -((float)Math.atan((double)(f6 / 80.0F))) * 20.0F;
            this.npc.prevRotationYawHead = this.npc.rotationYawHead = this.npc.rotationYaw;
            GL11.glTranslatef(0.0F, this.npc.yOffset, 0.0F);
            RenderManager.instance.playerViewY = 180.0F;

            try
            {
                RenderManager.instance.renderEntityWithPosYaw(this.npc, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
            }
            catch (Exception var15)
            {
                ;
            }

            this.npc.ai.orientation = rotation;
            this.npc.renderYawOffset = size;
            this.npc.rotationYaw = f3;
            this.npc.rotationPitch = line;
            this.npc.prevRotationYawHead = this.npc.rotationYawHead = f7;
            GL11.glPopMatrix();
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        }

        super.drawScreen(mouseX, mouseY, partialTick);
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 0.5F, 100.065F);
        int var16 = 0;

        for (Iterator var17 = this.lines.iterator(); var17.hasNext(); ++var16)
        {
            TextBlockClient var18 = (TextBlockClient)var17.next();
            int var19 = ClientProxy.Font.width(var18.getName() + ": ");
            this.drawString(var18.getName() + ": ", -4 - var19, var18.color, var16);

            for (Iterator var20 = var18.lines.iterator(); var20.hasNext(); ++var16)
            {
                IChatComponent var21 = (IChatComponent)var20.next();
                this.drawString(var21.getFormattedText(), 0, var18.color, var16);
            }
        }

        if (!this.options.isEmpty())
        {
            if (!this.dialog.showWheel)
            {
                this.drawLinedOptions(mouseY);
            }
            else
            {
                this.drawWheel();
            }
        }

        GL11.glPopMatrix();
    }

    private void drawWheel()
    {
        int yoffset = this.guiTop + this.dialogHeight + 14;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.wheel);
        this.drawTexturedModalRect(this.width / 2 - 31, yoffset, 0, 0, 63, 40);
        this.selectedX += Mouse.getDX();
        this.selectedY += Mouse.getDY();
        byte limit = 80;

        if (this.selectedX > limit)
        {
            this.selectedX = limit;
        }

        if (this.selectedX < -limit)
        {
            this.selectedX = -limit;
        }

        if (this.selectedY > limit)
        {
            this.selectedY = limit;
        }

        if (this.selectedY < -limit)
        {
            this.selectedY = -limit;
        }

        this.selected = 1;

        if (this.selectedY < -20)
        {
            ++this.selected;
        }

        if (this.selectedY > 54)
        {
            --this.selected;
        }

        if (this.selectedX < 0)
        {
            this.selected += 3;
        }

        this.mc.renderEngine.bindTexture(this.wheelparts[this.selected]);
        this.drawTexturedModalRect(this.width / 2 - 31, yoffset, 0, 0, 85, 55);
        Iterator var3 = this.dialog.options.keySet().iterator();

        while (var3.hasNext())
        {
            int slot = ((Integer)var3.next()).intValue();
            DialogOption option = (DialogOption)this.dialog.options.get(Integer.valueOf(slot));

            if (option != null && option.optionType != EnumOptionType.Disabled)
            {
                int color = option.optionColor;

                if (slot == this.selected)
                {
                    color = 8622040;
                }

                if (slot == 0)
                {
                    this.drawString(this.fontRendererObj, option.title, this.width / 2 + 13, yoffset - 6, color);
                }

                if (slot == 1)
                {
                    this.drawString(this.fontRendererObj, option.title, this.width / 2 + 33, yoffset + 12, color);
                }

                if (slot == 2)
                {
                    this.drawString(this.fontRendererObj, option.title, this.width / 2 + 27, yoffset + 32, color);
                }

                if (slot == 3)
                {
                    this.drawString(this.fontRendererObj, option.title, this.width / 2 - 13 - ClientProxy.Font.width(option.title), yoffset - 6, color);
                }

                if (slot == 4)
                {
                    this.drawString(this.fontRendererObj, option.title, this.width / 2 - 33 - ClientProxy.Font.width(option.title), yoffset + 12, color);
                }

                if (slot == 5)
                {
                    this.drawString(this.fontRendererObj, option.title, this.width / 2 - 27 - ClientProxy.Font.width(option.title), yoffset + 32, color);
                }
            }
        }

        this.mc.renderEngine.bindTexture(this.indicator);
        this.drawTexturedModalRect(this.width / 2 + this.selectedX / 4 - 2, yoffset + 16 - this.selectedY / 6, 0, 0, 8, 8);
    }

    private void drawLinedOptions(int j)
    {
        this.drawHorizontalLine(this.guiLeft - 60, this.guiLeft + this.xSize + 120, this.guiTop + this.dialogHeight, -1);
        int offset = this.dialogHeight + 4;
        int count;

        if (j >= this.guiTop + offset)
        {
            count = (j - (this.guiTop + offset)) / ClientProxy.Font.height();

            if (count < this.options.size())
            {
                this.selected = count;
            }
        }

        if (this.selected >= this.options.size())
        {
            this.selected = 0;
        }

        if (this.selected < 0)
        {
            this.selected = 0;
        }

        count = 0;

        for (int k = 0; k < this.options.size(); ++k)
        {
            int id = ((Integer)this.options.get(k)).intValue();
            DialogOption option = (DialogOption)this.dialog.options.get(Integer.valueOf(id));
            int y = this.guiTop + offset + count * ClientProxy.Font.height();

            if (this.selected == k)
            {
                this.drawString(this.fontRendererObj, ">", this.guiLeft - 60, y, 14737632);
            }

            this.drawString(this.fontRendererObj, NoppesStringUtils.formatText(option.title, new Object[] {this.player, this.npc}), this.guiLeft - 30, y, option.optionColor);
            ++count;
        }
    }

    private void drawString(String text, int left, int color, int count)
    {
        int height = count - this.rowStart;
        this.drawString(this.fontRendererObj, text, this.guiLeft + left, this.guiTop + height * ClientProxy.Font.height(), color);
    }

    /**
     * Renders the specified text to the screen.
     */
    public void drawString(FontRenderer fontRendererIn, String text, int x, int y, int color)
    {
        ClientProxy.Font.drawString(text, x, y, color);
    }

    private int getSelected()
    {
        return this.selected <= 0 ? 0 : (this.selected < this.options.size() ? this.selected : this.options.size() - 1);
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (key == this.mc.gameSettings.keyBindForward.getKeyCode() || key == 200)
        {
            --this.selected;
        }

        if (key == this.mc.gameSettings.keyBindBack.getKeyCode() || key == 208)
        {
            ++this.selected;
        }

        if (key == 28)
        {
            this.handleDialogSelection();
        }

        if (this.closeOnEsc && (key == 1 || this.isInventoryKey(key)))
        {
            NoppesUtilPlayer.sendData(EnumPlayerPacket.Dialog, new Object[] {Integer.valueOf(this.dialog.id), Integer.valueOf(-1)});
            this.closed();
            this.close();
        }

        super.keyTyped(character, key);
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        if (this.selected == -1 && this.options.isEmpty() || this.selected >= 0)
        {
            this.handleDialogSelection();
        }
    }

    private void handleDialogSelection()
    {
        int optionId = -1;

        if (this.dialog.showWheel)
        {
            optionId = this.selected;
        }
        else if (!this.options.isEmpty())
        {
            optionId = ((Integer)this.options.get(this.selected)).intValue();
        }

        NoppesUtilPlayer.sendData(EnumPlayerPacket.Dialog, new Object[] {Integer.valueOf(this.dialog.id), Integer.valueOf(optionId)});

        if (this.dialog != null && this.dialog.hasOtherOptions() && !this.options.isEmpty())
        {
            DialogOption option = (DialogOption)this.dialog.options.get(Integer.valueOf(optionId));

            if (option != null && option.optionType == EnumOptionType.DialogOption)
            {
                this.lines.add(new TextBlockClient(this.player.getDisplayName(), option.title, 280, option.optionColor, new Object[] {this.player, this.npc}));
                this.calculateRowHeight();
                NoppesUtil.clickSound();
            }
            else
            {
                this.closed();
                this.close();
            }
        }
        else
        {
            this.closed();
            this.close();
        }
    }

    private void closed()
    {
        this.grabMouse(false);
        NoppesUtilPlayer.sendData(EnumPlayerPacket.CheckQuestCompletionWithOpenDialog, npc.getEntityId());
    }

    public void save() {}

    public void appendDialog(Dialog dialog)
    {
        this.closeOnEsc = !dialog.disableEsc;
        this.dialog = dialog;
        this.options = new ArrayList();

        if (dialog.sound != null && !dialog.sound.isEmpty())
        {
            MusicController.Instance.stopMusic();
            MusicController.Instance.playSound(dialog.sound, (float)this.npc.posX, (float)this.npc.posY, (float)this.npc.posZ);
        }

        this.lines.add(new TextBlockClient(this.npc, dialog.text, 280, 14737632, new Object[] {this.player, this.npc}));
        Iterator var2 = dialog.options.keySet().iterator();

        while (var2.hasNext())
        {
            int slot = ((Integer)var2.next()).intValue();
            DialogOption option = (DialogOption)dialog.options.get(Integer.valueOf(slot));

            if (option != null && option.optionType != EnumOptionType.Disabled)
            {
                this.options.add(Integer.valueOf(slot));
            }
        }

        this.calculateRowHeight();
        this.grabMouse(dialog.showWheel);
    }

    private void calculateRowHeight()
    {
        if (this.dialog.showWheel)
        {
            this.dialogHeight = this.ySize - 58;
        }
        else
        {
            this.dialogHeight = this.ySize - 3 * ClientProxy.Font.height() - 4;

            if (this.dialog.options.size() > 3)
            {
                this.dialogHeight -= (this.dialog.options.size() - 3) * ClientProxy.Font.height();
            }
        }

        this.rowTotal = 0;
        TextBlockClient block;

        for (Iterator max = this.lines.iterator(); max.hasNext(); this.rowTotal += block.lines.size() + 1)
        {
            block = (TextBlockClient)max.next();
        }

        int max1 = this.dialogHeight / ClientProxy.Font.height();
        this.rowStart = this.rowTotal - max1;

        if (this.rowStart < 0)
        {
            this.rowStart = 0;
        }
    }

    public void setClose(int i, NBTTagCompound data)
    {
        this.grabMouse(false);
    }
}
