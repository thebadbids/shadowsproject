package noppes.npcs.client.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import noppes.npcs.containers.ContainerEmpty;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public abstract class GuiContainerNPCInterface extends GuiContainer
{
    public boolean drawDefaultBackground = false;
    public int guiLeft;
    public int guiTop;
    public EntityClientPlayerMP player;
    public EntityNPCInterface npc;
    private HashMap<Integer, GuiNpcButton> buttons = new HashMap();
    private HashMap<Integer, GuiMenuTopButton> topbuttons = new HashMap();
    private HashMap<Integer, GuiNpcTextField> textfields = new HashMap();
    private HashMap<Integer, GuiNpcLabel> labels = new HashMap();
    private HashMap<Integer, GuiCustomScroll> scrolls = new HashMap();
    private HashMap<Integer, GuiNpcSlider> sliders = new HashMap();
    public String title;
    public boolean closeOnEsc = false;
    private SubGuiInterface subgui;
    public int mouseX;
    public int mouseY;

    public GuiContainerNPCInterface(EntityNPCInterface npc, Container cont)
    {
        super(cont);
        this.player = Minecraft.getMinecraft().thePlayer;
        this.npc = npc;
        this.title = "Npc Mainmenu";
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        GuiNpcTextField.unfocus();
        this.buttonList.clear();
        this.buttons.clear();
        this.topbuttons.clear();
        this.scrolls.clear();
        this.sliders.clear();
        this.labels.clear();
        this.textfields.clear();
        Keyboard.enableRepeatEvents(true);

        if (this.subgui != null)
        {
            this.subgui.setWorldAndResolution(this.mc, this.width, this.height);
            this.subgui.initGui();
        }

        this.buttonList.clear();
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2;
    }

    public ResourceLocation getResource(String texture)
    {
        return new ResourceLocation("customnpcs", "textures/gui/" + texture);
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
        Iterator var1 = (new ArrayList(this.textfields.values())).iterator();

        while (var1.hasNext())
        {
            GuiNpcTextField tf = (GuiNpcTextField)var1.next();

            if (tf.enabled)
            {
                tf.updateCursorCounter();
            }
        }

        super.updateScreen();
    }

    /**
     * Called when the mouse is clicked.
     */
    public void mouseClicked(int mouseX, int mouseY, int mouseButton)
    {
        if (this.subgui != null)
        {
            this.subgui.mouseClicked(mouseX, mouseY, mouseButton);
        }
        else
        {
            Iterator var4 = (new ArrayList(this.textfields.values())).iterator();

            while (var4.hasNext())
            {
                GuiNpcTextField scroll = (GuiNpcTextField)var4.next();

                if (scroll.enabled)
                {
                    scroll.mouseClicked(mouseX, mouseY, mouseButton);
                }
            }

            if (mouseButton == 0)
            {
                var4 = (new ArrayList(this.scrolls.values())).iterator();

                while (var4.hasNext())
                {
                    GuiCustomScroll scroll1 = (GuiCustomScroll)var4.next();
                    scroll1.mouseClicked(mouseX, mouseY, mouseButton);
                }
            }

            this.mouseEvent(mouseX, mouseY, mouseButton);
            super.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    public void mouseEvent(int i, int j, int k) {}

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (this.subgui != null)
        {
            this.subgui.keyTyped(character, key);
        }
        else
        {
            Iterator var3 = (new ArrayList(this.textfields.values())).iterator();

            while (var3.hasNext())
            {
                GuiNpcTextField tf = (GuiNpcTextField)var3.next();
                tf.textboxKeyTyped(character, key);
            }

            if (this.closeOnEsc && (key == 1 || key == this.mc.gameSettings.keyBindInventory.getKeyCode() && !GuiNpcTextField.isActive()))
            {
                this.close();
            }
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        if (this.subgui != null)
        {
            this.subgui.buttonEvent(guiButton);
        }
        else
        {
            this.buttonEvent(guiButton);
        }
    }

    public void buttonEvent(GuiButton guibutton) {}

    public void close()
    {
        GuiNpcTextField.unfocus();
        this.save();
        this.player.closeScreen();
        this.displayGuiScreen((GuiScreen)null);
        this.mc.setIngameFocus();
    }

    public void addButton(GuiNpcButton button)
    {
        this.buttons.put(Integer.valueOf(button.id), button);
        this.buttonList.add(button);
    }

    public void addTopButton(GuiMenuTopButton button)
    {
        this.topbuttons.put(Integer.valueOf(button.id), button);
        this.buttonList.add(button);
    }

    public GuiNpcButton getButton(int i)
    {
        return (GuiNpcButton)this.buttons.get(Integer.valueOf(i));
    }

    public void addTextField(GuiNpcTextField tf)
    {
        this.textfields.put(Integer.valueOf(tf.id), tf);
    }

    public GuiNpcTextField getTextField(int i)
    {
        return (GuiNpcTextField)this.textfields.get(Integer.valueOf(i));
    }

    public void addLabel(GuiNpcLabel label)
    {
        this.labels.put(Integer.valueOf(label.id), label);
    }

    public GuiNpcLabel getLabel(int i)
    {
        return (GuiNpcLabel)this.labels.get(Integer.valueOf(i));
    }

    public GuiMenuTopButton getTopButton(int i)
    {
        return (GuiMenuTopButton)this.topbuttons.get(Integer.valueOf(i));
    }

    public void addSlider(GuiNpcSlider slider)
    {
        this.sliders.put(Integer.valueOf(slider.id), slider);
        this.buttonList.add(slider);
    }

    public GuiNpcSlider getSlider(int i)
    {
        return (GuiNpcSlider)this.sliders.get(Integer.valueOf(i));
    }

    public void addScroll(GuiCustomScroll scroll)
    {
        scroll.setWorldAndResolution(this.mc, 350, 250);
        this.scrolls.put(Integer.valueOf(scroll.id), scroll);
    }

    public GuiCustomScroll getScroll(int id)
    {
        return (GuiCustomScroll)this.scrolls.get(Integer.valueOf(id));
    }

    protected void drawGuiContainerForegroundLayer(int par1, int par2) {}

    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        this.drawCenteredString(this.fontRendererObj, StatCollector.translateToLocal(this.title), this.width / 2, this.guiTop - 8, 16777215);
        Iterator var4 = (new ArrayList(this.labels.values())).iterator();

        while (var4.hasNext())
        {
            GuiNpcLabel scroll = (GuiNpcLabel)var4.next();
            scroll.drawLabel(this, this.fontRendererObj);
        }

        var4 = (new ArrayList(this.textfields.values())).iterator();

        while (var4.hasNext())
        {
            GuiNpcTextField scroll1 = (GuiNpcTextField)var4.next();
            scroll1.drawTextBox(i, j);
        }

        var4 = (new ArrayList(this.scrolls.values())).iterator();

        while (var4.hasNext())
        {
            GuiCustomScroll scroll2 = (GuiCustomScroll)var4.next();
            scroll2.drawScreen(i, j, f, this.hasSubGui() ? 0 : Mouse.getDWheel());
        }
    }

    public abstract void save();

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        Container container = this.inventorySlots;

        if (this.subgui != null)
        {
            this.inventorySlots = new ContainerEmpty();
        }

        super.drawScreen(mouseX, mouseY, partialTick);
        this.zLevel = 0.0F;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        if (this.subgui != null)
        {
            this.inventorySlots = container;
            RenderHelper.disableStandardItemLighting();
            this.subgui.drawScreen(mouseX, mouseY, partialTick);
        }
    }

    public void drawDefaultBackground()
    {
        if (this.drawDefaultBackground && this.subgui == null)
        {
            super.drawDefaultBackground();
        }
    }

    public FontRenderer getFontRenderer()
    {
        return this.fontRendererObj;
    }

    public void closeSubGui(SubGuiInterface gui)
    {
        this.subgui = null;
    }

    public boolean hasSubGui()
    {
        return this.subgui != null;
    }

    public SubGuiInterface getSubGui()
    {
        return this.hasSubGui() && this.subgui.hasSubGui() ? this.subgui.getSubGui() : this.subgui;
    }

    public void displayGuiScreen(GuiScreen gui)
    {
        this.mc.displayGuiScreen(gui);
    }

    public void setSubGui(SubGuiInterface gui)
    {
        this.subgui = gui;
        this.subgui.setWorldAndResolution(this.mc, this.width, this.height);
        this.subgui.parent = this;
        this.initGui();
    }

    public void drawNpc(int x, int y)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)(this.guiLeft + x), (float)(this.guiTop + y), 50.0F);
        float scale = 1.0F;

        if ((double)this.npc.height > 2.4D)
        {
            scale = 2.0F / this.npc.height;
        }

        GL11.glScalef(-30.0F * scale, 30.0F * scale, 30.0F * scale);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = this.npc.renderYawOffset;
        float f3 = this.npc.rotationYaw;
        float f4 = this.npc.rotationPitch;
        float f7 = this.npc.rotationYawHead;
        float f5 = (float)(this.guiLeft + x) - (float)this.mouseX;
        float f6 = (float)(this.guiTop + y - 50) - (float)this.mouseY;
        GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-((float)Math.atan((double)(f6 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        this.npc.renderYawOffset = (float)Math.atan((double)(f5 / 40.0F)) * 20.0F;
        this.npc.rotationYaw = (float)Math.atan((double)(f5 / 40.0F)) * 40.0F;
        this.npc.rotationPitch = -((float)Math.atan((double)(f6 / 40.0F))) * 20.0F;
        this.npc.rotationYawHead = this.npc.rotationYaw;
        GL11.glTranslatef(0.0F, this.npc.yOffset, 0.0F);
        RenderManager.instance.playerViewY = 180.0F;
        RenderManager.instance.renderEntityWithPosYaw(this.npc, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        this.npc.renderYawOffset = f2;
        this.npc.rotationYaw = f3;
        this.npc.rotationPitch = f4;
        this.npc.rotationYawHead = f7;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }
}
