package noppes.npcs.client.gui.util;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import noppes.npcs.ModelData;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.util.EntityUtil;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiModelInterface extends GuiNPCInterface
{
    public ModelData playerdata;
    private static float rotation = 0.0F;
    private GuiNpcButton left;
    private GuiNpcButton right;
    private GuiNpcButton zoom;
    private GuiNpcButton unzoom;
    private static float zoomed = 60.0F;
    public int xOffset = 0;
    public EntityCustomNpc npc;
    private long start = -1L;

    public GuiModelInterface(EntityCustomNpc npc)
    {
        this.npc = npc;
        this.playerdata = npc.modelData;
        this.xSize = 380;
        this.drawDefaultBackground = false;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addButton(this.unzoom = new GuiNpcButton(666, this.guiLeft + 148 + this.xOffset, this.guiTop + 200, 20, 20, "-"));
        this.addButton(this.zoom = new GuiNpcButton(667, this.guiLeft + 214 + this.xOffset, this.guiTop + 200, 20, 20, "+"));
        this.addButton(this.left = new GuiNpcButton(668, this.guiLeft + 170 + this.xOffset, this.guiTop + 200, 20, 20, "<"));
        this.addButton(this.right = new GuiNpcButton(669, this.guiLeft + 192 + this.xOffset, this.guiTop + 200, 20, 20, ">"));
        this.addButton(new GuiNpcButton(66, this.width - 22, 2, 20, 20, "X"));
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        if (guiButton.id == 66)
        {
            this.close();
        }
    }

    /**
     * Returns true if this GUI should pause the game when it is displayed in single-player
     */
    public boolean doesGuiPauseGame()
    {
        return false;
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        if (Mouse.isButtonDown(0))
        {
            if (this.left.mousePressed(this.mc, mouseX, mouseY))
            {
                rotation += partialTick * 2.0F;
            }
            else if (this.right.mousePressed(this.mc, mouseX, mouseY))
            {
                rotation -= partialTick * 2.0F;
            }
            else if (this.zoom.mousePressed(this.mc, mouseX, mouseY))
            {
                zoomed += partialTick * 2.0F;
            }
            else if (this.unzoom.mousePressed(this.mc, mouseX, mouseY) && zoomed > 10.0F)
            {
                zoomed -= partialTick * 2.0F;
            }
        }

        this.drawDefaultBackground();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        Object entity = this.playerdata.getEntity(this.npc);

        if (entity == null)
        {
            entity = this.npc;
        }

        EntityUtil.Copy(this.npc, (EntityLivingBase)entity);
        int l = this.guiLeft + 190 + this.xOffset;
        int i1 = this.guiTop + 180;
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)l, (float)i1, 60.0F);
        GL11.glScalef(-zoomed, zoomed, zoomed);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = ((EntityLivingBase)entity).renderYawOffset;
        float f3 = ((EntityLivingBase)entity).rotationYaw;
        float f4 = ((EntityLivingBase)entity).rotationPitch;
        float f7 = ((EntityLivingBase)entity).rotationYawHead;
        float f5 = (float)l - (float) mouseX;
        float f6 = (float)(i1 - 50) - (float) mouseY;
        GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-((float)Math.atan((double)(f6 / 80.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        ((EntityLivingBase)entity).prevRenderYawOffset = ((EntityLivingBase)entity).renderYawOffset = rotation;
        ((EntityLivingBase)entity).prevRotationYaw = ((EntityLivingBase)entity).rotationYaw = (float)Math.atan((double)(f5 / 80.0F)) * 40.0F + rotation;
        ((EntityLivingBase)entity).rotationPitch = -((float)Math.atan((double)(f6 / 80.0F))) * 20.0F;
        ((EntityLivingBase)entity).prevRotationYawHead = ((EntityLivingBase)entity).rotationYawHead = ((EntityLivingBase)entity).rotationYaw;
        GL11.glTranslatef(0.0F, ((EntityLivingBase)entity).yOffset, 0.0F);
        RenderManager.instance.playerViewY = 180.0F;

        try
        {
            RenderManager.instance.renderEntityWithPosYaw((Entity)entity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        }
        catch (Exception var14)
        {
            this.playerdata.setEntityClass((Class)null);
        }

        ((EntityLivingBase)entity).prevRenderYawOffset = ((EntityLivingBase)entity).renderYawOffset = f2;
        ((EntityLivingBase)entity).prevRotationYaw = ((EntityLivingBase)entity).rotationYaw = f3;
        ((EntityLivingBase)entity).rotationPitch = f4;
        ((EntityLivingBase)entity).prevRotationYawHead = ((EntityLivingBase)entity).rotationYawHead = f7;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 0.0F, 500.065F);
        super.drawScreen(mouseX, mouseY, partialTick);
        GL11.glPopMatrix();
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        super.keyTyped(character, key);

        if (key == 1)
        {
            this.close();
        }
    }

    public void close()
    {
        this.mc.displayGuiScreen((GuiScreen)null);
        this.mc.setIngameFocus();
    }

    public void save() {}
}
