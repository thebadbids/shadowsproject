package noppes.npcs.client.gui.model;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.NPCRendererHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import noppes.npcs.ModelData;
import noppes.npcs.client.gui.util.GuiNPCInterface;
import noppes.npcs.client.gui.util.GuiNPCStringSlot;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.util.EntityUtil;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.Collections;
import java.util.Vector;

public class GuiEntitySelection extends GuiNPCInterface
{
    private GuiNPCStringSlot slot;
    private GuiCreationScreen parent;
    private Class <? extends EntityLivingBase > prevModel;
    private ModelData playerdata;
    private EntityCustomNpc npc;

    public GuiEntitySelection(GuiCreationScreen parent, ModelData playerdata, EntityCustomNpc npc)
    {
        this.parent = parent;
        this.playerdata = playerdata;
        this.npc = npc;
        this.drawDefaultBackground = false;
        this.prevModel = playerdata.getEntityClass();
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        Vector list = new Vector(this.parent.data.keySet());
        list.add("CustomNPC");
        Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
        this.slot = new GuiNPCStringSlot(list, this, false, 18);

        if (this.playerdata.getEntityClass() != null)
        {
            this.slot.selected = (String)EntityList.classToStringMapping.get(this.playerdata.getEntityClass());
        }
        else
        {
            this.slot.selected = "CustomNPC";
        }

        this.slot.registerScrollButtons(4, 5);
        this.buttonList.add(new GuiNpcButton(2, this.width / 2 - 100, this.height - 44, 98, 20, "gui.back"));
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        Object entity = this.playerdata.getEntity(this.npc);

        if (entity == null)
        {
            entity = this.npc;
        }
        else
        {
            EntityUtil.Copy(this.npc, (EntityLivingBase)entity);
        }

        int l = this.width / 2 - 180;
        int i1 = this.height / 2 - 90;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)(l + 33), (float)(i1 + 131), 50.0F);
        float scale = 1.0F;

        if ((double)((EntityLivingBase)entity).height > 2.4D)
        {
            scale = 2.0F / ((EntityLivingBase)entity).height;
        }

        GL11.glScalef(-50.0F * scale, 50.0F * scale, 50.0F * scale);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = ((EntityLivingBase)entity).renderYawOffset;
        float f3 = ((EntityLivingBase)entity).rotationYaw;
        float f4 = ((EntityLivingBase)entity).rotationPitch;
        float f7 = ((EntityLivingBase)entity).rotationYawHead;
        float f5 = (float)(l + 33) - (float) mouseX;
        float f6 = (float)(i1 + 131 - 50) - (float) mouseY;
        GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-((float)Math.atan((double)(f6 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        ((EntityLivingBase)entity).renderYawOffset = (float)Math.atan((double)(f5 / 40.0F)) * 20.0F;
        ((EntityLivingBase)entity).rotationYaw = (float)Math.atan((double)(f5 / 40.0F)) * 40.0F;
        ((EntityLivingBase)entity).rotationPitch = -((float)Math.atan((double)(f6 / 40.0F))) * 20.0F;
        ((EntityLivingBase)entity).rotationYawHead = ((EntityLivingBase)entity).rotationYaw;
        GL11.glTranslatef(0.0F, ((EntityLivingBase)entity).yOffset, 0.0F);
        RenderManager.instance.playerViewY = 180.0F;

        try
        {
            RenderManager.instance.renderEntityWithPosYaw((Entity)entity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        }
        catch (Exception var15)
        {
            this.playerdata.setEntityClass((Class)null);
        }

        ((EntityLivingBase)entity).renderYawOffset = f2;
        ((EntityLivingBase)entity).rotationYaw = f3;
        ((EntityLivingBase)entity).rotationPitch = f4;
        ((EntityLivingBase)entity).rotationYawHead = f7;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        this.slot.drawScreen(mouseX, mouseY, partialTick);
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    public void elementClicked()
    {
        try
        {
            this.playerdata.setEntityClass((Class)this.parent.data.get(this.slot.selected));
            EntityLivingBase ex = this.playerdata.getEntity(this.npc);

            if (ex != null)
            {
                RendererLivingEntity render = (RendererLivingEntity)RenderManager.instance.getEntityRenderObject(ex);
                this.npc.display.texture = NPCRendererHelper.getTexture(render, ex);
            }
            else
            {
                this.npc.display.texture = "customnpcs:textures/entity/humanmale/Steve.png";
            }

            this.npc.display.glowTexture = "";
            this.npc.textureLocation = null;
            this.npc.textureGlowLocation = null;
            this.npc.updateHitbox();
        }
        catch (Exception var3)
        {
            this.npc.display.texture = "customnpcs:textures/entity/humanmale/Steve.png";
        }
    }

    public void doubleClicked()
    {
        this.close();
    }

    /**
     * Fired when a key is typed. This is the equivalent of KeyListener.keyTyped(KeyEvent e).
     */
    public void keyTyped(char character, int key)
    {
        if (key == 1)
        {
            this.close();
        }
    }

    public void close()
    {
        this.mc.displayGuiScreen(this.parent);
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        this.close();
    }

    public void save() {}
}
