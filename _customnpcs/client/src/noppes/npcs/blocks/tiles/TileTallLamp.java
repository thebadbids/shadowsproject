package noppes.npcs.blocks.tiles;

import net.minecraft.util.AxisAlignedBB;

public class TileTallLamp extends TileColorable
{
    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox((double)this.xCoord, (double)this.yCoord, (double)this.zCoord, (double)(this.xCoord + 1), (double)(this.yCoord + 2), (double)(this.zCoord + 1));
    }
}
