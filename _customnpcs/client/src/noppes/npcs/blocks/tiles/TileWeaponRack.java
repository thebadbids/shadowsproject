package noppes.npcs.blocks.tiles;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.util.AxisAlignedBB;

public class TileWeaponRack extends TileNpcContainer
{
    /**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
     */
    public boolean isItemValidForSlot(int var1, ItemStack itemstack)
    {
        return itemstack != null && itemstack.getItem() instanceof ItemBlock ? false : super.isItemValidForSlot(var1, itemstack);
    }

    /**
     * Returns the number of slots in the inventory.
     */
    public int getSizeInventory()
    {
        return 3;
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox((double)this.xCoord, (double)this.yCoord, (double)this.zCoord, (double)(this.xCoord + 1), (double)(this.yCoord + 2), (double)(this.zCoord + 1));
    }

    public String getName()
    {
        return "tile.npcWeaponRack.name";
    }

    /**
     * Overriden in a sign to provide the text.
     */
    public Packet getDescriptionPacket()
    {
        NBTTagCompound compound = new NBTTagCompound();
        this.writeToNBT(compound);
        S35PacketUpdateTileEntity packet = new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, compound);
        return packet;
    }

    public int powerProvided()
    {
        int power = 0;

        for (int i = 0; i < 3; ++i)
        {
            if (this.getStackInSlot(i) != null)
            {
                power += 5;
            }
        }

        return power;
    }
}
