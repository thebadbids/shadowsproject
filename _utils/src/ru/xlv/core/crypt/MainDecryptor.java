package ru.xlv.core.crypt;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

public class MainDecryptor implements IDecryptor {

    private static class ClassLoader extends java.lang.ClassLoader {
        public void defineClassBytes(String fullClassName, byte[] bytes) {
            defineClass(fullClassName, bytes, 0, bytes.length);
        }
    }

    private Object dcd;
    private Method method;

    private InputStream getEncryptedInputStream0(InputStream inputStream) throws Throwable {
        return (InputStream) method.invoke(dcd, inputStream);
    }

    public MainDecryptor(String token, String primaryKey, String url, String username) {
        try(
                InputStream request = doRequest(String.format(url, token, username));
                DataInputStream dataInputStream = new DataInputStream(request)
        ) {
            byte[] serverKey = readArray(dataInputStream, dataInputStream.readInt());
            byte[] decoderBytes = readArray(dataInputStream, dataInputStream.readInt());
            byte[] obycBytes = readArray(dataInputStream, dataInputStream.readInt());
            byte[] imagePathBytes = readArray(dataInputStream, dataInputStream.readInt());
            byte[][] ss = new byte[dataInputStream.readInt()][];
            for (int i = 0; i < ss.length; i++) {
                ss[i] = new byte[dataInputStream.readInt()];
                dataInputStream.read(ss[i]);
            }
            ClassLoader classLoader = new ClassLoader();
            try {
                String classPath = "Obyc";
                obycBytes = Base64.getDecoder().decode(obycBytes);
                classLoader.defineClassBytes(classPath, obycBytes);
                Class<?> clazz = classLoader.loadClass(classPath);
                decoderBytes = (byte[]) clazz.getMethods()[0].invoke(null, decoderBytes);
                decoderBytes = (byte[]) clazz.getMethods()[0].invoke(null, decoderBytes);
                classPath = "Main";
                classLoader.defineClassBytes(classPath, decoderBytes);
                Class<?> clazz1 = classLoader.loadClass(classPath);
//                for (Method method : clazz1.getMethods()) {
//                    System.out.println(method.getName());
//                }
                serverKey = (byte[]) clazz1.getMethod("a", byte[].class, byte[].class).invoke(null, serverKey, primaryKey.getBytes());
                serverKey = (byte[]) clazz.getMethods()[0].invoke(null, serverKey);
//                System.out.println(new String(serverKey));
                imagePathBytes = (byte[]) clazz1.getMethod("a", byte[].class, byte[].class).invoke(null, imagePathBytes, serverKey);
                imagePathBytes = (byte[]) clazz.getMethods()[0].invoke(null, imagePathBytes);
//                System.out.println(new String(imagePathBytes));
                byte[] clientKey = (byte[]) clazz1.getMethod("b", String.class, byte[].class).invoke(null, new String(imagePathBytes), serverKey);
//                System.out.println(new String(clientKey));
                for (int i = 0; i < ss.length; i++) {
                    ss[i] = (byte[]) clazz1.getMethod("a", byte[].class, byte[].class).invoke(null, ss[i], serverKey);
                    ss[i] = (byte[]) clazz.getMethods()[0].invoke(null, ss[i]);
//                    System.out.println(new String(ss[i]));
                }
                dcd = clazz1.getConstructor(byte[].class).newInstance((Object) clientKey);
                method = dcd.getClass().getMethod("d", InputStream.class);
            } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
                e.printStackTrace();
            }
//            String imagePath = new String(imagePathBytes);
//            System.out.println(new String(serverKey));
//            System.out.println(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private InputStream doRequest(String request) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(request).openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        return connection.getInputStream();
    }

    private byte[] readArray(DataInputStream dataInputStream, int length) throws IOException {
        byte[] bytes = new byte[length];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = dataInputStream.readByte();
        }
        return bytes;
    }

    @Override
    public InputStream getDecryptedInputStream(InputStream inputStream) throws Throwable {
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            byte[] b = new byte[1024];
            int numberOfBytedRead;
            InputStream is = getEncryptedInputStream0(inputStream);
            while ((numberOfBytedRead = is.read(b)) >= 0) {
                baos.write(b, 0, numberOfBytedRead);
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(baos.toByteArray());
            baos.close();
            return byteArrayInputStream;
        }
    }
}
