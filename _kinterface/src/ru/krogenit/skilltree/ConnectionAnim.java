package ru.krogenit.skilltree;

import org.lwjgl.util.vector.Vector2f;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;

public class ConnectionAnim {
    private final Vector2f startPoint;
    private final Vector2f endPoint;
    private final Vector2f currentPoint1;
    private final Vector2f currentPoint2;
    private static final float LINE_LENGTH = 10f;
    private float animTime;

    public ConnectionAnim(Vector2f startPoint, Vector2f endPoint) {
        this.startPoint = new Vector2f(startPoint);
        this.endPoint = new Vector2f(endPoint);
        this.currentPoint1 = new Vector2f(startPoint);
        this.currentPoint2 = new Vector2f(startPoint);
    }

    private void updatePoint(Vector2f point) {
        float speed = 1.0f;
        if(point.x < endPoint.x) {
            point.x = AnimationHelper.updateAnim(point.x, endPoint.x, speed);
        } else {
            point.x = AnimationHelper.updateAnim(point.x, endPoint.x, -speed);
        }

        if(point.y < endPoint.y) {
            point.y = AnimationHelper.updateAnim(point.y, endPoint.y, speed);
        } else {
            point.y = AnimationHelper.updateAnim(point.y, endPoint.y, -speed);
        }
    }

    public void addXYToPoints(float x, float y) {
        this.currentPoint1.x += x;
        this.currentPoint2.x += x;
        this.currentPoint1.y += y;
        this.currentPoint2.y += y;
    }

    public void update() {
        animTime += AnimationHelper.getAnimationSpeed();
        updatePoint(currentPoint1);

        if(animTime >= 30) {
            updatePoint(currentPoint2);
        }
    }

    public void draw(float lineWidth, Vector2f treePosAnim, float scaleAnim) {
        if(!isComplete() && !currentPoint1.equals(startPoint)) {
            float halfLine = lineWidth / 2f;
            float x1 = ScaleGui.getCenterX(960 + (currentPoint1.x + treePosAnim.x) * scaleAnim);
            float y1 = ScaleGui.getCenterY(540 + (currentPoint1.y + treePosAnim.y) * scaleAnim);
            float x2 = ScaleGui.getCenterX(960 + (currentPoint2.x + treePosAnim.x) * scaleAnim);
            float y2 = ScaleGui.getCenterY(540 + (currentPoint2.y + treePosAnim.y) * scaleAnim);
            GuiDrawUtils.drawRectXY(Math.min(x1, x2) - halfLine, Math.min(y1, y2) - halfLine,
                    Math.max(x1, x2) + halfLine, Math.max(y1, y2) + halfLine);
        }
    }

    public boolean isHalfComplete() {
        return currentPoint1.equals(endPoint);
    }

    public boolean isComplete() {
        return currentPoint2.equals(endPoint);
    }

    public void reset() {
        this.currentPoint1.x = startPoint.x;
        this.currentPoint1.y = startPoint.y;
        this.currentPoint2.x = startPoint.x;
        this.currentPoint2.y = startPoint.y;
        this.animTime = 0f;
    }
}
