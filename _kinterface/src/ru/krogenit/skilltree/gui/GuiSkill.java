package ru.krogenit.skilltree.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.skilltree.CoreSkillTree;
import ru.krogenit.skilltree.SkillAttribute;
import ru.krogenit.skilltree.SkillValue;
import ru.krogenit.skilltree.type.EnumSkillStatType;
import ru.krogenit.skilltree.type.EnumSkillTreeAvailableType;
import ru.krogenit.skilltree.type.EnumSkillTreeType;
import ru.krogenit.utils.AnimationHelper;
import ru.krogenit.utils.Utils;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.glBlendFuncSeparate;

public class GuiSkill extends GuiButton {

    private EnumSkillTreeType type;
    private EnumSkillTreeAvailableType availableType;
    private float totalHeight;
    private Vector3f cost;
    private List<SkillValue> values;
    private List<SkillAttribute> attributes;
    private final ResourceLocation lockedSkillTexture;
    private final ResourceLocation learnedSkillTexture;
    private final ResourceLocation availableSkillTexture;
    private float xBase, yBase, widthBase, heightBase;
    private final String textureName;
    private boolean isAnim;
    private float anim, maskAnim;
    private String name, description;

    public GuiSkill(int id, EnumSkillTreeType type, EnumSkillTreeAvailableType availableType, String textureName) {
        super(id, 0, 0, 0, 0, "");
        this.type = type;
        this.availableType = availableType;
        this.lockedSkillTexture = new ResourceLocation(CoreSkillTree.MODID, "textures/locked/" + textureName + "_" + (type == EnumSkillTreeType.PASSIVE ? "hex" : "square")  + "_256x256.png");
        this.learnedSkillTexture = new ResourceLocation(CoreSkillTree.MODID, "textures/learned/" + textureName + "_" + (type == EnumSkillTreeType.PASSIVE ? "hex" : "square")  + "_256x256.png");
        this.availableSkillTexture = new ResourceLocation(CoreSkillTree.MODID, "textures/available/" + textureName + "_" + (type == EnumSkillTreeType.PASSIVE ? "hex" : "square")  + "_256x256.png");
        this.textureName = textureName;
    }

    public void setCost(Vector3f cost) {
        this.cost = cost;
    }

    public void setAttributes(List<SkillAttribute> attributes) {
        this.attributes = attributes;
    }

    public void setValues(List<SkillValue> values) {
        this.values = values;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPositionAndSize(float xPos, float yPos, float width, float height) {
        this.xPosition = xBase = xPos;
        this.yPosition = yBase = yPos;
        this.width = widthBase = width;
        this.height = heightBase = height;
    }

    public void drawTooltip(Minecraft mc, int mouseX, int mouseY) {
        if(isHovered(mouseX, mouseY)) {
            float mouseOffset = mc.displayHeight /  50f;
            float x = xPosition + (-xPosition + mouseX);
            float y = yPosition + (-yPosition + mouseY);
            float startY = y;
            if(totalHeight == 0) totalHeight = ScaleGui.get(565f);
            float width = ScaleGui.get(378f);
            float height;
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GuiDrawUtils.renderToolTipSkill(x + mouseOffset, y, x + width + mouseOffset, y + totalHeight, mc.displayHeight / 36f);
            GL11.glEnable(GL11.GL_TEXTURE_2D);

            float fs = 1.963f;
            x += ScaleGui.get(54f);
            y += ScaleGui.get(54f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, name.toUpperCase(), x, y, fs, 0xffffff);
            y +=  ScaleGui.get(30f);
            fs = ScaleGui.get(0.981f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, XlvsMainMod.INSTANCE.getCurrentCharacterType().getDisplayName().toUpperCase(), x, y, fs, 0x666666);
            GL11.glColor4f(1f,1f,1f,1f);
            y += ScaleGui.get(27f);
            float backX = x;

            for(SkillAttribute attribute : attributes) {
                Vector3f color = attribute.getColor();
                fs = 0.72f;
                String s = attribute.getName();
                width = ScaleGui.get(21.6f) + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * ScaleGui.get(fs);
                height = ScaleGui.get(15.4f);
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GL11.glColor4f(color.x, color.y ,color.z, 0.95f);
                GuiDrawUtils.renderToolTipSkillType(x, y, x + width, y + height, mc.displayHeight / 150f);
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, s, x + ScaleGui.get(9f), y + ScaleGui.get(6.3f), fs, 0x7e7e7e);
                x += width * 1.05f;
            }

            y += ScaleGui.get(30f);
            x = backX;
            GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, ScaleGui.get(270f), 1);
            y += ScaleGui.get(17f);
            fs = 0.9f;
            y += GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, description,
                x, y, fs, ScaleGui.get(327f), -1, 0xffffff, EnumStringRenderType.DEFAULT);
            y += ScaleGui.get(10.8f);
            GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, ScaleGui.get(270f), 1);
            y += ScaleGui.get(11.25f);
            float iconWidth;
            float iconHeight;
            if(type == EnumSkillTreeType.ACTIVE) {
                iconWidth = ScaleGui.get(38.5f);
                iconHeight = ScaleGui.get(38.5f);
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconSkillArmor, x , y, iconWidth, iconHeight);
                backX = x;
                x += iconWidth +ScaleGui.get(13.5f);
                y += iconHeight / 2f - ScaleGui.get(2.16f);
                fs = ScaleGui.get(1.2f);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "РАСКОЛ БРОНИ", x, y, fs, 0xffffff);
                y += iconHeight / 2f +ScaleGui.get(13.5f);
                x = backX;
                GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, ScaleGui.get(270f), 1);
                y += ScaleGui.get(11.25f);
                for(int i=0;i<values.size();i++) {
                    SkillValue value = values.get(i);
                    iconWidth = ScaleGui.get(27f);
                    iconHeight = ScaleGui.get(27f);
                    GuiDrawUtils.drawRect(TextureRegister.getSkillStatTexture(value.getStatType()), x , y, iconWidth, iconHeight);
                    backX = x;
                    x += iconWidth + ScaleGui.get(13.5f);
                    y += iconHeight / 2f - ScaleGui.get(3f);
                    GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "" + value.getValue(), x, y, fs, 0xffffff);
                    if(value.getStatType() == EnumSkillStatType.TIME) {
                        y += ScaleGui.get(3.85f);
                        x += FontType.HelveticaNeueCyrMedium.getFontContainer().width("" + value.getValue()) * fs + ScaleGui.get(3f);
                        fs = 0.77f;
                        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "сек", x, y, fs, 0x7e7e7e);
                        fs = 1.2f;
                        y -= ScaleGui.get(3.85f);
                    }
                    x = backX;
                    if(i < values.size() -1)y += iconHeight / 2f + ScaleGui.get(4.9f);
                }
            } else if(type == EnumSkillTreeType.PASSIVE) {
                fs = 1.2f;
                for(int i=0;i<values.size();i++) {
                    SkillValue value = values.get(i);
                    iconWidth = ScaleGui.get(27f);
                    iconHeight = ScaleGui.get(27f);
                    GuiDrawUtils.drawRect(TextureRegister.getSkillStatTexture(value.getStatType()), x , y, iconWidth, iconHeight);
                    backX = x;
                    x += iconWidth + ScaleGui.get(13.5f);
                    y += iconHeight / 2f - ScaleGui.get(3f);
                    GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, EnumChatFormatting.GREEN + "+" + value.getValue(), x, y, fs, 0xffffff);
                    x = backX;
                    if(i < values.size() -1) y += iconHeight / 2f + ScaleGui.get(4.9f);
                }
            }

            y += ScaleGui.get(28f);
            GL11.glColor4f(1f,1f,1f,1f);
            x = backX;
            GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, ScaleGui.get(270f), 1);
            y += ScaleGui.get(23.5f);
            fs = 0.9f;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "ТРЕБОВАНИЯ ОПЫТА", x, y, fs, 0x7e7e7e);
            y += ScaleGui.get(16.875f);
            iconWidth = ScaleGui.get(27f);
            iconHeight = ScaleGui.get(27f);
            GuiDrawUtils.drawRect(TextureRegister.textureTreeIconResCombat, x, y, iconWidth, iconHeight);
            String s = "" + (int)cost.x;
            fs = 2.16f;
            backX = x;
            x += iconWidth * 1.3f;
            GuiDrawUtils.drawStringNoXYScale(FontType.Marske, s, x, y + ScaleGui.get(5.4f), fs, 0x972c38);
            x += FontType.Marske.getFontContainer().width(s) * fs + iconWidth;
            glColor4f(1f, 1f, 1f, 1f);
            GuiDrawUtils.drawRect(TextureRegister.textureTreeIconResResearch, x, y, iconWidth, iconHeight);
            x += iconWidth * 1.3f;
            s = "" + (int)cost.y;
            GuiDrawUtils.drawStringNoXYScale(FontType.Marske, s, x, y + ScaleGui.get(5.4f), fs, 0x2f85aa);
            x += FontType.Marske.getFontContainer().width(s) * fs + iconWidth;
            glColor4f(1f, 1f, 1f, 1f);
            GuiDrawUtils.drawRect(TextureRegister.textureTreeIconResSurvive, x, y, iconWidth, iconHeight);
            x += iconWidth * 1.3f;
            s = "" + (int)cost.z;
            GuiDrawUtils.drawStringNoXYScale(FontType.Marske, s, x, y + ScaleGui.get(5.4f), fs, 0x468039);
            glColor4f(1f, 1f, 1f, 1f);
            y += ScaleGui.get(38.5f);
            x = backX;
            GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, ScaleGui.get(270f), 1);

            fs = 1.08f;
            y += ScaleGui.get(15.8f);
            if(availableType == EnumSkillTreeAvailableType.LEARNED) {
                iconWidth = ScaleGui.get(25f);
                iconHeight = ScaleGui.get(18f);
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconLearned, x , y, iconWidth, iconHeight);
                y += ScaleGui.get(6.35f);
                x += iconWidth + ScaleGui.get(15.8f);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "НАВЫК ИЗУЧЕН", x, y, fs, 0x666666);
            } else if(availableType == EnumSkillTreeAvailableType.AVAILABLE) {
                iconWidth = ScaleGui.get(18f);
                iconHeight = ScaleGui.get(30.8f);
                GuiDrawUtils.drawRect(TextureRegister.textureSkillIconLeftMouse, x , y, iconWidth, iconHeight);
                y += ScaleGui.get(13.5f);
                x += iconWidth + ScaleGui.get(15.8f);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, "ИЗУЧИТЬ", x, y, fs, 0x666666);
            }
            y += ScaleGui.get(54f);
            totalHeight = y - startY;
        }
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        boolean isHovering = isHovered(mouseX, mouseY);
        maskAnim += AnimationHelper.getAnimationSpeed() * 0.005f;
        if (maskAnim > 1f) {
            maskAnim -= 1f;
        }

        float off = height / 10f;
        float x = xPosition - off;
        float y = yPosition - off;
        float width = this.width + off * 2;
        float height = this.height + off * 2;

        ResourceLocation maskTexture, highlightTexture;
        if(type == EnumSkillTreeType.PASSIVE) {
            maskTexture = TextureRegister.textureSkillHexHoverMask;
            highlightTexture = TextureRegister.textureSkillHexHighlight;
        } else {
            maskTexture = TextureRegister.textureSkillActiveHoverMask;
            highlightTexture = TextureRegister.textureSkillActiveHighlight;
            off = this.height / 10f;
            x = xPosition - off;
            y = yPosition - off;
            width = this.width + off * 2;
            height = this.height + off * 2;
        }

        glDisable(GL_TEXTURE_2D);
        glColor4f(1f,1f,1f,1f);
        glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_ZERO, GL_ZERO);
        GuiDrawUtils.drawRect(0, 0, mc.displayWidth, mc.displayHeight);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);
        if(availableType == EnumSkillTreeAvailableType.LOCKED) {
            if(isHovering) {
                GL11.glColor4f(1f, 1f, 1f, 1f);
                GuiDrawUtils.drawMaskingButtonEffect(maskTexture, x, y, width, height, this.xPosition - this.width / 2f, this.yPosition + this.height / 6f, this.width*2f, this.height/1.5f, maskAnim * 360f);
            }
            Utils.bindTexture(lockedSkillTexture);
        } else if(availableType == EnumSkillTreeAvailableType.AVAILABLE) {
            if(isHovering) {
                GL11.glColor4f(0f, 1f, 1f, 1f);
                GuiDrawUtils.drawMaskingButtonEffect(maskTexture, x, y, width, height, this.xPosition - this.width / 2f, this.yPosition + this.height / 6f, this.width*2f, this.height/1.5f, maskAnim * 360f);
            }
            Utils.bindTexture(availableSkillTexture);
        } else {
            if(isHovering) {
                GL11.glColor4f(1f, 1f, 1f, 1f);
                GuiDrawUtils.drawMaskingButtonEffect(maskTexture, x, y, width, height, this.xPosition - this.width / 2f, this.yPosition + this.height / 6f, this.width * 2f, this.height / 1.5f, maskAnim * 360f);
            }
            Utils.bindTexture(learnedSkillTexture);
        }

        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

        if(type == EnumSkillTreeType.PASSIVE) {
            if(availableType == EnumSkillTreeAvailableType.LEARNED) Utils.bindTexture(TextureRegister.textureSkillPssiveBorderLearned);
            else Utils.bindTexture(TextureRegister.textureSkillPssiveBorder);
        } else {
            if(availableType == EnumSkillTreeAvailableType.LEARNED) Utils.bindTexture(TextureRegister.textureSkillActiveBorderLearned);
            else Utils.bindTexture(TextureRegister.textureSkillActiveBorder);
        }
        if(availableType == EnumSkillTreeAvailableType.LOCKED) {
            GL11.glColor4f(1f,1f,1f,1f);
        } else if(availableType == EnumSkillTreeAvailableType.AVAILABLE) {
            if(isAnim) {
                anim = AnimationHelper.updateAnim(anim, 1.0f, 0.025f);
                if(anim >= 0.98f) isAnim = false;
            } else {
                anim = AnimationHelper.updateAnim(anim, 0.0f, -0.025f);
                if(anim <= 0.02f) isAnim = true;
            }
            GL11.glColor4f(0f,1f,1f,anim);
        } else {
            GL11.glColor4f(1f,1f,1f,1f);
        }

        GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
        if(isHovering) {
            if(availableType == EnumSkillTreeAvailableType.AVAILABLE) GL11.glColor4f(0f,1f,1f,1f);
            else GL11.glColor4f(1f,1f,1f,1f);
            GuiDrawUtils.drawRect(highlightTexture, this.xPosition, this.yPosition, this.width, this.height);
        }
    }

    private boolean isHovered(int mouseX, int mouseY) {
        return this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
    }

    public void setAvailableType(EnumSkillTreeAvailableType availableType) {
        this.availableType = availableType;
    }

    public void setType(EnumSkillTreeType type) {
        this.type = type;
    }

    public EnumSkillTreeAvailableType getAvailableType() {
        return availableType;
    }

    public EnumSkillTreeType getType() {
        return type;
    }

    public ResourceLocation getSkillLearnedTexture() {
        return learnedSkillTexture;
    }

    public float getxBase() {
        return xBase;
    }

    public float getyBase() {
        return yBase;
    }

    public float getHeightBase() {
        return heightBase;
    }

    public float getWidthBase() {
        return widthBase;
    }

    public String getTextureName() {
        return textureName;
    }

    public String getName() {
        return name;
    }
}
