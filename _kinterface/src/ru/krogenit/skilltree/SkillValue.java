package ru.krogenit.skilltree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.krogenit.skilltree.type.EnumSkillStatType;

@Getter
@AllArgsConstructor
public class SkillValue {
    private final EnumSkillStatType statType;
    private final int value;
}
