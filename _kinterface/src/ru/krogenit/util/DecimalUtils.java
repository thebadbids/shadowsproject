package ru.krogenit.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class DecimalUtils {
    private static final DecimalFormat formatterTwoDigits = new DecimalFormat("#0.00");
    private static final DecimalFormat formatterOneDigit = new DecimalFormat("#0.0");
    private static final DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private static final DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

    static {
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
    }

    public static String getFormattedStringWithToDigits(double value) {
        return formatterTwoDigits.format(value);
    }

    public static String getFormattedStringWithOneDigit(double value) {
        return formatterOneDigit.format(value);
    }

    public static String getFormattedStringWithSpaces(double value) {
        return formatter.format(value);
    }
}
