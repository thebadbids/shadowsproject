package ru.krogenit.modifi.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ModifyStat {
    private final EnumModifyStatType modifyStatType;
    private final String value;
}
