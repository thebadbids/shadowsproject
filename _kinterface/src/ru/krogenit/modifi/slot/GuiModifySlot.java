package ru.krogenit.modifi.slot;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.modifi.stat.EnumModifyStatType;
import ru.krogenit.modifi.stat.ModifyItem;
import ru.krogenit.modifi.stat.ModifyStat;
import ru.xlv.customfont.FontType;

public class GuiModifySlot extends GuiButtonAdvanced {

    @Getter
    @Setter
    private ModifyItem modifyItem;
    @Getter
    private final EnumModifySlotType slotType;
    private final ResourceLocation textureSlot;

    public GuiModifySlot(int id, float x, float y, float width, float height, EnumModifySlotType slotType, ModifyItem modifyItem) {
        super(id, x, y, width, height, "");
        this.modifyItem = modifyItem;
        this.slotType = slotType;
        if(modifyItem != null) {
            textureSlot = TextureRegister.textureModifySlot;
            displayString = modifyItem.getItemStack().getDisplayName().toUpperCase();
        } else {
            switch (slotType) {
                case SIGHT: textureSlot = TextureRegister.textureModifySlotSight; displayString = "ПРИЦЕЛ"; break;
                case AMMO: textureSlot = TextureRegister.textureModifySlotAmmo; displayString = "БОЕПРИПАСЫ"; break;
                case BUTT: textureSlot = TextureRegister.textureModifySlotButt; displayString = "ПРИКЛАД"; break;
                case CASE: textureSlot = TextureRegister.textureModifySlotCase; displayString = "КОРПУС"; break;
                case MAGAZINE: textureSlot = TextureRegister.textureModifySlotMagazine; displayString = "МАГАЗИН"; break;
                case SILENCER: textureSlot = TextureRegister.textureModifySlotSilencer; displayString = "ГЛУШИТЕЛЬ"; break;
                default: textureSlot = TextureRegister.textureModifySlotTrigger; displayString = "СПУСКОВОЙ МЕХАНИЗМ";
            }
        }
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovering = isHovered(mouseX, mouseY);
        if(isHovering) GL11.glColor4f(1f, 1f, 1f, 1f);
        else GL11.glColor4f(0.75f, 0.75f, 0.75f, 1f);
        GuiDrawUtils.drawRect(modifyItem != null ? TextureRegister.textureModifySlot : textureSlot, xPosition, yPosition, width, height);
        if(modifyItem != null) {
            float offset = ScaleGui.get(0f);
            GuiDrawUtils.renderItem(modifyItem.getItemStack(), 1.0f, offset, offset, xPosition, yPosition, width, height, 0f);
            float x = xPosition + ScaleGui.get(232);
            float y = yPosition + ScaleGui.get(10);
            float yOffset = ScaleGui.get(21);
            float size = ScaleGui.get(20);
            float sx = ScaleGui.get(26);
            float sy = ScaleGui.get(9);
            for(ModifyStat stat : modifyItem.getStats()) {
                GuiDrawUtils.drawRect(getStatTexture(stat.getModifyStatType()), x, y, size, size);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, stat.getValue(), x + sx, y + sy, 28 / 32f, isHovering ? 0xffffff : 0xdddddd);
                y+=yOffset;
            }
        }
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, displayString, xPosition + ScaleGui.get(17), yPosition + ScaleGui.get(83), 32 / 32f, isHovering ? 0xffffff : 0xdddddd);
    }

    private ResourceLocation getStatTexture(EnumModifyStatType type) {
        switch (type) {
            case ACCURACY: return TextureRegister.textureModifyStatAccuracy;
            case CLIP_SIZE: return TextureRegister.textureModifyStatClip;
            case DURABILITY: return TextureRegister.textureModifyStatDurability;
            default: return TextureRegister.textureModifyStatKnockBack;
        }
    }
}
