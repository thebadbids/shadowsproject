package ru.krogenit.modifi;

import lombok.AllArgsConstructor;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.*;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.guns.item.ItemGunClient;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;
import ru.krogenit.math.MatrixUtils;
import ru.krogenit.modifi.slot.EnumModifySlotType;
import ru.krogenit.modifi.slot.GuiModifySlot;
import ru.krogenit.modifi.slot.GuiModifySlotGun;
import ru.krogenit.modifi.stat.ModifyItem;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.util.GuiWeaponPopup;
import ru.krogenit.util.ItemStatUtils;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiGunModify extends AbstractGuiScreenAdvanced {

    private final GuiScroll guiScroll = new GuiScroll();
    private boolean changeGun;
    private final List<GuiModifySlot> modifySlots = new ArrayList<>();
    private final List<GuiModifySlot> subSlots = new ArrayList<>();
    private final List<GuiModifySlotGun> inventorySlots = new ArrayList<>();
    private final GuiModifySlotGun slotGun = new GuiModifySlotGun(0, 0, 0, 0, 0, null);
    private GuiModifySlot activeSlot;
    private final Quaternion itemRotationQuaternion = new Quaternion();
    private static final Quaternion quaternionHelper = new Quaternion();
    private final Vector3f itemRotation = new Vector3f(0, 0, 0);
    private final Vector2f mouseLastClick = new Vector2f(0, 0);
    private boolean isRotatingItem;
    private final GuiWeaponPopup weaponPopup = new GuiWeaponPopup();
    private final List<InventorySection> inventorySections = new ArrayList<>();
    private final List<InventorySectionButton> inventorySectionButtons = new ArrayList<>();
    private final AxisAlignedBB inventoryAABB = AxisAlignedBB.getBoundingBox(0,0,-100,0,0,100);
    private int selectedInventorySection;
    private static final Vector4f axisVector = new Vector4f();
    private static final Matrix4f modelView = new Matrix4f();
    private static final Vector3f helpVector = new Vector3f();
    private static final FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

    public GuiGunModify() {
        super(ScaleGui.FULL_HD);
        ItemStack currentItem = mc.thePlayer.getCurrentEquippedItem();
        if(currentItem != null && currentItem.getItem() instanceof ItemGunClient) {
            selectGun(currentItem);
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        inventorySections.clear();
        inventorySectionButtons.clear();
        inventorySlots.clear();

        float buttonWidth = 243;
        float buttonHeight = 40;
        float x = 1598;
        float y = 220;
        GuiButtonAnimated b = new GuiButtonAnimated(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "СМЕНИТЬ ОРУЖИЕ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(b);

        x = 59;
        y = 163;
        float width = 310;
        float height = 105;
        float yOffset = 125;
        for(GuiModifySlot slot : modifySlots) {
            slot.setXPosition(ScaleGui.getCenterX(x));
            slot.setYPosition(ScaleGui.getCenterY(y));
            slot.setWidth(ScaleGui.get(width));
            slot.setHeight(ScaleGui.get(height));
            y += yOffset;
        }
        if(activeSlot != null) {
            x = 408;
            y = activeSlot.yPosition;
            yOffset = ScaleGui.get(yOffset);
            float maxY = ScaleGui.getCenterY(913);
            float currentY = y + yOffset * (subSlots.size() - 1);
            if(currentY > maxY) {
                float diff = currentY - maxY;
                y -= diff;
            }
            for(GuiModifySlot slot : subSlots) {
                slot.setXPosition(ScaleGui.getCenterX(x));
                slot.setYPosition(y);
                slot.setWidth(ScaleGui.get(width));
                slot.setHeight(ScaleGui.get(height));
                y += yOffset;
            }
        }

        slotGun.setXPosition(ScaleGui.getCenterX(1599));
        slotGun.setYPosition(ScaleGui.getCenterY(79));
        slotGun.setWidth(ScaleGui.get(241));
        slotGun.setHeight(ScaleGui.get(121));

        float startY = 281;
        inventorySections.add(new InventorySection("ИНВЕНТАРЬ"));

        x = 1225;
        y = 241;
        width = 73;
        height = 125;
        yOffset = 130;
        inventorySectionButtons.add(new InventorySectionButton(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), TextureRegister.textureModifyIconAll));
//        y += yOffset;
//        inventorySectionButtons.add(new InventorySectionButton(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), TextureRegister.textureModifyIconWeapons));
//        y += yOffset;
//        inventorySectionButtons.add(new InventorySectionButton(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), TextureRegister.textureModifyIconPistols));
//        y += yOffset;
//        inventorySectionButtons.add(new InventorySectionButton(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), TextureRegister.textureModifyIconKnifes));

        x = 1338;
        y = 323;
        width = 241;
        height = 122;
        float offsetX = 261;
        float offsetY = 141;
        float xBack = x;
        ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        MatrixInventory matrixInventory = clientMainPlayer.getMatrixInventory();

        int i=0;
        for(ItemStack itemStack : matrixInventory.getItems().values()) {
            Item item = itemStack.getItem();
            if(item instanceof ItemGunClient || item instanceof ItemMeleeWeaponClient) {
                if(i > 0) {
                    x += offsetX;
                    if(x > 1839) {
                        x = xBack;
                        y += offsetY;
                    }
                }
                inventorySlots.add(new GuiModifySlotGun(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), itemStack));
                i++;
            }
        }

        for(ItemStack itemStack : mc.thePlayer.inventory.mainInventory) {
            if(itemStack != null) {
                Item item = itemStack.getItem();
                if(item instanceof ItemGunClient || item instanceof ItemMeleeWeaponClient) {
                    if(i > 0) {
                        x += offsetX;
                        if (x > 1839) {
                            x = xBack;
                            y += offsetY;
                        }
                    }
                    inventorySlots.add(new GuiModifySlotGun(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), itemStack));
                    i++;
                }
            }
        }

        y += offsetY + 40;
        guiScroll.setScrollViewHeight(ScaleGui.get(798));
        guiScroll.setScrollTotalHeight(ScaleGui.get(y - startY));
    }

    private void selectGun(ItemStack currentItem) {
        //TODO: получение списка возможных модулей
        Item item = currentItem.getItem();
        if(item instanceof ItemGunClient) {
            ItemGunClient itemGun = (ItemGunClient) item;
            slotGun.setItemStack(currentItem);
            weaponPopup.setName(itemGun.getName());
            weaponPopup.setSecondName(itemGun.getSecondName());
            weaponPopup.setDescription(itemGun.getDescription());
            weaponPopup.setItemStats(ItemStatUtils.getWeaponDamageStats(itemGun));
            weaponPopup.setWeaponStats(ItemStatUtils.getWeaponStats(itemGun));
            weaponPopup.setTags(ItemStatUtils.getItemTags(itemGun));
            weaponPopup.setItemStatSmalls(ItemStatUtils.getWeaponMiscStats(itemGun));
            weaponPopup.setHeight(ScaleGui.get(494));
            modifySlots.clear();
            modifySlots.add(new GuiModifySlot(0,0,0,0,0,EnumModifySlotType.SIGHT,null));
            modifySlots.add(new GuiModifySlot(1,0,0,0,0,EnumModifySlotType.TRIGGER,null));
            modifySlots.add(new GuiModifySlot(2,0,0,0,0,EnumModifySlotType.SILENCER,null));
            modifySlots.add(new GuiModifySlot(3,0,0,0,0,EnumModifySlotType.BUTT,null));
            modifySlots.add(new GuiModifySlot(4,0,0,0,0,EnumModifySlotType.MAGAZINE,null));
            modifySlots.add(new GuiModifySlot(5,0,0,0,0,EnumModifySlotType.CASE,null));
            modifySlots.add(new GuiModifySlot(6,0,0,0,0,EnumModifySlotType.AMMO,null));
        } else if(item instanceof ItemMeleeWeaponClient) {
            ItemMeleeWeaponClient itemGun = (ItemMeleeWeaponClient) item;
            slotGun.setItemStack(currentItem);
            weaponPopup.setName(itemGun.getName());
            weaponPopup.setSecondName(itemGun.getSecondName());
            weaponPopup.setDescription(itemGun.getDescription());
//            weaponPopup.setItemStats(ItemStatUtils.getWeaponDamageStats(itemGun));
//            weaponPopup.setWeaponStats(ItemStatUtils.getWeaponStats(itemGun));
            weaponPopup.setTags(ItemStatUtils.getItemTags(itemGun));
//            weaponPopup.setItemStatSmalls(ItemStatUtils.getWeaponMiscStats(itemGun));
            weaponPopup.setHeight(ScaleGui.get(494));
            modifySlots.clear();
            modifySlots.add(new GuiModifySlot(0,0,0,0,0,EnumModifySlotType.SIGHT,null));
            modifySlots.add(new GuiModifySlot(1,0,0,0,0,EnumModifySlotType.TRIGGER,null));
            modifySlots.add(new GuiModifySlot(2,0,0,0,0,EnumModifySlotType.SILENCER,null));
            modifySlots.add(new GuiModifySlot(3,0,0,0,0,EnumModifySlotType.BUTT,null));
            modifySlots.add(new GuiModifySlot(4,0,0,0,0,EnumModifySlotType.MAGAZINE,null));
            modifySlots.add(new GuiModifySlot(5,0,0,0,0,EnumModifySlotType.CASE,null));
            modifySlots.add(new GuiModifySlot(6,0,0,0,0,EnumModifySlotType.AMMO,null));
        }


        float x = 59;
        float y = 163;
        float width = 310;
        float height = 105;
        float yOffset = 125;
        for(GuiModifySlot slot : modifySlots) {
            slot.setXPosition(ScaleGui.getCenterX(x));
            slot.setYPosition(ScaleGui.getCenterY(y));
            slot.setWidth(ScaleGui.get(width));
            slot.setHeight(ScaleGui.get(height));
            y += yOffset;
        }
    }

    private void setModifyToSlot(GuiModifySlot slot) {
        activeSlot.setModifyItem(slot.getModifyItem());
        subSlots.clear();
        SoundUtils.playGuiSound(SoundType.MODIFICATION_ADD);
    }

    private void getSubSlots() {
        subSlots.clear();
        EnumModifySlotType slotType = activeSlot.getSlotType();
        ModifyItem[] modificationsByType = getModificationsByType(slotType);
        float x = 408;
        float y = activeSlot.yPosition;
        float width = 310;
        float height = 105;
        float yOffset = ScaleGui.get(125);
        float maxY = ScaleGui.getCenterY(913);
        float currentY = y + yOffset * (modificationsByType.length - 1);
        if(currentY > maxY) {
            float diff = currentY - maxY;
            y -= diff;
        }

        for(ModifyItem modifyItem : modificationsByType) {
            subSlots.add(new GuiModifySlot(0, ScaleGui.getCenterX(x), y, ScaleGui.get(width), ScaleGui.get(height), slotType, modifyItem));
            y += yOffset;
        }
    }

    private ModifyItem[] getModificationsByType(EnumModifySlotType type) {
        List<ModifyItem> findedItems = new ArrayList<>();
        switch (type) {
            case SIGHT:
            case AMMO:
            case CASE:
            case BUTT:
            case MAGAZINE:
            case SILENCER:
            case TRIGGER:
//                findedItems.add(new ModifyItem(new ItemStack(Items.stick), new ModifyStat[]{new ModifyStat(EnumModifyStatType.ACCURACY, "32%")}));
//                findedItems.add(new ModifyItem(new ItemStack(Items.diamond), new ModifyStat[]{
//                        new ModifyStat(EnumModifyStatType.ACCURACY, "32%"),
//                        new ModifyStat(EnumModifyStatType.DURABILITY, "400")
//                }));
//                findedItems.add(new ModifyItem(new ItemStack(Items.emerald), new ModifyStat[]{
//                        new ModifyStat(EnumModifyStatType.ACCURACY, "32%"),
//                        new ModifyStat(EnumModifyStatType.DURABILITY, "400"),
//                        new ModifyStat(EnumModifyStatType.CLIP_SIZE, "30"),
//                }));
//                findedItems.add(new ModifyItem(new ItemStack(Items.leather), new ModifyStat[]{
//                        new ModifyStat(EnumModifyStatType.ACCURACY, "32%"),
//                        new ModifyStat(EnumModifyStatType.DURABILITY, "400"),
//                        new ModifyStat(EnumModifyStatType.CLIP_SIZE, "40"),
//                        new ModifyStat(EnumModifyStatType.KNOCKBACK, "15%"),
//                }));
        }

        return findedItems.toArray(new ModifyItem[0]);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 0 && !changeGun) {
            changeGun = true;
        }
    }

    private void drawConnections(int size) {
        GuiModifySlot slot = subSlots.get(0);
        glDisable(GL11.GL_TEXTURE_2D);
        GL11.glLineWidth(ScaleGui.get(2f));
        GL11.glColor4f(0.8f, 0.8f, 0.8f, 1.0f);
        GL11.glBegin(GL11.GL_LINES);
        float x = activeSlot.xPosition + activeSlot.width;
        float y = activeSlot.yPosition + activeSlot.height / 2f;
        float lineWidth = ScaleGui.get(39);
        if(size > 1) {
            float yOffset = ScaleGui.get(125);
            float halfLineWidth = lineWidth / 2f;
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x + halfLineWidth, y);
            x = slot.xPosition;
            y = slot.yPosition + slot.height / 2f;
            GL11.glVertex2f(x - halfLineWidth, y);
            GL11.glVertex2f(x - halfLineWidth, y + yOffset * (size - 1));
            for(int i=0;i<size; i++) {
                GL11.glVertex2f(x, y);
                GL11.glVertex2f(x - halfLineWidth, y);
                y += yOffset;
            }
        } else {
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x + lineWidth, y);
        }
        GL11.glEnd();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    private void drawSlots(int mouseX, int mouseY) {
        for(GuiModifySlot slot : modifySlots) {
            slot.drawButton(mouseX, mouseY);
        }
        int size = subSlots.size();
        if(size > 0) {
            drawConnections(size);
            for(GuiModifySlot sub : subSlots) {
                sub.drawButton(mouseX, mouseY);
            }
        }

    }

    private void renderGun() {
        ItemStack itemStack = slotGun.getItemStack();
        if(itemStack != null) {
            axisVector.x = 1;
            axisVector.y = 0;
            axisVector.z = 0;
            if(itemRotation.y > 0) {
                float speed = itemRotation.y * 0.1f;
                if(speed < 0.001f) speed = 0.001f;
                axisVector.w = speed;
                quaternionHelper.setFromAxisAngle(axisVector);
                Quaternion.mul(itemRotationQuaternion, quaternionHelper, itemRotationQuaternion);
                itemRotation.y -= speed;
            } else {
                float speed = itemRotation.y * 0.1f;
                if (speed > -0.001f) speed = -0.001f;
                axisVector.w = speed;
                quaternionHelper.setFromAxisAngle(axisVector);
                Quaternion.mul(itemRotationQuaternion, quaternionHelper, itemRotationQuaternion);
                itemRotation.y -= speed;
            }

            axisVector.x = 0;
            axisVector.y = 1;
            axisVector.z = 0;
            if(itemRotation.x > 0) {
                float speed = itemRotation.x * 0.1f;
                if(speed < 0.001f) speed = 0.001f;
                axisVector.w = speed;
                quaternionHelper.setFromAxisAngle(axisVector);
                Quaternion.mul(itemRotationQuaternion, quaternionHelper, itemRotationQuaternion);
                itemRotation.x -= speed;
            } else {
                float speed = itemRotation.x * 0.1f;
                if (speed > -0.001f) speed = -0.001f;
                axisVector.w = speed;
                quaternionHelper.setFromAxisAngle(axisVector);
                Quaternion.mul(itemRotationQuaternion, quaternionHelper, itemRotationQuaternion);
                itemRotation.x -= speed;
            }

            glPushMatrix();
            glEnable(GL11.GL_DEPTH_TEST);
            glEnable(GL11.GL_ALPHA_TEST);
            KrogenitShaders.forwardPBRDirectionalShader.enable();
            KrogenitShaders.forwardPBRDirectionalShader.setProjectionMatrix();
            float size = ScaleGui.get(400f);
            glGetFloat(GL11.GL_MODELVIEW_MATRIX, (FloatBuffer) matrixBuffer.position(0));
            modelView.load(matrixBuffer);
            helpVector.x = ScaleGui.screenCenterX;
            helpVector.y = ScaleGui.screenCenterY;
            helpVector.z = 1000f;
            modelView.translate(helpVector);
            MatrixUtils.toRotationMatrix(itemRotationQuaternion, modelView);
            helpVector.x = 0;
            helpVector.y = 1;
            helpVector.z = 0;
            modelView.rotate((float) Math.toRadians(90f), helpVector);
            helpVector.x = 1;
            helpVector.y = 0;
            helpVector.z = 0;
            modelView.rotate((float) Math.toRadians(180f), helpVector);
            helpVector.x = -size;
            helpVector.y = size;
            helpVector.z = size;
            modelView.scale(helpVector);
            matrixBuffer.clear();
            modelView.store(matrixBuffer);
            glLoadMatrix((FloatBuffer) matrixBuffer.position(0));
            KrogenitShaders.forwardPBRDirectionalShader.setDirectionLight(true);
            KrogenitShaders.forwardPBRDirectionalShader.setLightPos(10,-10,10f);
            KrogenitShaders.forwardPBRDirectionalShader.setLightColor(5f, 5f, 6f);
            glPushMatrix();
            glLoadIdentity();
            KrogenitShaders.forwardPBRDirectionalShader.setModelView();
            glPopMatrix();
            IItemRenderer itemRenderer = MinecraftForgeClient.getItemRenderer(itemStack, IItemRenderer.ItemRenderType.INVENTORY);
            if(itemRenderer instanceof AbstractItemGunRenderer) ((AbstractItemGunRenderer) itemRenderer).renderInModifyGui(itemStack);
            else if(itemRenderer instanceof AbstractMeleeWeaponRenderer) ((AbstractMeleeWeaponRenderer) itemRenderer).renderInModifyGui(itemStack);
            glPopMatrix();
            glDisable(GL11.GL_DEPTH_TEST);
            glDisable(GL11.GL_ALPHA_TEST);
            glClear(GL_DEPTH_BUFFER_BIT);
        }
    }

    private void drawItemPopup() {
        ItemStack itemStack = slotGun.getItemStack();
        if(itemStack != null) {
            float x = ScaleGui.getCenterX(1363);
            float y = ScaleGui.getCenterY(524);
            float width = ScaleGui.get(477);
            weaponPopup.setHeight(GuiDrawUtils.drawWeaponPopup(x, y, width, weaponPopup.getHeight(), weaponPopup.getName(), weaponPopup.getSecondName(), weaponPopup.getTags(),
                    weaponPopup.getDescription(), weaponPopup.getItemStats(), weaponPopup.getWeaponStats(), weaponPopup.getItemStatSmalls()));
        }
    }

    private void drawGunSelection(int mouseX, int mouseY) {
        glDisable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(0f, 0f, 0f, 0.7f);
        GuiDrawUtils.drawCenter(1298, 39, 582, 1001);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        guiScroll.drawScrollScaled(1855, 281, 727, 1018);
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeDevider, 1340, 240, 500, 2);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ТЕКУЩЕЕ", 1338, 91, 60 / 32f, 0xffffff);
        for (int i = 0; i < inventorySectionButtons.size(); i++) {
            inventorySectionButtons.get(i).draw(mouseX, mouseY, i == selectedInventorySection);
        }
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        int sx = (int) ScaleGui.getCenterX(1298);
        int sy = (int) (ScaleGui.screenHeight - ScaleGui.getCenterY(1040));
        int swidth = (int) ScaleGui.get(554);
        int sheight = (int) ScaleGui.get(799);
        GL11.glScissor(sx, sy, swidth, sheight);
        inventoryAABB.minX = sx; inventoryAABB.minY = this.height - sy - sheight;
        inventoryAABB.maxX = sx + swidth; inventoryAABB.maxY = this.height - sy;

        for(InventorySection section : inventorySections) {
            section.draw(ScaleGui.getCenterX(1340), ScaleGui.getCenterY(293) - guiScroll.getScrollAnim());
        }

        for(GuiModifySlotGun slot : inventorySlots) {
            slot.addYPosition(-guiScroll.getScrollAnim());
            slot.drawButton(mouseX, mouseY);
        }

        glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();
        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);
        renderGun();
        GuiDrawUtils.drawCenter(TextureRegister.textureModifyBorderTop, 80, 142, 1760, 143);
        GuiDrawUtils.drawCenter(TextureRegister.textureModifyBorderBot, 417, 940, 939, 78);
        GuiDrawUtils.drawCenter(TextureRegister.textureModifyRows, 1741, 296, 100, 210);
        GuiDrawUtils.drawCenter(TextureRegister.textureModifyBars, 975, 993, 317, 21);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "МОДИФИКАЦИИ ОРУЖИЯ", 82, 92, 60 / 32f, 0xffffff);
        drawButtons(mouseX, mouseY, partialTick);
        drawSlots(mouseX, mouseY);
        if(changeGun) drawGunSelection(mouseX, mouseY);
        slotGun.drawButton(mouseX, mouseY);
        drawItemPopup();
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        for(GuiModifySlot slot : modifySlots) {
            if(slot.mousePressed(mc, mouseX, mouseY)) {
                activeSlot = slot;
                getSubSlots();
                return;
            }
        }

        if(activeSlot != null) {
            for(GuiModifySlot slot : subSlots) {
                if(slot.mousePressed(mc, mouseX, mouseY)) {
                    setModifyToSlot(slot);
                    activeSlot = null;
                    return;
                }
            }
        }

        if(changeGun) {
            if(guiScroll.mouseClicked(mouseX, mouseY, mouseButton)) return;

            for(GuiModifySlotGun slot : inventorySlots) {
                if(inventoryAABB.intersectsWith(AxisAlignedBB.getBoundingBox(slot.xPosition, slot.yPosition, -10, slot.xPosition + slot.width, slot.yPosition + slot.height, 10))) {
                    if(slot.mousePressed(mc, mouseX, mouseY)) {
                        selectGun(slot.getItemStack());
                        changeGun = false;
                        return;
                    }
                }
            }

            for (int i = 0; i < inventorySectionButtons.size(); i++) {
                InventorySectionButton b = inventorySectionButtons.get(i);
                if(b.mouseClicked(mouseX, mouseY)) {
                    selectedInventorySection = i;
                    return;
                }
            }
        } else {
            if(!isRotatingItem) {
                isRotatingItem = true;
                mouseLastClick.x = mouseX;
                mouseLastClick.y = mouseY;
            }
        }

        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
        guiScroll.mouseMovedOrUp();
        isRotatingItem = false;
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
        if(changeGun) guiScroll.mouseClickMove(mouseX, mouseY);
        if(isRotatingItem) {
            float rotX = (mouseX - mouseLastClick.x);
            float rotY = (mouseY - mouseLastClick.y);
            itemRotation.x -= rotX / 100f;
            itemRotation.y += rotY / 100f;
            mouseLastClick.x = mouseX;
            mouseLastClick.y = mouseY;
        }
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        if(changeGun) {
            int d = Mouse.getEventDWheel();
            int mouseX = Mouse.getX();
            int mouseY = this.height - Mouse.getY() - 1;
            if (d != 0) {
                float x = ScaleGui.getCenterX(1298);
                float y = ScaleGui.getCenterY(39);
                float width = ScaleGui.get(582);
                float height = ScaleGui.get(1001);
                if(mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height) {
                    guiScroll.mouseScroll(d);
                }
            }
        }
    }

    @AllArgsConstructor
    public static class InventorySection {
        private final String name;

        public void draw(float x, float y) {
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, name, x, y, 60 / 32f, 0xffffff);
        }
    }

    @AllArgsConstructor
    public static class InventorySectionButton {
        private final float x, y, width, height;
        private final ResourceLocation texture;

        public void draw(int mouseX, int mouseY, boolean selected) {
            boolean isHovering = selected || isHovering(mouseX, mouseY);
            if(isHovering) GL11.glColor4f(0f, 0f, 0f, 0.7f);
            else GL11.glColor4f(0f, 0f, 0f, 0.5f);
            glDisable(GL11.GL_TEXTURE_2D);
            GuiDrawUtils.drawRect(x, y, width, height);

            if(selected) {
                GL11.glColor4f(1f, 1f, 1f, 1f);
                GuiDrawUtils.drawRect(x + ScaleGui.get(70), y + ScaleGui.get(15), ScaleGui.get(3), ScaleGui.get(95));
            }
            GL11.glEnable(GL11.GL_TEXTURE_2D);

            if(isHovering) GL11.glColor4f(1f, 1f, 1f, 1f);
            else GL11.glColor4f(0.75f, 0.75f, 0.75f, 1f);
            float size = ScaleGui.get(39);
            GuiDrawUtils.drawRect(texture, x + ScaleGui.get(20), y + ScaleGui.get(23), size, size);
        }

        private boolean isHovering(int mouseX, int mouseY) {
            return mouseX > x && mouseY > y && mouseX < x + width && mouseY < y + height;
        }

        public boolean mouseClicked(int mouseX, int mouseY) {
            return isHovering(mouseX, mouseY);
        }
    }
}
