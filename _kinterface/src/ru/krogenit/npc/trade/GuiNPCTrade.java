package ru.krogenit.npc.trade;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import noppes.npcs.client.gui.player.GuiNPCTrader;
import noppes.npcs.roles.RoleTrader;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.inventory.GuiMatrixSlot;
import ru.krogenit.inventory.popup.GuiPopupRenderer;
import ru.krogenit.npc.GuiNPCButtonSwitcher;
import ru.krogenit.npc.IInventoryWithDraggedSlot;
import ru.krogenit.npc.MatrixInventoryModule;
import ru.krogenit.npc.quest.GuiNPCQuests;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;
import ru.xlv.core.network.matrix.PacketMatrixInventoryTransactionItemMove;
import ru.xlv.core.network.matrix.PacketMatrixInventoryTransactionItemMultiMove;
import ru.xlv.core.network.matrix.PacketMatrixInventoryTransactionOpenClose;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.lwjgl.opengl.GL11.*;

public class GuiNPCTrade extends AbstractGuiScreenAdvanced implements IInventoryWithDraggedSlot, IGuiWithPopup {

    protected GuiMatrixSlot draggedSlot;
    protected float draggedSlotX, draggedSlotY;
    private final RoleTrader role;

    private final List<MatrixInventoryModule> modules = new ArrayList<>();
    private final MatrixInventory upInventoryBuffer = MatrixInventoryFactory.create(20, 60);
    private final MatrixInventory downInventoryBuffer = MatrixInventoryFactory.create(20, 60);

    private static final int PLAYER_INV_ID = 0;
    private static final int NPC_INV_ID = 1;
    private static final int BUY_BUFFER_ID = 2;
    private static final int SELL_BUFFER_ID = 3;

    private final Map<ItemStack, int[]> itemsPosFromPlayerInventory = new HashMap<>();
    private final Map<ItemStack, int[]> itemsPosFromNpcInventory = new HashMap<>();

    private GuiPopup popup;

    public GuiNPCTrade(GuiNPCTrader currentScreen) {
        super(ScaleGui.FULL_HD);
        this.role = currentScreen.getRole();
        for(int i=0;i<4;i++) {
            modules.add(new MatrixInventoryModule(this));
        }

        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryTransactionOpenClose(role.npc.getEntityId(), true));
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        initButtons();
        initInventories();
        if(popup != null) popup.initGui();
    }

    private void initButtons() {
        float xOffset = 74;
        float x = 1362;
        float y = 57;
        float buttonWidth = 47;
        float buttonHeight = 47;
        GuiNPCButtonSwitcher b = new GuiNPCButtonSwitcher(0, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonDialog);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(1, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonTrade);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(2, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonSave);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        buttonList.add(b);
        x += xOffset;
        b = new GuiNPCButtonSwitcher(3, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        b.setTexture(TextureRegister.textureNPCButtonLocked);
        b.setMaskTexture(TextureRegister.textureNPCButtonMask);
        b.enabled = false;
        buttonList.add(b);

        buttonWidth = 171;
        buttonHeight = 40;
        x = 765;
        y = 942;
        GuiButtonAnimated button = new GuiButtonAnimated(4, ScaleGui.getCenterX(x),  ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПРИНЯТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        x += 220;
        button = new GuiButtonAnimated(5, ScaleGui.getCenterX(x),  ScaleGui.getCenterY(y), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonCancel);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    private void initInventories() {
        ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        MatrixInventory matrixInventory = clientMainPlayer.getMatrixInventory();
        float x = 1303;
        float y = 237;
        float cellWidth = 57;
        float cellHeight = 57;
        MatrixInventoryModule module = modules.get(PLAYER_INV_ID);
        module.setInventoryPositionAndSize(x, y, cellWidth, cellHeight, 571, 736, matrixInventory.getHeight());
        module.setInventory(matrixInventory);
        x = 39;
        module = modules.get(NPC_INV_ID);
        module.setInventoryPositionAndSize(x, y, cellWidth, cellHeight, 571, 736, clientMainPlayer.getTransactionMatrixInventory().getHeight());
        module.setInventory(clientMainPlayer.getTransactionMatrixInventory());

        x = 676;
        y = 266;
        cellWidth = 28.55f;
        cellHeight = 28.55f;
        module = modules.get(BUY_BUFFER_ID);
        module.setInventoryPositionAndSize(x, y, cellWidth, cellHeight,571, 230, upInventoryBuffer.getHeight());
        module.setInventory(upInventoryBuffer);
        y = 570;
        module = modules.get(SELL_BUFFER_ID);
        module.setInventoryPositionAndSize(x, y, cellWidth, cellHeight, 571, 230, downInventoryBuffer.getHeight());
        module.setInventory(downInventoryBuffer);
    }

    private void drawMoney() {
        String platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        String credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        float fs = 50 / 32f;
        float slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        float slplatina = FontType.Marske.getFontContainer().width(platina) * fs;

        float creditWidth = 22;
        float creditHeight = 22;
        float platinaWidth = 22;
        float platinaHeight = 22;
        float x = 1873;
        float y = 220;
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, platina, x, y - 6, fs, 0xDFDEDE);
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, credits, x - platinaWidth * 2f - slplatina, y - 6, fs, 0xDFDEDE);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x - slcredits - slplatina - platinaWidth * 3f, y, creditWidth, creditHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x - slplatina - platinaWidth, y, platinaWidth, platinaHeight);

        x = 55;
        y = 220;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x, y, creditWidth, creditHeight);
        credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        GuiDrawUtils.drawStringCenter(FontType.Marske, credits, x + creditWidth, y - 6, fs, 0xDFDEDE);
        slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x + creditWidth + 24 + slcredits, y, platinaWidth, platinaHeight);
        platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        GuiDrawUtils.drawStringCenter(FontType.Marske, platina, x + platinaWidth + creditWidth + 24 + slcredits, y - 6, fs, 0xDFDEDE);



        x = 824;
        y = 248;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x, y, creditWidth, creditHeight);
        credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        GuiDrawUtils.drawStringCenter(FontType.Marske, credits, x + creditWidth, y - 6, fs, 0xDFDEDE);
        slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x + creditWidth + 24 + slcredits, y, platinaWidth, platinaHeight);
        platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        GuiDrawUtils.drawStringCenter(FontType.Marske, platina, x + platinaWidth + creditWidth + 24 + slcredits, y - 6, fs, 0xDFDEDE);

        platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        slplatina = FontType.Marske.getFontContainer().width(platina) * fs;
        x = 1080;
        y = 552;
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, platina, x, y - 6, fs, 0xDFDEDE);
        GuiDrawUtils.drawRightStringCenter(FontType.Marske, credits, x - platinaWidth * 2f - slplatina, y - 6, fs, 0xDFDEDE);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x - slcredits - slplatina - platinaWidth * 3f, y, creditWidth, creditHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x - slplatina - platinaWidth, y, platinaWidth, platinaHeight);

        creditWidth = 32;
        creditHeight = 32;
        platinaWidth = 32;
        platinaHeight = 32;
        x = 960;
        y = 857;
        fs = 70 / 32f;
        credits = "" + XlvsMainMod.INSTANCE.getClientMainPlayer().getCredits();
        platina = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getPlatinum();
        slcredits = FontType.Marske.getFontContainer().width(credits) * fs;
        slplatina = FontType.Marske.getFontContainer().width(platina) * fs;
        float l = ScaleGui.get(platinaWidth + creditWidth + 32 + slcredits + slplatina);
        glPushMatrix();
        glTranslatef(-l / 2f, 0f, 0f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconCredit, x, y, creditWidth, creditHeight);
        GuiDrawUtils.drawStringCenter(FontType.Marske, credits, x + creditWidth - 2, y - 8, fs, 0xDFDEDE);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvIconPlatina, x + creditWidth + 24 + slcredits, y, platinaWidth, platinaHeight);
        GuiDrawUtils.drawStringCenter(FontType.Marske, platina, x + platinaWidth + creditWidth + 22 + slcredits, y - 8, fs, 0xDFDEDE);
        glPopMatrix();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();

        glEnable(GL_BLEND);
        glAlphaFunc(GL_GREATER, 0.0001f);
        glDisable(GL_ALPHA_TEST);

        drawMoney();
        drawButtons(mouseX, mouseY, partialTick);

        float x = 36;
        float y = 64;
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, role.npc.display.name.toUpperCase(), x, y, fs, 0xffffff);
        y = 95;
        fs = 30 /  32f;
//        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ТОРГОВЕЦ ОРУЖИЕМ", x, y, fs, 0xffffff);

        x = 960;
        y = 67;
        fs = 60 / 32f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "ТОРГОВЛЯ", x, y, fs, 0xffffff);

        x = 1106;
        y = 236;
        float width = 141;
        float height = 25;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeArrowsRight, x, y, width, height);
        x = 676;
        y = 540;
        width = 141;
        height = 25;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeArrowsLeft, x, y, width, height);

        x = 39;
        y = 124;
        width = 1835;
        height = 2;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCDialogBorderBot, x, y, width, height);
        x = 1231;
        y = 35;
        width = 643;
        height = 90;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCBorderBotIconsCover, x, y, width, height);
        x = 676;
        y = 908;
        width = 571;
        height = 2;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeDevider, x, y, width, height);
        x = 676;
        y = 833;
        width = 71;
        height = 52;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeArrowLeft, x, y, width, height);
        x = 1174;
        y = 833;
        width = 71;
        height = 52;
        GuiDrawUtils.drawCenter(TextureRegister.textureNPCTradeArrowRight, x, y, width, height);

        x = 1873;
        y = 184;
        fs = 60 / 32f;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrLight, "ВАШ ИНВЕНТАРЬ", x, y, fs, 0x626262);

        x = 40;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ИНВЕНТАРЬ ТОРГОВЦА", x, y, fs, 0x626262);

        x = 676;
        y = 244;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "КУПИТЬ", x, y, fs, 0x626262);

        x = 1246;
        y = 549;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrLight, "ПРОДАТЬ", x, y, fs, 0x626262);

        x = 1872;
        y = 1000;
        fs = 18 / 32f;
        GuiDrawUtils.drawRightStringCenter(FontType.DeadSpace, "LoLorem ipsumLorem ipsumLorem ipsumLorem ipsumrem ipsum", x, y, fs, 0x626262);
        y += 11;
        GuiDrawUtils.drawRightStringCenter(FontType.DeadSpace, "dolor sit aLorem ipsumLorem ipsummet, consec", x, y, fs, 0x626262);
        y += 11;
        GuiDrawUtils.drawRightStringCenter(FontType.DeadSpace, "tetur adipisicing", x, y, fs, 0x626262);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        for(MatrixInventoryModule module : modules) {
            module.drawModule(mouseX, mouseY);
        }

        boolean popuped = false;
        for(MatrixInventoryModule module : modules) {
            if(module.drawPopups(mouseX, mouseY))
                popuped = true;
        }

        if(!popuped) {
            GuiPopupRenderer.updateBackAnimation();
        }

        if(draggedSlot != null) {
            if(draggedSlot.getItemStack() != null) {
                int w = MatrixInventoryUtils.getInvMatrixWidth(draggedSlot.getItemStack()) + 1;
                int h = MatrixInventoryUtils.getInvMatrixHeight(draggedSlot.getItemStack()) + 1;
                float offX = draggedSlot.getWidth() / (float)w;
                float offY = draggedSlot.getHeight() / (float)h;
                glPushMatrix();
                glTranslatef(-draggedSlot.getX() + mouseX - offX, -draggedSlot.getY() + mouseY - offY, 0);
                draggedSlot.renderItem(mouseX, mouseY);
                glPopMatrix();
            }
        }

        if(popup != null) popup.drawScreen(mouseX, mouseY, partialTick);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 4) {
            tryBuyItems();
        } else if(guiButton.id == 5) {
            Minecraft.getMinecraft().displayGuiScreen(null);
        } else if(guiButton.id == 2) {
            mc.displayGuiScreen(new GuiNPCQuests(role.npc));
        }
    }

    private void tryBuyItems() {
        if(itemsPosFromNpcInventory.size() > 0 || itemsPosFromPlayerInventory.size() > 0) {
            CompletableFuture.runAsync(() -> {
                AtomicBoolean atomicBooleanFirst = new AtomicBoolean();
                AtomicBoolean atomicBooleanSecond = new AtomicBoolean();
                XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketMatrixInventoryTransactionItemMultiMove(true, false, itemsPosFromPlayerInventory.values())).thenAcceptSync(success -> {
                    atomicBooleanFirst.set(success);
                });
                XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketMatrixInventoryTransactionItemMultiMove(false, true, itemsPosFromNpcInventory.values())).thenAcceptSync(atomicBooleanSecond::set);
                long currentTime = System.currentTimeMillis();

                //noinspection ConditionalBreakInInfiniteLoop
                while(true) {
                    if(atomicBooleanFirst.get() && atomicBooleanSecond.get()) {
                        break;
                    }

                    if(System.currentTimeMillis() - currentTime > 2000) {
                        break;
                    }
                }

                XlvsCore.INSTANCE.runUsingMainThread(() -> {
                    if(atomicBooleanFirst.get() && atomicBooleanSecond.get()) {
                        popup = new GuiPopup(this, "УСПЕШНО", "", GuiPopup.green);
                        MatrixInventory matrixInventory = modules.get(BUY_BUFFER_ID).getMatrixInventory();
                        matrixInventory.clear();
                        matrixInventory.setNeedSync(true);
                        MatrixInventory matrixInventory1 = modules.get(SELL_BUFFER_ID).getMatrixInventory();
                        matrixInventory1.clear();
                        matrixInventory1.setNeedSync(true);
                        itemsPosFromPlayerInventory.clear();
                        itemsPosFromNpcInventory.clear();
                    } else {
                        popup = new GuiPopup(this, "ПРОИЗОШЛА ОШИБКА", "", GuiPopup.red);
                    }
                });
            });
        }
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        int mouseX = Mouse.getX();
        int mouseY = this.height - Mouse.getY() - 1;
        if (d != 0) {
            for(MatrixInventoryModule module : modules) {
                module.mouseScroll(mouseX, mouseY, d);
            }
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (popup != null) {
            popup.mouseClicked(mouseX, mouseY, mouseButton);
            return;
        }
        super.mouseClicked(mouseX, mouseY, mouseButton);
        for(MatrixInventoryModule module : modules) {
            module.mouseClicked(mouseX, mouseY, mouseButton, draggedSlot);
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
        for(MatrixInventoryModule module : modules) {
            module.mouseMovedOrUp();
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
        for(MatrixInventoryModule module : modules) {
            module.mouseClickMove(mouseY);
        }
    }

    @Override
    public void keyTyped(char character, int key) {
        if(key == Keyboard.KEY_R && draggedSlot != null && draggedSlot.getItemStack() != null) {
            MatrixInventoryUtils.setInvMatrixRotation(draggedSlot.getItemStack(), MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack()) + 1);
            int w = MatrixInventoryUtils.getInvMatrixWidth(draggedSlot.getItemStack());
            int h = MatrixInventoryUtils.getInvMatrixHeight(draggedSlot.getItemStack());
            draggedSlot.setWidth(w * draggedSlot.getWidth() / (float) h);
            draggedSlot.setHeight(h * draggedSlot.getHeight() / (float) w);
            return;
        }

        super.keyTyped(character, key);
    }

    @Override
    public GuiMatrixSlot getDraggedSlot() {
        return draggedSlot;
    }

    @Override
    public void setDraggedSlotXY(float x, float y) {
        this.draggedSlotX = x;
        this.draggedSlotY = y;
    }

    @Override
    public void setDraggedSlot(GuiMatrixSlot draggedSlot) {
        this.draggedSlot = draggedSlot;
    }

    @Override
    public float getDraggedSlotX() {
        return draggedSlotX;
    }

    @Override
    public float getDraggedSlotY() {
        return draggedSlotY;
    }

    @Override
    public boolean canTakeItem(MatrixInventory matrixInventory, GuiMatrixSlot slot) {
        return true;
    }

    @Override
    public void putItem(int x, int y, MatrixInventory toInventory) {
        MatrixInventory fromMatrix = draggedSlot.getMatrixInventory();
        MatrixInventoryUtils.moveItem(fromMatrix, toInventory, (int)draggedSlotX, (int)draggedSlotY, x, y, MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack()));

        if(toInventory != fromMatrix) {
            if (fromMatrix == modules.get(PLAYER_INV_ID).getMatrixInventory()) {
                itemsPosFromPlayerInventory.put(draggedSlot.getItemStack(), new int[]{(int) draggedSlotX, (int) draggedSlotY});
            } else if (fromMatrix == modules.get(SELL_BUFFER_ID).getMatrixInventory()) {
                itemsPosFromPlayerInventory.remove(draggedSlot.getItemStack());
            } else if (fromMatrix == modules.get(NPC_INV_ID).getMatrixInventory()) {
                itemsPosFromNpcInventory.put(draggedSlot.getItemStack(), new int[]{(int) draggedSlotX, (int) draggedSlotY});
            } else if (fromMatrix == modules.get(BUY_BUFFER_ID).getMatrixInventory()) {
                itemsPosFromNpcInventory.remove(draggedSlot.getItemStack());
            }
        } else if(fromMatrix == modules.get(PLAYER_INV_ID).getMatrixInventory() && toInventory == modules.get(NPC_INV_ID).getMatrixInventory()) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryTransactionItemMove(
                    (int)draggedSlotX,
                    (int)draggedSlotY,
                    x,
                    y,
                    MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack()),
                    true,
                    true
            ));
        }
    }

    @Override
    public boolean canPutItem(MatrixInventory toInventory, GuiMatrixSlot slot) {
        MatrixInventory fromInventory = slot.getMatrixInventory();
        if(toInventory == fromInventory) return true;

        if(fromInventory == modules.get(NPC_INV_ID).getMatrixInventory()) {
            if(toInventory == modules.get(BUY_BUFFER_ID).getMatrixInventory()) {
                return true;
            }
        } else if(fromInventory == modules.get(PLAYER_INV_ID).getMatrixInventory()) {
            if(toInventory == modules.get(SELL_BUFFER_ID).getMatrixInventory()) {
                return true;
            }
        } else if(fromInventory == modules.get(BUY_BUFFER_ID).getMatrixInventory()) {
            if(toInventory == modules.get(NPC_INV_ID).getMatrixInventory()) {
                return true;
            }
        } else if(fromInventory == modules.get(SELL_BUFFER_ID).getMatrixInventory()) {
            if(toInventory == modules.get(PLAYER_INV_ID).getMatrixInventory()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isPlayerInventory(MatrixInventory matrixInventory) {
        return matrixInventory == modules.get(0).getMatrixInventory();
    }

    @Override
    public void popupAction(GuiButton button) {

    }

    @Override
    public void setPopup(GuiPopup popup) {
        this.popup = popup;
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryTransactionOpenClose(role.npc.getEntityId(), false));
    }
}
