package ru.krogenit.inventory;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.inventory.popup.GuiPopupRenderer;
import ru.krogenit.shaders.KrogenitShaders;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;
import ru.xlv.core.common.item.IItemUsable;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.gui.overlay.DropdownElement;
import ru.xlv.core.gui.overlay.DropdownMenu;
import ru.xlv.core.network.matrix.PacketMatrixInventoryDropItem;
import ru.xlv.core.network.matrix.PacketMatrixInventoryItemMove;
import ru.xlv.core.network.matrix.PacketMatrixInventorySpecItemMove;
import ru.xlv.core.network.matrix.PacketMatrixInventorySpecSpecItemMove;
import ru.xlv.core.util.Flex;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.lwjgl.opengl.GL11.*;

public abstract class GuiMatrixInventoryBase extends AbstractGuiScreenAdvanced {

    protected float scroll, scrollAnim, lastScroll;
    protected float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    protected float scrollX, scrollY, scrollWidth, scrollHeight;
    protected boolean mouseScrolling;
    protected float mouseStartY;
    protected AxisAlignedBB inventoryAABB;

    protected List<GuiMatrixSlot> slots = new ArrayList<>();
    protected Map<MatrixInventory.SlotType, GuiMatrixSlot> nonMatrixSlots = new HashMap<>();

    protected MatrixInventory mainMatrixInventory;
    protected GuiMatrixSlot draggedSlot;

    protected DropdownMenu itemDropdownMenu;

    protected float draggedSlotX, draggedSlotY;

    protected int matrixWidth, matrixHeight;

    protected float startX, startY;
    protected float cellWidth, cellHeight;

    public GuiMatrixInventoryBase() {
        super(ScaleGui.FULL_HD);
    }

    @Override
    public void initGui() {
        super.initGui();
        itemDropdownMenu = null;
    }

    protected void setInventoryPositionAndSize(float x, float y, float cellWidth, float cellHeight) {
        this.cellWidth = cellWidth;
        this.cellHeight = cellHeight;
        this.startX = x;
        this.startY = y;
    }

    protected void setInventory(MatrixInventory matrixInventory) {
        mainMatrixInventory = matrixInventory;
        matrixWidth = matrixInventory.getWidth();
        matrixHeight = matrixInventory.getHeight();
        slots.clear();
        matrixInventory.getItems().forEach((integer, itemStack) -> {
            if(draggedSlot != null && draggedSlot.getItemStack() == itemStack) {
                return;
            }
            int[] xy = matrixInventory.getPosOfItem(integer);
            if (xy != null) {
                int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
                int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
                GuiMatrixSlot slot = new GuiMatrixSlot(startX + xy[0] * cellWidth, startY + xy[1] * cellHeight, cellWidth * w, cellHeight * h, itemStack, matrixInventory);
                slots.add(slot);
            }
        });
        nonMatrixSlots.clear();

        for (MatrixInventory.SlotType value : MatrixInventory.SlotType.values()) {
            ItemStack itemStack = mc.thePlayer.inventory.getStackInSlot(value.getAssociatedSlotIndex());
            if(draggedSlot != null && draggedSlot.getItemStack() == itemStack) {
                itemStack = null;
            }
            if(value.ordinal() > MatrixInventory.SlotType.MELEE_WEAPON.ordinal()) {
                addSlot(new GuiHotSlot(0, 0, 0, 0, itemStack, value, mainMatrixInventory), value, nonMatrixSlots, 0, 0, 1f);
            } else addSlot(new GuiMatrixSlotSpecial(0, 0, 0, 0, itemStack, value, mainMatrixInventory), value, nonMatrixSlots, 0, 0, 1f);
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        if(mainMatrixInventory.isNeedSync()) {
            setInventory(mainMatrixInventory);
            mainMatrixInventory.setNeedSync(false);
        }
        GL11.glEnable(GL_SCISSOR_TEST);
        float x = ScaleGui.getCenterX(1304);
        float y = ScaleGui.getCenterY(1080 - 880);
        float width = ScaleGui.get(1304);
        float height = ScaleGui.get(734);
        glScissor((int)x, (int)y,(int)width , (int)height);
        boolean hovered = mouseX > startX && mouseY > startY && mouseX < startX + width && mouseY < startY + height;
        glPushMatrix();
        glTranslatef(0f, 0f, 10f);
        for(GuiMatrixSlot slot : slots) {
            slot.addYPosition(-ScaleGui.get(scrollAnim));
            slot.render(mouseX, mouseY, hovered);
        }
        GL11.glDisable(GL_SCISSOR_TEST);
        inventoryAABB = AxisAlignedBB.getBoundingBox(x, this.height - y - height, -100, x + width, this.height - y,100);
        KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
        glPushMatrix();
        glLoadIdentity();
        KrogenitShaders.forwardPBRDirectionalShaderOld.setModelView();
        glPopMatrix();
        KrogenitShaders.forwardPBRDirectionalShaderOld.setLightColor(1f, 1f, 1f);
        KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
        nonMatrixSlots.forEach((slotType, slot) -> slot.render(mouseX, mouseY, true));
        if(draggedSlot != null) {
            if(draggedSlot.getItemStack() != null) {
                renderSlotPreview(mouseX, mouseY);
                glPushMatrix();
                glTranslatef(-draggedSlot.getX() + mouseX - cellWidth / 2f, -draggedSlot.getY() + mouseY - cellHeight / 2f, 100f);
                draggedSlot.renderItem(mouseX, mouseY);
                glPopMatrix();
            }
        }
        AtomicBoolean popuped = new AtomicBoolean(false);
        if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            slots.forEach(slot -> {
                if (inventoryAABB.intersectsWith(AxisAlignedBB.getBoundingBox(slot.getX() + 1f, slot.getY() + 1f, -10, slot.getX() + slot.getWidth() - 1f, slot.getY() + slot.getHeight() - 1f, 10)))
                    if (slot.isHovered(mouseX, mouseY)) {
                        slot.renderPopup(mouseX, mouseY);
                        popuped.set(true);
                    }
            });
            nonMatrixSlots.forEach((slotType, slot) -> {
                if (slot.isHovered(mouseX, mouseY)) {
                    slot.renderPopup(mouseX, mouseY);
                    popuped.set(true);
                }
            });
        }
        glPopMatrix();


        if(!popuped.get()) {
            GuiPopupRenderer.updateBackAnimation();
        }

        if (itemDropdownMenu != null) {
            itemDropdownMenu.render(this, mouseX, mouseY, partialTick);
        }
    }

    private final Vector2f helpVector = new Vector2f();

    private void renderSlotPreview(int mouseX, int mouseY) {
        float x = mouseX;
        float y = mouseY;
        float invSizeX = startX + cellWidth * matrixWidth;
        float invSizeY = startY + cellHeight * matrixHeight;
        helpVector.x = -1;
        helpVector.y = -1;
        if(x > startX && y > startY && x <= invSizeX && y <= invSizeY) {
            glEnable(GL_SCISSOR_TEST);
            float sx = ScaleGui.getCenterX(1304);
            float sy = ScaleGui.getCenterY(1080 - 880);
            float swidth = ScaleGui.get(1304);
            float sheight = ScaleGui.get(734);
            glScissor((int)sx, (int)sy,(int)swidth , (int)sheight);
            x = startX;
            y = startY - ScaleGui.get(scrollAnim);
            while(x + cellWidth < mouseX) {
                x += cellWidth;
            }
            while(y + cellHeight < mouseY) {
                y += cellHeight;
            }
            if(x + draggedSlot.getWidth() > invSizeX) {
                x = invSizeX - draggedSlot.getWidth();
            }
            if(y + draggedSlot.getHeight() + ScaleGui.get(scrollAnim) > invSizeY) {
                y = invSizeY - draggedSlot.getHeight() - ScaleGui.get(scrollAnim);
            }

            AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(x, y, -100, x + draggedSlot.getWidth(), y + draggedSlot.getHeight(), 100);
            boolean isCollided = false;
            for(GuiMatrixSlot slot : slots) {
                if(aabb.intersectsWith(AxisAlignedBB.getBoundingBox(slot.getX() + 1f, slot.getY() + 1f, -10, slot.getX() + slot.getWidth() - 1f, slot.getY() + slot.getHeight() - 1f, 10))) {
                    isCollided = true;
                    break;
                }
            }

            if(!isCollided) {
                helpVector.x = x;
                helpVector.y = y;
            }

            GL11.glDisable(GL_TEXTURE_2D);
            if(isCollided) GuiDrawUtils.drawRect(x, y, draggedSlot.getWidth(), draggedSlot.getHeight(), 0.5f, 0.2f, 0.2f, 0.5f);
            else GuiDrawUtils.drawRect(x, y, draggedSlot.getWidth(), draggedSlot.getHeight(), 0.2f, 0.5f, 0.2f, 0.5f);
            GL11.glEnable(GL_TEXTURE_2D);
            glDisable(GL_SCISSOR_TEST);
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (itemDropdownMenu != null) {
            if (itemDropdownMenu.mouseClick(mouseX, mouseY)) {
                return;
            }
            itemDropdownMenu = null;
        }
        if (mouseButton == 0) {
            for (Object object : this.buttonList) {
                GuiButton guibutton = (GuiButton) object;
                if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                    guibutton.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(guibutton);
                    return;
                }
            }
            if(draggedSlot == null) {
                handleDragItem(mouseX, mouseY);
            } else {
                handlePutItem(mouseX, mouseY);
            }

            if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            }
        } else if(mouseButton == 1) {
            openContextMenu(mouseX, mouseY);
        }
    }

    protected void openContextMenu(int mx, int my) {
        GuiMatrixSlot clickedSlot = getClickedSlot(mx, my);
        if (clickedSlot != null) {
            itemDropdownMenu = new DropdownMenu(mx, my, 100, 20);
            int x = (int) ((mx - startX) / cellWidth);
            int y = (int) ((my - startY + ScaleGui.get(scrollAnim)) / cellHeight);
            itemDropdownMenu.addElement(new DropdownElement("Выбросить", () -> {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryDropItem(x, y));
                itemDropdownMenu = null;
            }));
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = ScaleGui.get(scrollTextureHeight) / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }
            }
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
        mouseScrolling = false;
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        int mouseX = Mouse.getX();
        int mouseY = this.height - Mouse.getY() - 1;
        if (d != 0) {
            if(mouseX >= startX && mouseY >= startY && mouseX < startX + cellWidth * 10 && mouseY < startY + cellHeight * 13) {
                float diff = scrollTotalHeight - scrollViewHeight;
                if(diff > 0) {
                    scroll -= d / 2f;
                    if(scroll < 0) {
                        scroll = 0;
                    } else if(scroll > diff) {
                        scroll = diff;
                    }
                }
            }
        }
    }

    public static void addSlot(GuiMatrixSlot slot, MatrixInventory.SlotType slotType, Map<MatrixInventory.SlotType, GuiMatrixSlot> nonMatrixSlots, float addX, float addY, float scale) {
        ResourceLocation slotTexture = slot.getSlotTexture();
        ResourceLocation bg = TextureRegister.textureInvSlotSpecBg;
        ResourceLocation gradient = TextureRegister.textureInvSlotSpecGradient;
        ResourceLocation frame = TextureRegister.textureInvSlotSpecFrame;

        float iconWidth = 0;
        float iconHeight = 0;
        float x = -9999;
        float y = -9999;

        if(slotType == MatrixInventory.SlotType.HEAD) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(1084 + addX);
            y = ScaleGui.getCenterY(257 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_HEAD;
        } else if(slotType == MatrixInventory.SlotType.BODY) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(1084 + addX);
            y = ScaleGui.getCenterY(394 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_BODY;
        } else if(slotType == MatrixInventory.SlotType.BRACERS) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(1084 + addX);
            y = ScaleGui.getCenterY(531 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_ARMS;
        } else if(slotType == MatrixInventory.SlotType.LEGS) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(1084 + addX);
            y = ScaleGui.getCenterY(668 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_LEGS;
        } else if(slotType == MatrixInventory.SlotType.FEET) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(1084 + addX);
            y = ScaleGui.getCenterY(805 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_BOOTS;
        } else if(slotType == MatrixInventory.SlotType.BACKPACK) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(494 + addX);
            y = ScaleGui.getCenterY(348 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_BACKPACK;
        } else if(slotType == MatrixInventory.SlotType.MELEE_WEAPON) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(494 + addX);
            y = ScaleGui.getCenterY(520 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_MELEE;
        } else if(slotType == MatrixInventory.SlotType.ADD_WEAPON) {
            iconWidth = ScaleGui.get(130 * scale);
            iconHeight = ScaleGui.get(129 * scale);
            x = ScaleGui.getCenterX(633 + addX);
            y = ScaleGui.getCenterY(520 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_PISTOL;
        } else if(slotType == MatrixInventory.SlotType.MAIN_WEAPON) {
            iconWidth = ScaleGui.get(326 * scale);
            iconHeight = ScaleGui.get(165 * scale);
            x = ScaleGui.getCenterX(494 + addX);
            y = ScaleGui.getCenterY(660 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_MAIN;
            bg = TextureRegister.TEXTURE_INV_SLOT_WPN_BG;
            gradient = TextureRegister.TEXTURE_INV_SLOT_WPN_GRADIENT;
            frame = TextureRegister.TEXTURE_INV_SLOT_WPN_FRAME;
        } else if(slotType == MatrixInventory.SlotType.SECOND_WEAPON) {
            iconWidth = ScaleGui.get(326 * scale);
            iconHeight = ScaleGui.get(165 * scale);
            x = ScaleGui.getCenterX(494 + addX);
            y = ScaleGui.getCenterY(846 + addY);
            slotTexture = TextureRegister.TEXTURE_INV_SLOT_MAIN;
            bg = TextureRegister.TEXTURE_INV_SLOT_WPN_BG;
            gradient = TextureRegister.TEXTURE_INV_SLOT_WPN_GRADIENT;
            frame = TextureRegister.TEXTURE_INV_SLOT_WPN_FRAME;
        } else if(slotType.ordinal() >= MatrixInventory.SlotType.HOT_SLOT0.ordinal()) {
            float offsetX = slotType.ordinal() - MatrixInventory.SlotType.HOT_SLOT0.ordinal();
            iconWidth = ScaleGui.get(76 * scale);
            iconHeight = ScaleGui.get(77 * scale);
            x = ScaleGui.getCenterX(1303 + 92 * offsetX + addX);
            y = ScaleGui.getCenterY(937 + addY);
        }

        if(slotType.ordinal() < MatrixInventory.SlotType.HOT_SLOT0.ordinal()) {
            GuiMatrixSlotSpecial s = new GuiMatrixSlotSpecial(0,0,0,0, slot.getItemStack(), slotType, slot.matrixInventory);
            s.setGradient(gradient);
            s.setFrame(frame);
            s.setBg(bg);
            slot = s;
        } else {
            slot = new GuiHotSlot(0,0,0,0,slot.getItemStack(), slotType, slot.matrixInventory);
        }

        slot.setX(x);
        slot.setY(y);
        slot.setWidth(iconWidth);
        slot.setHeight(iconHeight);
        slot.setSlotTexture(slotTexture);

        nonMatrixSlots.put(slotType, slot);
    }

    protected boolean handlePutItem(int mx, int my) {
        if (putItem(mx, my, draggedSlot)) {
            int x = MathHelper.floor_float((helpVector.x - startX)) / (int)cellWidth;
            int y = MathHelper.floor_float((helpVector.y - startY + ScaleGui.get(scrollAnim))) / (int)cellHeight;
            if(draggedSlot.getSlotType() != null) {
                Minecraft.getMinecraft().thePlayer.inventory.setInventorySlotContents(draggedSlot.getSlotType().getAssociatedSlotIndex(), null);
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventorySpecItemMove(true, draggedSlot.getSlotType(), x, y));
                if(draggedSlot.getSlotType().isArmorSlot()) {
                    SoundUtils.playGuiSound(SoundType.INV_ARMOR_PUT_DOWN);
                } else if(draggedSlot.getSlotType().isWeaponSlot()) {
                    if(draggedSlot.getItemStack().getItem() instanceof ItemBase) {
                        List<EnumItemTag> itemTags = ((ItemBase) draggedSlot.getItemStack().getItem()).getItemTags();
                        if (Flex.containsAny(itemTags, EnumItemTag.MACHINE_GUN, EnumItemTag.SNIPER)) {
                            SoundUtils.playGuiSound(SoundType.INV_HEAVY_WEAPON_PICK_DOWN);
                        } else if(Flex.containsAny(itemTags, EnumItemTag.SHOTGUN, EnumItemTag.AUTOMATIC)) {
                            SoundUtils.playGuiSound(SoundType.INV_MEDIUM_WEAPON_PICK_DOWN);
                        } else if(Flex.containsAny(itemTags, EnumItemTag.PISTOL, EnumItemTag.REVOLVER, EnumItemTag.SMG)) {
                            SoundUtils.playGuiSound(SoundType.INV_LIGHT_WEAPON_PICK_DOWN);
                        } else if(itemTags.contains(EnumItemTag.MELEE)) {
                            SoundUtils.playGuiSound(SoundType.INV_MELEE_PICK_DOWN);
                        }
                    }
                } else if(draggedSlot.getItemStack().getItem() instanceof IItemUsable) {
                    SoundUtils.playGuiSound(SoundType.INV_HEALING_ITEM_PICK_DOWN);
                } else {
                    SoundUtils.playGuiSound(SoundType.INV_ITEM_PICK_DOWN);
                }
            } else {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventoryItemMove((int)draggedSlotX, (int)(draggedSlotY), x,y, MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack())));
                if(draggedSlot.getItemStack().getItem() instanceof ItemArmor) {
                    SoundUtils.playGuiSound(SoundType.INV_ARMOR_PUT_DOWN);
                } else if(draggedSlot.getItemStack().getItem() instanceof ItemGun) {
                    List<EnumItemTag> itemTags = ((ItemBase) draggedSlot.getItemStack().getItem()).getItemTags();
                    if (Flex.containsAny(itemTags, EnumItemTag.MACHINE_GUN, EnumItemTag.SNIPER)) {
                        SoundUtils.playGuiSound(SoundType.INV_HEAVY_WEAPON_PICK_DOWN);
                    } else if(Flex.containsAny(itemTags, EnumItemTag.SHOTGUN, EnumItemTag.AUTOMATIC)) {
                        SoundUtils.playGuiSound(SoundType.INV_MEDIUM_WEAPON_PICK_DOWN);
                    } else if(Flex.containsAny(itemTags, EnumItemTag.PISTOL, EnumItemTag.REVOLVER, EnumItemTag.SMG)) {
                        SoundUtils.playGuiSound(SoundType.INV_LIGHT_WEAPON_PICK_DOWN);
                    } else if(itemTags.contains(EnumItemTag.MELEE)) {
                        SoundUtils.playGuiSound(SoundType.INV_MELEE_PICK_DOWN);
                    }
                } else if(draggedSlot.getItemStack().getItem() instanceof IItemUsable) {
                    SoundUtils.playGuiSound(SoundType.INV_HEALING_ITEM_PICK_DOWN);
                } else {
                    SoundUtils.playGuiSound(SoundType.INV_ITEM_PICK_DOWN);
                }
            }
            draggedSlot = null;
            draggedSlotX = -1;
            draggedSlotY = -1;
            return true;
        } else {
            MatrixInventory.SlotType slotType = canPutSpecItem(mx, my);
            if(slotType != null) {
                if (canPutItemToSlot(slotType, draggedSlot.getItemStack())) {
                    if (draggedSlot.getSlotType() != null) {
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventorySpecSpecItemMove(draggedSlot.getSlotType(), slotType));
                    } else {
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketMatrixInventorySpecItemMove(false, slotType, (int) draggedSlotX, (int) draggedSlotY));
                    }
                    MatrixInventory.SlotType slotType1 = draggedSlot.getSlotType() != null ? draggedSlot.getSlotType() : slotType;
                    if(slotType1.isArmorSlot()) {
                        SoundUtils.playGuiSound(SoundType.INV_ARMOR_PUT_ON);
                    } else if(slotType1.isWeaponSlot()) {
                        if(draggedSlot.getItemStack().getItem() instanceof ItemBase) {
                            List<EnumItemTag> itemTags = ((ItemBase) draggedSlot.getItemStack().getItem()).getItemTags();
                            if (Flex.containsAny(itemTags, EnumItemTag.MACHINE_GUN, EnumItemTag.SNIPER)) {
                                SoundUtils.playGuiSound(SoundType.INV_HEAVY_WEAPON_PICK_DOWN);
                            } else if(Flex.containsAny(itemTags, EnumItemTag.SHOTGUN, EnumItemTag.AUTOMATIC)) {
                                SoundUtils.playGuiSound(SoundType.INV_MEDIUM_WEAPON_PICK_DOWN);
                            } else if(Flex.containsAny(itemTags, EnumItemTag.PISTOL, EnumItemTag.REVOLVER, EnumItemTag.SMG)) {
                                SoundUtils.playGuiSound(SoundType.INV_LIGHT_WEAPON_PICK_DOWN);
                            } else if(itemTags.contains(EnumItemTag.MELEE)) {
                                SoundUtils.playGuiSound(SoundType.INV_MELEE_PICK_DOWN);
                            }
                        }
                    } else if(draggedSlot.getItemStack().getItem() instanceof IItemUsable) {
                        SoundUtils.playGuiSound(SoundType.INV_HEALING_ITEM_PICK_DOWN);
                    } else {
                        SoundUtils.playGuiSound(SoundType.INV_ITEM_PICK_DOWN);
                    }
                    if(draggedSlot.getSlotType() != null) Minecraft.getMinecraft().thePlayer.inventory.setInventorySlotContents(draggedSlot.getSlotType().getAssociatedSlotIndex(), null);
                    Minecraft.getMinecraft().thePlayer.inventory.setInventorySlotContents(slotType.getAssociatedSlotIndex(), draggedSlot.getItemStack());
                    addSlot(draggedSlot, slotType, nonMatrixSlots, 0, 0, 1f);
                    draggedSlotX = -1;
                    draggedSlotY = -1;
                    draggedSlot = null;
                    return true;
                }
            }
        }

        return false;
    }

    private boolean canPutItemToSlot(MatrixInventory.SlotType slotType, ItemStack itemStack) {
        if(slotType.ordinal() >= MatrixInventory.SlotType.HOT_SLOT0.ordinal()) {
            return !(itemStack.getItem() instanceof ItemArmor) && !(itemStack.getItem() instanceof ItemGun);
        } else {
            if(itemStack.getItem() instanceof ItemArmor) {
                ItemArmor itemArmor = (ItemArmor) itemStack.getItem();
                return itemArmor.isValidArmor(itemStack, 0, mc.thePlayer, slotType.getAssociatedSlotIndex());
            } else if(itemStack.getItem() instanceof ItemGun) {
                ItemGun itemGun = (ItemGun) itemStack.getItem();
                for(MatrixInventory.SlotType slotType1 : itemGun.getSlotTypes()) {
                    if(slotType1 == slotType) return true;
                }

                return false;
            } else if(itemStack.getItem() instanceof ItemMeleeWeaponClient) {
                ItemMeleeWeaponClient itemMelee = (ItemMeleeWeaponClient) itemStack.getItem();
                for(MatrixInventory.SlotType slotType1 : itemMelee.getSlotTypes()) {
                    if(slotType1 == slotType) return true;
                }

                return false;
            } else {
                return false;
            }
        }
    }

    protected void handleDragItem(int mx, int my) {
        GuiMatrixSlot slot = getDragItemSlot(mx, my);
        if(slot != null && slot.getItemStack() != null) {
            draggedSlotX = (mx - startX) / cellWidth;
            draggedSlotY = (my - startY + ScaleGui.get(scrollAnim)) / cellHeight;
            draggedSlot = slot;
            if(draggedSlot.getItemStack().getItem() instanceof ItemArmor) {
                SoundUtils.playGuiSound(SoundType.INV_ARMOR_PUT_ON);
            } else if(draggedSlot.getItemStack().getItem() instanceof ItemGun) {
                List<EnumItemTag> itemTags = ((ItemBase) draggedSlot.getItemStack().getItem()).getItemTags();
                if (Flex.containsAny(itemTags, EnumItemTag.MACHINE_GUN, EnumItemTag.SNIPER)) {
                    SoundUtils.playGuiSound(SoundType.INV_HEAVY_WEAPON_PICK_UP);
                } else if(Flex.containsAny(itemTags, EnumItemTag.SHOTGUN, EnumItemTag.AUTOMATIC)) {
                    SoundUtils.playGuiSound(SoundType.INV_MEDIUM_WEAPON_PICK_UP);
                } else if(Flex.containsAny(itemTags, EnumItemTag.PISTOL, EnumItemTag.REVOLVER, EnumItemTag.SMG)) {
                    SoundUtils.playGuiSound(SoundType.INV_LIGHT_WEAPON_PICK_UP);
                } else if(itemTags.contains(EnumItemTag.MELEE)) {
                    SoundUtils.playGuiSound(SoundType.INV_MELEE_PICK_UP);
                }
            } else if(draggedSlot.getItemStack().getItem() instanceof IItemUsable) {
                SoundUtils.playGuiSound(SoundType.INV_HEALING_ITEM_PICK_UP);
            } else {
                SoundUtils.playGuiSound(SoundType.INV_ITEM_PICK_UP);
            }
        }
    }

    @Override
    public void keyTyped(char character, int key) {
        if(key == Keyboard.KEY_E) {
            mc.displayGuiScreen(null);
            return;
        }
        if(key == Keyboard.KEY_R && draggedSlot != null && draggedSlot.getItemStack() != null) {
            MatrixInventoryUtils.setInvMatrixRotation(draggedSlot.getItemStack(), MatrixInventoryUtils.getInvMatrixRotation(draggedSlot.getItemStack()) + 1);
            int w = MatrixInventoryUtils.getInvMatrixWidth(draggedSlot.getItemStack());
            int h = MatrixInventoryUtils.getInvMatrixHeight(draggedSlot.getItemStack());
            draggedSlot.setWidth(w * cellWidth);
            draggedSlot.setHeight(h * cellHeight);
            return;
        }
        super.keyTyped(character, key);
    }

    protected GuiMatrixSlot getDragItemSlot(int mx, int my) {
        if(inventoryAABB.isVecInside(Vec3.createVectorHelper(mx, my, 0))) {
            Iterator<GuiMatrixSlot> iterator = slots.iterator();
            while (iterator.hasNext()) {
                GuiMatrixSlot slot = iterator.next();
                if(slot.clickSlot(mx, my)) {
                    iterator.remove();
                    return slot;
                }
            }
        }

        Iterator<MatrixInventory.SlotType> iterator1 = nonMatrixSlots.keySet().iterator();
        while (iterator1.hasNext()) {
            MatrixInventory.SlotType slotType = iterator1.next();
            GuiMatrixSlot slot = nonMatrixSlots.get(slotType);
            if(slot.clickSlot(mx, my)) {
                iterator1.remove();
                int w = MatrixInventoryUtils.getInvMatrixWidth(slot.getItemStack());
                int h = MatrixInventoryUtils.getInvMatrixHeight(slot.getItemStack());
                addSlot(slot.copyEmpty(), slotType, nonMatrixSlots, 0, 0, 1f);
                return new GuiMatrixSlotSpecial(slot.x, slot.y, cellWidth * w, cellHeight * h, slot.getItemStack(), slotType, mainMatrixInventory);
            }
        }
        return null;
    }

    protected boolean putItem(int mx, int my, GuiMatrixSlot slot) {
        if(slot.getItemStack() != null) {
            if(inventoryAABB.isVecInside(Vec3.createVectorHelper(mx, my, 0))) return helpVector.x != -1;
            else return false;
        }
        if(mx >= startX && mx <= startX + cellWidth * matrixWidth && my >= startY && my <= startY + cellHeight * matrixHeight) {
            for (float i = startX; i < startX + cellWidth * matrixWidth; i += cellWidth) {
                if (mx >= i && mx < i + cellWidth) {
                    slot.setX(i);
                    break;
                }
            }

            for (float i = startY - ScaleGui.get(scrollAnim); i < startY - ScaleGui.get(scrollAnim) + cellHeight * matrixHeight; i += cellHeight) {
                if (my >= i && my < i + cellHeight) {
                    slot.setY(i + ScaleGui.get(scrollAnim));
                    break;
                }
            }

            float w = MatrixInventoryUtils.getInvMatrixWidth(slot.getItemStack());
            float h = MatrixInventoryUtils.getInvMatrixHeight(slot.getItemStack());
            slot.setWidth(w * cellWidth);
            slot.setHeight(h * cellHeight);
            slots.add(slot);
            return true;
        }
        return false;
    }

    protected MatrixInventory.SlotType canPutSpecItem(int mx, int my) {
        for (MatrixInventory.SlotType slotType : nonMatrixSlots.keySet()) {
            GuiMatrixSlot slot1 = nonMatrixSlots.get(slotType);
            if(slot1.clickSlot(mx, my)) {
                if(slot1.getItemStack() == null) {
                    return slotType;
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    protected GuiMatrixSlot getClickedSlot(int mx, int my) {
        for (GuiMatrixSlot slot : slots) {
            if(slot.clickSlot(mx, my)) {
                return slot;
            }
        }
        return null;
    }
}
