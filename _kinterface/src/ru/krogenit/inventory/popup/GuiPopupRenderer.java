package ru.krogenit.inventory.popup;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.krogenit.armor.item.ItemArmorByPartClient;
import ru.krogenit.guns.item.ItemGunClient;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.guns.item.ammo.ItemAmmoClient;
import ru.krogenit.utils.AnimationHelper;

public class GuiPopupRenderer {

    public static float animation;
    public static float popupTotalHeight;

    private static final GuiItemPopup defaultPopup = new GuiItemPopup();
    private static final GuiGunPopup gunPopup = new GuiGunPopup();
    private static final GuiArmorPopup armorPopup = new GuiArmorPopup();
    private static final GuiAmmoPopup ammoPopup = new GuiAmmoPopup();
    private static final GuiMeleePopup meleePopup = new GuiMeleePopup();

    public static void updateBackAnimation() {
        animation = AnimationHelper.updateSlowEndAnim(animation, 0.0f, -0.2f, -0.0002f);
    }

    public static void drawPopup(int mx, int my, ItemStack itemStack) {
        animation = AnimationHelper.updateSlowEndAnim(animation, 1.0f, 0.2f, 0.0002f);
        Item item = itemStack.getItem();
        if(item instanceof ItemGunClient) {
            gunPopup.draw(mx, my, itemStack);
        } else if(item instanceof ItemArmorByPartClient) {
            armorPopup.draw(mx, my, itemStack);
        } else if(item instanceof ItemAmmoClient) {
            ammoPopup.draw(mx, my, itemStack);
        } else if(item instanceof ItemMeleeWeaponClient) {
            meleePopup.draw(mx, my, itemStack);
        } else {
            defaultPopup.draw(mx, my, itemStack);
        }
    }
}
