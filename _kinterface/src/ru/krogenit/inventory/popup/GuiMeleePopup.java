package ru.krogenit.inventory.popup;

import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.util.ColorUtils;
import ru.krogenit.util.DecimalUtils;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.customfont.FontType;

import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiMeleePopup extends GuiItemPopup {

    @Override
    public void draw(int mx, int my, ItemStack itemStack) {
        ItemMeleeWeaponClient itemGun = (ItemMeleeWeaponClient) itemStack.getItem();
        float xOffset = ScaleGui.get(12f);
        float yOffset = ScaleGui.get(12f);
        float startX = mx + xOffset;
        float startY = my + yOffset;
        float popupWidthNoAnim = ScaleGui.get(377f);
        float popupWidth = popupWidthNoAnim * GuiPopupRenderer.animation;
        float popupHeight = GuiPopupRenderer.popupTotalHeight;

        if(startX + popupWidth > mc.displayWidth) {
            startX = mc.displayWidth - popupWidth;
        }
        if(startY + popupHeight > mc.displayHeight) {
            startY = mc.displayHeight - popupHeight;
        }

        Vector3f itemColor = ColorUtils.getColorByRarity((itemStack.getItem()));
        GuiDrawUtils.renderTooltipItem(startX, startY, startX + popupWidth, startY + popupHeight, ScaleGui.get(48f), new Vector4f(itemColor.x/2f, itemColor.y/2f, itemColor.z/2f, 0.97f * GuiPopupRenderer.animation));

        glEnable(GL_SCISSOR_TEST);
        glScissor((int)startX, mc.displayHeight - ((int)startY + (int) popupHeight), (int) popupWidth, (int) popupHeight);
        RenderItem instance = RenderItem.getInstance();
        instance.zLevel += 500f;
        GuiDrawUtils.renderItem(itemStack, 1.5f, startX + popupWidth / 4.5f, startY + ScaleGui.get(20f), 0, 0, ScaleGui.get(150f), ScaleGui.get(150f), 0f);
        instance.zLevel -= 500f;

        float y = startY + ScaleGui.get(230f);
        float x = startX + ScaleGui.get(28f);
        float fs = 62 / 32f;
        String displayName = itemGun.getDisplayName();
        if(displayName == null) displayName = itemGun.getName();
        y += GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, displayName.toUpperCase(), x, y, fs, ScaleGui.get(325f), -1, 0xffffff, EnumStringRenderType.DEFAULT);
        y -= ScaleGui.get(5f);
        fs = 1f;
        String secondName = itemGun.getSecondName();
        if(secondName == null) secondName = "";
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, secondName.toUpperCase(), x, y, fs, ColorUtils.getHexColorByRarity(itemStack.getItem()));
        y += ScaleGui.get(29f);
        List<EnumItemTag> itemTags = itemGun.getItemTags();
        fs = 24 / 32f;
        float width = ScaleGui.get(16f);
        float height = ScaleGui.get(25f);
        float xBack = x;
        for(EnumItemTag tag : itemTags) {
            Vector3f color = new Vector3f(0.10f, 0f, 0.1f);
            String s = tag.getDisplayName().toUpperCase();
            float slotWidth = width + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * ScaleGui.get(fs);
            if(x + slotWidth > startX + popupWidthNoAnim) {
                x = xBack;
                y += ScaleGui.get(30f);
            }
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glColor4f(color.x, color.y ,color.z, 0.95f);
            GuiDrawUtils.renderToolTipSkillType(x, y, x + slotWidth, y + height, mc.displayHeight / 150f);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, s, x + ScaleGui.get(8f), y + ScaleGui.get(11f), fs, 0x7e7e7e);
            x += slotWidth + ScaleGui.get(8f);
        }
        GL11.glColor4f(1f,1f,1f,1f);
        width = ScaleGui.get(265f);
        y += ScaleGui.get(48f);
        x = xBack;
        GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, width, 1);
        y += ScaleGui.get(21f);
        fs = 26 / 32f;
        y += GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, itemGun.getDescription(),
                x, y, fs, ScaleGui.get(325f), -1, 0xffffff, EnumStringRenderType.DEFAULT);
        y += ScaleGui.get(9f);
        GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, width, 1);
        y += ScaleGui.get(16f);
        width = ScaleGui.get(48f);
        height = ScaleGui.get(48f);
        GuiDrawUtils.drawRect(TextureRegister.textureInvIconCircleStatEnergy, x , y, width, height);

        float totalDamage = 0f;
        for(int i = CharacterAttributeType.BALLISTIC_DAMAGE.ordinal(); i < CharacterAttributeType.PRICKLE_DAMAGE.ordinal(); i++) {
            CharacterAttributeType type = CharacterAttributeType.values()[i];
            totalDamage += itemGun.getCharacterAttributeBoost(type);
        }

        fs = 96 / 32f;
        float fontScaleLittleGray = 28 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrBlack, DecimalUtils.getFormattedStringWithOneDigit(totalDamage), x + ScaleGui.get(68f), y + ScaleGui.get(16f), fs, 0xffffff);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "Общий Урон", x + ScaleGui.get(68f), y + ScaleGui.get(51f), fontScaleLittleGray, 0x7e7e7e);

        y += ScaleGui.get(65f);
        GuiDrawUtils.drawRect(TextureRegister.textureInvIconCircleStatEnergy, x , y, width, height);
        fs = 96 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrBlack, "" + DecimalUtils.getFormattedStringWithOneDigit(itemGun.getCharacterAttributeBoost(CharacterAttributeType.PENETRATION)), x + ScaleGui.get(68f), y + ScaleGui.get(18f), fs, 0xffffff);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "Пробиваемость", x + ScaleGui.get(68f), y + ScaleGui.get(53f), fontScaleLittleGray, 0x7e7e7e);

        y += ScaleGui.get(74f);
        width = ScaleGui.get(265f);
        GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x , y, width, 1);
        y += ScaleGui.get(25f);
        width = ScaleGui.get(31f);
        height = ScaleGui.get(31f);
        fs = 40 / 32f;
        for(int i = CharacterAttributeType.BALLISTIC_DAMAGE.ordinal(); i <= CharacterAttributeType.PRICKLE_DAMAGE.ordinal();i++) {
            CharacterAttributeType characterAttributeType = CharacterAttributeType.values()[i];
            double value = itemGun.getCharacterAttributeBoost(characterAttributeType);
            if(value > 0) {
                ResourceLocation texture = null;
                switch (characterAttributeType) {
                    case ENERGY_DAMAGE: texture = TextureRegister.textureInvIconEnergy; break;
                    case THERMAL_DAMAGE: texture = TextureRegister.textureInvIconHeat; break;
                    case FIRE_DAMAGE: texture = TextureRegister.textureInvIconFire; break;
                    case ELECTRIC_DAMAGE: texture = TextureRegister.textureInvIconElectricity; break;
                    case TOXIC_DAMAGE: texture = TextureRegister.textureInvIconToxins; break;
                    case RADIATION_DAMAGE: texture = TextureRegister.textureInvIconRadiation; break;
                    case EXPLOSION_DAMAGE: texture = TextureRegister.textureInvIconExplosion; break;
                    default: texture = TextureRegister.textureInvIconEnergy; break;
                }
                GuiDrawUtils.drawRect(texture, x , y, width, height);
                String att = characterAttributeType.getDisplayName();
                float fsAttName = 32 / 32f;
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, att, x + ScaleGui.get(41f),y + ScaleGui.get(14f), fsAttName, 0xffffff);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrMedium, DecimalUtils.getFormattedStringWithOneDigit(value), x + ScaleGui.get(48f) + FontType.HelveticaNeueCyrLight.getFontContainer().width(att) * ScaleGui.get(fsAttName),y + ScaleGui.get(14f), fs, 0xffffff);
                y += ScaleGui.get(33f);
            }
        }
        glDisable(GL_SCISSOR_TEST);
        GuiPopupRenderer.popupTotalHeight = y - startY + ScaleGui.get(24f);
    }
}
