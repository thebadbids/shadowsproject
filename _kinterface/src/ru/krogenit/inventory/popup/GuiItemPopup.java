package ru.krogenit.inventory.popup;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.util.ColorUtils;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.customfont.FontType;

import static org.lwjgl.opengl.GL11.*;

public class GuiItemPopup {

    protected static final Minecraft mc = Minecraft.getMinecraft();

    public void draw(int mx, int my, ItemStack itemStack) {
        float xOffset = ScaleGui.get(12f);
        float yOffset = ScaleGui.get(12f);
        float startX = mx + xOffset;
        float startY = my + yOffset;
        float popupWidthNoAnim = ScaleGui.get(377f);
        float popupWidth = popupWidthNoAnim * GuiPopupRenderer.animation;
        float popupHeight = GuiPopupRenderer.popupTotalHeight;

        if(startX + popupWidth > mc.displayWidth) {
            startX = mc.displayWidth - popupWidth;
        }
        if(startY + popupHeight > mc.displayHeight) {
            startY = mc.displayHeight - popupHeight;
        }

        Item item = itemStack.getItem();
        Vector3f itemColor = ColorUtils.getColorByRarity(item);
        GuiDrawUtils.renderTooltipItem(startX, startY, startX + popupWidth, startY + popupHeight, ScaleGui.get(48f), new Vector4f(itemColor.x/2f, itemColor.y/2f, itemColor.z/2f, 0.97f * GuiPopupRenderer.animation));

        glEnable(GL_SCISSOR_TEST);
        glScissor((int)startX, mc.displayHeight - ((int)startY + (int) popupHeight), (int) popupWidth, (int) popupHeight);
        RenderItem instance = RenderItem.getInstance();
        instance.zLevel += 500f;
        GuiDrawUtils.renderItem(itemStack, 1.5f, startX + popupWidth / 4.5f, startY + ScaleGui.get(20f), 0, 0, ScaleGui.get(150f), ScaleGui.get(150f), 0f);
        instance.zLevel -= 500f;

        float y = startY + ScaleGui.get(230f);
        float x = startX + ScaleGui.get(28f);
        float fs = 62 / 32f;

        String itemName = itemStack.getDisplayName().toUpperCase();
        if(item instanceof ItemBase) {
            itemName = ((ItemBase) item).getDisplayName();
            if(itemName != null) itemName = itemName.toUpperCase();
        }

        y += GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, itemName, x, y, fs, ScaleGui.get(325f), -1, 0xffffff, EnumStringRenderType.DEFAULT);
        y -= ScaleGui.get(5f);

        if(item instanceof ItemBase) {
            fs = 26 / 32f;
            String description = ((ItemBase) item).getDescription();
            if(description != null && description.length() > 0) {
                float width = ScaleGui.get(265f);
                y += GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, description,
                        x, y, fs, ScaleGui.get(325f), -1, 0xffffff, EnumStringRenderType.DEFAULT);
                y += ScaleGui.get(9f);
                GuiDrawUtils.drawRect(TextureRegister.textureInvToolTipBorder, x, y, width, 1);
                y += ScaleGui.get(21f);
            }
        }

        y += ScaleGui.get(29f);
        glDisable(GL_SCISSOR_TEST);
        GuiPopupRenderer.popupTotalHeight = y - startY + ScaleGui.get(24f);
    }
}
