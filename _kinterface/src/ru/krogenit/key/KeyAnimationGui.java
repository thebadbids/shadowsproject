package ru.krogenit.key;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.MinecraftForgeClient;
import ru.krogenit.animator.GuiAnimation;
import ru.krogenit.client.key.AbstractKey;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;
import ru.krogenit.guns.render.ICustomizableRenderer;

public class KeyAnimationGui extends AbstractKey {
    private boolean keyDown = false;
    private boolean keyUp = true;
    private final Minecraft mc = Minecraft.getMinecraft();

    public KeyAnimationGui(KeyBinding keyBindings) {
        super(keyBindings);
    }

    @Override
    public void keyDown() {
        EntityPlayer p = mc.thePlayer;
        if (mc.currentScreen == null && !keyDown && p != null && mc.thePlayer.capabilities.isCreativeMode) {
            ItemStack item = p.getCurrentEquippedItem();
            if (item != null) {
                if(item.getItem() instanceof ItemGun) {
                    AbstractItemGunRenderer render = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(item, ItemRenderType.EQUIPPED_FIRST_PERSON);
                    mc.displayGuiScreen(new GuiAnimation(render));
                } else if(item.getItem() instanceof ItemMeleeWeaponClient) {
                    AbstractMeleeWeaponRenderer render = (AbstractMeleeWeaponRenderer) MinecraftForgeClient.getItemRenderer(item, ItemRenderType.EQUIPPED_FIRST_PERSON);
                    mc.displayGuiScreen(new GuiAnimation(render));
                } else {
                    IItemRenderer itemRenderer = MinecraftForgeClient.getItemRenderer(item, ItemRenderType.FIRST_PERSON_MAP);
                    if(itemRenderer instanceof ICustomizableRenderer) {
                        mc.displayGuiScreen(new GuiAnimation((ICustomizableRenderer) itemRenderer));
                    }
                }

            }
        }
    }

    @Override
    public void keyUp() {
        if (!keyUp) {
            keyDown = false;
            keyUp = true;
        }
    }
}
