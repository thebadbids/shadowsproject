package ru.krogenit.pda.gui.windows.achievements;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;

@AllArgsConstructor
@Getter
@Setter
public class Achievement {
    private EnumAchievementType type;
    private String achievementName;
    private boolean hasProgressBar;
    private int currentProgress, maxProgress;
    private boolean achieved;
    private ResourceLocation texture;
}
