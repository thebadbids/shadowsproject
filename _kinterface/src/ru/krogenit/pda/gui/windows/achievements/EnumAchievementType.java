package ru.krogenit.pda.gui.windows.achievements;

public enum EnumAchievementType {
    BRONZE, SILVER, GOLD;
}
