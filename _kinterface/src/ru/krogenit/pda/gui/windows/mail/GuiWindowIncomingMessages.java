package ru.krogenit.pda.gui.windows.mail;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.AxisAlignedBB;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.*;
import ru.krogenit.shop.field.GuiSearchField;
import ru.xlv.core.XlvsCore;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.handle.PostHandler;
import ru.xlv.post.network.PacketPostDelete;
import ru.xlv.post.network.PacketPostSync;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiWindowIncomingMessages extends GuiWindow implements IGuiSearch, IGuiSortable, IGuiWindowWithSubActions {

    protected GuiSearchField searchField;
    protected int outgoing, incoming;

    protected final List<GuiButtonMail> mails = new ArrayList<>();
    protected final List<GuiButtonMail> filteredResults = new ArrayList<>();
    private final PostHandler postHandler = XlvsPostMod.INSTANCE.getPostHandler();

    public GuiWindowIncomingMessages(GuiPda pda) {
        super(EnumWindowType.INCOMING_MESSAGES, pda, 743, 854);
        setHeader("ВХОДЯЩИЕ ПИСЬМА");
        setLeftCornerDesc("FILE №89065012_CODE_MAIL");
        setBackgroundTexture(TextureRegister.texturePDAWinMailBg);
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostSync()).thenAcceptSync(result -> {
            if (result.isSuccess()) {
                postHandler.syncPost(result.getPostObjectList());
                refreshPoints();
            }
        });
    }

    @Override
    public void initGui() {
        super.initGui();
        initButtons();
        searchField = new GuiSearchField(ScaleGui.get(45), ScaleGui.get(150),ScaleGui.get(278), ScaleGui.get(37), ScaleGui.get(28 / 32f), "Поиск...", this);
        initColumnButtons();
        Keyboard.enableRepeatEvents(true);
        refreshPoints();
    }

    protected void initColumnButtons() {
        float x = ScaleGui.get(93);
        float y = ScaleGui.get(220);
        float buttonWidth = 194 - 38;
        float buttonHeight = 30;
        GuiWinButtonColumn b = new GuiWinButtonColumn(4, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Отправитель", this);
        buttonList.add(b);
        buttonWidth = 218;
        x += ScaleGui.get(204 - 38);
        b = new GuiWinButtonColumn(5, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Тема", this);
        buttonList.add(b);
        buttonWidth = 139;
        x += ScaleGui.get(228);
        b = new GuiWinButtonColumn(6, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Дата", this);
        buttonList.add(b);
    }

    protected void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(326);
        float y = ScaleGui.get(70);
        GuiButtonAnimated button = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАПИСАТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        x += ScaleGui.get(178);
        button = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИСХОДЯЩИЕ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
            return;
        } else if(b.id == 3) {
            pda.showOrCreateWindow(EnumWindowType.OUTGOING_MESSAGES, new GuiWindowOutgoingMessages(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_OUTBOX, true);
            return;
        }
        super.actionPerformed(b);
    }

    protected void refreshPoints() {
        mails.clear();
        filteredResults.clear();
        initPlayers();
        filteredResults.addAll(mails);
        updateValuesPosition();
    }

    protected void initPlayers() {
        float buttonWidth = ScaleGui.get(619);
        float buttonHeight = ScaleGui.get(37);

        int id = 1;
        for(PostObject post : postHandler.getIncomingPost()) {
            GuiButtonMail mail = new GuiButtonMail(id, 0, 0, buttonWidth, buttonHeight, this,
                    post.getSender(), post.getTitle(), post.getCreationTimeMills(), post.getText(), post.getUuid());
            mails.add(mail);
            id++;
        }

        incoming = postHandler.getIncomingPost().size();
        outgoing = postHandler.getOutgoingPost().size();
    }

    protected void updateValuesPosition() {
        float x = ScaleGui.get(44);
        float y = ScaleGui.get(250);
        float startY = y;
        float yOffset = ScaleGui.get(42);
        int id = 0;
        for (GuiButtonMail p : filteredResults) {
            p.id = id; p.setXPosition(x); p.setYPosition(y);
            y += yOffset;
            id++;
        }

        guiScroll.setScrollViewHeight(ScaleGui.get(577));
        guiScroll.setScrollTotalHeight(y - startY);
    }

    @Override
    public void subAction(GuiWinButtonWithSubs button, int id) {
        GuiButtonMail mail = (GuiButtonMail) button;
        if(id == -1) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_READ, new GuiWindowReadMessage(pda,mail.getTarget(), mail.getSubject(),mail.getDate(), mail.getText(), false));
        } else if(id == 0) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostDelete(mail.getUuid())).thenAcceptSync(result -> {
                if(result.isSuccess()) {
                    mails.remove(button);
                    filteredResults.remove(button);
                    updateValuesPosition();
                } else {
                    pda.setPopup(new GuiPopup(pda, "НЕ УДАЛОСЬ УДАЛИТЬ СООБЩЕНИЕ", result.getResponseMessage(), GuiPopup.red));
                }
            });
        }
    }

    @Override
    public void search(String value) {
        filteredResults.clear();

        for(GuiButtonMail b : mails) {
            if(StringUtils.containsIgnoreCase(b.getTarget(), value)) filteredResults.add(b);
        }

        updateValuesPosition();
    }

    @Override
    public void sort(int id, EnumSortType state) {
        for(GuiButton guiButton : buttonList) {
            if(guiButton.id != id) {
                if(guiButton instanceof GuiWinButtonColumn) {
                    GuiWinButtonColumn b = (GuiWinButtonColumn) guiButton;
                    b.resetSort();
                }
            }
        }

        if(state == EnumSortType.NONE) {
            filteredResults.clear();
            filteredResults.addAll(mails);
        } else {
            switch (id) {
                case 4:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparing(GuiButtonMail::getTarget));
                    else filteredResults.sort(Comparator.comparing(GuiButtonMail::getTarget).reversed());
                    break;
                case 5:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparing(GuiButtonMail::getSubject));
                    else filteredResults.sort(Comparator.comparing(GuiButtonMail::getSubject).reversed());
                    break;
                case 6:
                    if(state == EnumSortType.ASCENDING) filteredResults.sort(Comparator.comparing(GuiButtonMail::getDate));
                    else filteredResults.sort(Comparator.comparing(GuiButtonMail::getDate).reversed());
                    break;
            }
        }

        updateValuesPosition();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawScroll();
        drawBackgroundThings();
        drawSearchField(mouseX, mouseY);
        drawPlayers(mouseX, mouseY);
    }

    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(128), ScaleGui.get(620), ScaleGui.get(2));
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(208), ScaleGui.get(620), ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(14);
        float stringPositiveYOffset = ScaleGui.get(10);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ВХОДЯЩИЕ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, "" + incoming, x + stringXOffset + fontContainer.width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ИСХОДЯЩИЕ: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, outgoing + "", x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        fs = 28 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "№", windowXAnim + ScaleGui.get(57), windowYAnim + ScaleGui.get(234.5f), fs, 0x666666);
    }

    protected void drawScroll() {
        guiScroll.drawScroll(windowXAnim + ScaleGui.get(688),
                windowYAnim + ScaleGui.get(250),
                ScaleGui.get(528)  * secondPoint * collapseAnim,
                windowYAnim + ScaleGui.get(788));
    }

    protected void drawSearchField(int mouseX, int mouseY) {
        searchField.addXPosition(windowXAnim);
        searchField.addYPosition(windowYAnim);
        searchField.drawTextBox(mouseX, mouseY);
    }

    protected void drawPlayers(int mouseX, int mouseY) {
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        scissorX = (int) (windowXAnim);
        float height = ScaleGui.get(578);
        float height1 = ScaleGui.get(windowHeight - 14);
        scissorHeight = (int) (height * firstPoint * collapseAnim);
        scissorY = (int) (mc.displayHeight - (windowCenterYAnim + halfHeight - ScaleGui.get(27)) + height1 * (1.0f - firstPoint) + height1 * (1.0f - collapseAnim));
        scissorWidth = (int) scaledWidth;
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        scissorAABB = AxisAlignedBB.getBoundingBox(scissorX, windowYAnim + ScaleGui.get(249), -100, scissorX + scissorWidth, windowYAnim + ScaleGui.get(827), 100);

        for(GuiButtonMail b : filteredResults) {
            b.addXPosition(windowXAnim);
            b.addYPosition(windowYAnim - guiScroll.getScrollAnim());
            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(b.xPosition, b.yPosition, 0, b.xPosition + b.width, b.yPosition + b.height, 0)))
                b.drawButton(mouseX, mouseY);
        }

        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public boolean mouseClickedWindow(int mouseX, int mouseY, int mouseButton) {
        searchField.mouseClicked(mouseX, mouseY, mouseButton);
        for(GuiButtonMail b : mails) {
            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(b.xPosition, b.yPosition, 0, b.xPosition + b.width, b.yPosition + b.height, 0))
                    && b.mousePressed(mc, mouseX, mouseY)) {
                return true;
            }
        }

        return super.mouseClickedWindow(mouseX, mouseY, mouseButton);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        searchField.updateCursorCounter();
    }

    @Override
    public void keyTyped(char character, int key) {
        super.keyTyped(character, key);
        searchField.textboxKeyTyped(character, key);
    }

    @Override
    public void setCollapsed(boolean collapsed) {
        super.setCollapsed(collapsed);
        if(!collapsed) {
            Keyboard.enableRepeatEvents(true);
        }
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_INBOX, false);
    }

    @Override
    public void onWindowCollapsed() {
        Keyboard.enableRepeatEvents(false);
    }
}
