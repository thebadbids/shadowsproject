package ru.krogenit.pda.gui.windows;

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

public class GuiWinButtonWithSubs extends GuiButtonAdvanced {

    private final IGuiWindowWithSubActions guiWindow;
    protected final List<ButtonContent> contents = new ArrayList<>();

    public GuiWinButtonWithSubs(int id, float x, float y, float width, float height, IGuiWindowWithSubActions guiWindow) {
        super(id, x, y, width, height, "");
        this.guiWindow = guiWindow;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovering = isHovered(mouseX, mouseY);
        float color = isHovering ? 97 / 255f : 51 / 255f;
        float x = xPosition;
        float y = yPosition + height / 2f;
        float height = this.height;
        float xOffset = ScaleGui.get(10);

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(color, color, color, 0.9f);
        GL11.glBegin(GL11.GL_LINES);
        for (int i = 0; i < contents.size() - 1; i++) {
            ButtonContent buttonContent = contents.get(i);
            float width = ScaleGui.get(buttonContent.getWidth());
            x += width;
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x + xOffset, y);
            x += xOffset;
        }
        GL11.glEnd();
        GL11.glColor4f(1f, 1f, 1f, 1f);
        x = xPosition;
        y = yPosition;
        for(ButtonContent buttonContent : contents) {
            float width = ScaleGui.get(buttonContent.getWidth());
            boolean subHovering = buttonContent.getId() != -1 && mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height;
            if(subHovering) GuiDrawUtils.drawRect(x, y, width, height, 1f, 1f, 1f, 0.9f);
            else GuiDrawUtils.drawRect(x, y, width, height, color, color, color, 0.9f);

            if(buttonContent.getTexture() != null) {
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                float textureOffset = buttonContent.getTextureOffset();
                float off = ScaleGui.get(textureOffset);
                float sizeOff = off * 2f;
                if(subHovering) GuiDrawUtils.drawRect(buttonContent.getTexture(), x + off, y + off, width - sizeOff, height - sizeOff, 0f, 0f, 0f, 1f);
                else GuiDrawUtils.drawRect(buttonContent.getTexture(), x + off, y + off, width - sizeOff, height - sizeOff, 1f, 1f, 1f, 1f);
                GL11.glDisable(GL11.GL_TEXTURE_2D);
            }

            if(buttonContent.getInfo() != null) {
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                float fs = 28 / 32f;
                if(buttonContent.getStringRenderType() == EnumStringRenderType.CENTERED)
                    GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, buttonContent.getInfo(), x + width / 2f, y + height / 2f, fs, subHovering ? 0x000000 : 0xffffff);
                else {
                    float offsetS = ScaleGui.get(9f);
                    GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, buttonContent.getInfo(), x + offsetS, y + height / 2f, fs, subHovering ? 0x000000 : 0xffffff);
                }
                GL11.glDisable(GL11.GL_TEXTURE_2D);
            }

            x += width + xOffset;
        }

        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        float x = xPosition;
        float y = yPosition;
        float xOffset = ScaleGui.get(10);
        for(ButtonContent buttonContent : contents) {
            int id = buttonContent.getId();
            float width = ScaleGui.get(buttonContent.getWidth());
            if(id != -1) {
                if(mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height) {
                    guiWindow.subAction(this, id);
                    return true;
                }
            }
            x += width + xOffset;
        }

        if(isHovered(mouseX, mouseY)) {
            guiWindow.subAction(this, -1);
            return true;
        }

        return false;
    }

    @Override
    protected boolean isHovered(int mouseX, int mouseY) {
        return mouseX > xPosition && mouseY > yPosition && mouseX <= xPosition + getTotalWidth() && mouseY <= yPosition + height;
    }

    public float getTotalWidth() {
        float totalWidth = 0;
        float xOffset = ScaleGui.get(10);
        for(ButtonContent buttonContent : contents) {
            totalWidth += ScaleGui.get(buttonContent.getWidth()) + xOffset;
        }
        totalWidth -= xOffset;
        return totalWidth;
    }

    public void addContent(ButtonContent content) {
        this.contents.add(content);
    }
}
