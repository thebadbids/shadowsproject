package ru.krogenit.pda.gui.windows.friends;

import lombok.Getter;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.pda.gui.windows.ButtonContent;
import ru.krogenit.pda.gui.windows.IGuiWindowWithSubActions;
import ru.krogenit.pda.gui.windows.players.GuiButtonPlayer;
import ru.krogenit.util.TimeUtils;
import ru.xlv.core.common.player.character.CharacterType;

@Getter
public class GuiButtonFriend extends GuiButtonPlayer {

    public GuiButtonFriend(int id, float x, float y, float width, float height, IGuiWindowWithSubActions guiWindow, String playerName,
                           CharacterType characterType, int level, long lastOnline, boolean isInGroup) {
        super(id, x, y, width, height, guiWindow, playerName, characterType, level, lastOnline, true, isInGroup, false);
        contents.clear();
        addContent(ButtonContent.newBuilder().setString("" + id).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString(playerName).setWidth(283).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.getClassIcon(characterType)).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString("" + level).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString(TimeUtils.getLastOnlineTime(lastOnline)).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString("Написать").setStringRenderType(EnumStringRenderType.CENTERED).setId(0).setWidth(86).build());
        addContent(ButtonContent.newBuilder().setString("Профиль").setStringRenderType(EnumStringRenderType.CENTERED).setId(1).setWidth(83).build());
        addContent(ButtonContent.newBuilder().setString(isInGroup ? "Из группы" : "В группу").setStringRenderType(EnumStringRenderType.CENTERED).setId(2).setWidth(81).build());
        addContent(ButtonContent.newBuilder().setString("Удалить").setStringRenderType(EnumStringRenderType.CENTERED).setId(3).setWidth(81).build());
    }

    public void setLastOnline(long time) {
        contents.get(4).setInfo(TimeUtils.getLastOnlineTime(time));
    }
}
