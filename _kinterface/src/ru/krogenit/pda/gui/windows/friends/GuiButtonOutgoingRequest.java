package ru.krogenit.pda.gui.windows.friends;

import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.pda.gui.windows.ButtonContent;
import ru.krogenit.pda.gui.windows.IGuiWindowWithSubActions;
import ru.xlv.core.common.player.character.CharacterType;

public class GuiButtonOutgoingRequest extends GuiButtonIncomingRequest {

    public GuiButtonOutgoingRequest(int id, float x, float y, float width, float height, IGuiWindowWithSubActions guiWindow, String playerName, CharacterType characterType, int level) {
        super(id, x, y, width, height, guiWindow, playerName, characterType, level);
        contents.clear();
        addContent(ButtonContent.newBuilder().setString("" + id).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString(playerName).setWidth(283).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.getClassIcon(characterType)).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString("" + level).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
    }
}
