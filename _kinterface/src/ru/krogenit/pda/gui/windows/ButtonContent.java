package ru.krogenit.pda.gui.windows;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.EnumStringRenderType;

@Setter
@Getter
public class ButtonContent {
    private String info;
    private EnumStringRenderType stringRenderType = EnumStringRenderType.DEFAULT;
    private ResourceLocation texture;
    private float width, textureOffset = 2;
    private int id = -1;

    public static ButtonContentBuilder newBuilder() {
        return new ButtonContent().new ButtonContentBuilder();
    }

    public class ButtonContentBuilder {

        public ButtonContentBuilder setId(int id) {
            ButtonContent.this.id = id;
            return this;
        }

        public ButtonContentBuilder setWidth(float width) {
            ButtonContent.this.width = width;
            return this;
        }

        public ButtonContentBuilder setTextureOffset(float value) {
            ButtonContent.this.textureOffset = value;
            return this;
        }

        public ButtonContentBuilder setStringRenderType(EnumStringRenderType renderType) {
            ButtonContent.this.setStringRenderType(renderType);
            return this;
        }

        public ButtonContentBuilder setTexture(ResourceLocation texture) {
            ButtonContent.this.setTexture(texture);
            return this;
        }

        public ButtonContentBuilder setString(String value) {
            ButtonContent.this.setInfo(value);
            return this;
        }

        public ButtonContent build() {
            return ButtonContent.this;
        }
    }
}
