package ru.krogenit.pda.gui;

import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.GuiTextFieldScrollable;
import ru.xlv.customfont.FontType;

public class GuiProfileDescription extends GuiTextFieldScrollable {

    public GuiProfileDescription(float x, float y, float width, float height, float fontScale) {
        super(FontType.HelveticaNeueCyrLight, x, y, width, height, fontScale);
        this.enabledColor = 0x1AC3EA;
        this.setMaxStringLength(50);
        this.canEdit = true;
    }

    @Override
    protected void drawBackground() {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(87 / 255f, 85 / 255f, 84 / 255f, 1.0f);
        GL11.glLineWidth(2f);
        GL11.glBegin(GL11.GL_LINE_STRIP);
        GL11.glVertex2f(this.xPosition, this.yPosition);
        GL11.glVertex2f(this.xPosition + width, this.yPosition);
        GL11.glVertex2f(this.xPosition + width, this.yPosition + height);
        GL11.glVertex2f(this.xPosition, this.yPosition + height);
        GL11.glVertex2f(this.xPosition, this.yPosition);
        GL11.glEnd();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(1f, 1f, 1f, 1f);
    }
}
