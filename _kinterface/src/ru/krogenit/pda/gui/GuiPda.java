package ru.krogenit.pda.gui;

import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.pages.auction.AuctionItem;
import ru.krogenit.pda.gui.pages.auction.GuiPageAuction;
import ru.krogenit.pda.gui.pages.community.GuiPageCommunity;
import ru.krogenit.pda.gui.pages.group.GuiPageGroup;
import ru.krogenit.pda.gui.pages.mail.GuiPageMail;
import ru.krogenit.pda.gui.pages.map.GuiPageMap;
import ru.krogenit.pda.gui.pages.map.NpcMapPin;
import ru.krogenit.pda.gui.pages.map.PlayerMapPin;
import ru.krogenit.pda.gui.pages.map.QuestMapPin;
import ru.krogenit.pda.gui.pages.missions.EnumCombatMissionsRank;
import ru.krogenit.pda.gui.pages.missions.EnumResearchMissionsRank;
import ru.krogenit.pda.gui.pages.missions.EnumSurviveMissionsRank;
import ru.krogenit.pda.gui.pages.missions.GuiPageMissions;
import ru.krogenit.pda.gui.pages.profile.EnumOnlineStatus;
import ru.krogenit.pda.gui.pages.profile.GuiPageProfile;
import ru.krogenit.pda.gui.pages.profile.GuiSlotAchieveProfile;
import ru.krogenit.pda.gui.pages.wiki.EnumWikiSection;
import ru.krogenit.pda.gui.pages.wiki.GuiPageWiki;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWindow;
import ru.krogenit.pda.gui.windows.achievements.GuiWindowAchievements;
import ru.krogenit.pda.gui.windows.friends.GuiWindowFriends;
import ru.krogenit.pda.gui.windows.friends.GuiWindowIncomingFriendRequests;
import ru.krogenit.pda.gui.windows.friends.GuiWindowOutgoingFriendRequests;
import ru.krogenit.pda.gui.windows.mail.GuiWindowIncomingMessages;
import ru.krogenit.pda.gui.windows.mail.GuiWindowOutgoingMessages;
import ru.krogenit.pda.gui.windows.mail.GuiWindowWriteMessage;
import ru.krogenit.pda.gui.windows.players.GuiWindowPlayers;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.core.player.ClientPlayerManager;
import ru.xlv.customfont.FontType;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.handle.FriendHandler;
import ru.xlv.friend.network.PacketFriendSync;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.common.Group;
import ru.xlv.group.network.PacketGroupSync;
import ru.xlv.mochar.XlvsMainMod;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.handle.PostHandler;
import ru.xlv.post.network.PacketPostSync;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class GuiPda extends AbstractGuiScreenAdvanced implements IGuiWithPopup {

    protected final List<GuiButtonLeftMenu> leftMenuButtons = new ArrayList<>();
    protected AbstractGuiScreenAdvanced page, nextPage;

    private final List<GuiWindow> windows = new ArrayList<>();
    private final Map<EnumWindowType, GuiWindow> windowsByType = new HashMap<>();

    public int scissorX, scissorY, scissorWidth, scissorHeight;
    private float pdaOpenAnim1, pdaOpenAnim2, pdaOpenAnim3, pdaOpenAnim4;
    private float newsStringAnim;
    private float firstPoint, secondPoint, thirdPoint, fs1, fs2, fs3;
    protected boolean playOpenCloseAnim, openedFirstPage;
    @Getter private final int playersCount;
    @Getter private long ping;
    private String location;
    private final String miscInfo;
    private GuiPopup popup;
    private Group group;
    private final FriendHandler friendHandler = XlvsFriendMod.INSTANCE.getFriendHandler();
    private final PostHandler postHandler = XlvsPostMod.INSTANCE.getPostHandler();
    @Getter protected final AxisAlignedBB pageAABB = AxisAlignedBB.getBoundingBox(0, 0, -100, 0, 0, 100);

    public GuiPda() {
        super(ScaleGui.FULL_HD);
        this.miscInfo = "controls_menu_help/expand/collapse/exit".toUpperCase();
        createGui();
        long now = System.currentTimeMillis();
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupSync()).thenAcceptSync(success -> {
            ping = (System.currentTimeMillis() - now) / 2;
            if(success) {
                group = XlvsGroupMod.INSTANCE.getGroup();
                GuiButtonLeftMenu leftMenuButton = getLeftMenuButton(EnumPdaLeftMenuSection.GROUP);
                if(leftMenuButton != null) leftMenuButton.enabled = true;
            }
        });
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendSync()).thenAcceptSync(result -> {
            if(result.getFriendRelations() != null) {
                friendHandler.sync(result.getFriendRelations());
                setOnlineAndTotalFriends(friendHandler.getAllOnlineFriends().size(), friendHandler.getAllFriends().size());
                setIncomingAndOutgoingInvites(friendHandler.getAllIncomingInvites().size(), friendHandler.getAllOutgoingInvites().size());
            }
        });
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostSync()).thenAcceptSync(result -> {
            if (result.isSuccess()) {
                postHandler.syncPost(result.getPostObjectList());
            }
        });

        playersCount = mc.thePlayer.sendQueue.playerInfoList.size();
    }

    public void setOnlineAndTotalFriends(int onlineFriends, int friends) {
        GuiButtonLeftMenu leftMenuButton = getLeftMenuButton(EnumPdaLeftMenuSection.COMMUNITY);
        if(leftMenuButton != null) {
            List<GuiButtonLeftMenu> subButtons = leftMenuButton.getSubButtons();
            subButtons.get(0).setInfoString(onlineFriends + " / " + friends);
        }
    }

    public void setIncomingAndOutgoingInvites(int incomingInvites, int outgoingInvites) {
        GuiButtonLeftMenu leftMenuButton = getLeftMenuButton(EnumPdaLeftMenuSection.COMMUNITY);
        if(leftMenuButton != null) {
            List<GuiButtonLeftMenu> subButtons = leftMenuButton.getSubButtons();
            subButtons.get(1).setInfoString(incomingInvites + " / " + outgoingInvites);
        }
    }

    protected void createControlButtons() {
        GuiButtonAdvanced button = new GuiButtonAdvanced(0, 0, 0, 0, 0, "");
        button.setTexture(TextureRegister.texturePDAButtonHelp);
        button.setTextureHover(TextureRegister.texturePDAButtonHelpHover);
        buttonList.add(button);
        button = new GuiButtonAdvanced(1, 0, 0, 0, 0, "");
        button.setTexture(TextureRegister.texturePDAButtonCollapse);
        button.setTextureHover(TextureRegister.texturePDAButtonCollapseHover);
        buttonList.add(button);
        button = new GuiButtonAdvanced(2, 0, 0, 0, 0, "");
        button.setTexture(TextureRegister.texturePDAButtonClose);
        button.setTextureHover(TextureRegister.texturePDAButtonCloseHover);
        buttonList.add(button);
    }

    protected void createGui() {
        createControlButtons();
        GuiButtonLeftMenu b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.PROFILE, 0, 0, 0, 0, "ПРОФИЛЬ", false);
        b.setTexture(EnumPdaLeftMenuSection.PROFILE);
//        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.PROFILE_STATISTICS, 0, 0, 0, 0, "СТАТИСТИКА", false));
//        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.PROFILE_ACHIEVEMENTS, 0, 0, 0, 0, "ДОСТИЖЕНИЯ", false));
//        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.PROFILE_AWARDS, 0, 0, 0, 0, "НАГРАДЫ", false));
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.COMMUNITY, 0, 0, 0, 0, "СООБЩЕСТВО", false);
        b.setTexture(EnumPdaLeftMenuSection.COMMUNITY);
        GuiButtonLeftMenu sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.COMMUNITY_FRIENDS, 0, 0, 0, 0, "ДРУЗЬЯ", true);
        sub.setInfoString(friendHandler.getAllOnlineFriends().size() + " / " + friendHandler.getAllFriends().size());
        b.addSubButton(sub);
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.COMMUNITY_REQUESTS, 0, 0, 0, 0, "ЗАЯВКИ", true);

        sub.setInfoString(friendHandler.getAllIncomingInvites().size() + " / " + friendHandler.getAllOutgoingInvites().size());
        b.addSubButton(sub);
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.COMMUNITY_SERVER, 0, 0, 0, 0, "СЕРВЕР", false));
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.GROUP, 0, 0, 0, 0, "ГРУППА", false);
        b.setTexture(EnumPdaLeftMenuSection.GROUP);
        b.enabled = false;
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MAP, 0, 0, 0, 0, "КАРТА", false);
        b.setTexture(EnumPdaLeftMenuSection.MAP);
        b.enabled = false;
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MISSIONS, 0, 0, 0, 0, "ЗАДАЧИ", false);
        b.setTexture(EnumPdaLeftMenuSection.MISSIONS);
        b.enabled = false;
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MISSIONS_CURRENT, 0, 0, 0, 0, "ТЕКУЩИЕ", true);
        sub.setInfoString("0 / 0");//TODO: получение текущих задач
        b.addSubButton(sub);
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MISSIONS_COMPLETED, 0, 0, 0, 0, "ЗАВЕРШЕННЫЕ", true);
        sub.setInfoString("0");//TODO: получение завершенных задач
        b.addSubButton(sub);
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MISSIONS_FAILED, 0, 0, 0, 0, "ПРОВАЛЕННЫЕ", true);
        sub.setInfoString("0");//TODO: получение проваленных задач
        b.addSubButton(sub);
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.WIKI, 0, 0, 0, 0, "СПРАВОЧНИК", false);
        b.setTexture(EnumPdaLeftMenuSection.WIKI);
        b.enabled = false;
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.WIKI_LOCATIONS, 0, 0, 0, 0, "ЛОКАЦИИ", false));
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.WIKI_CREATURES, 0, 0, 0, 0, "СУЩЕСТВА", false));
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.WIKI_ARMOR, 0, 0, 0, 0, "БРОНЯ", false));
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.WIKI_WEAPON, 0, 0, 0, 0, "ОРУЖИЕ", false));
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.WIKI_MECHANICS, 0, 0, 0, 0, "ИГРОВАЯ МЕХАНИКА", false));
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MAIL, 0, 0, 0, 0, "ПОЧТА", false);
        b.setTexture(EnumPdaLeftMenuSection.MAIL);
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MAIL_WRITE, 0, 0, 0, 0, "НАПИСАТЬ ПИСЬМО", false));
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MAIL_INBOX, 0, 0, 0, 0, "ВХОДЯЩИЕ", true);
        sub.setInfoString("" + postHandler.getIncomingPost().size());
        b.addSubButton(sub);
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.MAIL_OUTBOX, 0, 0, 0, 0, "ИСХОДЯЩИЕ", true);
        sub.setInfoString("" + postHandler.getOutgoingPost().size());
        b.addSubButton(sub);
        leftMenuButtons.add(b);

        b = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.AUCTION, 0, 0, 0, 0, "АУКЦИОН", false);
        b.enabled = false;
        b.setTexture(EnumPdaLeftMenuSection.AUCTION);
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.AUCTION_MARKET, 0, 0, 0, 0, "ТОРГОВАЯ ПЛОЩАДКА", false));
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.AUCTION_LOTS, 0, 0, 0, 0, "ВАШИ ЛОТЫ", true);
        sub.setInfoString("0");//TODO: Получение лотов
        b.addSubButton(sub);
        sub = new GuiButtonLeftMenu(EnumPdaLeftMenuSection.AUCTION_BIDS, 0, 0, 0, 0, "ВАШИ СТАВКИ", true);
        sub.setInfoString("0");//TODO: Получение ставок
        b.addSubButton(sub);
        b.addSubButton(new GuiButtonLeftMenu(EnumPdaLeftMenuSection.AUCTION_CREATE_LOT, 0, 0, 0, 0, "ВЫСТАВИТЬ ЛОТ", false));
        leftMenuButtons.add(b);
    }

    @Override
    public void initGui() {
        super.initGui();
        float buttonWidth = 35;
        float buttonHeight = 35;
        float x = 1733;
        float y = 67;
        float xOffset = 44;
        GuiButtonAdvanced button = (GuiButtonAdvanced) buttonList.get(0);
        button.xPosition = ScaleGui.getCenterX(x, buttonWidth);
        button.yPosition = ScaleGui.getCenterY(y, buttonHeight);
        button.width = ScaleGui.get(buttonWidth);
        button.height = ScaleGui.get(buttonHeight);
        x += xOffset;
        button = (GuiButtonAdvanced) buttonList.get(1);
        button.xPosition = ScaleGui.getCenterX(x, buttonWidth);
        button.yPosition = ScaleGui.getCenterY(y, buttonHeight);
        button.width = ScaleGui.get(buttonWidth);
        button.height = ScaleGui.get(buttonHeight);
        x += xOffset;
        button = (GuiButtonAdvanced) buttonList.get(2);
        button.xPosition = ScaleGui.getCenterX(x, buttonWidth);
        button.yPosition = ScaleGui.getCenterY(y, buttonHeight);
        button.width = ScaleGui.get(buttonWidth);
        button.height = ScaleGui.get(buttonHeight);

        initLeftButtons();

        if (page != null) page.initGui();

        for (GuiWindow window : windows) {
            window.initGui();
        }

        location = "Unknown";
        if(popup != null) popup.initGui();
    }

    protected void initLeftButtons() {
        float buttonWidth = 306;
        float buttonHeight = 44;
        float x = 346;
        float y = 185;
        float yOffset = 11 + buttonHeight;

        float subButtonWidth = 246;
        float subButtonHeight = 44;

        for (GuiButtonLeftMenu b : leftMenuButtons) {
            b.xPosition = b.xPositionBase = ScaleGui.getCenterX(x, buttonWidth);
            b.yPosition = b.yPositionBase = ScaleGui.getCenterY(y, buttonHeight);
            b.width = ScaleGui.get(buttonWidth);
            b.height = ScaleGui.get(buttonHeight);

            for (GuiButtonLeftMenu sub : b.getSubButtons()) {
                sub.width = sub.widthBase = ScaleGui.get(subButtonWidth);
                sub.height = ScaleGui.get(subButtonHeight);
            }

            y += yOffset;
        }
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if (b.id == 1 || b.id == 2) {
            this.mc.displayGuiScreen(null);
        }
    }

    protected void leftMenuAction(GuiButtonLeftMenu b) {
        boolean ignoreClassCheck = false;
        switch (b.getEnumId()) {
            case PROFILE:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                GuiPageProfile pageProfile = new GuiPageProfile(minAspect, this);
                nextPage = pageProfile;
                nextPage.initGui();
                break;
            case PROFILE_ACHIEVEMENTS:
//                GuiWindow window = windowsByType.get(EnumWindowType.ACHIEVEMENTS);
//                if (window != null) {
//                    closeWindow(window);
//                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.PROFILE_ACHIEVEMENTS, false);
//                } else {
//                    showAchievements(null, null);
//                }
                break;
            case COMMUNITY:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                GuiPageCommunity pageCommunity = new GuiPageCommunity(minAspect, this);
                nextPage = pageCommunity;
                nextPage.initGui();
                break;
            case COMMUNITY_FRIENDS:
                GuiWindow window = windowsByType.get(EnumWindowType.FRIENDS);
                if (window != null) {
                    closeWindow(window);
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_FRIENDS, false);
                } else {
                    showFriends();
                }
                break;
            case COMMUNITY_REQUESTS:
                window = windowsByType.get(EnumWindowType.INCOMING_FRIEND_REQUESTS);
                if(window != null) {
                    closeWindow(window);
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_REQUESTS, false);
                } else {
                    showIncomingFriendRequests();
                }
                break;
            case COMMUNITY_SERVER:
                window = windowsByType.get(EnumWindowType.PLAYERS);
                if (window != null) {
                    closeWindow(window);
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_SERVER, false);
                } else {
                    showServerPlayers();
                }
                break;
            case MAIL:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                GuiPageMail pageMail = new GuiPageMail(this, minAspect);
                pageMail.setMailAdmins(0);
                pageMail.setMailAuction(0);
                pageMail.setMailFromFriends(0);
                pageMail.setMailTotal(postHandler.getIncomingPost().size());
                nextPage = pageMail;
                nextPage.initGui();
                break;
            case MAIL_WRITE:
                window = windowsByType.get(EnumWindowType.MAIL_WRITE);
                if (window != null) {
                    closeWindow(window);
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, false);
                } else {
                    showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(this));
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
                }
                break;
            case MAIL_INBOX:
                window = windowsByType.get(EnumWindowType.INCOMING_MESSAGES);
                if (window != null) {
                    closeWindow(window);
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_INBOX, false);
                } else {
                    showOrCreateWindow(EnumWindowType.INCOMING_MESSAGES, new GuiWindowIncomingMessages(this));
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_INBOX, true);
                }
                break;
            case MAIL_OUTBOX:
                window = windowsByType.get(EnumWindowType.OUTGOING_MESSAGES);
                if (window != null) {
                    closeWindow(window);
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_OUTBOX, false);
                } else {
                    showOrCreateWindow(EnumWindowType.OUTGOING_MESSAGES, new GuiWindowOutgoingMessages(this));
                    setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_OUTBOX, true);
                }
                break;
            case GROUP:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                nextPage = new GuiPageGroup(minAspect, this, group);
                nextPage.initGui();
                break;
            case MISSIONS:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                GuiPageMissions pageMissions = new GuiPageMissions(minAspect, this);
                pageMissions.setCombatRank(EnumCombatMissionsRank.NOVICE);
                pageMissions.setResearchRank(EnumResearchMissionsRank.LEARNER);
                pageMissions.setSurviveRank(EnumSurviveMissionsRank.SCOUT);
                pageMissions.setNextCombatRank(EnumCombatMissionsRank.MARINE);
                pageMissions.setNextResearchRank(EnumResearchMissionsRank.CONNOISSEUR);
                pageMissions.setNextSurviveRank(EnumSurviveMissionsRank.DIVERSIONIST);
                pageMissions.setCombatExperience(mc.theWorld.rand.nextInt(10000));
                pageMissions.setResearchExperience(mc.theWorld.rand.nextInt(10000));
                pageMissions.setSurviveExperience(mc.theWorld.rand.nextInt(10000));
                pageMissions.setCompletedCombatMissions(mc.theWorld.rand.nextInt(20));
                pageMissions.setCompletedResearchMissions(mc.theWorld.rand.nextInt(20));
                pageMissions.setCompletedSurviveMissions(mc.theWorld.rand.nextInt(20));
                pageMissions.setCompletedCombatMissionsToNextRank(mc.theWorld.rand.nextInt(8));
                pageMissions.setCompletedResearchMissionsToNextRank(mc.theWorld.rand.nextInt(8));
                pageMissions.setCompletedSurviveMissionsToNextRank(mc.theWorld.rand.nextInt(8));
                pageMissions.setTotalCombatMissionsToNextRank(mc.theWorld.rand.nextInt(20) + 10);
                pageMissions.setTotalResearchMissionsToNextRank(mc.theWorld.rand.nextInt(20) + 10);
                pageMissions.setTotalSurviveMissionsToNextRank(mc.theWorld.rand.nextInt(20) + 10);

                nextPage = pageMissions;
                nextPage.initGui();
                break;
            case WIKI:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                for (GuiButtonLeftMenu sub : b.getSubButtons()) {
                    sub.setIsPressed(false);
                }

                GuiPageWiki pageWiki = new GuiPageWiki(minAspect, this, EnumWikiSection.ALL);
                nextPage = pageWiki;
                nextPage.initGui();
                break;
            case WIKI_CREATURES:
                showWikiPage(b, EnumWikiSection.CREATURES);
                ignoreClassCheck = true;
                break;
            case WIKI_ARMOR:
                showWikiPage(b, EnumWikiSection.ARMOR);
                ignoreClassCheck = true;
                break;
            case WIKI_LOCATIONS:
                showWikiPage(b, EnumWikiSection.LOCATIONS);
                ignoreClassCheck = true;
                break;
            case WIKI_MECHANICS:
                showWikiPage(b, EnumWikiSection.MECHANICS);
                ignoreClassCheck = true;
                break;
            case WIKI_WEAPON:
                showWikiPage(b, EnumWikiSection.WEAPON);
                ignoreClassCheck = true;
                break;
            case AUCTION:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                GuiPageAuction pageAuction = new GuiPageAuction(minAspect);
                pageAuction.setTotalLots(mc.theWorld.rand.nextInt(20000));
                pageAuction.setDayLots(mc.theWorld.rand.nextInt(400));
                pageAuction.setBronze(mc.theWorld.rand.nextInt(20000));
                pageAuction.setSilver(mc.theWorld.rand.nextInt(2000));
                pageAuction.setGold(mc.theWorld.rand.nextInt(50));
                pageAuction.setBronzeBank(mc.theWorld.rand.nextInt(20000));
                pageAuction.setSilverBank(mc.theWorld.rand.nextInt(2000));
                pageAuction.setGoldBank(mc.theWorld.rand.nextInt(50));
                pageAuction.setOpenedLots(mc.theWorld.rand.nextInt(50));
                pageAuction.setClosedLots(mc.theWorld.rand.nextInt(50));
                pageAuction.setIncomingPurchases(mc.theWorld.rand.nextInt(5));
                pageAuction.setIncomingPayments(mc.theWorld.rand.nextInt(5));
                pageAuction.addAuctionItem(new AuctionItem(TextureRegister.texturePDAAuctionItem, mc.theWorld.rand.nextInt(50),
                        mc.theWorld.rand.nextInt(2000), mc.theWorld.rand.nextInt(20000), mc.theWorld.rand.nextInt(50), mc.theWorld.rand.nextInt(2000),
                        mc.theWorld.rand.nextInt(20000)));
                nextPage = pageAuction;
                nextPage.initGui();
                break;
            case MAP:
                setLeftMenuButtonPressed(b.getEnumId(), !b.isPressed(), true, false);
                GuiPageMap pageMap = new GuiPageMap(minAspect, this);
                pageMap.setLocation("ЗАГНИВАЮЩИЙ ЛЕСОЧЕК");
                boolean isLeader = mc.theWorld.rand.nextBoolean();
                pageMap.addPin(new PlayerMapPin(new Vector2f((float) mc.thePlayer.posX, (float) mc.thePlayer.posZ), mc.thePlayer.getDisplayName().toUpperCase(),
                        XlvsMainMod.INSTANCE.getCurrentCharacterType(), isLeader, mc.theWorld.rand.nextInt(100), "ЗАХУДАЛЫЙ ЛЕС", new Vector3f(mc.theWorld.rand.nextFloat(), mc.theWorld.rand.nextFloat(), mc.theWorld.rand.nextFloat())));

                for (int i = 0; i < 10; i++) {
                    List<String> desc = new ArrayList<>();
                    desc.add("Убить когото там 0/10");
                    desc.add("Убить когото там 0/10");
                    QuestMapPin mapPin = new QuestMapPin(new Vector2f(mc.theWorld.rand.nextInt(10000) - 5000, mc.theWorld.rand.nextInt(10000) - 5000), "QUEST TEST NAME BEZ CHELOV I TABLIC " + i, desc);
                    pageMap.addPin(mapPin);
                }

                for (int i = 0; i < 10; i++) {
                    List<String> desc = new ArrayList<>();
                    desc.add("ТОРГОВЛЯ ИСХОДНИКАМИ");
                    desc.add("КОД ШАДОВ ПРОДЖЕКТА ПО 5 РУБЛЕЙ");
                    NpcMapPin mapPin = new NpcMapPin(new Vector2f(mc.theWorld.rand.nextInt(10000) - 5000, mc.theWorld.rand.nextInt(10000) - 5000), "NPC TEST NAME " + i, desc);
                    pageMap.addPin(mapPin);
                }

                nextPage = pageMap;
                nextPage.initGui();
                break;
        }

        if (nextPage != null) {
            if (page != null && !ignoreClassCheck && page.getClass().equals(nextPage.getClass())) {
                nextPage = null;
                playOpenCloseAnim = true;
            } else {
                playOpenCloseAnim = true;
            }
        }
    }

    public void setLeftMenuButtonPressed(EnumPdaLeftMenuSection id, boolean pressed, boolean enabled, boolean withSubs) {
        GuiButtonLeftMenu b = getLeftMenuButton(id);
        if(b != null) {
            b.setIsPressed(pressed);
            b.enabled = enabled;
            if (withSubs) {
                for (GuiButtonLeftMenu b1 : b.getSubButtons()) {
                    b1.setIsPressed(pressed);
                }
            }
        }
    }

    public void setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection id, boolean pressed) {
        GuiButtonLeftMenu leftMenuButton = getLeftMenuButton(id);
        if(leftMenuButton != null) leftMenuButton.setIsPressed(pressed);
    }

    private GuiButtonLeftMenu getLeftMenuButton(EnumPdaLeftMenuSection id) {
        for (GuiButtonLeftMenu b : leftMenuButtons) {
            if(b.getEnumId() == id) return b;
            for (GuiButtonLeftMenu b1 : b.getSubButtons()) {
                if (b1.getEnumId() == id) return b1;
            }
        }

        return null;
    }

    public void changeAvatar() {

    }

    public void showFriends() {
        showOrCreateWindow(EnumWindowType.FRIENDS, new GuiWindowFriends(this, 964, 854));
        setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_FRIENDS, true);
    }

    public void showIncomingFriendRequests() {
        showOrCreateWindow(EnumWindowType.INCOMING_FRIEND_REQUESTS, new GuiWindowIncomingFriendRequests(this, 686, 874));
        setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_REQUESTS, true);
    }

    public void showOutgoingFriendRequests() {
        showOrCreateWindow(EnumWindowType.OUTGOING_FRIEND_REQUESTS, new GuiWindowOutgoingFriendRequests(this, 686, 874));
        setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_REQUESTS, true);
    }

    public void showServerPlayers() {
        showOrCreateWindow(EnumWindowType.PLAYERS, new GuiWindowPlayers(this, 1120, 854));
        setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_SERVER, true);
    }

    public void showAchievements(GuiPageProfile pageProfile, GuiSlotAchieveProfile slot) {
        showOrCreateWindow(EnumWindowType.ACHIEVEMENTS, new GuiWindowAchievements(this, (1372 / 2f), (1748 / 2f), pageProfile, slot));
        setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.PROFILE_ACHIEVEMENTS, true);
    }

    public void showOrCreateWindow(EnumWindowType type, GuiWindow window) {
        showOrCreateWindow(type, window, true);
    }

    public void showOrCreateWindow(EnumWindowType type, GuiWindow window, boolean openCollapsedWindow) {
        GuiWindow window1 = windowsByType.get(type);
        if (window1 != null && openCollapsedWindow) {
            if (window1.isCollapsed()) window1.setCollapsed(false);
            setActiveWindow(window1);
            return;
        }

        for(GuiWindow window2 : windows) {
            window2.setActive(false);
        }

        window.initGui();
        window.setActive(true);
        windows.add(window);
        windowsByType.put(window.getWindowType(), window);
    }

    public void saveProfileDescription(String text) {

    }

    public void showPlayerLocation(String playerName) {

    }

    public void writeMessageToPlayer(String playerName) {

    }

    public void showProfile(String playerName) {
        if(!mc.thePlayer.getDisplayName().equals(playerName)) {
            ClientPlayer clientPlayer = ClientPlayerManager.INSTANCE.getPlayerByName(playerName);
            if(clientPlayer != null) {
                setLeftMenuButtonPressed(EnumPdaLeftMenuSection.PROFILE, true, true, false);
                GuiPageProfile pageProfile = new GuiPageProfile(minAspect, this);
                pageProfile.setCharacterType(clientPlayer.getCharacterType());
                pageProfile.setPlayerLvl(clientPlayer.getLevel());
                pageProfile.setStatus(EnumOnlineStatus.ONLINE);
                pageProfile.setPlayerName(playerName.toUpperCase());
                pageProfile.setEntityPlayer((AbstractClientPlayer)mc.theWorld.getPlayerEntityByName(playerName));
                nextPage = pageProfile;
                nextPage.initGui();
                playOpenCloseAnim = true;
            }
        }
    }

    public void leaveFromGroup() {
        setLeftMenuButtonPressed(EnumPdaLeftMenuSection.GROUP, false, false, false);
        nextPage = null;
        playOpenCloseAnim = true;
    }

    public void dissolveGroup() {
        setLeftMenuButtonPressed(EnumPdaLeftMenuSection.GROUP, false, false, false);
        nextPage = null;
        playOpenCloseAnim = true;
    }

    public void showCurrentMissions() {

    }

    public void showCompletedMissions() {

    }

    public void showRandomWikiPage() {

    }

    private void showWikiPage(GuiButtonLeftMenu b, EnumWikiSection section) {
        GuiButtonLeftMenu parent = b.getParent();
        for (GuiButtonLeftMenu sub : parent.getSubButtons()) {
            if (sub != b) {
                sub.setIsPressed(false);
            }
        }

        if (page instanceof GuiPageWiki) {
            if (((GuiPageWiki) page).getSection() != section) {
                b.setIsPressed(true);
                GuiPageWiki pageWiki = new GuiPageWiki(minAspect, this, section);
                pageWiki.setPrevious((GuiPageWiki) page);
                nextPage = pageWiki;
                nextPage.initGui();
            } else if (b.isPressed()) {
                b.setIsPressed(false);
                GuiPageWiki pageWiki = new GuiPageWiki(minAspect, this, EnumWikiSection.ALL);
                pageWiki.setPrevious((GuiPageWiki) page);
                nextPage = pageWiki;
                nextPage.initGui();
            }
        }
    }

    public void showPreviousWikiPage(GuiPageWiki pageToShow) {
        nextPage = pageToShow;
        nextPage.initGui();
        playOpenCloseAnim = true;
        updateWikiSubButtons(pageToShow.getSection());
    }

    public void wikiLeftButton(GuiPageWiki current) {
        EnumWikiSection leftSection = null;
        int index = current.getSection().ordinal();
        EnumWikiSection[] sections = EnumWikiSection.values();
        while (leftSection == null) {
            index--;
            if (index < 1) {
                index = sections.length - 1;
            }

            leftSection = sections[index];
        }

        GuiPageWiki pageWiki = new GuiPageWiki(minAspect, this, leftSection);
        pageWiki.setPrevious(current);
        nextPage = pageWiki;
        nextPage.initGui();
        playOpenCloseAnim = true;
        updateWikiSubButtons(leftSection);
    }

    public void wikiRightButton(GuiPageWiki current) {
        EnumWikiSection rightSection = null;
        int index = current.getSection().ordinal();
        EnumWikiSection[] sections = EnumWikiSection.values();
        while (rightSection == null) {
            index++;
            if (index == sections.length) {
                index = 1;
            }

            rightSection = sections[index];
        }

        GuiPageWiki pageWiki = new GuiPageWiki(minAspect, this, rightSection);
        pageWiki.setPrevious(current);
        nextPage = pageWiki;
        nextPage.initGui();
        playOpenCloseAnim = true;
        updateWikiSubButtons(rightSection);
    }

    public void showWikiPage(EnumWikiSection section, GuiPageWiki prevPage) {
        GuiPageWiki pageWiki = new GuiPageWiki(minAspect, this, section);
        pageWiki.setPrevious(prevPage);
        nextPage = pageWiki;
        nextPage.initGui();
        playOpenCloseAnim = true;
        updateWikiSubButtons(section);
    }

    private void updateWikiSubButtons(EnumWikiSection section) {
        EnumPdaLeftMenuSection enumToFind = null;
        switch (section) {
            case ARMOR:
                enumToFind = EnumPdaLeftMenuSection.WIKI_ARMOR;
                break;
            case MECHANICS:
                enumToFind = EnumPdaLeftMenuSection.WIKI_MECHANICS;
                break;
            case WEAPON:
                enumToFind = EnumPdaLeftMenuSection.WIKI_WEAPON;
                break;
            case LOCATIONS:
                enumToFind = EnumPdaLeftMenuSection.WIKI_LOCATIONS;
                break;
            case CREATURES:
                enumToFind = EnumPdaLeftMenuSection.WIKI_CREATURES;
                break;
        }

        if (enumToFind != null) {
            for (GuiButtonLeftMenu b : leftMenuButtons) {
                if (b.getEnumId() == EnumPdaLeftMenuSection.WIKI) {
                    for (GuiButtonLeftMenu sub : b.getSubButtons()) {
                        sub.setIsPressed(sub.getEnumId() == enumToFind);
                    }
                }
            }
        } else {
            for (GuiButtonLeftMenu b : leftMenuButtons) {
                if (b.getEnumId() == EnumPdaLeftMenuSection.WIKI) {
                    for (GuiButtonLeftMenu sub : b.getSubButtons()) {
                        sub.setIsPressed(false);
                    }
                }
            }
        }
    }

    public boolean isWindowOpened(EnumWindowType type) {
        return windowsByType.containsKey(type);
    }

    public void collapseWindow(GuiWindow window) {
        window.onWindowCollapsed();
        int size = windows.size();
        if (size > 1) {
            GuiWindow nextWindow = windows.get(size - 2);
            nextWindow.setActive(true);
            windows.set(size - 2, window);
            windows.set(size - 1, nextWindow);
        }
    }

    public void closeWindow(GuiWindow window) {
        window.onWindowClosed();
        windows.remove(window);
        windowsByType.remove(window.getWindowType());
        int size = windows.size();
        if(size > 0) {
            windows.get(size - 1).setActive(true);
        }
    }

    protected void drawProfileId() {
        glColor4f(1f, 1f, 1f, (pdaOpenAnim1 + pdaOpenAnim2) / 2f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconIdPlayer, 100, 73, 40, 56);
        float x = 135;
        float y = 48;
        int color = (int) Long.parseLong(Integer.toHexString(new Color(102 / 255f, 102 / 255f, 102 / 255f, (pdaOpenAnim1 + pdaOpenAnim2) / 2f).getRGB()), 16);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ID PDA100000000028", x, y, 0.555f, color);
        y += 23;
        String playerName = mc.thePlayer.getDisplayName();
        color = (int) Long.parseLong(Integer.toHexString(new Color(1f, 1f, 1f, (pdaOpenAnim1 + pdaOpenAnim2) / 2f).getRGB()), 16);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerName.toUpperCase(), x, y, 1.5f, color);
        y += 23;
        ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        String classString = clientMainPlayer.getCharacterType().getDisplayName().toUpperCase();
        String lvl = " LVL " + clientMainPlayer.getLvl();
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, classString + lvl, x, y, 0.9375f, color);
        glColor4f(1f, 1f, 1f, 1f);
    }

    protected void drawLoadingBar() {
        float yStart = 1008 - 81 - 125 / 2f;
        float elementHeight = 1027 - yStart;
        float load;
        if (page != null && !playOpenCloseAnim) load = pdaOpenAnim3;
        else
            load = (pdaOpenAnim1 + pdaOpenAnim2 + firstPoint + secondPoint + thirdPoint + fs1 + fs2 + fs3 + pdaOpenAnim4) / 9f;//(float) ((Math.cos(System.nanoTime()/5000000000f) + 1f) / 2f);//64 / 100f;
        float y = (1027 - elementHeight) * pdaOpenAnim1 + elementHeight;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAElementLoadingBorder, 306, y, 452, 25);
        glColor4f(6 / 255f, 77 / 255f, 89 / 255f, 1.0f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAElementLoadingBar, 302, y, 438, 19);
        elementHeight = 998 - yStart;
        y = (998 - elementHeight) * pdaOpenAnim1 + elementHeight;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "LOADING", 80, y, 0.75f, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, load >= 0.99f ? "DONE" : (int) (load * 100) + "%", 462, y, 0.75f, 0xffffff);
        float x = 83;
        elementHeight = 1018 - yStart;
        y = (1018 - elementHeight) * pdaOpenAnim1 + elementHeight;
        float maskWidth = 438;
        float maskHeight = 18;
        GuiDrawUtils.clearMaskBuffer(0, 0, width, height);
        glColor4f(38 / 255f, 171 / 255f, 202 / 255f, 1.0f);
        float loadMaskWidth = 0f;
        float totalMaskWidth = load * maskWidth - 15f;
        float step = ScaleGui.get(8.51f);
        if (load <= 0.005f) {
            loadMaskWidth = 0;
        } else if (load > 0.98f) {
            loadMaskWidth = maskWidth;
        } else {
            while (loadMaskWidth < totalMaskWidth) {
                loadMaskWidth += step;
            }
        }

        GuiDrawUtils.drawLoadingBarMaskingEffect(TextureRegister.texturePDAElementLoadingBar, x, y, maskWidth, maskHeight, loadMaskWidth, maskHeight);
    }

    protected void drawStatus() {
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconServerStatusMisc, 1614, 1033, 451 * pdaOpenAnim2, 13);
        float x = 1440;
        float y = 1002;
        float iconWidth = 19;
        float stringMinusYOffset = 7;
        float stringPositiveYOffset = 7;
        float stringXOffset = 15;
        float fs = 0.7f * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "СЕРВЕР: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        String serverStatus = "ONLINE";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, serverStatus, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ПИНГ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        String pingS = ping + " МС";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, pingS, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        x = 1565;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        LocalDateTime time = LocalDateTime.now();
        String data = DateTimeFormatter.ofPattern("dd.MM.yyyy").format(time);
        fs = 0.7f * fs2;
        s = "ДАТА: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, data, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ВРЕМЯ: ";
        String timeString = DateTimeFormatter.ofPattern("HH:mm:ss").format(time);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, timeString, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        x = 1695;
        fs = 0.7f * fs3;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * thirdPoint, iconWidth * thirdPoint);
        s = "ЛОКАЦИЯ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, location, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        String playerCount = "" + playersCount;
        s = "ИГРОКОВ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerCount, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        GL11.glColor4f(1f, 1f, 1f, 1f);
    }

    private void drawNewsString() {
        glEnable(GL_SCISSOR_TEST);
        float x = ScaleGui.getCenterX(560, 0);
        float y = ScaleGui.getCenterY(1080 - 112, 0);
        float width = ScaleGui.getCenterX(1360, 0) - x;
        float height = ScaleGui.getCenterY(26, 0);
        glScissor((int) x, (int) y, (int) width, (int) height);
        String txt = "Самые горячие новости за последние время проекта Shadows Project:            Были брошены все силы проекта для разработки модуля челы + таблицы.           Владелец проекта станцевал Uno на вебкамеру во время стрима.               " +
                "Спустя долгое время отсутствия главного геймдизайнера проекта, выяснилось что это был ни коронавирус, ни армия, а деприсняк. Наконец-то уволен с проекта дизич.              " +
                "Фанат сервера оставил на форуме более 70 тем с идеями, но это оказались простые картинки с гугла.                 Завершена реализация сiкпейдi.                  " +
                "#СёмаЖиви.              Главный артдиректор решил добавить меч из варфрейма даже неизменив логотип на самой моделе.                " +
                "На сервере обнаружены новые блоки, полублоки и каменные заборы, но тонких заборов найти не удалось.                ";
        float fs = 0.95f;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, txt, 1387 - newsStringAnim, 98, fs, 0xffffff);
        glDisable(GL_SCISSOR_TEST);
        float sl = FontType.HelveticaNeueCyrLight.getFontContainer().width(txt) * fs;

        float newsLineWidth = 1387 - 532;
        float maxStringAnim = sl + newsLineWidth;
        if (newsStringAnim < maxStringAnim) {
            newsStringAnim += AnimationHelper.getAnimationSpeed() * 1.0f;
            if (newsStringAnim > maxStringAnim) {
                newsStringAnim = 0f;
            }
        }
    }

    protected void drawControlsString() {
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, miscInfo, 1463 - 960, 81 - 540, 0.5f, 0x666666);
        glColor4f(1f, 1f, 1f, 1f);
    }

    protected void drawLeftMenu(int mouseX, int mouseY, float partialTick) {
        float x = 358;
        float y = 545;
        float leftMenuWidth = 370;
        float leftMenuHeight = 805;
        glDisable(GL_TEXTURE_2D);
        glColor4f(0f, 0f, 0f, 0.4f);
        GuiDrawUtils.drawCenterCentered(x, y, leftMenuWidth, leftMenuHeight * pdaOpenAnim2);
        glColor4f(1f, 1f, 1f, 1f);
        glEnable(GL_TEXTURE_2D);

        glEnable(GL_SCISSOR_TEST);
        x = ScaleGui.getCenterX(173, 0);
        y = ScaleGui.getCenterY(150, 0);
        float width = ScaleGui.getCenterX(541, 0) - x;
        float height = ScaleGui.getCenterY(924, 0) - y;
        glScissor((int) x, (int) y, (int) width, (int) height);
        float yOffset = 0;
        float xOffset = -400;
        float prevAnim = 1f;
        for (GuiButtonLeftMenu b : leftMenuButtons) {
            if (pdaOpenAnim2 >= 0.95f && prevAnim >= 0.9f) {
                b.updateSideAnimation();
                prevAnim = b.getSideAnim();
            }
            b.addXPosition(xOffset * (1f - b.getSideAnim()));
            b.addYPosition(yOffset);
            b.drawButton(mouseX, mouseY);
            yOffset += b.getSubButtonsBlockHeight();
            pdaOpenAnim4 = b.getSideAnim();
        }
        glDisable(GL_SCISSOR_TEST);
    }

    protected void drawBorders() {
        float elementWidth = 3520 / 2f;
        float elementHeight = 125 / 2f;
        float y = pdaOpenAnim1 * (1008 - 81 - elementHeight) + 81 + elementHeight;
        glColor4f(1f, 1f, 1f, 1f * pdaOpenAnim1);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDABorderBot, 960, y, elementWidth, elementHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDABorderTop, 960, 81, elementWidth, elementHeight);
        y = (544 - 50 - elementHeight) * pdaOpenAnim1 + 50 + elementHeight;
        elementHeight = 1730 / 2f * pdaOpenAnim1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDABorderLeft, 111, y, 125 / 2f, elementHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDABorderRight, 1808, y, 125 / 2f, elementHeight);
        glColor4f(1f, 1f, 1f, 1f);
    }

    protected void drawWorkSpace() {
        if (pdaOpenAnim3 > 0.001f) {
            float x = 1160;
            float y = 545;
            float workSpaceWidth = 1176f * pdaOpenAnim3;
            float workSpaceHeight = 806f;

            glDisable(GL_TEXTURE_2D);
            glColor4f(0f, 0f, 0f, 0.4f);
            GuiDrawUtils.drawCenterCentered(x, y, workSpaceWidth, workSpaceHeight);
            float lineWidth = 2f;
            glLineWidth(lineWidth);
            glColor4f(102 / 255f, 102 / 255f, 102 / 255f, 1f);
            float workSpaceHalfWidth = workSpaceWidth / 2f;
            float workSpaceHalfHeight = workSpaceHeight / 2f;
            glBegin(GL_LINE_STRIP);
            lineWidth = 0f;
            glVertex2f(ScaleGui.getCenterX(x - workSpaceHalfWidth + lineWidth, 0), ScaleGui.getCenterY(y - workSpaceHalfHeight + lineWidth, 0));
            glVertex2f(ScaleGui.getCenterX(x + workSpaceHalfWidth - lineWidth, 0), ScaleGui.getCenterY(y - workSpaceHalfHeight + lineWidth, 0));
            glVertex2f(ScaleGui.getCenterX(x + workSpaceHalfWidth - lineWidth, 0), ScaleGui.getCenterY(y + workSpaceHalfHeight - lineWidth, 0));
            glVertex2f(ScaleGui.getCenterX(x - workSpaceHalfWidth + lineWidth, 0), ScaleGui.getCenterY(y + workSpaceHalfHeight - lineWidth, 0));
            glVertex2f(ScaleGui.getCenterX(x - workSpaceHalfWidth + lineWidth, 0), ScaleGui.getCenterY(y - workSpaceHalfHeight + lineWidth, 0));
            glEnd();

            for (GuiButtonLeftMenu b : leftMenuButtons) {
                if (b.isPressed()) {
                    glBegin(GL_LINES);
                    y = b.yPosition + b.height / 3f;
                    glVertex2f(b.xPosition + b.width, y);
                    glVertex2f(ScaleGui.getCenterX(x - workSpaceHalfWidth + lineWidth, 0), y);
                    glEnd();
                }
            }

            glEnable(GL_TEXTURE_2D);
        }
    }

    protected void drawPage(int mouseX, int mouseY, float partialTick) {
        glEnable(GL_SCISSOR_TEST);
        scissorX = (int) ScaleGui.getCenterX(573 + (1747 - 572) / 2f * (1f - pdaOpenAnim3));
        scissorY = (int) ScaleGui.getCenterY(133);
        scissorWidth = (int) ((ScaleGui.getCenterX(1747) - ScaleGui.getCenterX(573)) * pdaOpenAnim3);
        scissorHeight = (int) (ScaleGui.getCenterY(938) - scissorY);
        pageAABB.minX = scissorX;
        pageAABB.maxX = scissorX + scissorWidth;
        pageAABB.minY = ScaleGui.getCenterY(143);
        pageAABB.maxY = pageAABB.minY + ScaleGui.get(804);
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        if (page != null) page.drawScreen(mouseX, mouseY, partialTick);
        glDisable(GL_SCISSOR_TEST);
    }

    protected void drawWindows(int mouseX, int mouseY, float partialTick) {
        for (GuiWindow window : windows) {
            if(window.isActive()) window.drawScreen(mouseX, mouseY, partialTick);
            else window.drawScreen(-100, -100, partialTick);
        }
    }

    protected void openFirstPage() {
        setLeftMenuButtonPressed(EnumPdaLeftMenuSection.PROFILE, true, true, false);
        GuiPageProfile pageProfile = new GuiPageProfile(minAspect, this);
        nextPage = pageProfile;
        nextPage.initGui();
        playOpenCloseAnim = true;
    }

    private void updateAnimations() {
        pdaOpenAnim1 = AnimationHelper.updateSlowEndAnim(pdaOpenAnim1, 1f, 0.1f, 0.0001f);

        if (pdaOpenAnim1 > 0.9f) {
            pdaOpenAnim2 = AnimationHelper.updateSlowEndAnim(pdaOpenAnim2, 1f, 0.1f, 0.0001f);
        }

        if (playOpenCloseAnim || page == null) {
            pdaOpenAnim3 = AnimationHelper.updateSlowEndAnim(pdaOpenAnim3, 0f, -0.25f, -0.003f);
            if (pdaOpenAnim3 < 0.001f) {
                playOpenCloseAnim = false;
                page = nextPage;
                nextPage = null;
            }
        } else {
            pdaOpenAnim3 = AnimationHelper.updateSlowEndAnim(pdaOpenAnim3, 1f, 0.15f, 0.0001f);
        }

        if (pdaOpenAnim2 > 0.9f) {
            firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
            if (firstPoint > 0.9f) {
                fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
                if (fs1 > 0.9f) {
                    secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                    if (secondPoint > 0.9f) {
                        fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                        if (fs2 > 0.9f) {
                            thirdPoint = AnimationHelper.updateSlowEndAnim(thirdPoint, 1f, 0.1f, 0.0001f);
                            if (thirdPoint > 0.9f) {
                                fs3 = AnimationHelper.updateSlowEndAnim(fs3, 1f, 0.1f, 0.0001f);
                            }
                        }
                    }

                    if(!openedFirstPage) {
                        openFirstPage();
                        openedFirstPage = true;
                    }
                }
            }
        }
    }

    protected void drawBackgroundThings(int mouseX, int mouseY, float partialTick) {
        drawButtons(mouseX, mouseY, partialTick);

        drawBorders();
        drawProfileId();
        drawLoadingBar();
        drawNewsString();
        drawControlsString();
        drawStatus();
        drawLeftMenu(mouseX, mouseY, partialTick);
        drawWorkSpace();
        drawPage(mouseX, mouseY, partialTick);
        drawWindows(mouseX, mouseY, partialTick);
    }

    protected void drawBackground() {
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.1f, 0.1f, 0.1f, 0.9f);
        GuiDrawUtils.drawRect(0, 0, width, height);
        glEnable(GL_TEXTURE_2D);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        updateAnimations();

        glEnable(GL_BLEND);
        glDisable(GL_ALPHA_TEST);

        drawBackground();
        drawBackgroundThings(mouseX, mouseY, partialTick);
        if(popup != null) popup.drawScreen(mouseX, mouseY, partialTick);
    }

    private void releaseLeftMenuButtons(GuiButtonLeftMenu b1) {
        for (GuiButtonLeftMenu b : leftMenuButtons) {
            if (b != b1) {
                b.setIsPressed(false);
            }
        }
    }

    @Override
    public void updateScreen() {
        if (page != null) page.updateScreen();
        int size = windows.size();
        if (size > 0) windows.get(size - 1).updateScreen();
    }

    private void setActiveWindow(GuiWindow window) {
        int size = windows.size();
        GuiWindow prevWindow = windows.get(size - 1);
        prevWindow.setActive(false);

        int i = windows.indexOf(window);
        for (int j = size - 2; j >= i; j--) {
            GuiWindow prevPrevWindow = windows.get(j);
            windows.set(j, prevWindow);
            prevWindow = prevPrevWindow;
        }

        window.setActive(true);
        windows.set(size - 1, window);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        firstPoint = fs1 = secondPoint = fs2 = thirdPoint = fs3 = 1f;
        if (popup != null) {
            popup.mouseClicked(mouseX, mouseY, mouseButton);
            return;
        }
        int size = windows.size();
        if (size > 0 && windows.get(size - 1).mouseClickedWindow(mouseX, mouseY, mouseButton)) return;

        if (size > 1) {
            for (int i = size - 2; i >= 0; i--) {
                GuiWindow window = windows.get(i);
                if (window.tryActivateWindow(mouseX, mouseY, mouseButton)) {
                    setActiveWindow(window);
                    return;
                }
            }
        }

        if (page != null) page.mouseClicked(mouseX, mouseY, mouseButton);

        if (mouseButton == 0) {
            for (GuiButton b : this.buttonList) {
                if (b.mousePressed(this.mc, mouseX, mouseY)) {
                    this.selectedButton = b;
                    b.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(b);
                    return;
                }
            }

            if (!playOpenCloseAnim)
                for (GuiButtonLeftMenu b : leftMenuButtons) {
                    GuiButtonLeftMenu b1 = b.mousePressed(mouseX, mouseY);
                    if (b1 != null) {
                        releaseLeftMenuButtons(b);
                        this.selectedButton = b;
                        b1.playClickSound(this.mc.getSoundHandler());
                        this.leftMenuAction(b1);
                        return;
                    }
                }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        int size = windows.size();
        if (size > 0 && windows.get(size - 1).mouseClickMoveWindow(mouseX, mouseY))
            return;

        if (page != null) page.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        int size = windows.size();
        if (size > 0) windows.get(size - 1).mouseMovedOrUp(mouseX, mouseY, which);

        if (page != null) page.mouseMovedOrUp(mouseX, mouseY, which);
    }

    @Override
    public void handleMouseInput() {
        int mouseX = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int mouseY = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
        int k = Mouse.getEventButton();
        int d = Mouse.getEventDWheel();

        int size = windows.size();
        if (size > 0 && windows.get(size - 1).scrollInputWithReturn(mouseX, mouseY, d)) return;

        if (page != null) page.scrollInput(mouseX, mouseY, d);

        if (Mouse.getEventButtonState()) {
            if (this.mc.gameSettings.touchscreen && this.field_146298_h++ > 0) {
                return;
            }

            this.eventButton = k;
            this.lastMouseEvent = Minecraft.getSystemTime();
            this.mouseClicked(mouseX, mouseY, this.eventButton);
        } else if (k != -1) {
            if (this.mc.gameSettings.touchscreen && --this.field_146298_h > 0) {
                return;
            }

            this.eventButton = -1;
            this.mouseMovedOrUp(mouseX, mouseY, k);
        } else if (this.eventButton != -1 && this.lastMouseEvent > 0L) {
            long l = Minecraft.getSystemTime() - this.lastMouseEvent;
            this.mouseClickMove(mouseX, mouseY, this.eventButton, l);
        }
    }

    @Override
    public void keyTyped(char character, int key) {
        int size = windows.size();
        if (size > 0) windows.get(size - 1).keyTyped(character, key);
        if (page != null) page.keyTyped(character, key);
        super.keyTyped(character, key);
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        for(GuiWindow window : windows) {
            window.onWindowClosed();
        }
    }

    public void setPopup(GuiPopup popup) {
        this.popup = popup;
    }

    @Override
    public void popupAction(GuiButton button) {

    }
}
