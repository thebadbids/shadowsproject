package ru.krogenit.pda.gui.pages.map;

import lombok.Getter;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;

@Getter
public class QuestMapPin extends MapPin {

    private final List<String> description;

    public QuestMapPin(Vector2f position, String name, List<String> description) {
        super(name, EnumPinType.QUEST_KILL, position);
        this.description = description;
    }
}
