package ru.krogenit.pda.gui.pages.map;

import lombok.Getter;
import lombok.Setter;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import ru.xlv.core.common.player.character.CharacterType;

@Getter
@Setter
public class PlayerMapPin extends MapPin {
    private CharacterType characterType;
    private boolean isLeader;
    private int playerLvl;
    private String playerLocation;
    private Vector3f playerColor;

    public PlayerMapPin(Vector2f position, String playerName, CharacterType characterType, boolean isLeader, int playerLvl, String playerLocation, Vector3f playerColor) {
        super(playerName, EnumPinType.PLAYER, position);
        this.characterType = characterType;
        this.isLeader = isLeader;
        this.playerLvl = playerLvl;
        this.playerLocation = playerLocation;
        this.playerColor = playerColor;
    }
}
