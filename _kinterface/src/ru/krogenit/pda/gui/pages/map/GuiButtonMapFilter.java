package ru.krogenit.pda.gui.pages.map;

import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;

public class GuiButtonMapFilter extends GuiButtonAdvanced {

    private boolean isPressed;

    public GuiButtonMapFilter(int buttonId, float x, float y, float widthIn, float heightIn) {
        super(buttonId, x, y, widthIn, heightIn, "");
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            GuiDrawUtils.drawRect(isPressed ? textureHover : texture, this.xPosition, this.yPosition, this.width, this.height);
        }
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public boolean isPressed() {
        return isPressed;
    }
}
