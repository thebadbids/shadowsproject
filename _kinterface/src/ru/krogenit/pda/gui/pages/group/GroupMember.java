package ru.krogenit.pda.gui.pages.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;
import ru.xlv.core.common.player.character.CharacterType;

@Getter
@Setter
@AllArgsConstructor
public class GroupMember {
    private String playerName;
    private int lvl;
    private String location;
    private CharacterType characterType;
    private boolean isLeader;
    private ResourceLocation avatar;
}
