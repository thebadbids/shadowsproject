package ru.krogenit.pda.gui.pages.auction;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class GuiAuctionLot extends GuiButtonAdvanced {

    private List<GuiButtonAnimated> buttons = new ArrayList<>();
    private AuctionItem item;

    private float firstPoint, fs1;

    public GuiAuctionLot(float x, float y, float widthIn, float heightIn, String buttonText, AuctionItem item) {
        super(-1, x, y, widthIn, heightIn, buttonText);
        this.item = item;
        createButtons();
    }

    public void createButtons() {
        buttons.clear();
        float buttonWidth = ScaleGui.get(231f);
        float buttonHeight = ScaleGui.get(40f);
        float x = xPosition + ScaleGui.get(961f) - buttonWidth / 2f;
        float y = yPosition + ScaleGui.get(114f) - buttonHeight / 2f;
        GuiButtonAnimated b = new GuiButtonAnimated(0, x, y, buttonWidth, buttonHeight, "ЗАБРАТЬ ПОКУПКУ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttons.add(b);
    }

    private void actionPerformed(int id) {

    }

    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            updateAnimation();
            GuiDrawUtils.drawRect(texture, this.xPosition, this.yPosition, this.width, this.height);
            float x = xPosition + ScaleGui.get(21f);
            float y = yPosition + ScaleGui.get(20f);
            float itemWidth = ScaleGui.get(173f);
            float itemHeight = ScaleGui.get(116f);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(185/255f, 18/255f, 187/255f, 0.2f);
            tessellator.addVertex(x, y + itemHeight,0f);
            tessellator.addVertex(x + itemWidth, y + itemHeight, 0);
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0f);
            tessellator.addVertex(x + itemWidth, y, 0f);
            tessellator.addVertex(x, y, 0f);
            tessellator.draw();
            GL11.glShadeModel(GL11.GL_FLAT);
            GL11.glLineWidth(width / (1096 / 2f));
            GL11.glColor4f(98/255f,98/255f,98/255f,1.0f);
            GL11.glBegin(GL11.GL_LINE_STRIP);
            GL11.glVertex2f(x, y + itemHeight);
            GL11.glVertex2f(x + itemWidth, y  + itemHeight);
            GL11.glVertex2f(x + itemWidth, y);
            GL11.glVertex2f(x, y);
            GL11.glVertex2f(x, y + itemHeight);
            GL11.glEnd();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glColor4f(1f,1f,1f,1.0f);

            itemWidth = ScaleGui.get(174f);
            itemHeight = ScaleGui.get(103f);
            GuiDrawUtils.drawRect(item.getTexture(), x, y, itemWidth, itemHeight);
            drawText();

            x = xPosition + ScaleGui.get(226f);
            y = yPosition + ScaleGui.get(115f);
            float stringMinusYOffset = ScaleGui.get(14f);
            float stringPositiveYOffset = ScaleGui.get(10f);
            float stringXOffset = ScaleGui.get(27f);
            float iconWidth = ScaleGui.get(37f) * firstPoint;
            float iconHeight = ScaleGui.get(37f) * firstPoint;
            float fs = 1.15f * fs1;
            float iconYOffset = ScaleGui.get(2f);
            float valuesYOffset = 0;
            float valuesXOffset = ScaleGui.get(12f);
            GL11.glColor4f(1f,1f,1f,1f);
            GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth, iconHeight);
            iconWidth = ScaleGui.get(14f) * fs1;
            iconHeight = ScaleGui.get(14f) * fs1;
            String s = "НАЧАЛЬНАЯ СТАВКА: ";
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
            float xOffset = FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + stringXOffset + 10;
            GuiDrawUtils.drawRectCentered(TextureRegister.textureInvIconGold, x + xOffset, y - stringMinusYOffset + iconYOffset, iconWidth, iconHeight);
            fs = fs1;
            s = "" + item.getGoldStart();
            xOffset +=  valuesXOffset;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y - stringMinusYOffset + valuesYOffset, fs, 0xffffff);
            xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
            GuiDrawUtils.drawRectCentered(TextureRegister.textureInvIconSilver, x + xOffset, y - stringMinusYOffset + iconYOffset, iconWidth, iconHeight);
            s = "" + item.getSilverStart();
            xOffset += valuesXOffset;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y - stringMinusYOffset + valuesYOffset, fs, 0xffffff);
            xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
            GuiDrawUtils.drawRectCentered(TextureRegister.textureInvIconBronze, x + xOffset, y - stringMinusYOffset + iconYOffset, iconWidth, iconHeight);
            s = "" + item.getBronzeStart();
            xOffset += valuesXOffset;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y - stringMinusYOffset + valuesYOffset, fs, 0xffffff);

            s = "ВАША СТАВКА: ";
            fs = 1.15f * fs1;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
            xOffset = FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + stringXOffset + 10;
            GuiDrawUtils.drawRectCentered(TextureRegister.textureInvIconGold, x + xOffset, y + stringPositiveYOffset + iconYOffset, iconWidth, iconHeight);
            fs = fs1;
            s = "" + item.getGoldYour();
            xOffset +=  valuesXOffset;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y + stringPositiveYOffset + valuesYOffset, fs, 0xffffff);
            xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
            GuiDrawUtils.drawRectCentered(TextureRegister.textureInvIconSilver, x + xOffset, y + stringPositiveYOffset + iconYOffset, iconWidth, iconHeight);
            s = "" + item.getSilverYour();
            xOffset += valuesXOffset;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y + stringPositiveYOffset + valuesYOffset, fs, 0xffffff);
            xOffset += FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs + 20;
            GuiDrawUtils.drawRectCentered(TextureRegister.textureInvIconBronze, x + xOffset, y + stringPositiveYOffset + iconYOffset, iconWidth, iconHeight);
            s = "" + item.getBronzeYour();
            xOffset += valuesXOffset;
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + xOffset, y + stringPositiveYOffset + valuesYOffset, fs, 0xffffff);

            for(GuiButtonAnimated b : buttons) {
                b.drawButton(mouseX, mouseY);
            }
        }
    }

    protected void drawText() {
        float textScale = 48 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, displayString, this.xPosition + ScaleGui.get(208f), this.yPosition + ScaleGui.get(27f), textScale, 0xffffff);
        GL11.glColor4f(1f, 1f, 1f, 1f);
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
        }
    }


    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if(this.enabled && this.visible && isHovered(mouseX, mouseY)) {
            for(GuiButtonAnimated b : buttons) {
                if(b.mousePressed(mc, mouseX, mouseY)) {
                    actionPerformed(b.id);
                    return false;
                }
            }
        }

        return false;
    }
}
