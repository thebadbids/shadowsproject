package ru.krogenit.pda.gui.pages.auction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;

@Getter
@Setter
@AllArgsConstructor
public class AuctionItem {
    private ResourceLocation texture;
    private int goldStart, silverStart, bronzeStart;
    private int goldYour, silverYour, bronzeYour;
}
