package ru.krogenit.pda.gui.pages.wiki;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class WikiItem {
    private List<ResourceLocation> textures;
    private String localizedName;
    private EnumWikiItemType type;
    private EnumWikiSection section;
}
