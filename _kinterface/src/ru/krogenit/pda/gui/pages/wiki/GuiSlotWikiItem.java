package ru.krogenit.pda.gui.pages.wiki;

import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

public class GuiSlotWikiItem extends GuiSlotWiki {

    private final WikiItem item;

    public GuiSlotWikiItem(WikiItem item, float x, float y, float widthIn, float heightIn) {
        super(item.getSection(), x, y, widthIn, heightIn);
        this.item = item;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            updateAnimation();
            boolean isHovered = isHovered(mouseX, mouseY);
            Utils.bindTexture(texture);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            if(isHovered) tessellator.setColorRGBA_F(0f, 174/255f, 1f, 0.4f);
            else tessellator.setColorRGBA_F(0f, 174/255f, 1f, 0.2f);
            tessellator.addVertex(xPosition, yPosition + height,0f);
            tessellator.addVertex(xPosition + width, yPosition + height, 0);
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0.1f);
            tessellator.addVertex(xPosition + width, yPosition, 0f);
            tessellator.addVertex(xPosition, yPosition, 0f);
            tessellator.draw();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_FLAT);

            drawText();
        }
    }

    protected void drawText() {
        float x = xPosition + ScaleGui.get(43f);
        float y = yPosition + ScaleGui.get(239f);
        float iconWidth = ScaleGui.get(37f);
        float stringMinusYOffset = ScaleGui.get(12f);
        float stringPositiveYOffset = ScaleGui.get(8f);
        float stringXOffset = ScaleGui.get(27f);
        float fs = (36f / 32f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  item.getType().getLocalizedName(), x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  item.getLocalizedName(), x + stringXOffset, y + stringPositiveYOffset, fs, 0x1BC3EC);
    }
}
