package ru.krogenit.pda.gui.pages.profile;

import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.gui.windows.achievements.Achievement;
import ru.xlv.customfont.FontType;

public class GuiSlotAchieveProfile extends GuiButtonAdvanced {

    private Achievement achievement;
    private String stage;

    public GuiSlotAchieveProfile(int id, float x, float y, float width , float height, String string) {
        super(id, x, y, width, height, string);
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            boolean isHovering = isHovered(mouseX, mouseY);
            if(isHovering) GuiDrawUtils.drawRect(texture, xPosition, yPosition, width, height, 1f, 1f, 1f, 1f);
            else GuiDrawUtils.drawRect(texture, xPosition, yPosition, width, height, 0.7f, 0.7f, 0.7f, 1f);
            if(achievement != null) {
                float iconWidth = ScaleGui.get(90f);
                float iconHeight = ScaleGui.get(86f);
                GuiDrawUtils.drawRectCentered(achievement.getTexture(), xPosition + width / 2.0f, yPosition + ScaleGui.get(73f), iconWidth, iconHeight);

                float fs = 26 / 32f;
                float x = xPosition + width / 2f;
                float y = yPosition + ScaleGui.get(150f);
                GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, achievement.getAchievementName(), x, y, fs, 0xffffff);
                fs = 22 / 32f;
                y += ScaleGui.get(18f);
                GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, stage, x, y, fs, 0xffffff);
            } else {
                float offsetY = mc.displayHeight / 400f;
                float textScale = 26 / 32f;
                GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, displayString, this.xPosition + this.width / 2.0f,
                        this.yPosition + this.height / 1.5f - offsetY, textScale, ScaleGui.get(150f*textScale), -1, 0x474645, EnumStringRenderType.CENTERED);
            }
        }
    }

    public void setAchievement(Achievement achievement) {
        switch (achievement.getType()) {
            case BRONZE:
                setTexture(TextureRegister.texturePDAAchieveBronze);
                break;
            case SILVER:
                setTexture(TextureRegister.texturePDAAchieveSilver);
                break;
            case GOLD:
                setTexture(TextureRegister.texturePDAAchieveGold);
                break;
        }
        this.achievement = achievement;
        this.stage = "Этап " + (achievement.getType().ordinal() + 1);
    }
}
