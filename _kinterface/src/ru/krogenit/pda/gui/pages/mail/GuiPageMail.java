package ru.krogenit.pda.gui.pages.mail;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.mail.GuiWindowIncomingMessages;
import ru.krogenit.pda.gui.windows.mail.GuiWindowOutgoingMessages;
import ru.krogenit.pda.gui.windows.mail.GuiWindowWriteMessage;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Setter
@Getter
public class GuiPageMail extends AbstractGuiScreenAdvanced {

    private float firstPoint, secondPoint, fs1, fs2;
    private int mailTotal, mailFromFriends, mailAuction, mailAdmins;
    private final GuiPda pda;

    public GuiPageMail(GuiPda pda, float minAspect) {
        super(minAspect);
        this.pda = pda;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 231;
        float buttonHeight = 40;
        float x = ScaleGui.getCenterX(869, buttonWidth);
        float y = ScaleGui.getCenterY(642, buttonHeight);
        GuiButtonAnimated b = new GuiButtonAnimated(0, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАПИСАТЬ ПИСЬМО");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1158, buttonWidth);
        b = new GuiButtonAnimated(1, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВХОДЯЩИЕ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1446, buttonWidth);
        b = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИСХОДЯЩИЕ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 0) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
        } else if(b.id == 1) {
            pda.showOrCreateWindow(EnumWindowType.INCOMING_MESSAGES, new GuiWindowIncomingMessages(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_INBOX, true);
        } else if(b.id == 2) {
            pda.showOrCreateWindow(EnumWindowType.OUTGOING_MESSAGES, new GuiWindowOutgoingMessages(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_OUTBOX, true);
        }
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
            if(fs1 > 0.9f) {
                secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                if(secondPoint > 0.9f) {
                    fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                }
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();

        GL11.glColor4f(1f, 1f, 1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMailServerImage, 1160, 331, 1096, 299 * firstPoint);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 502, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 581, 1096 * fs1, 2);

        LocalDateTime time = LocalDateTime.now();
        float x = 1665;
        float y = 236;
        float fs = 72 / 32f;
        String data = DateTimeFormatter.ofPattern("dd.MM.yyyy").format(time);
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, data, x, y, fs, 0xffffff);
        y+=44;
        fs = 18 / 32f;
        String timeString = DateTimeFormatter.ofPattern("HH:mm:ss").format(time);
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, timeString, x, y, fs, 0xffffff);

        x = 629;
        y = 543;
        float iconWidth = 37;
        float stringMinusYOffset = 14;
        float stringPositiveYOffset = 10;
        float stringXOffset = 27;
        fs = 1.15f  * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ВХОДЯЩИЕ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  "" + mailTotal, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ОТ ДРУЗЕЙ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + mailFromFriends, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        fs = 1.15f  * fs2;
        x += 240;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        s = "АУКЦИОН: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + mailAuction, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "АДМИНИСТРАЦИЯ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + mailAdmins, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        drawButtons(mouseX, mouseY, partialTick);
    }
}
