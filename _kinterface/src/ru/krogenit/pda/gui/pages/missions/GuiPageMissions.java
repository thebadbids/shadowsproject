package ru.krogenit.pda.gui.pages.missions;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

@Getter
@Setter
public class GuiPageMissions extends AbstractGuiScreenAdvanced {

    private float firstPoint, secondPoint, fs1, fs2;

    private int completedCombatMissions, completedResearchMissions, completedSurviveMissions;
    private int combatExperience, researchExperience, surviveExperience;

    private int completedCombatMissionsToNextRank, totalCombatMissionsToNextRank;
    private int completedResearchMissionsToNextRank, totalResearchMissionsToNextRank;
    private int completedSurviveMissionsToNextRank, totalSurviveMissionsToNextRank;

    private EnumCombatMissionsRank combatRank, nextCombatRank;
    private EnumResearchMissionsRank researchRank, nextResearchRank;
    private EnumSurviveMissionsRank surviveRank, nextSurviveRank;

    private GuiPda pda;

    public GuiPageMissions(float minAspect, GuiPda pda) {
        super(minAspect);
        this.pda = pda;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 231;
        float buttonHeight = 40;
        float x = 1015;
        float y = ScaleGui.getCenterY(884, buttonHeight);
        GuiButtonAnimated b = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ТЕКУЩИЕ ЗАДАЧИ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x += 289;
        b = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЫПОЛНЕННЫЕ ЗАДАЧИ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 0) {
            pda.showCurrentMissions();
        } else if(b.id == 1) {
            pda.showCompletedMissions();
        }
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
            if(fs1 > 0.9f) {
                secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                if(secondPoint > 0.9f) {
                    fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                }
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glColor4f(1f, 1f, 1f,1f);

        float x = 785;
        float y = 494;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsBattleBg, x, y, 345, 623 * firstPoint);
        x += 375;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsResearchBg, x, y, 345, 623 * firstPoint);
        x += 375;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsSurviveBg, x, y, 345, 623 * firstPoint);

        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 834, 1096 * fs1, 2);
        x = 784;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsDevider, x, 550, 285 * fs1, 2);
        x+=375;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsDevider, x, 550, 285 * fs1, 2);
        x+=375;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsDevider, x, 550, 285 * fs1, 2);

        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsIconBattle, 674, 495, 68 * fs1, 72 * fs1);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsIconResearch, 1053, 495, 71 * fs1, 72 * fs1);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMissionsIconSurvive, 1428, 494, 60 * fs1, 72 * fs1);

        float fs = 48 / 32f * fs1;
        x = 735;
        y = 488;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "БОЕВЫЕ", x, y, fs, 0xffffff);
        y+=24;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "МИССИИ", x, y, fs, 0xffffff);
        x += 375;
        y = 488;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "МИССИИ", x, y, fs, 0xffffff);
        y+=24;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ИССЛЕДОВАНИЯ", x, y, fs, 0xffffff);
        x += 375;
        y = 488;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "МИССИИ", x, y, fs, 0xffffff);
        y+=24;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ВЫЖИВАНИЯ", x, y, fs, 0xffffff);

        x = 657;
        y = 594;
        float iconWidth = 37;
        float stringMinusYOffset = 14;
        float stringPositiveYOffset = 10;
        float stringXOffset = 27;
        fs = 1.15f  * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "РАНГ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + combatRank.getLocalizedName(), x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "СЛЕД.: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, nextCombatRank.getLocalizedName() + " (" + completedCombatMissionsToNextRank  + "/" + totalCombatMissionsToNextRank + ")",
                x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        y += 73;
        GL11.glColor4f(1f,1f,1f,1f);
        fs = 1.15f * fs2;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        s = "ВЫПОЛНЕНО: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + completedCombatMissions, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ПОЛ. ОПЫТ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + combatExperience, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        y -= 73;

        x += 375;
        fs = 1.15f  * fs1;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        s = "РАНГ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + researchRank.getLocalizedName(), x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "СЛЕД.: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, nextResearchRank.getLocalizedName() + " (" + completedResearchMissionsToNextRank  + "/" + totalResearchMissionsToNextRank + ")",
                x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        y += 73;
        fs = 1.15f * fs2;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        s = "ВЫПОЛНЕНО: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + completedResearchMissions, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs , 0x1BC3EC);
        s = "ПОЛ. ОПЫТ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + researchExperience, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        y -= 73;

        x += 375;
        fs = 1.15f  * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        s = "РАНГ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + surviveRank.getLocalizedName(), x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "СЛЕД.: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s , x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, nextSurviveRank.getLocalizedName() + " (" + completedSurviveMissionsToNextRank  + "/" + totalSurviveMissionsToNextRank + ")",
                x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        y += 73;
        fs = 1.15f * fs2;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        s = "ВЫПОЛНЕНО: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + completedSurviveMissions, x + stringXOffset  + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ПОЛ. ОПЫТ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + surviveExperience, x + stringXOffset  + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        drawButtons(mouseX, mouseY, partialTick);
    }
}

