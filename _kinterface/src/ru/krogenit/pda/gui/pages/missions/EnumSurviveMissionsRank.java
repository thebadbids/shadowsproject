package ru.krogenit.pda.gui.pages.missions;

public enum EnumSurviveMissionsRank {
    SCOUT("СКАУТ"), DIVERSIONIST("ДИВЕРСАНТ");

    String localized;

    EnumSurviveMissionsRank(String localized) {
        this.localized = localized;
    }

    public String getLocalizedName() {
        return localized;
    }
}
