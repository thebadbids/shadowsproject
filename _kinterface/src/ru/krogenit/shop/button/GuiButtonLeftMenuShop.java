package ru.krogenit.shop.button;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.TextureRegister;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiButtonLeftMenu;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;

public class GuiButtonLeftMenuShop extends GuiButtonLeftMenu {
    public GuiButtonLeftMenuShop(EnumPdaLeftMenuSection buttonId, float x, float y, float widthIn, float heightIn, String buttonText, boolean info) {
        super(buttonId, x, y, widthIn, heightIn, buttonText, info);
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        boolean hovered = isHovered(mouseX, mouseY);
        if(hovered && !this.hovered) {
            SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
        }
        this.hovered = hovered;
        super.drawButton(mc, mouseX, mouseY);
    }

    @Override
    public void setTexture(EnumPdaLeftMenuSection section) {
        ResourceLocation[] textures = TextureRegister.getPDAButtonTexture(section);
        setTexture(textures[0]);
        setTextureHover(textures[1]);
        setActiveTexture(textures[2]);
    }
}
