package ru.krogenit.shop.button;

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

public class GuiButtonAnimatedShop extends GuiButtonAnimated {

    private final boolean isPlatina;
    public GuiButtonAnimatedShop(int buttonId, float x, float y, float widthIn, float heightIn, String buttonText, boolean isPlatina) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
        this.isPlatina = isPlatina;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (enabled && visible) {
            drawButtonEffect(mouseX, mouseY);
            boolean hovered = isHovered(mouseX, mouseY);
            if(hovered && !this.hovered) {
                SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
            }
            this.hovered = hovered;
            if (hovered) {
                blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending > 1) blending = 1;
            } else {
                blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending < 0) blending = 0f;
            }

            Utils.bindTexture(texture);
            GL11.glColor4f(1f, 1f, 1f, 1f - blending);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

            if (textureHover != null) {
                Utils.bindTexture(textureHover);
                GL11.glColor4f(1f, 1f, 1f, blending);
                GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
            }
            GL11.glColor4f(1f, 1f, 1f, 1f);

            float iconSize = ScaleGui.get(22);
            float iconOff = ScaleGui.get(20);
            float offX = (FontType.Marske.getFontContainer().width(displayString) * ScaleGui.get(70 / 32f) - iconSize) / 2f;

            if(!isPlatina) GuiDrawUtils.drawRect(TextureRegister.textureInvIconCredit,
                    xPosition + width / 2f - offX - iconSize / 2f - iconOff, yPosition + iconSize / 2.5f, iconSize, iconSize);
            else GuiDrawUtils.drawRect(TextureRegister.textureInvIconPlatina,
                    xPosition + width / 2f - offX - iconSize / 2f - iconOff, yPosition + iconSize / 2.5f, iconSize, iconSize);

            drawText(mc, (int)-(offX), 0);
        }
    }

    protected void drawText(Minecraft mc, int xOffset, int yOffset) {
        float offsetY = ScaleGui.get(12f);
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, displayString, this.xPosition + this.width / 2.0f + xOffset + ScaleGui.get(2f),
                this.yPosition + this.height / 2.0f - offsetY + ScaleGui.get(2f), 90 / 32f, 0x000000);
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, displayString, this.xPosition + this.width / 2.0f + xOffset,
                this.yPosition + this.height / 2.0f - offsetY, 90 / 32f, 0xffffff);
    }
}
