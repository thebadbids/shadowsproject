package ru.krogenit.shop.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.inventory.popup.GuiPopupRenderer;
import ru.krogenit.shop.button.GuiButtonAnimatedShop;
import ru.krogenit.shop.pages.GuiPageShop;
import ru.krogenit.util.ColorUtils;
import ru.krogenit.util.DecimalUtils;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

public class GuiShopItem extends GuiButtonAdvanced {

    private final GuiPageShop pageShop;
    private final ShopItem shopItem;
    private final List<GuiButtonAnimatedShop> buttons = new ArrayList<>();

    public GuiShopItem(int id, float x, float y, float width, float height, ShopItem shopItem, GuiPageShop pageShop) {
        super(id, x, y, width, height, "");
        this.shopItem = shopItem;
        this.pageShop = pageShop;
        float bWidth = ScaleGui.get(221);
        float bHeight = ScaleGui.get(41);
        GuiButtonAnimatedShop b = new GuiButtonAnimatedShop(0, xPosition + ScaleGui.get(21), yPosition + height - ScaleGui.get(124),
                bWidth, bHeight, DecimalUtils.getFormattedStringWithSpaces(shopItem.getCreditCost()), false);
        b.setTexture(TextureRegister.textureShopButtonItem);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttons.add(b);
        b = new GuiButtonAnimatedShop(1, xPosition + ScaleGui.get(21), yPosition + height - ScaleGui.get(64),
                bWidth, bHeight, DecimalUtils.getFormattedStringWithSpaces(shopItem.getPlatinaCost()), true);
        b.setTexture(TextureRegister.textureShopButtonItem);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttons.add(b);
    }

    private void actionPerformed(GuiButtonAnimatedShop b) {
        if(b.id == 0) {
            pageShop.tryBuyItemCredits(shopItem);
        } else {
            pageShop.tryBuyItemPlatina(shopItem);
        }
    }

    public void draw(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        ItemStack itemStack = shopItem.getItemStack();
        Vector3f colorByRarity = ColorUtils.getColorByRarity(itemStack.getItem());
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GuiDrawUtils.drawGradient(xPosition, yPosition, width, height, 0f, 0f, 0f, 0f, colorByRarity.x, colorByRarity.y, colorByRarity.z, isHovered ? 1.0f : 0.6f);
        GL11.glColor4f(98 / 255f, 98 / 255f, 98 / 255f, 1.0f);
        GL11.glLineWidth(ScaleGui.get(2f));
        GL11.glBegin(GL11.GL_LINE_LOOP);
        GL11.glVertex2f(xPosition, yPosition);
        GL11.glVertex2f(xPosition + width, yPosition);
        GL11.glVertex2f(xPosition + width, yPosition + height);
        GL11.glVertex2f(xPosition, yPosition + height);
        GL11.glEnd();
        GL11.glEnable(GL11.GL_TEXTURE_2D);

        GuiDrawUtils.renderItem(itemStack, 0.9f, ScaleGui.get(30f), -ScaleGui.get(34f), xPosition, yPosition, width, height, 0f);

        float x = xPosition + ScaleGui.get(194);
        float y = yPosition + ScaleGui.get(6);
        float width = ScaleGui.get(77);
        float height = ScaleGui.get(38);
        float yOffset = ScaleGui.get(38);
        for(EnumItemBadge badge : shopItem.getBadges()) {
            GuiDrawUtils.drawRect(badge == EnumItemBadge.LTD ? TextureRegister.textureShopBadgeLtd : badge == EnumItemBadge.NEW ? TextureRegister.textureShopBadgeNew : TextureRegister.textureShopBadgeTop, x, y, width, height);
            y += yOffset;
        }

        Item item = itemStack.getItem();
        String itemName = itemStack.getDisplayName();
        String itemType = "";
        if(item instanceof ItemBase) {
            ItemBase itemBase =  (ItemBase) item;
            String displayName = itemBase.getDisplayName();
            if(displayName != null) itemName = displayName.toUpperCase();
            itemType = itemBase.getSecondName();
        }
        yOffset = isHovered ? ScaleGui.get(-136) : 0f;
        x = xPosition + this.width / 2f;
        y = yPosition + this.height;
        float fs = 50 / 32f;
        GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrBlack, itemName, x, y - ScaleGui.get(60f) + yOffset, fs, this.width, -1, 0xffffff, EnumStringRenderType.CENTERED);
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrRoman, itemType, x, y - ScaleGui.get(25f) + yOffset, 30 / 32f, 0xffffff);
        if(isHovered) {
            GuiDrawUtils.drawRect(TextureRegister.textureShopItemDevider, xPosition + ScaleGui.get(25), yPosition + ScaleGui.get(240), ScaleGui.get(213), ScaleGui.get(2));
            GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrRoman, "- или -", x, yPosition + ScaleGui.get(308), 20 / 32f, 0x666666);
            for(GuiButtonAnimatedShop b : buttons) {
                b.drawButtonEffect(mouseX, mouseY);
                b.drawButton(mouseX, mouseY);
            }
        }
    }

    public void drawPopup(int mouseX, int mouseY) {
        if(isHovered(mouseX, mouseY)) GuiPopupRenderer.drawPopup(mouseX, mouseY, shopItem.getItemStack());
    }

    @Override
    public void addYPosition(float y) {
        super.addYPosition(y);
        for(GuiButtonAnimatedShop b : buttons) {
            b.addYPosition(y);
        }
    }

    public boolean mousePressed(int mouseX, int mouseY) {
        for(GuiButtonAnimatedShop b : buttons) {
            if(b.mousePressed(mc, mouseX, mouseY)) {
                actionPerformed(b);
                return false;
            }
        }

        return mouseX >= xPosition && mouseY >= yPosition && mouseX < xPosition + width && mouseY < yPosition + height;
    }
}
