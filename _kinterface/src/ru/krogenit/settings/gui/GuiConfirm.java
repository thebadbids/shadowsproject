package ru.krogenit.settings.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.customfont.FontType;

public class GuiConfirm extends AbstractGuiScreenAdvanced {

    private final IGuiWithPopupConfirm parent;

    public GuiConfirm(IGuiWithPopupConfirm parent) {
        super(parent.getCurrentAspect());
        this.parent = parent;
        this.mc = Minecraft.getMinecraft();
        initGui();
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 171;
        float buttonHeight = 39;
        float x = 960;
        float y = -24 + 540;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПОДТВЕРДИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);

        y += buttonHeight * 1.35f;
        button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonCancel);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 0) {
            parent.applySettings();
        } else {
            parent.cancel();
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        GuiDrawUtils.renderConfirmPopup(960-317, 540-220, 960+317, 540+90, 40f);
        drawButtons(mouseX, mouseY, partialTick);
        float x = 960;
        float y = 540-164;
        float fs = 1.4f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "ПРИНЯТЬ ИЗМЕНЕНИЯ?", x, y, fs, 0xffffff);

        float iconWidth = 502;
        float iconHeight = 1;
        y += 25;
        x = 960;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureTreeBorderConfirm, x , y, iconWidth, iconHeight);
        y += 22;
        fs = 0.9f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "Подтвердите принятие изменений настроек и выход", x, y, fs, 0xffffff);
        y+=FontType.HelveticaNeueCyrLight.getFontContainer().height() * fs;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, "в предыдущее меню.", x, y, fs, 0xffffff);
        y+=FontType.HelveticaNeueCyrLight.getFontContainer().height() * fs + 6;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureTreeBorderConfirm, x , y, iconWidth, iconHeight);
    }
}
