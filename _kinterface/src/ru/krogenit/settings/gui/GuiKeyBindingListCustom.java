package ru.krogenit.settings.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiListExtended;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.EnumChatFormatting;
import org.apache.commons.lang3.ArrayUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.xlv.customfont.FontType;

import java.util.*;

@SideOnly(Side.CLIENT)
public class GuiKeyBindingListCustom extends GuiListExtended {
    private final GuiControlSettings guiControlSettings;
    private final Minecraft mc;
    private final GuiListExtended.IGuiListEntry[] guiListEntries;
    private float stringWidth = 0;
    private final GuiScroll guiScroll = new GuiScroll();

    public GuiKeyBindingListCustom(GuiControlSettings guiControlSettings, Minecraft minecraft) {
        super(minecraft, guiControlSettings.width, guiControlSettings.height, (int) ScaleGui.getCenterY(176), (int) (ScaleGui.getCenterY(1010)), (int) ScaleGui.get(40));
        this.guiControlSettings = guiControlSettings;
        this.mc = minecraft;
        List<KeyBinding> akeybinding = new ArrayList<>(Arrays.asList(ArrayUtils.clone(minecraft.gameSettings.keyBindings)));

        for(int i=0;i<akeybinding.size();i++) {
            KeyBinding keyBinding = akeybinding.get(i);
            if(keyBinding.isOnlyForCreative() && (mc.thePlayer == null || !mc.thePlayer.capabilities.isCreativeMode)) {
                akeybinding.remove(i);
                i--;
            }
        }


        Set keybinds = new HashSet();
        KeyBinding.getKeybinds().forEach(o -> {
            String category = (String)o;
            boolean foundKey = false;
            for(KeyBinding keyBinding : akeybinding) {
                if(keyBinding.getKeyCategory().equals(category)) {
                    foundKey = true;
                    break;
                }
            }
            if(foundKey) {
                keybinds.add(category);
            }
        });

        this.guiListEntries = new GuiListExtended.IGuiListEntry[akeybinding.size() + keybinds.size()];
        akeybinding.sort(KeyBinding::compareTo);
        int i = 0;
        String s = null;
        float y = 0;
        float yOffset = ScaleGui.get(50);

        for (KeyBinding keybinding : akeybinding) {
            String s1 = keybinding.getKeyCategory();

            if (!s1.equals(s)) {
                s = s1;
                this.guiListEntries[i++] = new CategoryEntry(s1);
                y += yOffset;
            }

            float l = FontType.HelveticaNeueCyrLight.getFontContainer().width(I18n.format(keybinding.getKeyDescription()));

            if (l > this.stringWidth) {
                this.stringWidth = l;
            }

            this.guiListEntries[i++] = new KeyEntry(keybinding);
            y += yOffset;
        }

        y += yOffset + ScaleGui.get(10);
        guiScroll.setScrollViewHeight(ScaleGui.get(858));
        guiScroll.setScrollTotalHeight(y);
    }

    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
//        this.drawBackground();

        if (mouseX > this.left && mouseX < this.right && mouseY > this.top && mouseY < this.bottom) {
            if (Mouse.isButtonDown(0) && this.func_148125_i()) {
                if (this.initialClickY == -1.0F) {
                    if (mouseY >= this.top && mouseY <= this.bottom) {
                        guiScroll.mouseClicked(mouseX, mouseY, 0);
                        this.initialClickY = (float) mouseY;
                    } else {
                        this.initialClickY = -2.0F;
                    }
                } else if (this.initialClickY >= 0.0F) {
                    guiScroll.mouseClickMove(mouseX, mouseY);
                    this.initialClickY = (float) mouseY;
                }
            } else {
                guiScroll.mouseMovedOrUp();
                for (; !this.mc.gameSettings.touchscreen && Mouse.next(); this.mc.currentScreen.handleMouseInput()) {
                    int d = Mouse.getEventDWheel();
                    guiScroll.mouseScroll(d);
                }

                this.initialClickY = -1.0F;
            }
        }
        this.amountScrolled = guiScroll.getScrollAnim();

        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);
        Tessellator tessellator = Tessellator.instance;
        float x = ScaleGui.getCenterX(962) - this.getListWidth() / 2f;
        float y = this.top + ScaleGui.get(4) - this.amountScrolled;

        this.drawSelectionBox((int) x, (int) y, mouseX, mouseY);
        float smoothHeight = ScaleGui.get(6);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        this.overlayBackground(0, this.top, 255, 255);
        this.overlayBackground(this.bottom, this.height, 255, 255);
        OpenGlHelper.glBlendFunc(770, 771, 0, 1);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_I(0, 0);
        tessellator.addVertexWithUV(this.left, (this.top + smoothHeight), 0.0D, 0.0D, 1.0D);
        tessellator.addVertexWithUV(this.right, (this.top + smoothHeight), 0.0D, 1.0D, 1.0D);
        tessellator.setColorRGBA_I(0, 255);
        tessellator.addVertexWithUV(this.right, this.top, 0.0D, 1.0D, 0.0D);
        tessellator.addVertexWithUV(this.left, this.top, 0.0D, 0.0D, 0.0D);
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_I(0, 255);
        tessellator.addVertexWithUV(this.left, this.bottom, 0.0D, 0.0D, 1.0D);
        tessellator.addVertexWithUV(this.right, this.bottom, 0.0D, 1.0D, 1.0D);
        tessellator.setColorRGBA_I(0, 0);
        tessellator.addVertexWithUV(this.right, (this.bottom - smoothHeight), 0.0D, 1.0D, 0.0D);
        tessellator.addVertexWithUV(this.left, (this.bottom - smoothHeight), 0.0D, 0.0D, 0.0D);
        tessellator.draw();

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glShadeModel(GL11.GL_FLAT);

        guiScroll.drawScroll(ScaleGui.getCenterX(1360), ScaleGui.getCenterY(230), ScaleGui.get(710), ScaleGui.getCenterY(950));
    }

    @Override
    protected void drawSelectionBox(int x, int y, int mouseX, int mouseY) {
        int i1 = this.getSize();
        Tessellator tessellator = Tessellator.instance;

        float y1 = y + this.headerPadding;
        float yOffset = ScaleGui.get(10);
        for (int j1 = 0; j1 < i1; ++j1) {
            int l1 = this.slotHeight - 4;

            if (y1 <= this.bottom && y1 + l1 >= this.top) {
                this.drawSlot(j1, x, (int) y1, l1, tessellator, mouseX, mouseY);
            }

            y1 += slotHeight + yOffset;
        }
    }

    @Override
    public int func_148135_f() {
        return (int) (this.getContentHeight() - (this.bottom - this.top - ScaleGui.get(4)));
    }

    @Override
    protected int getContentHeight() {
        return this.getSize() * this.slotHeight + this.headerPadding;
    }

    protected int getSize() {
        return this.guiListEntries.length;
    }

    public GuiListExtended.IGuiListEntry getListEntry(int p_148180_1_) {
        return this.guiListEntries[p_148180_1_];
    }

    protected int getScrollBarX() {
        return (int) ScaleGui.getCenterX(1400);
    }

    public int getListWidth() {
        return (int) ScaleGui.get(super.getListWidth() + 32);
    }

    @Override
    public boolean func_148179_a(int mouseX, int mouseY, int mouseButton) {
        if (this.func_148141_e(mouseY)) {
            for (IGuiListEntry guiListEntry : guiListEntries) {
                if (guiListEntry.mousePressed(0, mouseX, mouseY, mouseButton, 0, 0)) {
                    this.func_148143_b(false);
                    return true;
                }
            }
        }

        return false;
    }

    @SideOnly(Side.CLIENT)
    public class CategoryEntry implements GuiListExtended.IGuiListEntry {
        private final String string;

        public CategoryEntry(String string) {
            this.string = I18n.format(string).toUpperCase();
        }

        public void drawEntry(int p_148279_1_, int p_148279_2_, int p_148279_3_, int p_148279_4_, int p_148279_5_, Tessellator p_148279_6_, int p_148279_7_, int p_148279_8_, boolean p_148279_9_) {
            GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, string, ScaleGui.getCenterX(960), p_148279_3_ + p_148279_5_ - ScaleGui.get(18), 1.4f, 16777215);
        }

        public boolean mousePressed(int p_148278_1_, int p_148278_2_, int p_148278_3_, int p_148278_4_, int p_148278_5_, int p_148278_6_) {
            return false;
        }

        public void mouseReleased(int p_148277_1_, int p_148277_2_, int p_148277_3_, int p_148277_4_, int p_148277_5_, int p_148277_6_) {
        }
    }

    @SideOnly(Side.CLIENT)
    public class KeyEntry implements GuiListExtended.IGuiListEntry {
        private final KeyBinding field_148282_b;
        private final String keyDescription;
        private final GuiButtonAdvanced btnChangeKeyBinding;
        private final GuiButtonAnimated btnReset;

        protected KeyEntry(KeyBinding p_i45029_2_) {
            this.field_148282_b = p_i45029_2_;
            this.keyDescription = I18n.format(p_i45029_2_.getKeyDescription()).toUpperCase();
            float width = ScaleGui.get(270);
            float height = ScaleGui.get(39);
            this.btnChangeKeyBinding = new GuiButtonAdvanced(0, 0, 0, width, height, I18n.format(p_i45029_2_.getKeyDescription()).toUpperCase(), ScaleGui.get(1.8f));
            btnChangeKeyBinding.setTexture(TextureRegister.textureSettingsButton);
            width = ScaleGui.get(171);
            this.btnReset = new GuiButtonAnimated(0, 0, 0, width, height, I18n.format("controls.reset").toUpperCase());
            btnReset.setTexture(TextureRegister.textureTreeButtonCancel);
            btnReset.setTextureHover(TextureRegister.textureTreeButtonHover);
            btnReset.setMaskTexture(TextureRegister.textureTreeButtonMask);
        }

        public void drawEntry(int p_148279_1_, int x, int y, int p_148279_4_, int p_148279_5_, Tessellator p_148279_6_, int mouseX, int mouseY, boolean p_148279_9_) {
            boolean flag1 = GuiKeyBindingListCustom.this.guiControlSettings.buttonId == this.field_148282_b;
            float fs = 1.0f;
            GuiDrawUtils.drawRightStringNoXYScale(FontType.HelveticaNeueCyrLight, this.keyDescription, x + ScaleGui.get(140) - ScaleGui.get(GuiKeyBindingListCustom.this.stringWidth),
                    y + p_148279_5_ / 2f, fs, 16777215);
            this.btnReset.xPosition = x + ScaleGui.get(290);
            this.btnReset.yPosition = y;
            this.btnReset.enabled = this.field_148282_b.getKeyCode() != this.field_148282_b.getKeyCodeDefault();
            this.btnReset.drawButton(GuiKeyBindingListCustom.this.mc, mouseX, mouseY);
            this.btnChangeKeyBinding.xPosition = x - ScaleGui.get(10);
            this.btnChangeKeyBinding.yPosition = y;
            this.btnChangeKeyBinding.displayString = GameSettings.getKeyDisplayString(this.field_148282_b.getKeyCode()).toUpperCase();
            boolean flag2 = false;

            if (this.field_148282_b.getKeyCode() != 0) {
                KeyBinding[] akeybinding = GuiKeyBindingListCustom.this.mc.gameSettings.keyBindings;

                for (KeyBinding keybinding : akeybinding) {
                    if (keybinding != this.field_148282_b && keybinding.getKeyCode() == this.field_148282_b.getKeyCode()) {
                        flag2 = true;
                        break;
                    }
                }
            }

            if (flag1) {
                this.btnChangeKeyBinding.displayString = EnumChatFormatting.WHITE + "> " + EnumChatFormatting.YELLOW + this.btnChangeKeyBinding.displayString + EnumChatFormatting.WHITE + " <";
            } else if (flag2) {
                this.btnChangeKeyBinding.displayString = EnumChatFormatting.RED + this.btnChangeKeyBinding.displayString;
            }

            this.btnChangeKeyBinding.drawButton(GuiKeyBindingListCustom.this.mc, mouseX, mouseY);
        }

        public boolean mousePressed(int p_148278_1_, int p_148278_2_, int p_148278_3_, int p_148278_4_, int p_148278_5_, int p_148278_6_) {
            if (this.btnChangeKeyBinding.mousePressed(GuiKeyBindingListCustom.this.mc, p_148278_2_, p_148278_3_)) {
                GuiKeyBindingListCustom.this.guiControlSettings.buttonId = this.field_148282_b;
                return true;
            } else if (this.btnReset.mousePressed(GuiKeyBindingListCustom.this.mc, p_148278_2_, p_148278_3_)) {
                GuiKeyBindingListCustom.this.mc.gameSettings.setOptionKeyBinding(this.field_148282_b, this.field_148282_b.getKeyCodeDefault());
                KeyBinding.resetKeyBindingArrayAndHash();
                return true;
            } else {
                return false;
            }
        }

        public void mouseReleased(int index, int x, int y, int mouseEvent, int relativeX, int relativeY) {
            this.btnChangeKeyBinding.mouseReleased(x, y);
            this.btnReset.mouseReleased(x, y);
        }
    }
}