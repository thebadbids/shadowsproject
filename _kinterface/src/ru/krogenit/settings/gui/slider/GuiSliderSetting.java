package ru.krogenit.settings.gui.slider;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

import java.awt.*;

public class GuiSliderSetting extends GuiButtonAdvanced {
    private final String settingName;
    public float value;
    public boolean isMoving;
    private final GameSettings.Options option;
    private float blending;
    private final float yBasePos;

    public GuiSliderSetting(int id, float x, float y, float width, float height, GameSettings.Options option) {
        super(id, x, y, width, height, "");
        this.settingName = I18n.format(option.getEnumString());
        this.value = option.normalizeValue(Minecraft.getMinecraft().gameSettings.getOptionFloatValue(option));
        this.option = option;
        this.displayString = Minecraft.getMinecraft().gameSettings.getOptionName(option, Minecraft.getMinecraft().gameSettings.getOptionFloatValue(option));
        this.yBasePos = y;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            boolean hovered = isHovered(mouseX, mouseY);
            if (hovered) {
                blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending > 1) blending = 1;
            } else {
                blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending < 0) blending = 0f;
            }

            Utils.bindTexture(texture);
            if (enabled) GL11.glColor4f(1f, 1f, 1f, 1f);
            else GL11.glColor4f(0.3f, 0.3f, 0.3f, 1f);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

            if (enabled && blending > 0) {
                GL11.glColor4f(1f, 1f, 1f, blending);
                GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
            }

            mouseDragged(mc, mouseX, mouseY);

            GL11.glColor4f(1f, 1f, 1f, 1f);
            drawText(mc, 0, 0);
        }
    }

    protected void drawText(Minecraft mc, int xOffset, int yOffset) {
        float offsetY = mc.displayHeight / 400f;
        float textScale = 0.9f;

        String s = settingName.toUpperCase();
        GuiDrawUtils.drawRightStringNoXYScale(FontType.HelveticaNeueCyrLight, s, this.xPosition - this.width / 10.0f + xOffset,
                this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale, enabled ? 0xffffff : 0x444444);

        int j;
        if (enabled) {
            int r = ((0x959595 & 0xFF0000) >> 16) + (int) ((255 - ((0x959595 & 0xFF0000) >> 16)) * blending);
            int g = ((0x959595 & 0xFF00) >> 8) + (int) ((255 - ((0x959595 & 0xFF00) >> 8)) * blending);
            int b = (0x959595 & 0xFF) + (int) ((255 - ((0x959595 & 0xFF))) * blending);
            j = (int) Long.parseLong(Integer.toHexString(new Color(r, g, b).getRGB()), 16);
        } else {
            j = 0x333333;
        }
        s = displayString.toUpperCase();
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrBold, s, this.xPosition + this.width / 2.0f + xOffset,
                this.yPosition + this.height / 2.0f + yOffset - offsetY, textScale, j);
        GL11.glColor4f(1f, 1f, 1f, 1f);
    }

    /**
     * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over this button and 2 if it IS hovering
     * over this button.
     */
    public int getHoverState(boolean isHovering) {
        return 0;
    }

    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {
        if (this.visible) {
            if (this.isMoving) {
                this.value = (mouseX - (this.xPosition + 4)) / (this.width - 8);

                if (this.value < 0.0F) {
                    this.value = 0.0F;
                }

                if (this.value > 1.0F) {
                    this.value = 1.0F;
                }

                float f = this.option.denormalizeValue(this.value);
                this.displayString = mc.gameSettings.getOptionName(option, f);
            }

            Utils.bindTexture(TextureRegister.textureSettingsSlider);
            if (enabled) GL11.glColor4f(1f, 1f, 1f, 1f);
            else GL11.glColor4f(0.3f, 0.3f, 0.3f, 1f);

            float width = this.width / 16f;
            GuiDrawUtils.drawRect(this.xPosition + this.value * (this.width - width), this.yPosition, width, this.height);

            if (enabled && blending > 0) {
                GL11.glColor4f(1f, 1f, 1f, blending);
                GuiDrawUtils.drawRect(this.xPosition + this.value * (this.width - width), this.yPosition, width, this.height);
            }
        }
    }

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of
     * MouseListener.mousePressed(MouseEvent e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if (super.mousePressed(mc, mouseX, mouseY)) {
            this.value = (mouseX - (this.xPosition + 4)) / (this.width - 8);

            if (this.value < 0.0F) {
                this.value = 0.0F;
            }

            if (this.value > 1.0F) {
                this.value = 1.0F;
            }

            float f = this.option.denormalizeValue(this.value);
            this.displayString = mc.gameSettings.getOptionName(option, f);
            this.isMoving = true;
            return true;
        } else {
            return false;
        }
    }

    public void playClickSound(SoundHandler sound) {
    }

    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int mouseX, int mouseY) {
        this.isMoving = false;
    }

    public void setCurrentValue(float floatValue) {
        this.displayString = Minecraft.getMinecraft().gameSettings.getOptionName(option, floatValue);
        this.value = option.normalizeValue(floatValue);
    }

    public GameSettings.Options getOption() {
        return option;
    }

    public void setYPosByScroll(float scroll) {
        this.yPosition = yBasePos + scroll;
    }
}
