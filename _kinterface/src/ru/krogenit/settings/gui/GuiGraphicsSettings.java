package ru.krogenit.settings.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.src.TooltipManager;
import net.minecraft.src.TooltipProviderOptions;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import org.lwjgl.input.Mouse;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.client.renderer.ReloadableRendererManager;
import ru.krogenit.settings.SettingsPreset;
import ru.krogenit.settings.gui.button.GuiButtonSetting;
import ru.krogenit.settings.gui.slider.GuiSliderSetting;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.resource.ResourceManager;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiGraphicsSettings extends AbstractGuiScreenAdvanced implements IGuiWithPopupConfirm {

    private final GuiScreen parentGuiScreen;
    private final TooltipManager tooltipManager = new TooltipManager(this, new TooltipProviderOptions());

    private AbstractGuiScreenAdvanced popup;
    private AxisAlignedBB scissorAABB;
    private float scroll, scrollAnim, lastScroll;
    private float settingsHeight, viewHeight;
    private float scrollX, scrollY, scrollWidth, scrollHeight, scrollTextureHeight;
    private boolean mouseScrolling;
    private float mouseStartY;

    public GuiGraphicsSettings(GuiScreen guiScreen) {
        super(ScaleGui.SXGA);
        this.parentGuiScreen = guiScreen;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui() {
        super.initGui();
        this.buttonList.clear();

        float yStart = 240;
        float x = 75 + 960;
        float y = yStart;
        float buttonWidth = 270;
        float buttonHeight = 39;
        float yOffset = buttonHeight + 20;

        GuiButtonSetting gs = new GuiButtonSetting(GameSettings.Options.GRAPHICS_PRESET.returnEnumOrdinal(), ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), GameSettings.Options.GRAPHICS_PRESET, this);
        gs.setTexture(TextureRegister.textureSettingsButton);
        this.buttonList.add(gs);
        y += yOffset;
        boolean customSettings = mc.gameSettings.getCurrentValue(GameSettings.Options.GRAPHICS_PRESET) == 5;

        GameSettings.Options[] videoOptions = new GameSettings.Options[]{GameSettings.Options.FOV, GameSettings.Options.FRAMERATE_LIMIT, GameSettings.Options.GAMMA, GameSettings.Options.SHOW_FPS, GameSettings.Options.USE_FULLSCREEN,
                GameSettings.Options.GRAPHICS, GameSettings.Options.TEXTURE_SIZE, GameSettings.Options.RENDER_DISTANCE, GameSettings.Options.CHUNK_LOADING,
                GameSettings.Options.PRELOADED_CHUNKS, GameSettings.Options.CHUNK_UPDATES, GameSettings.Options.CHUNK_UPDATES_DYNAMIC, GameSettings.Options.LOAD_FAR, GameSettings.Options.FAST_MATH, GameSettings.Options.FAST_RENDER,
                GameSettings.Options.AMBIENT_OCCLUSION, GameSettings.Options.AO_LEVEL, GameSettings.Options.ADVANCED_OPENGL, GameSettings.Options.DYNAMIC_LIGHTS, GameSettings.Options.BLOOM, GameSettings.Options.LIGHTING, GameSettings.Options.LIGHTING_DEFERRED_DISTANCE,
                GameSettings.Options.LIGHTING_FORWARD_DISTANCE, GameSettings.Options.NORMAL_MAPPING, GameSettings.Options.SPECULAR_MAPPING, GameSettings.Options.GLOSS_MAPPING, GameSettings.Options.EMISSION_MAPPING,
                GameSettings.Options.PARTICLES, GameSettings.Options.MIPMAP_LEVELS, GameSettings.Options.MIPMAP_TYPE, GameSettings.Options.ANISOTROPIC_FILTERING, GameSettings.Options.CONNECTED_TEXTURES, GameSettings.Options.SMOOTH_FPS
        };

        for (GameSettings.Options o : videoOptions) {
            if (o.getEnumFloat()) {
                GuiSliderSetting gsl = new GuiSliderSetting(o.returnEnumOrdinal(), ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), o);
                gsl.setTexture(TextureRegister.textureSettingsButton);
                gsl.enabled = customSettings || o == GameSettings.Options.FOV || o == GameSettings.Options.GAMMA || o == GameSettings.Options.FRAMERATE_LIMIT;
                this.buttonList.add(gsl);
            } else {
                gs = new GuiButtonSetting(o.returnEnumOrdinal(), ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), o, this);
                gs.setTexture(TextureRegister.textureSettingsButton);
                gs.enabled = customSettings || o == GameSettings.Options.SHOW_FPS || o == GameSettings.Options.USE_FULLSCREEN;
                this.buttonList.add(gs);
            }

            y += yOffset;
        }

        viewHeight = yOffset * 7f;
        settingsHeight = y - yStart - yOffset;
        buttonWidth = 171;
        buttonHeight = 39;
        x = 840;
        y = 870;
        GuiButtonAnimated b = new GuiButtonAnimated(200, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ПОДТВЕРДИТЬ");
        b.setTexture(TextureRegister.textureTreeButtonAccept);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);
        x = 1080;
        b = new GuiButtonAnimated(201, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        b.setTexture(TextureRegister.textureTreeButtonCancel);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);

        if (popup != null) popup.initGui();
    }

    protected void actionPerformed(GuiButton guiButton) {
        this.actionPerformed(guiButton, 1);
    }

    private void actionPerformed(GuiButton button, int val) {
        if (button.enabled) {
            if (button.id == 200) {
                popup = new GuiConfirm(this);
                popup.initGui();
            } else if (button.id == 201) {
                this.mc.displayGuiScreen(this.parentGuiScreen);
            }
        }
    }

    private void drawMiscElements() {
        float fsx = 2.4f;
        String s = "НАСТРОЙКИ ГРАФИКИ";
        float x = 960;
        float y = 110f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0x626262);

        fsx = 0.8f;
        x = 1866;
        y = 937;
        s = "VERSION 0.163 BETA";
        GuiDrawUtils.drawRightStringRightBot(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0xffffff);
        s = "Shadows Project - an original MMORPG-FPS\n" +
                "Massively Multiplayer Online Role-Playing First\n" +
                "Person Shooter) in a fantasy anti-utopian hi-tec future where the horrors\n" +
                "of space and all the “charms” of interplanetary travel await you.";
        fsx = 0.7f;
        y = 971;
        GuiDrawUtils.drawSplittedStringRightBot(FontType.DeadSpace, s, x, y, fsx, 1000f, -1f, 0x626262, EnumStringRenderType.RIGHT);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureSettingsBorderTop, 960, -346 + 540, 821, 78);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureSettingsBorderBot, 960, 230 + 540, 821, 78);
        GuiDrawUtils.drawBotCentered(TextureRegister.textureESCLogo, 137, 960, 184, 53);

        x = 268 + 960;
        y = -315 + 540;
        float iconWidth = 7;
        float iconHeight = 4;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvArrowTop, x, y, iconWidth, iconHeight);
        y += iconHeight + 2;
        scrollTextureHeight = 429;
        float scrollValue = viewHeight / settingsHeight;
        float scrollYValue = scrollTextureHeight / settingsHeight;
        if (scrollValue > 1) scrollValue = 1f;
        if (scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += iconHeight / 2f + scrollAnim * scrollYValue;
        scrollX = ScaleGui.getCenterX(x, iconWidth);
        scrollY = ScaleGui.getCenterY(y, iconHeight);
        scrollWidth = ScaleGui.get(iconWidth);
        scrollHeight = ScaleGui.get(iconHeight);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvScrollBody, x, y, iconWidth, iconHeight);
        y = 130 + 540;
        iconHeight = 4;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvArrowBot, x, y, iconWidth, iconHeight);
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();
        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);

        if (scrollAnim < scroll) {
            float speed = AnimationHelper.getAnimationSpeed() * (scroll - scrollAnim) * 0.25f;
            if (speed < 0.1f) speed = 0.1f;
            scrollAnim += speed;
            if (scrollAnim > scroll) {
                scrollAnim = scroll;
            }
        } else if (scrollAnim > scroll) {
            float speed = AnimationHelper.getAnimationSpeed() * (scrollAnim - scroll) * 0.25f;
            if (speed < 0.1f) speed = 0.1f;
            scrollAnim -= speed;
            if (scrollAnim < scroll) {
                scrollAnim = scroll;
            }
        }

        for (Object o : this.buttonList) {
            if (o instanceof GuiButtonSetting)
                ((GuiButtonSetting) o).setYPosByScroll(ScaleGui.get(-scrollAnim));
            else if (o instanceof GuiSliderSetting)
                ((GuiSliderSetting) o).setYPosByScroll(ScaleGui.get(-scrollAnim));
        }

        drawMiscElements();

        glEnable(GL_SCISSOR_TEST);
        float x = ScaleGui.getCenterX(0, 0);
        float y = ScaleGui.getCenterY(274, 0);
        float width = this.width;
        float height = ScaleGui.getCenterY(924, 0) - y;
        glScissor((int) x, (int) y, (int) width, (int) height);
        scissorAABB = AxisAlignedBB.getBoundingBox(x, this.height - y - height, -100, x + width, this.height - y, 100);
        for (Object o : this.buttonList) {
            if (!(o instanceof GuiButtonAnimated))
                ((GuiButton) o).drawButton(this.mc, mouseX, mouseY);
        }
        glDisable(GL_SCISSOR_TEST);
        for (Object o : this.buttonList) {
            if (o instanceof GuiButtonAnimated)
                ((GuiButton) o).drawButton(this.mc, mouseX, mouseY);
        }

        this.tooltipManager.drawTooltips(mouseX, mouseY, this.buttonList);

        if (popup != null) popup.drawScreen(mouseX, mouseY, partialTick);
    }

    public void setPreset(int preset) {
        if (preset == 5) {
            for (Object o : buttonList) {
                if (o instanceof GuiButton) {
                    GuiButton b = (GuiButton) o;
                    b.enabled = true;
                }
            }
        } else {
            for (int i = 1; i < buttonList.size(); i++) {
                Object o = buttonList.get(i);
                if (o instanceof GuiButtonSetting) {
                    GuiButtonSetting b = (GuiButtonSetting) o;
                    if (b.getOption() != GameSettings.Options.SHOW_FPS && b.getOption() != GameSettings.Options.USE_FULLSCREEN) {
                        b.enabled = false;
                        Float value = SettingsPreset.presets.get(preset).getValues().get(b.getOption());
                        if (value != null) b.setCurrentValue((int) value.floatValue());
                    }
                } else if (o instanceof GuiSliderSetting) {
                    GuiSliderSetting b = (GuiSliderSetting) o;
                    if (b.getOption() != GameSettings.Options.FOV && b.getOption() != GameSettings.Options.GAMMA && b.getOption() != GameSettings.Options.FRAMERATE_LIMIT) {
                        b.enabled = false;
                        Float value = SettingsPreset.presets.get(preset).getValues().get(b.getOption());
                        if (value != null)
                            b.setCurrentValue(SettingsPreset.presets.get(preset).getValues().get(b.getOption()));
                    }
                }
            }
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (popup != null) {
            popup.mouseClicked(mouseX, mouseY, mouseButton);
        } else {
            for (Object o : buttonList) {
                if (o instanceof GuiButtonSetting) {
                    GuiButtonSetting b = (GuiButtonSetting) o;
                    if (b.isPressed) b.isPressed = b.isHovered(mouseX, mouseY);
                }
            }

            if (mouseButton == 0) {
                for (GuiButton b : this.buttonList) {
                    if ((scissorAABB.isVecInside(Vec3.createVectorHelper(mouseX, mouseY, 0)) || b instanceof GuiButtonAnimated) && b.mousePressed(this.mc, mouseX, mouseY)) {
                        this.selectedButton = b;
                        b.playClickSound(this.mc.getSoundHandler());
                        this.actionPerformed(b);
                    }
                }
            }

            if (mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);

        if (mouseScrolling) {
            float heightDiff = settingsHeight - viewHeight;
            if (heightDiff > 0) {
                float diff = ScaleGui.get(scrollTextureHeight) / settingsHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > settingsHeight - viewHeight) {
                    scroll = settingsHeight - viewHeight;
                }
            }
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
        mouseScrolling = false;
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int d = Mouse.getEventDWheel();
        if (d != 0) {
            float heightDiff = settingsHeight - viewHeight;
            if (heightDiff > 0) {
                scroll -= d / 2f;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > settingsHeight - viewHeight) {
                    scroll = settingsHeight - viewHeight;
                }
            }
        }
    }

    @Override
    public void applySettings() {
        int prevTextureSize = mc.gameSettings.textureSize;
        List<GuiButton> buttons = new ArrayList<>(buttonList);

        for (GuiButton o : buttons) {
            if (o instanceof GuiButtonSetting) {
                GuiButtonSetting b = (GuiButtonSetting) o;
                mc.gameSettings.setValue(b.getOption(), b.getCurrentValue());
            } else if (o instanceof GuiSliderSetting) {
                GuiSliderSetting b = (GuiSliderSetting) o;
                mc.gameSettings.setOptionFloatValue(b.getOption(), b.getOption().denormalizeValue(b.value));
            }
        }

        int textureSize = mc.gameSettings.textureSize;

        if(textureSize != prevTextureSize) {
            ResourceManager.clear();
            ReloadableRendererManager.reload();
        }

        mc.gameSettings.saveOptions();
        this.mc.displayGuiScreen(this.parentGuiScreen);

        if(mc.getRefreshTexturePacksScheduled()) {
            mc.refreshResources();
            mc.setRefreshTexturePacksScheduled(false);
        }
    }

    @Override
    public void cancel() {
        popup = null;
    }

    @Override
    public float getCurrentAspect() {
        return minAspect;
    }
}
