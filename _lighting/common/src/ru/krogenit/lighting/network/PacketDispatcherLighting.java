package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import ru.krogenit.lighting.CoreLightingCommon;

public class PacketDispatcherLighting {

	private static final SimpleNetworkWrapper dispatcher = NetworkRegistry.INSTANCE.newSimpleChannel(CoreLightingCommon.MODID);
	private static byte packetId = 0;
	
	public static void registerMessage(Class handlerClass, Class messageClass, Side side) {
		dispatcher.registerMessage(handlerClass, messageClass, packetId++, side);
	}

	public static void sendTo(IMessage message, EntityPlayerMP player) {
		dispatcher.sendTo(message, player);
	}

	private static void sendToAllAround(IMessage message, NetworkRegistry.TargetPoint point) {
		dispatcher.sendToAllAround(message, point);
	}

	static void sendToAllAround(IMessage message, int dimension, double x, double y, double z, double range) {
		sendToAllAround(message, new NetworkRegistry.TargetPoint(dimension, x, y, z, range));
	}

	public static void sendToAllAround(IMessage message, EntityPlayer player, double range) {
		sendToAllAround(message, player.worldObj.provider.dimensionId, player.posX, player.posY, player.posZ, range);
	}
	
	public static void sendPacketToAllPlayers(IMessage message) {
		dispatcher.sendToAll(message);
	}

	public static void sendToDimension(IMessage message, int dimensionId) {
		dispatcher.sendToDimension(message, dimensionId);
	}

	public static void sendToServer(IMessage message) {
		dispatcher.sendToServer(message);
	}
}
