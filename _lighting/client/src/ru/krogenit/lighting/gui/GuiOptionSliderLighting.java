package ru.krogenit.lighting.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@SideOnly(Side.CLIENT)
public class GuiOptionSliderLighting extends GuiButton {
    public float value;
    public boolean field_146135_o;

    NumberFormat formatter = new DecimalFormat("#0.00");

    public GuiOptionSliderLighting(int p_i45016_1_, int p_i45016_2_, int p_i45016_3_, String displayString, float currentValue) {
        super(p_i45016_1_, p_i45016_2_, p_i45016_3_, 150, 20, "");
        this.value = 1.0F;
        this.value = currentValue;
        this.displayString = displayString + formatter.format(currentValue);
    }

    /**
     * Returns 0 if the button is disabled, 1 if the mouse is NOT hovering over this button and 2 if it IS hovering over
     * this button.
     */
    public int getHoverState(boolean isHovering) {
        return 0;
    }

    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {
        if (this.visible) {
            if (this.field_146135_o) {
                if (this.value < 0.0F) {
                    this.value = 0.0F;
                } else if (this.value > 1.0F) {
                    this.value = 1.0F;
                }

                String oldValue = formatter.format(value);
                this.value = (mouseX - (this.xPosition + 4)) / (this.width - 8);

                if (this.value < 0.0F) {
                    this.value = 0.0F;
                } else if (this.value > 1.0F) {
                    this.value = 1.0F;
                }
                String anim = formatter.format(value);
                this.displayString = displayString.substring(0, displayString.indexOf(oldValue)) + anim;
            }

            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.drawTexturedModalRect(this.xPosition + (int) (this.value * (float) (this.width - 8)), this.yPosition, 0, 66, 4, 20);
            this.drawTexturedModalRect(this.xPosition + (int) (this.value * (float) (this.width - 8)) + 4, this.yPosition, 196, 66, 4, 20);
        }
    }

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if (super.mousePressed(mc, mouseX, mouseY)) {
            if (this.value < 0.0F) {
                this.value = 0.0F;
            } else if (this.value > 1.0F) {
                this.value = 1.0F;
            }

            String oldValue = formatter.format(value);
            this.value = (mouseX - (this.xPosition + 4)) / (this.width - 8);

            if (this.value < 0.0F) {
                this.value = 0.0F;
            } else if (this.value > 1.0F) {
                this.value = 1.0F;
            }
            String anim = formatter.format(value);
            this.displayString = displayString.substring(0, displayString.indexOf(oldValue)) + anim;
            this.field_146135_o = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int p_146118_1_, int p_146118_2_) {
        this.field_146135_o = false;
    }
}