package ru.krogenit.lighting.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.world.ChunkWatchEvent;
import ru.krogenit.lighting.LightManagerServer;
import ru.krogenit.lighting.PointLightServer;
import ru.krogenit.lighting.network.PacketDispatcherLighting;
import ru.krogenit.lighting.network.PacketUpdateBlockLight;

import java.util.List;

public class ChunkLightingEvents {

	@SubscribeEvent
	@SuppressWarnings("unused")
	public void event(ChunkWatchEvent.Watch event) {
		List<PointLightServer> lightsInChunk = LightManagerServer.getLightsFromChunk(event.chunk.chunkXPos, event.chunk.chunkZPos);
		if(lightsInChunk != null && lightsInChunk.size() > 0)
			for(PointLightServer p : lightsInChunk) {
				PacketDispatcherLighting.sendTo(new PacketUpdateBlockLight(p.power, p.color.x, p.color.y, p.color.z, (int) (p.pos.x - LightManagerServer.BLOCK_OFFSET), (int)(p.pos.y - LightManagerServer.BLOCK_OFFSET), (int)(p.pos.z - LightManagerServer.BLOCK_OFFSET)), event.player);
			}
	}
}
