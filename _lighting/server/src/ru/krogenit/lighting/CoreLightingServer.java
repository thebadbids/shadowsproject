package ru.krogenit.lighting;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraftforge.common.MinecraftForge;
import ru.krogenit.lighting.event.BlockLightingEvents;
import ru.krogenit.lighting.event.ChunkLightingEvents;
import ru.krogenit.lighting.item.ItemLightPlacer;
import ru.krogenit.lighting.network.*;

@Mod(modid = CoreLightingCommon.MODID, name = "Lighting Manager", version = "1.0.0")
@SuppressWarnings("unused")
public class CoreLightingServer {

    @Instance(CoreLightingCommon.MODID)
    public static CoreLightingServer instance;

    @EventHandler
    public void event(FMLPreInitializationEvent event) {
        PacketDispatcherLighting.registerMessage(PacketUpdateLightHandlerServer.class, PacketUpdateLight.class, Side.SERVER);
        PacketDispatcherLighting.registerMessage(PacketUpdateBlockLightHandlerServer.class, PacketUpdateBlockLight.class, Side.SERVER);
        MinecraftForge.EVENT_BUS.register(new BlockLightingEvents());
        MinecraftForge.EVENT_BUS.register(new ChunkLightingEvents());
        LightManagerServer.loadLights();
    }

    @EventHandler
    public void event(FMLInitializationEvent e) {
        LightManager.registerLights();
        registerItem();
    }

    private void registerItem() {
        CoreLightingCommon.itemLightPlacer = new ItemLightPlacer();
        GameRegistry.registerItem(CoreLightingCommon.itemLightPlacer, "itemLightPlacer");
    }

    @EventHandler
    public void event(FMLServerStoppingEvent event) {
        LightManagerServer.saveLights();
    }
}
