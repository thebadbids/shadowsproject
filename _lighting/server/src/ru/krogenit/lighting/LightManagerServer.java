package ru.krogenit.lighting;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.minecraft.block.Block;
import net.minecraft.util.MathHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.util.vector.Vector3f;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class LightManagerServer extends LightManager {

	@ToString
	@RequiredArgsConstructor
	private static class PositionChunk {
		private final int x, z;

		@Override
		@SuppressWarnings("all")
		public boolean equals(Object o) {
			PositionChunk that = (PositionChunk) o;
			return x == that.x && z == that.z;
		}

		@Override
		public int hashCode() {
			return Objects.hash(x, z);
		}
	}

	@ToString
	@RequiredArgsConstructor
	private static class PositionPointLight {
		private final float x, y, z;

		@Override
		@SuppressWarnings("all")
		public boolean equals(Object o) {
			PositionPointLight that = (PositionPointLight) o;
			return Float.compare(that.x, x) == 0 && Float.compare(that.y, y) == 0 && Float.compare(that.z, z) == 0;
		}

		@Override
		public int hashCode() {
			return Objects.hash(x, y, z);
		}
	}

	private static final Logger logger = LogManager.getLogger("LIGHT MANAGER");
	private static final Map<PositionChunk, List<PointLightServer>> serverLightsChunked = new HashMap<>();
	public static final float BLOCK_OFFSET = 0.55f;
	protected static Map<PositionPointLight, PointLightServer> allLights = new HashMap<>();
	static HashMap<Block, PointLightServer> blockLightTypes = new HashMap<>();

	public static boolean isLightBlock(Block b) {
		return blockLightTypes.get(b) != null;
	}

	public static void removeLight(int x, int y, int z) {
		PointLightServer light = getLight(x, y, z);
		if (light != null) {
			if (allLights.remove(new PositionPointLight(light.pos.x, light.pos.y, light.pos.z)) != null) {
				int chunkX = x >> 4;
				int chunkZ = z >> 4;
				List<PointLightServer> pointLightServers = getLightsFromChunk(chunkX, chunkZ);
				pointLightServers.remove(light);
			}
		}
	}
	
	private static void addLightToChunkServer(PointLightServer l) {
		int chunkX = MathHelper.floor_double(l.pos.x) >> 4;
		int chunkZ = MathHelper.floor_double(l.pos.z) >> 4;
		getLightsFromChunk(chunkX, chunkZ).add(l);
	}

	@Nonnull
	public static List<PointLightServer> getLightsFromChunk(int x, int z) {
		return serverLightsChunked.computeIfAbsent(new PositionChunk(x, z), positionChunk -> new ArrayList<>());
	}
	
	public static void addLightToServer(PointLightServer p) {
		allLights.put(new PositionPointLight(p.pos.x, p.pos.y, p.pos.z), p);
		addLightToChunkServer(p);
	}
	
	public static void updateLightServer(int x, int y, int z, float r, float g, float b, float power) {
		PointLightServer p = getLight(x, y, z);
		if(p != null) {
			p.color.x = r;
			p.color.y = g;
			p.color.z = b;
			p.power = power;
		} else {
			p = new PointLightServer(new Vector3f(x + BLOCK_OFFSET, y + BLOCK_OFFSET, z + BLOCK_OFFSET), new Vector3f(r, g, b), power);
			addLightToServer(p);
		}
	}

	@Nullable
	public static synchronized PointLightServer getLight(int x, int y, int z) {
		return allLights.get(new PositionPointLight(x + BLOCK_OFFSET, y + BLOCK_OFFSET, z + BLOCK_OFFSET));
	}
	
	public static void saveLights() {
		ConfigPointLightServer configPointLightServer = new ConfigPointLightServer();
		allLights.values().forEach(configPointLightServer::addPointLightServer);
		configPointLightServer.save();
		logger.log(Level.INFO, "Сохранено " + allLights.size() + " источников света");
	}
	
	public static void loadLights() {
		ConfigPointLightServer configPointLightServer = new ConfigPointLightServer();
		configPointLightServer.load();
		configPointLightServer.getPointLightServers().forEach(pointLightServerWrap -> addLightToServer(new PointLightServer(
				new Vector3f(pointLightServerWrap.getX(), pointLightServerWrap.getY(), pointLightServerWrap.getZ()),
				new Vector3f(pointLightServerWrap.getR(), pointLightServerWrap.getG(), pointLightServerWrap.getB()),
				pointLightServerWrap.getPower()
		)));
		logger.log(Level.INFO, "Загружено " + allLights.size() + " источников света");
	}
}
