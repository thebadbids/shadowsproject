package ru.krogenit.lighting;

import lombok.Getter;
import lombok.ToString;
import org.lwjgl.util.vector.Vector3f;

import java.io.Serializable;
import java.util.Objects;

@Getter
@ToString
public class PointLightServer implements Serializable {

    public Vector3f pos, color;
    public float power;

    public PointLightServer(PointLightServer p) {
        this.pos = new Vector3f(p.getPos());
        this.color = new Vector3f(p.getColor());
        this.power = p.getPower();
    }

    public PointLightServer(Vector3f pos, Vector3f color, float power) {
        this.pos = pos;
        this.color = color;
        this.power = power;
    }

    public PointLightServer(float r, float g, float b, float power) {
        this.pos = new Vector3f();
        this.color = new Vector3f(r, g, b);
        this.power = power;
    }

    public void setPosition(Vector3f pos) {
        this.pos.x = pos.x;
        this.pos.y = pos.y;
        this.pos.z = pos.z;
    }

    public boolean equals(PointLightServer pointLightServer) {
        return pointLightServer.pos.equals(pos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pos);
    }
}
