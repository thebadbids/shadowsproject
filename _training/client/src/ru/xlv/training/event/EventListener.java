package ru.xlv.training.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import org.lwjgl.input.Keyboard;
import ru.xlv.core.XlvsCore;
import ru.xlv.training.XlvsTrainingMod;
import ru.xlv.training.network.PacketTrainingKeyPress;

public class EventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(InputEvent.KeyInputEvent event) {
        if(XlvsTrainingMod.INSTANCE.getActiveTrainer() != null) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketTrainingKeyPress(Keyboard.getEventKey()));
        }
    }
}
