package ru.krogenit.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.input.Keyboard;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.gui.GameOverlayElement;
import ru.xlv.customfont.FontType;
import ru.xlv.training.XlvsTrainingMod;
import ru.xlv.training.common.trainer.*;

import static org.lwjgl.opengl.GL11.*;

public class HudTrainingElement extends GameOverlayElement {

    private static final Minecraft mc = Minecraft.getMinecraft();

    @Override
    public void init(ScaledResolution scaledResolution) {
        super.init(scaledResolution);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {

    }

    @Override
    public void renderPre(RenderGameOverlayEvent.Pre event) {
        ITrainer activeTrainer = XlvsTrainingMod.INSTANCE.getActiveTrainer();
        if (event.type == RenderGameOverlayEvent.ElementType.HOTBAR && !mc.thePlayer.capabilities.isCreativeMode && activeTrainer != null) {
            glMatrixMode(GL_PROJECTION);
            glPushMatrix();
            glLoadIdentity();
            glOrtho(0.0D, mc.displayWidth, mc.displayHeight, 0.0D, 1000.0D, 3000.0D);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glLoadIdentity();
            glTranslatef(0.0F, 0.0F, -2000.0F);

            glEnable(GL_BLEND);
            glAlphaFunc(GL_GREATER, 0.0001f);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glDepthMask(false);
            glDisable(GL_ALPHA_TEST);

            float x = 1600;
            float y = 650;
            float halfSizeX = 250;
            float sizeY = 200;
            float rectX1 = ScaleGui.getCenterX(x - halfSizeX);
            float rectY1 = ScaleGui.getCenterY(y);
            float rectX2 = ScaleGui.getCenterX(x + halfSizeX);
            float rectY2 = ScaleGui.getCenterY(y + sizeY);
            float halfY1 = (rectY2 - rectY1) / 1.4f;
            float halfY2 = (rectY2 - rectY1) / 1.4f;
            glDisable(GL_TEXTURE_2D);
            GuiDrawUtils.renderCuttedRect(rectX1, rectY1, rectX2, rectY2, ScaleGui.get(45f), halfY1, halfY2, 0f, 0f, 0f, 0.95f, 0.1f, 0.4f, 0.3f, 0.95f);
            glEnable(GL_TEXTURE_2D);

            String header = "Header";
            String description = "Description";

            if(activeTrainer instanceof EquipItemTrainer) {
                EquipItemTrainer equipItemTrainer = (EquipItemTrainer) activeTrainer;
                Item item = equipItemTrainer.getItem();
                String itemName = "";
                if(item instanceof ItemBase) itemName = ((ItemBase) item).getDisplayName();
                if(itemName == null) itemName = item.getUnlocalizedName();
                header = "ЭКИПИРУЙТЕ ПРЕДМЕТ: " + itemName.toUpperCase();
                description = equipItemTrainer.getDescription();
            } else if(activeTrainer instanceof ItemStackTrainer) {
                ItemStackTrainer equipItemTrainer = (ItemStackTrainer) activeTrainer;
                ItemStack itemStack = equipItemTrainer.getItemStack();
                String itemName = "";
                if(itemStack.getItem() instanceof ItemBase) itemName = ((ItemBase) itemStack.getItem()).getDisplayName();
                if(itemName == null) itemName = itemStack.getUnlocalizedName();
                header = "СДЕЛАЙТЕ ЧТО НИБУДЬ С ПРЕДМЕТОМ: " + itemName.toUpperCase();
                description = equipItemTrainer.getDescription();
            } else if(activeTrainer instanceof KeyTrainer) {
                KeyTrainer keyTrainer = (KeyTrainer) activeTrainer;
                header = "НАЖМИТЕ НА КЛАВИШУ: " + Keyboard.getKeyName(keyTrainer.getKeyCode());
                description = keyTrainer.getDescription();
            } else if(activeTrainer instanceof MarkerTrainer) {
                MarkerTrainer markerTrainer = (MarkerTrainer) activeTrainer;
                WorldPosition worldPosition = markerTrainer.getWorldPosition();
                header = "ДОБЕРИТЕСЬ ДО МЕТКИ: " + (int) worldPosition.getX() + " " + (int) worldPosition.getY() + " " + (int) worldPosition.getZ();
                description = markerTrainer.getDescription();
            } else if(activeTrainer instanceof SkillLearnTrainer) {
                SkillLearnTrainer skillLearnTrainer = (SkillLearnTrainer) activeTrainer;
                header = "ИЗУЧИТЕ СКИЛЛ: " + skillLearnTrainer.getSkillId();
                description = skillLearnTrainer.getDescription();
            } else if(activeTrainer instanceof SkillSelectTrainer) {
                SkillSelectTrainer skillSelectTrainer = (SkillSelectTrainer) activeTrainer;
                header = "ВЫБЕРИТЕ СКИЛЛ: " + skillSelectTrainer.getSkillId();
                description = skillSelectTrainer.getDescription();
            } else if(activeTrainer instanceof SkillUseTrainer) {
                SkillUseTrainer skillSelectTrainer = (SkillUseTrainer) activeTrainer;
                header = "АКТИВИРУЙТЕ СКИЛЛ: " + skillSelectTrainer.getSkillId();
                description = skillSelectTrainer.getDescription();
            } else if(activeTrainer instanceof NpcInteractTrainer) {
                NpcInteractTrainer npcInteractTrainer = (NpcInteractTrainer) activeTrainer;
                header = "ПОГОВОРИТЕ С НПС: " + npcInteractTrainer.getNpcName();
                description = npcInteractTrainer.getDescription();
            }

            y += 35;
            float fs = 1.4f;
            GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, header, x, y, fs, 0xffffff);

            float iconWidth = 502;
            float iconHeight = 1;
            y += 26;
            GuiDrawUtils.drawCenterCentered(new ResourceLocation("skilltree", "textures/border_confirm.png"), x , y, iconWidth, iconHeight);
            y += 20;
            fs = 0.9f;
            y += GuiDrawUtils.drawSplittedStringCenter(FontType.HelveticaNeueCyrLight, description, x, y, fs, ScaleGui.get(600), -1, 0xffffff, EnumStringRenderType.CENTERED);
            y += 12;
            GuiDrawUtils.drawCenterCentered(new ResourceLocation("skilltree", "textures/border_confirm.png"), x , y, iconWidth, iconHeight);

            glAlphaFunc(GL_GREATER, 0.5f);
            glPopMatrix();
            glMatrixMode(GL_PROJECTION);
            glPopMatrix();
            glMatrixMode(GL_MODELVIEW);
        }
    }
}
