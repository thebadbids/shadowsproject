package ru.xlv.training.network;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.training.XlvsTrainingMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketTrainingKeyPress implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int keyCode = bbis.readInt();
        XlvsTrainingMod.INSTANCE.getTrainingHandler().keyPressed(entityPlayer.getCommandSenderName(), keyCode);
    }
}
