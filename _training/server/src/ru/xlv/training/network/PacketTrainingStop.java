package ru.xlv.training.network;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.training.XlvsTrainingMod;

@NoArgsConstructor
public class PacketTrainingStop implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) {
        XlvsTrainingMod.INSTANCE.getTrainingManager().stopTraining(FlexPlayer.of(entityPlayer));
    }
}
