package ru.xlv.auction.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.auction.XlvsAuctionMod;
import ru.xlv.auction.common.util.AuctionUtils;
import ru.xlv.auction.handle.lot.AuctionLot;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketAuctionLotsSync implements IPacketCallbackOnServer {

    private static final RequestController<String> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private List<AuctionLot> auctionLots;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if (REQUEST_CONTROLLER.tryRequest(entityPlayer.getCommandSenderName())) {
            auctionLots = XlvsAuctionMod.INSTANCE.getAuctionHandler().getAuctionLotList();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        if(auctionLots != null) {
            bbos.writeBoolean(true);
            AuctionUtils.writeAuctionLotList(auctionLots, bbos);
        } else {
            bbos.writeBoolean(false);
            bbos.writeUTF(XlvsAuctionMod.INSTANCE.getLocalization().responseTooManyRequests);
        }
    }
}
