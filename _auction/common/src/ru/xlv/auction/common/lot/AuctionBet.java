package ru.xlv.auction.common.lot;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class AuctionBet {

    private String bidder;
    private int amount;
}
