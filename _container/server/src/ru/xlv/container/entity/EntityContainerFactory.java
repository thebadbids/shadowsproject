package ru.xlv.container.entity;

import net.minecraft.item.ItemStack;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.player.InsuranceManager;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.Character;

import java.util.ArrayList;
import java.util.List;

public class EntityContainerFactory {

    public static EntityContainer createContainer(ServerPlayer serverPlayer) {
        EntityContainer entityContainer = new EntityContainerServer(serverPlayer.getEntityPlayer().worldObj);
        entityContainer.setPosition(serverPlayer.getEntityPlayer().posX, serverPlayer.getEntityPlayer().posY + 1, serverPlayer.getEntityPlayer().posZ);
        if(serverPlayer.getEntityPlayer().worldObj.getBlock((int) serverPlayer.getEntityPlayer().posX, (int) serverPlayer.getEntityPlayer().posY + 1, (int) serverPlayer.getEntityPlayer().posZ) != null) {
            int r = 3;
            label:
            for (int y = -r; y < r; y++) {
                for (int x = -r; x < r; x++) {
                    for (int z = -r; z < r; z++) {
                        if(serverPlayer.getEntityPlayer().worldObj.getBlock((int) serverPlayer.getEntityPlayer().posX + x, (int) serverPlayer.getEntityPlayer().posY + 1 + y, (int) serverPlayer.getEntityPlayer().posZ + z) == null) {
                            entityContainer.setPosition((int) serverPlayer.getEntityPlayer().posX + x, (int) serverPlayer.getEntityPlayer().posY + 1 + y, (int) serverPlayer.getEntityPlayer().posZ + z);
                            break label;
                        }
                    }
                }
            }
        }
        entityContainer.getMatrixInventory().setMatrixSize(Character.MATRIX_INV_WIDTH, Character.MATRIX_INV_HEIGHT);
        List<ItemStack> list = new ArrayList<>(serverPlayer.getSelectedCharacter().getMatrixInventory().getItems().values());
        for (MatrixInventory.SlotType value : MatrixInventory.SlotType.values()) {
            ItemStack stackInSlot = serverPlayer.getEntityPlayer().inventory.getStackInSlot(value.getAssociatedSlotIndex());
            if (stackInSlot != null) {
                list.add(stackInSlot);
                serverPlayer.getEntityPlayer().inventory.setInventorySlotContents(value.getAssociatedSlotIndex(), null);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            ItemStack itemStack = list.get(i);
            if(!EnumItemTag.canBeDropped(itemStack) && serverPlayer.getEntityPlayer().worldObj.rand.nextInt(100) > 10) {
                InsuranceManager.INSTANCE.insure(serverPlayer.getEntityPlayer(), itemStack);
                continue;
            }
            if(entityContainer.getMatrixInventory().addItem(itemStack) == AddItemResult.SUCCESS) {
                continue;
            }
            entityContainer.getMatrixInventory().setMatrixSize(entityContainer.getMatrixInventory().getWidth(), entityContainer.getMatrixInventory().getHeight() + 1);
            i--;
        }
        entityContainer.setPlayerName(serverPlayer.getEntityPlayer().getDisplayName());
        entityContainer.setRotation(serverPlayer.getEntityPlayer().rotationYaw);
        entityContainer.setCharacterType(serverPlayer.getSelectedCharacter().getCharacterType());
        entityContainer.setPlayerLevel(serverPlayer.getSelectedCharacter().getSkillManager().getLearnedSkills().size());
        return entityContainer;
    }
}
