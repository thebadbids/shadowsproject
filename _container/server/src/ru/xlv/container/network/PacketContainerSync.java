package ru.xlv.container.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.container.entity.EntityContainerServer;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketContainerSync implements IPacketOutServer {

    private EntityContainerServer entityContainerServer;

    public PacketContainerSync(EntityContainerServer entityContainerServer) {
        this.entityContainerServer = entityContainerServer;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityContainerServer.getEntityId());
        bbos.writeBoolean(entityContainerServer.isGrowing());
        bbos.writeFloat(entityContainerServer.getRotation());
        bbos.writeInt(entityContainerServer.getRarity().ordinal());
    }
}
