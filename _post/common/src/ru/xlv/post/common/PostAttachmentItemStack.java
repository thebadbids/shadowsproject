package ru.xlv.post.common;

import net.minecraft.item.ItemStack;

public class PostAttachmentItemStack extends PostAttachment<ItemStack> {

    public PostAttachmentItemStack(ItemStack attachment) {
        super(attachment, PostAttachmentType.ITEM_STACK);
    }
}
