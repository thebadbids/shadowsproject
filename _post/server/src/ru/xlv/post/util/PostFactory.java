package ru.xlv.post.util;

import ru.xlv.post.common.PostObject;

public class PostFactory {

    private static final String ADMIN_SENDER = "Administration";
    private static final String ADMIN_TITLE = "A message from administration";

    public static PostObject createAdminPostObject(String text, String recipient) {
        return new PostObject(ADMIN_TITLE, text, ADMIN_SENDER, recipient);
    }

    public static PostObject createAdminPostObject(String title, String text, String recipient) {
        return new PostObject(title, text, ADMIN_SENDER, recipient);
    }

    public static PostObject createPostObject(String title, String text, String sender, String recipient) {
        return new PostObject(title, text, sender, recipient);
    }
}
