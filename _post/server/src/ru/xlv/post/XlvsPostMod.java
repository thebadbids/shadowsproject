package ru.xlv.post;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.post.command.CommandSendPost;
import ru.xlv.post.database.DatabaseEventListener;
import ru.xlv.post.database.DatabaseManager;
import ru.xlv.post.handle.PostHandler;
import ru.xlv.post.network.*;
import ru.xlv.post.util.PostLocalization;

import static ru.xlv.post.XlvsPostMod.MODID;

@Mod(
        name = "XlvsPostMod",
        version = "1.0",
        modid = MODID
)
public class XlvsPostMod {

    static final String MODID = "xlvspost";

    @Mod.Instance(MODID)
    public static XlvsPostMod INSTANCE;

    @Getter
    private DatabaseManager databaseManager;
    @Getter
    private PostHandler postHandler;
    @Getter
    private final PostLocalization localization = new PostLocalization();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        databaseManager = new DatabaseManager();
        databaseManager.init(new DatabaseEventListener());
        postHandler = new PostHandler();
        postHandler.init();

        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketPostMarkViewed(),
                new PacketPostSend(),
                new PacketPostSync(),
                new PacketPostTakeAttachment(),
                new PacketPostDelete(),
                new PacketPostNew()
        );
        localization.load();
    }

    @Mod.EventHandler
    public void event(FMLServerStoppingEvent event) {
        postHandler.shutdown();
        databaseManager.shutdown();
    }

    @Mod.EventHandler
    public void event(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandSendPost());
    }
}
