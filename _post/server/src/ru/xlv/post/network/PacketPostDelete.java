package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.handle.result.PostDeleteResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketPostDelete implements IPacketCallbackOnServer {

    private PostDeleteResult postDeleteResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String uuid = bbis.readUTF();
        XlvsPostMod.INSTANCE.getPostHandler().deletePostObject(entityPlayer, uuid).thenAccept(postDeleteResult1 -> {
            postDeleteResult = postDeleteResult1;
            packetCallbackSender.send();
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(postDeleteResult == PostDeleteResult.SUCCESS);
        bbos.writeUTF(postDeleteResult.getResponseMessage());
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
