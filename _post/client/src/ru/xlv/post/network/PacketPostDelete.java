package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.UUID;

@NoArgsConstructor
public class PacketPostDelete implements IPacketCallbackEffective<PacketPostDelete.Result> {

    @Getter
    public static class Result {
        private boolean success;
        private String responseMessage;
    }

    private final Result result = new Result();
    private UUID uuid;

    public PacketPostDelete(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(uuid.toString());
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
        result.responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
