package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketPostMarkViewed implements IPacketOut {

    private String uuid;

    public PacketPostMarkViewed(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(uuid);
    }
}
