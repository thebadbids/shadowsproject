package ru.krogenit.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.item.ItemStack;
import ru.krogenit.armor.item.ItemArmorByPart;
import ru.xlv.core.common.event.EventIsArmorValidForNPC;
import ru.xlv.core.common.inventory.MatrixInventory;

public class EventNPCArmor {

    @SubscribeEvent
    public void event(EventIsArmorValidForNPC event) {
        ItemStack itemStack = event.getItemStack();
        int armorType = event.getArmorType();
        ItemArmorByPart item = (ItemArmorByPart) itemStack.getItem();
        MatrixInventory.SlotType slotType = item.getSlotTypes().get(0);
        switch (slotType) {
            case HEAD:
                event.setCanceled(armorType == 0);
                break;
            case BODY:
                event.setCanceled(armorType == 1);
                break;
            case BRACERS:
                event.setCanceled(armorType == 2);
                break;
            case LEGS:
                event.setCanceled(armorType == 3);
                break;
            case FEET:
                event.setCanceled(armorType == 4);
                break;
        }
    }
}
