package ru.krogenit.armor.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import ru.krogenit.armor.CoreArmorClient;
import ru.krogenit.armor.client.IArmorRenderer;
import ru.krogenit.client.event.RenderFirstPersonHand;
import ru.xlv.core.common.inventory.MatrixInventory;

public class EventRenderFirstPersonHand {

    private static final Minecraft mc = Minecraft.getMinecraft();

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(RenderFirstPersonHand e) {
        ItemStack itemStackBracers = mc.thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.BRACERS.getAssociatedSlotIndex());
        ItemStack itemStackBody = mc.thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.BODY.getAssociatedSlotIndex());
        if(itemStackBracers != null || itemStackBody != null) {
            GL11.glPushMatrix();
            GL11.glTranslatef(-0.535f, 0.36f, -0.12f);
            GL11.glRotatef(5f, 0, 0, 1);
            GL11.glScalef(1.05f, 1.05f, 1.05f);
            if(itemStackBracers != null) {
                IArmorRenderer render = CoreArmorClient.getModelForArmor(itemStackBracers.getItem().getUnlocalizedName());
                if(render != null) {
                    render.renderRightBracerFirstPerson();
                }
            }
            if(itemStackBody != null) {
                IArmorRenderer render = CoreArmorClient.getModelForArmor(itemStackBody.getItem().getUnlocalizedName());
                if(render != null) {
                    render.renderRightBodyArmFirstPerson();
                }
            }
            GL11.glPopMatrix();
        }
    }
}
