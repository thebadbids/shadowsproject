package ru.xlv.armor;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import ru.krogenit.event.EventNPCArmor;
import ru.xlv.armor.event.EventListener;
import ru.xlv.armor.item.ItemArmorByPartServer;
import ru.xlv.armor.network.PacketArmorSync;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;

import static ru.xlv.armor.CoreArmorCommon.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0.0"
)
public class CoreArmorServer {

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLInitializationEvent event) {
        CoreArmorCommon.murusKLHead = new ItemArmorByPartServer("murusKLHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.MURUS_KL);
        CoreArmorCommon.murusKLBody = new ItemArmorByPartServer("murusKLBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.MURUS_KL);
        CoreArmorCommon.murusKLBracers = new ItemArmorByPartServer("murusKLBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.MURUS_KL);
        CoreArmorCommon.murusKLLegs = new ItemArmorByPartServer("murusKLLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.MURUS_KL);
        CoreArmorCommon.murusKLFeet = new ItemArmorByPartServer("murusKLFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.MURUS_KL);

        CoreArmorCommon.censorHead = new ItemArmorByPartServer("censorHead", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.censorBody = new ItemArmorByPartServer("censorBody", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.censorBracers = new ItemArmorByPartServer("censorBracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.censorLegs = new ItemArmorByPartServer("censorLegs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.censorFeet = new ItemArmorByPartServer("censorFeet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.bku0992Head = new ItemArmorByPartServer("bku0992Head", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.BKU_0992);
        CoreArmorCommon.bku0992Body = new ItemArmorByPartServer("bku0992Body", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.BKU_0992);
        CoreArmorCommon.bku0992Bracers = new ItemArmorByPartServer("bku0992Bracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.BKU_0992);
        CoreArmorCommon.bku0992Legs = new ItemArmorByPartServer("bku0992Legs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.BKU_0992);
        CoreArmorCommon.bku0992Feet = new ItemArmorByPartServer("bku0992Feet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.BKU_0992);

        CoreArmorCommon.ptnHead = new ItemArmorByPartServer("ptnHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.PTN);
        CoreArmorCommon.ptnBody = new ItemArmorByPartServer("ptnBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.PTN);
        CoreArmorCommon.ptnBracers = new ItemArmorByPartServer("ptnBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.PTN);
        CoreArmorCommon.ptnLegs = new ItemArmorByPartServer("ptnLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.PTN);
        CoreArmorCommon.ptnFeet = new ItemArmorByPartServer("ptnFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.PTN);

        CoreArmorCommon.enigmaBHead = new ItemArmorByPartServer("enigmaBHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.ENIGMA_B);
        CoreArmorCommon.enigmaBBody = new ItemArmorByPartServer("enigmaBBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.ENIGMA_B);
        CoreArmorCommon.enigmaBBracers = new ItemArmorByPartServer("enigmaBBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.ENIGMA_B);
        CoreArmorCommon.enigmaBLegs = new ItemArmorByPartServer("enigmaBLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.ENIGMA_B);
        CoreArmorCommon.enigmaBFeet = new ItemArmorByPartServer("enigmaBFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.ENIGMA_B);

        CoreArmorCommon.azurESHead = new ItemArmorByPartServer("azurESHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.AZUR_ES);
        CoreArmorCommon.azurESBody = new ItemArmorByPartServer("azurESBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.AZUR_ES);
        CoreArmorCommon.azurESBracers = new ItemArmorByPartServer("azurESBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.AZUR_ES);
        CoreArmorCommon.azurESLegs = new ItemArmorByPartServer("azurESLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.AZUR_ES);
        CoreArmorCommon.azurESFeet = new ItemArmorByPartServer("azurESFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.AZUR_ES);

        CoreArmorCommon.specriotHead = new ItemArmorByPartServer("specriotHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.SPEC_RIOT);
        CoreArmorCommon.specriotBody = new ItemArmorByPartServer("specriotBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.SPEC_RIOT);
        CoreArmorCommon.specriotBracers = new ItemArmorByPartServer("specriotBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.SPEC_RIOT);
        CoreArmorCommon.specriotLegs = new ItemArmorByPartServer("specriotLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.SPEC_RIOT);
        CoreArmorCommon.specriotFeet = new ItemArmorByPartServer("specriotFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.SPEC_RIOT);

        CoreArmorCommon.conditor07Head = new ItemArmorByPartServer("conditor07Head", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.conditor07Body = new ItemArmorByPartServer("conditor07Body", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.conditor07Bracers = new ItemArmorByPartServer("conditor07Bracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.conditor07Legs = new ItemArmorByPartServer("conditor07Legs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.conditor07Feet = new ItemArmorByPartServer("conditor07Feet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.nihilHead = new ItemArmorByPartServer("nihilHead", MatrixInventory.SlotType.HEAD, ItemArmor.SetFamily.NIHIL);
        CoreArmorCommon.nihilBody = new ItemArmorByPartServer("nihilBody", MatrixInventory.SlotType.BODY, ItemArmor.SetFamily.NIHIL);
        CoreArmorCommon.nihilBracers = new ItemArmorByPartServer("nihilBracers", MatrixInventory.SlotType.BRACERS, ItemArmor.SetFamily.NIHIL);
        CoreArmorCommon.nihilLegs = new ItemArmorByPartServer("nihilLegs", MatrixInventory.SlotType.LEGS, ItemArmor.SetFamily.NIHIL);
        CoreArmorCommon.nihilFeet = new ItemArmorByPartServer("nihilFeet", MatrixInventory.SlotType.FEET, ItemArmor.SetFamily.NIHIL);

        CoreArmorCommon.hex6Head = new ItemArmorByPartServer("hex6Head", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.hex6Body = new ItemArmorByPartServer("hex6Body", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.hex6Bracers = new ItemArmorByPartServer("hex6Bracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.hex6Legs = new ItemArmorByPartServer("hex6Legs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.hex6Feet = new ItemArmorByPartServer("hex6Feet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.steelSunHead = new ItemArmorByPartServer("steelSunHead", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.steelSunBody = new ItemArmorByPartServer("steelSunBody", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.steelSunBracers = new ItemArmorByPartServer("steelSunBracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.steelSunLegs = new ItemArmorByPartServer("steelSunLegs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.steelSunFeet = new ItemArmorByPartServer("steelSunFeet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.paxiaXHead = new ItemArmorByPartServer("paxiaXHead", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.paxiaXBody = new ItemArmorByPartServer("paxiaXBody", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.paxiaXBracers = new ItemArmorByPartServer("paxiaXBracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.paxiaXLegs = new ItemArmorByPartServer("paxiaXLegs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.paxiaXFeet = new ItemArmorByPartServer("paxiaXFeet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.omniTAUHead = new ItemArmorByPartServer("omniTAUHead", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.omniTAUBody = new ItemArmorByPartServer("omniTAUBody", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.omniTAUBracers = new ItemArmorByPartServer("omniTAUBracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.omniTAULegs = new ItemArmorByPartServer("omniTAULegs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.omniTAUFeet = new ItemArmorByPartServer("omniTAUFeet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.razmusMHead = new ItemArmorByPartServer("razmusMHead", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.razmusMBody = new ItemArmorByPartServer("razmusMBody", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.razmusMBracers = new ItemArmorByPartServer("razmusMBracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.razmusMLegs = new ItemArmorByPartServer("razmusMLegs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.razmusMFeet = new ItemArmorByPartServer("razmusMFeet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.maradeurHead = new ItemArmorByPartServer("maradeurHead", MatrixInventory.SlotType.HEAD);
        CoreArmorCommon.maradeurBody = new ItemArmorByPartServer("maradeurBody", MatrixInventory.SlotType.BODY);
        CoreArmorCommon.maradeurBracers = new ItemArmorByPartServer("maradeurBracers", MatrixInventory.SlotType.BRACERS);
        CoreArmorCommon.maradeurLegs = new ItemArmorByPartServer("maradeurLegs", MatrixInventory.SlotType.LEGS);
        CoreArmorCommon.maradeurFeet = new ItemArmorByPartServer("maradeurFeet", MatrixInventory.SlotType.FEET);

        CoreArmorCommon.registerItems();
        XlvsCoreCommon.EVENT_BUS.register(new EventListener());
        FMLCommonHandler.instance().bus().register(new EventListener());
        XlvsCoreCommon.EVENT_BUS.register(new EventNPCArmor());
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketArmorSync());
    }
}
