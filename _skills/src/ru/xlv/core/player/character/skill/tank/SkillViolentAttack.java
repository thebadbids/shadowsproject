package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillViolentAttack extends ActivableSkill {

    private static final UUID MOD_UUID = UUID.randomUUID();
    private static final UUID MOD_UUID1 = UUID.randomUUID();
    private static final UUID MOD_UUID2 = UUID.randomUUID();

    private final CharacterAttributeMod characterAttributeMod, characterAttributeMod1, characterAttributeMod2;

    public SkillViolentAttack(SkillType skillType) {
        super(skillType);
        double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        double balDamageMod = 1D + skillType.getCustomParam("income_ballistic_damage_value", double.class) / 100D;
        double energyDamageMod = 1D + skillType.getCustomParam("income_energy_damage_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(skillType.getDuration())
                .uuid(MOD_UUID)
                .build();
        characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.BALLISTIC_PROTECTION)
                .valueMod(balDamageMod)
                .period(skillType.getDuration())
                .uuid(MOD_UUID1)
                .build();
        characterAttributeMod2 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.ENERGY_PROTECTION)
                .valueMod(energyDamageMod)
                .period(skillType.getDuration())
                .uuid(MOD_UUID2)
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod2, true);
    }

    @Nonnull
    @Override
    public SkillViolentAttack clone() {
        return new SkillViolentAttack(getSkillType());
    }
}
