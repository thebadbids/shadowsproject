package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillLifeTouch extends ActivableSkill {

    private final double healAmount;
    private final double radius;

    private double x, y, z;

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillLifeTouch(SkillType skillType) {
        super(skillType);
        healAmount = skillType.getCustomParam("hp_add_ally_low_hp_value", double.class);
        long period = (long) (skillType.getCustomParam("delta_time", double.class) * 1000);
        radius = skillType.getCustomParam("shield_add_ally_low_hp_range", double.class);
        scheduledConsumableTask = new ScheduledConsumableTask<>(period, serverPlayer -> {
            GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
            if (playerGroup != null) {
                ServerPlayer nearest = Flex.getUniqueElement(
                        playerGroup.getPlayers(),
                        serverPlayer1 -> serverPlayer.getEntityPlayer().getDistanceToEntity(serverPlayer1.getEntityPlayer()),
                        (aFloat, aFloat2) -> aFloat2 < radius && aFloat > aFloat2
                );
                if(nearest != null) {
                    nearest.getSelectedCharacter().healBySkill(nearest, (float) healAmount);
                }
            } else {
                serverPlayer.getSelectedCharacter().healBySkill(serverPlayer, (float) healAmount);
            }
        });
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        x = serverPlayer.getEntityPlayer().posX;
        y = serverPlayer.getEntityPlayer().posY;
        z = serverPlayer.getEntityPlayer().posZ;
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if(isActive()) {
            if(serverPlayer.getEntityPlayer().posX != x || serverPlayer.getEntityPlayer().posY != y || serverPlayer.getEntityPlayer().posZ != z) {
                deactivateSkill(serverPlayer);
                return;
            }
            scheduledConsumableTask.update(serverPlayer);
        }
    }

    @Nonnull
    @Override
    public SkillLifeTouch clone() {
        return new SkillLifeTouch(getSkillType());
    }
}
