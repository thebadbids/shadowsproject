package ru.xlv.core.player.character.skill.tank;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillPrimordialRage extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID(), MOD_UUID1 = UUID.randomUUID();

    private final double chance, healthAfterRespawn;

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    private ServerPlayer serverPlayer;

    public SkillPrimordialRage(SkillType skillType) {
        super(skillType);
        double healthLimit = skillType.getCustomParam("if_hp_is_lower_than", double.class);
        double protectionMod = 1D + skillType.getCustomParam("protect_add_self_all_value", double.class) / 100D;
        double healthRegen = skillType.getCustomParam("hp_regen_self_value", double.class);
        double manaRegen = skillType.getCustomParam("mana_regen_self_value", double.class);
        double cooldownMod = 1D - skillType.getCustomParam("self_skill_add_recharge_time", double.class) / 100D;
        chance = skillType.getCustomParam("chance", double.class);
        healthAfterRespawn = skillType.getCustomParam("hp_add_self_after_death", double.class);
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        CharacterAttributeMod characterAttributeMod1 = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.COOLDOWN_MOD)
                .valueMod(cooldownMod)
                .period(1000L)
                .uuid(MOD_UUID1)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            if(serverPlayer.getEntityPlayer().getHealth() < healthLimit) {
                serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
                serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
                serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(manaRegen);
                serverPlayer.getEntityPlayer().heal((float) healthRegen);
            }
        });
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void event(LivingDeathEvent event) {
        if(event.entityLiving.getCommandSenderName().equals(serverPlayer.getPlayerName())) {
            int i = event.entityLiving.worldObj.rand.nextInt(100);
            if(i < chance) {
                event.setCanceled(true);
                event.entityLiving.heal((float) healthAfterRespawn);
            }
        }
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillPrimordialRage clone() {
        return new SkillPrimordialRage(getSkillType());
    }
}
