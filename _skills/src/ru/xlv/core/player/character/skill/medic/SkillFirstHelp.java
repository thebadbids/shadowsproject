package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.player.ServerPlayer;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillFirstHelp extends ActivableSkill {

    private final double healAmount;
    private final double healRadius;

    public SkillFirstHelp(SkillType skillType) {
        super(skillType);
        this.healAmount = skillType.getCustomParam("hp_add_ally_all_value", double.class);
        this.healRadius = skillType.getCustomParam("hp_add_ally_all_range", double.class);
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
        if (playerGroup != null) {
            playerGroup.getPlayers()
                    .stream()
                    .filter(serverPlayer1 -> serverPlayer != serverPlayer1)
                    .filter(serverPlayer1 -> serverPlayer.getEntityPlayer().getDistanceSqToEntity(serverPlayer1.getEntityPlayer()) <= healRadius)
                    .forEach(serverPlayer1 -> serverPlayer1.getSelectedCharacter().healBySkill(serverPlayer1, ((float) healAmount)));
        } else {
            serverPlayer.getSelectedCharacter().healBySkill(serverPlayer, (float) healAmount);
        }
    }

    @Nonnull
    @Override
    public SkillFirstHelp clone() {
        return new SkillFirstHelp(getSkillType());
    }
}
