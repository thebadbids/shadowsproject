package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillHematopoieticStimulator extends ActivableSkill {

    private final ScheduledConsumableTask<ServerPlayer> scheduledTask;

    public SkillHematopoieticStimulator(SkillType skillType) {
        super(skillType);
        final double healthRegen = skillType.getCustomParam("hp_regen_self_value", double.class);
        scheduledTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> serverPlayer.getEntityPlayer().heal(((float) healthRegen)));
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {}

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if(isActive()) {
            scheduledTask.update(serverPlayer);
        }
    }

    @Nonnull
    @Override
    public SkillHematopoieticStimulator clone() {
        return new SkillHematopoieticStimulator(getSkillType());
    }
}
