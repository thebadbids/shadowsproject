package ru.xlv.core.player.character.skill.tank;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.EntityDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillAbsorbingShield extends ActivableSkill {

    private final double damageLimit;
    private final long reduceCooldownAmount;
    private final double damageMod;

    private ServerPlayer serverPlayer;
    private double damageReceived;

    public SkillAbsorbingShield(SkillType skillType) {
        super(skillType);
        damageMod = skillType.getCustomParam("income_damage_value", double.class);
        damageLimit = skillType.getCustomParam("each_income_damage", double.class);
        reduceCooldownAmount = ((long) (skillType.getCustomParam("reduce_reload_skill_time", double.class) * 1000));
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    protected void onDeactivated(ServerPlayer serverPlayer) {
        super.onDeactivated(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(EntityDamagePlayerEvent.Post event) {
        if(event.getServerPlayer() == serverPlayer) {
            event.setAmount(event.getAmount() + (float) (event.getAmount() * damageMod));
            damageReceived += event.getAmount();
            long reduceValue = 0;
            while(damageReceived >= damageLimit) {
                damageReceived -= damageLimit;
                reduceValue += reduceCooldownAmount;
            }
            if(reduceValue != 0) {
                long finalReduceValue = reduceValue;
                event.getServerPlayer().getSelectedCharacter().getSkillManager().getSelectedActiveSkills().forEach(activableSkill -> activableSkill.reduceCooldown(finalReduceValue));
            }
        }
    }

    @Nonnull
    @Override
    public SkillAbsorbingShield clone() {
        return new SkillAbsorbingShield(getSkillType());
    }
}
