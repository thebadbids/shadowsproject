package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillSuperSonicJerk extends ActivableSkill {

    private final CharacterAttributeMod characterAttributeMod;

    public SkillSuperSonicJerk(SkillType skillType) {
        super(skillType);
        final double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .period(getSkillType().getDuration())
                .valueMod(speedMod)
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.setDisableBlockMovingTimer((int) getSkillType().getDuration());
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod);
    }

    @Nonnull
    @Override
    public SkillSuperSonicJerk clone() {
        return new SkillSuperSonicJerk(getSkillType());
    }
}
