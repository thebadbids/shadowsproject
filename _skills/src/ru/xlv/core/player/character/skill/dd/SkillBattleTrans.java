package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillBattleTrans extends ActivableSkill {

    private final double healthRegen;
    private final double speedMod;
    private final double reloadMod;

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillBattleTrans(SkillType skillType) {
        super(skillType);
        healthRegen = skillType.getCustomParam("hp_regen_self_value", double.class);
        speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        reloadMod = 1D + skillType.getCustomParam("reloadspeed_add_value", double.class) / 100D;
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> serverPlayer.getEntityPlayer().heal((float) healthRegen));
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(getSkillType().getDuration())
                .build());
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.WEAPON_RELOAD_MOD)
                .valueMod(reloadMod)
                .period(getSkillType().getDuration())
                .build());
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if (isActive()) {
            scheduledConsumableTask.update(serverPlayer);
        }
    }

    @Nonnull
    @Override
    public SkillBattleTrans clone() {
        return new SkillBattleTrans(getSkillType());
    }
}
