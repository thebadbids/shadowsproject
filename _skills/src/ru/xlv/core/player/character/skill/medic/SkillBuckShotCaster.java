package ru.xlv.core.player.character.skill.medic;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.event.PlayerDamageEntityEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillBuckShotCaster extends Skill {

    private final double damageMod;
    private ServerPlayer serverPlayer;

    public SkillBuckShotCaster(SkillType skillType) {
        super(skillType);
        damageMod = 1D + skillType.getCustomParam("shotgun_add_value", double.class) / 100D;
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerDamageEntityEvent.Post event) {
        if(serverPlayer == event.getServerPlayer()) {
            ItemStack itemStack = event.getServerPlayer().getEntityPlayer().inventory.getCurrentItem();
            if (EnumItemTag.hasTag(itemStack, EnumItemTag.SHOTGUN)) {
                event.setAmount((float) (event.getAmount() * damageMod));
            }
        }
    }

    @Nonnull
    @Override
    public SkillBuckShotCaster clone() {
        return new SkillBuckShotCaster(getSkillType());
    }
}
