package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;

public class SkillRevenge extends Skill {

    private final double radius;
    private final double healthAdd;
    private final double damageMod;

    private long startTimer;

    private ServerPlayer serverPlayer;

    public SkillRevenge(SkillType skillType) {
        super(skillType);
        radius = skillType.getCustomParam("range", double.class);
        healthAdd = 1D + skillType.getCustomParam("hp_add_self_value", double.class) / 100D;
        damageMod = 1D + skillType.getCustomParam("outcome_damage_value", double.class) / 100D;
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        MinecraftForge.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        MinecraftForge.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(LivingDeathEvent event) {
        if(startTimer < System.currentTimeMillis() && event.entityLiving instanceof EntityPlayer) {
            Set<ServerPlayer> allyPlayersAround = SkillUtils.getAllyPlayersAround(serverPlayer, radius);
            for (ServerPlayer serverPlayer1 : allyPlayersAround) {
                if (serverPlayer1.getPlayerName().equals(event.entityLiving.getCommandSenderName())) {
                    serverPlayer.getEntityPlayer().heal((float) healthAdd);
                    serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                            .attributeType(CharacterAttributeType.DAMAGE_MOD)
                            .valueMod(damageMod)
                            .period(getSkillType().getDuration())
                            .build());
                    startTimer = System.currentTimeMillis() + getSkillType().getCooldown();
                    break;
                }
            }
        }
    }

    @Nonnull
    @Override
    public SkillRevenge clone() {
        return new SkillRevenge(getSkillType());
    }
}
