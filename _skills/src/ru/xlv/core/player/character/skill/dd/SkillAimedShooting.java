package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerDamageEntityEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillAimedShooting extends Skill {

    private final double chance;
    private final double damageMod;
    private final double healthPercent;

    private ServerPlayer serverPlayer;

    public SkillAimedShooting(SkillType skillType) {
        super(skillType);
        chance = skillType.getCustomParam("chance", double.class);
        damageMod = 1D + skillType.getCustomParam("outcome_damage_value", double.class) / 100D;
        healthPercent = skillType.getCustomParam("hp_add_percent_of_damage", double.class) / 100D;
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerDamageEntityEvent.Post event) {
        if(event.getServerPlayer() == serverPlayer) {
            int i = event.getServerPlayer().getEntityPlayer().worldObj.rand.nextInt(100);
            if(i < chance) {
                float damage = (float) (event.getAmount() * damageMod);
                event.setAmount(damage);
                event.getServerPlayer().getEntityPlayer().heal((float) (damage * healthPercent));
            }
        }
    }

    @Nonnull
    @Override
    public SkillAimedShooting clone() {
        return new SkillAimedShooting(getSkillType());
    }
}
