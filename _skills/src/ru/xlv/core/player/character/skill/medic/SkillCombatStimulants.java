package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillCombatStimulants extends Skill {

    private final CharacterAttributeMod characterAttributeMod;

    public SkillCombatStimulants(SkillType skillType) {
        super(skillType);
        final double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .build();
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        serverPlayer.getSelectedCharacter().removeAttributeMod(characterAttributeMod);
    }

    @Nonnull
    @Override
    public SkillCombatStimulants clone() {
        return new SkillCombatStimulants(getSkillType());
    }
}
