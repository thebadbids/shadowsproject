package ru.xlv.core.player.character.skill.medic;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.event.EntityDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class SkillLifeThirst extends Skill implements ISkillUpdatable {

    private ServerPlayer serverPlayer;

    private final double shieldDamage;
    private final long period;
    private final double healAmount;

    private boolean inCooldown;
    private long startTime;
    private float damage;

    public SkillLifeThirst(SkillType skillType) {
        super(skillType);
        period = (long) (skillType.getCustomParam("delta_time", double.class) * 1000);
        shieldDamage = skillType.getCustomParam("shield_add_self_value", double.class);
        healAmount = skillType.getCustomParam("hp_add_self_value", double.class);
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
        CommonUtils.registerEvents(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
        CommonUtils.unregisterEvents(this);
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        if(startTime != 0) {
            if (!inCooldown && System.currentTimeMillis() - startTime >= period) {
                if (serverPlayer.isOnline()) {
                    inCooldown = true;
                    startTime = System.currentTimeMillis() + getSkillType().getCooldown();
                    serverPlayer.getSelectedCharacter().healBySkill(serverPlayer, (float) healAmount);
                }
            } else if(System.currentTimeMillis() >= startTime) {
                inCooldown = false;
            }
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(EntityDamagePlayerEvent.Post event) {
        if(!inCooldown && startTime != 0 && serverPlayer == event.getServerPlayer()) {
            damage += event.getAmount();
            if(damage < shieldDamage) {
                event.setAmount(0);
                event.setCanceled(true);
            } else {
                startTime = 0;
                damage = 0;
            }
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void event(LivingDeathEvent event) {
        if(!inCooldown && event.entityLiving.getCommandSenderName().equals(serverPlayer.getEntityPlayer().getCommandSenderName())) {
            if(event.source.getSourceOfDamage() instanceof EntityPlayer) {
                ItemStack currentItem = ((EntityPlayer) event.source.getSourceOfDamage()).inventory.getCurrentItem();
                if(currentItem != null) {
                    List<EnumItemTag> list = new ArrayList<>();
                    List<EnumItemTag> itemTags = EnumItemTag.getItemTags(currentItem);
                    if (itemTags != null) {
                        list.addAll(itemTags);
                    }
                    if (!Flex.containsAll(list, EnumItemTag.MELEE)) {
                        return;
                    }
                }
            }
            if(startTime == 0 && damage == 0) {
                startTime = System.currentTimeMillis();
                event.entityLiving.setHealth(1F);
                event.setCanceled(true);
            }
        }
    }

    @Nonnull
    @Override
    public SkillLifeThirst clone() {
        return new SkillLifeThirst(getSkillType());
    }
}
