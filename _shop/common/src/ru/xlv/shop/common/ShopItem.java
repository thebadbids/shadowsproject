package ru.xlv.shop.common;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.xlv.core.common.network.IPacketComposable;
import ru.xlv.core.common.util.config.ConfigComment;
import ru.xlv.core.common.util.config.Configurable;

import java.util.List;

@ToString
@Getter
@RequiredArgsConstructor
public class ShopItem implements IPacketComposable {

    @Configurable
    @ConfigComment("Тип предмета. Варианты: ITEM")
    private final ShopItemType shopItemType;
    @Configurable
    @ConfigComment("Уникальный id.")
    private final int id;
    @Configurable
    private final String name;
    @Configurable
    private final String description;
    @Configurable
    private final List<ShopItemCategory> categories;
    @Configurable
    private final List<ShopItemCost> costs;

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        writableList.add(shopItemType.ordinal());
        writableList.add(id);
        writableList.add(name);
        writableList.add(description);
        writeComposableCollection(categories, writableList, byteBufOutputStream);
        writeComposableCollection(costs, writableList, byteBufOutputStream);
    }
}
