package ru.xlv.shop.handle;

import lombok.experimental.UtilityClass;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.util.Maps;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.network.PacketShopSyncCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@UtilityClass
public class ShopItemManager {

    public final List<ShopItem> SHOP_ITEMS = new ArrayList<>();

    public final Map<ShopItemCategory, List<ShopItem>> CATEGORISED_SHOP_ITEMS = new HashMap<>();

    public void addItem(ShopItem shopItem) {
        SHOP_ITEMS.add(shopItem);
        shopItem.getCategories().forEach(shopItemCategory -> Maps.addElemToMappedList(CATEGORISED_SHOP_ITEMS, shopItemCategory, shopItem));
    }

    public void syncCategory(ShopItemCategory shopItemCategory) {
        CATEGORISED_SHOP_ITEMS.put(shopItemCategory, null);
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketShopSyncCategory(shopItemCategory)).thenAcceptSync(shopItems -> {
            CATEGORISED_SHOP_ITEMS.put(shopItemCategory, shopItems);
        });
    }

    public void syncCategory(ShopItemCategory shopItemCategory, Consumer<List<ShopItem>> consumer) {
        if(!CATEGORISED_SHOP_ITEMS.containsKey(shopItemCategory)) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketShopSyncCategory(shopItemCategory)).thenAcceptSync(shopItems -> {
                CATEGORISED_SHOP_ITEMS.put(shopItemCategory, shopItems);
                consumer.accept(CATEGORISED_SHOP_ITEMS.get(shopItemCategory));
            });
        } else {
            consumer.accept(CATEGORISED_SHOP_ITEMS.get(shopItemCategory));
        }
    }

    public boolean isCategoryLoaded(ShopItemCategory shopItemCategory) {
        return CATEGORISED_SHOP_ITEMS.containsKey(shopItemCategory);
    }
}
