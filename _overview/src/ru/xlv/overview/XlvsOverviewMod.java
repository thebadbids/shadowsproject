package ru.xlv.overview;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;

@Mod(
        modid = XlvsOverviewMod.MODID,
        name = XlvsOverviewMod.MODID,
        version = "1.0"
)
public class XlvsOverviewMod {

    static final String MODID = "xlvsoverview";

    public static KeyBinding keyOverview;
    static KeyBinding keyToggleMode;

    public static boolean viewClamping = true;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        FMLCommonHandler.instance().bus().register(new XlvsEvents());
        keyOverview = new KeyBinding("key.overview.desc", Keyboard.KEY_LMENU, "key.overview.category");
        keyToggleMode = new KeyBinding("key.togglemode.desc", Keyboard.KEY_RMENU, "key.overview.category");
        ClientRegistry.registerKeyBinding(keyOverview);
        ClientRegistry.registerKeyBinding(keyToggleMode);
    }
}
