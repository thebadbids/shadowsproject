package ru.xlv.guide.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.guide.XlvsGuideMod;
import ru.xlv.guide.common.Guide;
import ru.xlv.guide.common.GuideUtils;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketGuideSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        List<Guide> guides = GuideUtils.readGuideList(bbis);
        XlvsGuideMod.INSTANCE.getGuideManager().sync(guides);
    }
}
