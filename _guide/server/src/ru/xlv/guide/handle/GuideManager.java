package ru.xlv.guide.handle;

import ru.xlv.guide.common.Guide;
import ru.xlv.guide.common.GuideCategory;
import ru.xlv.guide.util.GuideConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GuideManager {

    private final List<Guide> guideList = Collections.synchronizedList(new ArrayList<>());

    public void init() {
        for (GuideCategory value : GuideCategory.values()) {
            GuideConfig guideConfig = new GuideConfig(value.name().toLowerCase());
            guideConfig.elements.forEach(guideConfigElement -> {
                guideList.add(new Guide(guideConfigElement.article, guideConfigElement.text, guideConfigElement.imageURL, value));
            });
        }
    }

    public synchronized List<Guide> getGuideList() {
        return guideList;
    }
}
