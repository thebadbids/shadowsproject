package ru.xlv.gun;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import ru.xlv.gun.container.ContainerCustomPlayer;
import ru.xlv.gun.container.ContainerGunModTable;
import ru.xlv.gun.util.ExtendedPlayer;

public class CommonGuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID == 1) {
			return new ContainerCustomPlayer(player, player.inventory, ExtendedPlayer.get(player).inventory);
		}
		if (ID == 2) {
			return new ContainerGunModTable(player.inventory, world);
		} else {
			return null;
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return null;
	}
}