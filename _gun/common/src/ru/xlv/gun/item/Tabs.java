package ru.xlv.gun.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class Tabs {
    public static final CreativeTabs tabGuns = new CreativeTabs("tabGuns") {
        @Override
        public Item getTabIconItem() {
            return Items.ak74;
        }
    };
    public static final CreativeTabs tabAttachments = new CreativeTabs("tabAttachments") {
        @Override
        public Item getTabIconItem() {
            return Items.pso;
        }

    };
    public static final CreativeTabs tabMagazines = new CreativeTabs("tabMagazines") {
        @Override
        public Item getTabIconItem() {
            return Items.Magaa12;
        }
    };
    public static final CreativeTabs tabItems = new CreativeTabs("tabItems") {
        @Override
        public Item getTabIconItem() {
            return Items.armorRem;
        }
    };
    public static final CreativeTabs tabArmor = new CreativeTabs("Armor") {
        @Override
        public Item getTabIconItem() {
            return net.minecraft.init.Items.golden_horse_armor;
        }
    };
}
