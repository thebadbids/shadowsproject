package ru.xlv.gun.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public enum AttachmentType {

	grip,
	scope,
	stvol;

	public static AttachmentType get(String s) {
		for (AttachmentType type : values()) {
			if (type.toString().equals(s)) {
				return type;
			}
		}
		return scope;
	}

    public static boolean checkAttachment(ItemStack is, String attName, Item attachment) {
        if(is != null) {
            NBTTagCompound nbtstack = is.getTagCompound();
            if(nbtstack != null) {
                if(is.stackTagCompound != null) {
                    if(nbtstack.getString(attName).length() > 2) {
                        return ItemStack.loadItemStackFromNBT(is.stackTagCompound.getCompoundTag(attName)).getItem() == attachment;
                    }
                }
            }
        }
        return false;

    }
}
