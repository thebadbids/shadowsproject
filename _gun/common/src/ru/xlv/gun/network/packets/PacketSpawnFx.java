package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;

public class PacketSpawnFx implements IMessage {

	public String shooter;
	public boolean isCrawl;

	public PacketSpawnFx() {
	}

	public PacketSpawnFx(String shooterg, boolean crawl) {
		this.shooter = shooterg;
		this.isCrawl = crawl;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, shooter);
		buf.writeBoolean(isCrawl);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		shooter = ByteBufUtils.readUTF8String(buf);
		isCrawl = buf.readBoolean();
	}
}
