package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import ru.xlv.gun.network.packets.PacketDetach;

public class PacketDetachHandler implements IMessageHandler<PacketDetach, IMessage> {

	@Override
	public IMessage onMessage(PacketDetach message, MessageContext ctx) {
		return null;
	}
}