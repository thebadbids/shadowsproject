package ru.xlv.gun.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;

public class ModelSmoke extends ModelBase {

    ModelRenderer Shape1;


    public ModelSmoke() {
        super.textureWidth = 1836;
        super.textureHeight = 1760;
        this.Shape1 = new ModelRenderer(this, 0, 0);
        this.Shape1.addBox(-918.0F, -880.0F, 0.0F, 1836, 1760, 0);
        this.Shape1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Shape1.setTextureSize(1836, 1760);
        this.Shape1.mirror = true;
        this.setRotation(this.Shape1, 0.0F, 0.0F, 0.0F);
    }

    public void func_78088_a(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        this.Shape1.render(f5);
    }

    public void render(float f5) {
        GL11.glPushMatrix();
        GL11.glEnable(3042);
        this.Shape1.render(f5);
        GL11.glDisable(3042);
        GL11.glPopMatrix();
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, (Entity) null);
    }
}
