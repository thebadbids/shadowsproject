package ru.xlv.gun.render.guns.shotgun;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class AA12Render implements IItemRenderer {
	
	
	
	
	//public static final IModelCustom model = spproject.loaderModel.Modell.loadModel(new ResourceLocation("batmod", "models/guns/ak74.sp"));
	
    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/aa12/texture.png");
   
    private static RenderAttachments renderAtt;
    RenderWeaponThings rWT = new RenderWeaponThings();
   
	Minecraft mc = Minecraft.getMinecraft();
	IModelCustom model;
	
	public AA12Render() {
		
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/aa12/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;

		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;
	    	switch (type) {
		case EQUIPPED_FIRST_PERSON: {
			rWT.doAnimations();
 			if(shootTimer == weapon.shootSpeed-1){
			GL11.glPushMatrix();
			GL11.glRotated(1.0F*shootAnim, 1.0, 0.0, 1.0); 
			GL11.glTranslatef(0.1F * aimAnim, 0.203F * aimAnim, -1.570F * aimAnim);
			GL11.glScalef(0.25F, 0.25F, 0.25F);
			GL11.glRotatef(135, 0, 1, 0);
			GL11.glTranslatef(-1F, 5F, 25F);
			rWT.flash();
			GL11.glPopMatrix();
			}		
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(is.getTagCompound().getString("scope").equals("Прицел ИК ПНВ")){
				gameSettings.fovSetting = 90 - 50 * aimAnim;
				x = -0.304F;
				y = -0.13F;
				z = 0.303F;
				
			}
			if(is.getTagCompound().getString("scope").equals("Коллиматор")){
				gameSettings.fovSetting = 90 - 10 * aimAnim;
				x = -0.237F;
				y = -0.122F;
				z = 0.236F;
				
			}
			
			float dSWS = 1F;
			float f1 = 1F;
			if((is.getTagCompound().getString("scope") != null || is.getTagCompound().getString("planka") != null) && Utils.isPlayerAiming()){
			   dSWS = 0.4F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(0F*aimAnim, 1.0, 0.0, 01.0);
			GL11.glTranslatef(-0.645F * aimAnim+ x*aimAnim, -0.1F * aimAnim + y*aimAnim, -0.015F * aimAnim + z*aimAnim);
			GL11.glRotated(8.0F*shootAnim, 1.0, 0.0, 1.0);
			GL11.glRotated(5.0F*shootAnim, 1.0, -1.0, 0.0);
			GL11.glTranslatef(0.005F * shootAnim*dSWS, 0.2F*shootAnim, 0.024F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(0.7F, 0.7F, 0.7F);
			
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.4F, 1.2F, -0.325F);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderAll();
			renderAttachments(type, is);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			GL11.glPopMatrix();
			
			
			GL11.glPushMatrix();
            rWT.renderRightArm(0.21F, -1.5F, 0.85F, -90, -5, -25, 1.6F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.59F, -0.35F, -0.24F, 10, 25, -80, 2.8F);
			GL11.glPopMatrix();
			
			
			
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, 0.1F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
			 GL11.glPushMatrix();
			 GL11.glScalef(0.05F, 0.05F, 0.05F);
			 GL11.glTranslatef(35.7F, -9.8F, -5.47F);
          renderAtt.renderLaser(type);
          GL11.glPopMatrix();
		 }
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();	
		GL11.glScalef(0.25F, 0.25F, 0.25F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0F, 0F, 0F);
		ru.xlv.core.util.Utils.bindTexture(tex);
		model.renderAll();
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		
		GL11.glPopMatrix();
		
    }

}

