package ru.xlv.gun.render.guns.rifle;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

import javax.vecmath.Vector3f;

public class AbakanRender implements IItemRenderer {
	

    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/abakan/texture.png");
    private static RenderAttachments rAtt;
	Minecraft mc = Minecraft.getMinecraft();
	RenderWeaponThings rWT = new RenderWeaponThings();
	IModelCustom model;

	
	public AbakanRender() {
		
		rAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/abakan/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);

	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;

		if(Minecraft.getMinecraft().isSingleplayer()) throw new RuntimeException("Something go wrong...");
		switch (type) {
		
		case EQUIPPED_FIRST_PERSON: 
		{
			
			rWT.doAnimations();
			Vector3f posInScope = new Vector3f(0, 0, 0);
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			
			{
				if(AttachmentType.checkAttachment(is,"scope", Items.pkas)){
				//if(rWT.isGunHadObves(is, "Прицел ПК-АС")){
				posInScope.x = -0.342F; posInScope.y = -0.229F; posInScope.z = 0.359F;
				}
				if(AttachmentType.checkAttachment(is,"scope", Items.kobra)){
				posInScope.x = -0.329F; posInScope.y = -0.199F; posInScope.z = 0.348F;
				}
				if(AttachmentType.checkAttachment(is,"scope", Items.pso)){
				posInScope.x = 0.082F; posInScope.y = -0.2F; posInScope.z = 0.006F;
				}
			}
			
			//данная функция убавляет визуал отдачу при прицеливании
			float dSWS = 1F;
			if(Utils.isPlayerAiming()){
			   dSWS = 0.2F;
			}
			float fov = gameSettings.fovSetting/10000;
			
			{
				if(!Utils.isPlayerAiming())
					GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
				else
					GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
				GL11.glRotatef(65*reloadAnim, 0, 0, 1);
				GL11.glRotatef(-25*runAnim, 1, 0, 0);
				GL11.glRotatef(-15*runAnim, 0, 0, 1);
				GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
				GL11.glRotated(0.5F*aimAnim, 1.0, 0.0, 1.0);
				GL11.glTranslatef(aimAnim*(-0.444F+posInScope.x), aimAnim*(0.172F+posInScope.y), aimAnim*(-0.120F+posInScope.z));
				GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
				GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
				GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.1F*shootAnim*dSWS, 0.084F * shootAnim*dSWS);
			}
			
			GL11.glPushMatrix();
			{
				GL11.glScalef(1.3F, 1.3F, 1.3F);
				GL11.glRotated(45.0, 0.0, 1.0, 0.0);
				GL11.glTranslatef(-0.42F, 0.426F, -0.265F);
				renderAttachments(type, is);
				ru.xlv.core.util.Utils.bindTexture(tex);
				model.renderAll();
				if(shootTimer == weapon.shootSpeed - 1){
					GL11.glPushMatrix();
					GL11.glScalef(0.13F, 0.13F, 0.13F);
					GL11.glRotatef(90, 0, 1, 0);
					GL11.glTranslatef(-0.1F, 3.6F, 30F);
					rWT.flash();
					GL11.glPopMatrix();
				}		
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			}
			GL11.glPopMatrix();
			
			{
				GL11.glPushMatrix();
					rWT.renderRightArm(0.3F, -1.8F, 0.92F, -90, -5, -25, 2.1F);
				GL11.glPopMatrix();
				GL11.glPushMatrix();
					rWT.renderLeftArm(-0.59F, -0.35F, -0.35F, 20, 27, -80, 2.6F);
				GL11.glPopMatrix();
			}
		    
			GL11.glPopMatrix();
		}
		break;
		
		case EQUIPPED: 
		{
			GL11.glPushMatrix();
				GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    	GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    	GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    	GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        	GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				renderEntityAndEquipped(type, is);
			GL11.glPopMatrix();
		}
		break;
		
		case ENTITY: 
		{	 
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	
	public void renderAttachments(ItemRenderType type, ItemStack is){
		

		if(AttachmentType.checkAttachment(is,"scope", Items.pkas)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.2F, 0.04F, 0.03F);
			GL11.glScalef(1.15F, 1.15F, 1.15F);
			rAtt.renderPKAS(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"scope", Items.kobra)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.3F, 0.078F, 0.022F);
			GL11.glScalef(1.1F, 1.1F, 1.1F);
			rAtt.renderKobra(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"scope", Items.pso)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.1F, 0.04F, -0.018F);
			GL11.glScalef(1.1F, 1.1F, 1.1F);
			rAtt.renderPSO(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"stvol", Items.pbs4)){
			 GL11.glPushMatrix();
			 GL11.glTranslatef(0.62F, -0.01F, -0.0F);
			 rAtt.renderPBS4();
			 GL11.glPopMatrix();
			
		}		
		if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
			 GL11.glPushMatrix();
			 GL11.glScalef(0.05F, 0.05F, 0.05F);
			 GL11.glTranslatef(23.7F, -8.8F, -1.47F);
            rAtt.renderLaser(type);
            GL11.glPopMatrix();
		 }
	}
	
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(tex);
		GL11.glScalef(0.4F, 0.4F, 0.4F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		model.renderAll();
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		GL11.glPopMatrix();
    }
}
