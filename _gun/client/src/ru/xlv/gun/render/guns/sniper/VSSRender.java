package ru.xlv.gun.render.guns.sniper;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class VSSRender implements IItemRenderer {
	

    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/vss/texture.png");
    private static RenderAttachments renderAtt;
    
	Minecraft mc = Minecraft.getMinecraft();
	RenderWeaponThings rWT = new RenderWeaponThings();
	IModelCustom model;
	
	public VSSRender() {
		
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/vss/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;

		switch (type) {
		
		case EQUIPPED_FIRST_PERSON: {
			
			rWT.doAnimations();
 			
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(AttachmentType.checkAttachment(is,"scope", Items.pkas)){
				//gameSettings.fovSetting = 90 - 20 * aimAnim;
				x = -0.3015F;
				y = -0.204F;
				z = 0.306F;
				
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.kobra)){
				//gameSettings.fovSetting = 90 -20 * aimAnim;
				x = -0.115F;
				y = -0.22F;
				z = 0.126F;
				
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.pso)){
				//gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = 0.019F;
				y = -0.202F;
				z = 0.035F;
				
			}
			if(AttachmentType.checkAttachment(is,"scope", Items.posp)){
				//gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = 0.071F;y = -0.23F;z = -0.036F;
			}
			float dSWS = 1F;
			float f1 = 1F;
			if(Utils.isPlayerAiming()){
			   dSWS = 0.2F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(0.4F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.46F * aimAnim+ x*aimAnim, 0.158F * aimAnim + y*aimAnim, -0.042F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(-1.0F*shootAnim*dSWS, 0.0, 1.0, 1.0);
			GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.1F*shootAnim*dSWS, 0.084F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(1.39F, 1.39F, 1.39F);
			
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.42F, 0.5F, -0.255F);
			renderAttachments(type, is);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderAll();
			if(shootTimer == weapon.shootSpeed-1){
				GL11.glPushMatrix();
				GL11.glScalef(0.13F, 0.13F, 0.13F);
				GL11.glRotatef(90, 0, 1, 0);
				GL11.glTranslatef(-0.1F, 3.6F, 30F);
				rWT.flash();
				GL11.glPopMatrix();
			}	
			
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			rWT.renderRightArm(0.32F, -1.7F, 0.95F, -90, -5, -25, 2.1F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.7F, -0.45F, -0.38F, 20, 27, -80, 2.3F);
			GL11.glPopMatrix();
			
		    
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.05F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        
			renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			
			 
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	public void renderAttachments(ItemRenderType type, ItemStack is){
		if(AttachmentType.checkAttachment(is,"scope", Items.pkas)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.2F, -0.01F, 0.01F);
			renderAtt.renderPKAS(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"scope", Items.kobra)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.15F, 0.02F, -0.00F);
			renderAtt.renderKobra(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"scope", Items.pso)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.2F, -0.03F, -0.024F);
			//GL11.glScalef(0.9F, 0.9F, 0.9F);
			renderAtt.renderPSO(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"scope", Items.posp)){
			GL11.glPushMatrix();
			GL11.glTranslatef(0.1F, 0.0F, -0.01F);
			//GL11.glScalef(1.25F, 1.25F, 1.25F);
			renderAtt.renderPOSP(Utils.isPlayerAiming());
			GL11.glPopMatrix();
		}
		if(AttachmentType.checkAttachment(is,"stvol", Items.pbs4)){
			 GL11.glPushMatrix();
			 GL11.glTranslatef(-0.02F, -0.145F, -0.03F);
			 renderAtt.renderPBS4();
			 GL11.glPopMatrix();
			
		}		
		if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
			 GL11.glPushMatrix();
			 GL11.glScalef(0.05F, 0.05F, 0.05F);
			 GL11.glTranslatef(12.7F, -9.8F, -1.97F);
           renderAtt.renderLaser(type);
           GL11.glPopMatrix();
		 }
	}
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(tex);
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		model.renderAll();
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		GL11.glPopMatrix();
    }

}
