package ru.xlv.gun.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;

public class ModelLaser extends ModelBase {

	   ModelRenderer Shape0;
	   ModelRenderer Shape1;
	   ModelRenderer Shape2;
	   ModelRenderer Shape3;
	   ModelRenderer Shape4;
	   ModelRenderer Shape5;
	   ModelRenderer Shape6;
	   ModelRenderer Shape7;
	   ModelRenderer Shape8;
	   ModelRenderer Shape9;
	   ModelRenderer Shape10;
	   ModelRenderer Shape11;
	   ModelRenderer Shape12;
	   ModelRenderer Shape13;
	   ModelRenderer Shape14;
	   ModelRenderer Shape15;
	   ModelRenderer Shape16;
	   ModelRenderer Shape17;
	   ModelRenderer Shape18;
	   ModelRenderer laser;


	   public ModelLaser() {
	      super.textureWidth = 256;
	      super.textureHeight = 128;
	      this.Shape0 = new ModelRenderer(this, 0, 0);
	      this.Shape0.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape0.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape0.setTextureSize(256, 128);
	      this.Shape0.mirror = true;
	      this.setRotation(this.Shape0, 0.0F, 0.0F, 0.0F);
	      this.Shape1 = new ModelRenderer(this, 0, 0);
	      this.Shape1.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape1.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape1.setTextureSize(256, 128);
	      this.Shape1.mirror = true;
	      this.setRotation(this.Shape1, 0.0F, 0.0F, 0.4886922F);
	      this.Shape2 = new ModelRenderer(this, 0, 0);
	      this.Shape2.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape2.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape2.setTextureSize(256, 128);
	      this.Shape2.mirror = true;
	      this.setRotation(this.Shape2, 0.0F, 0.0F, 0.9773844F);
	      this.Shape3 = new ModelRenderer(this, 0, 0);
	      this.Shape3.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape3.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape3.setTextureSize(256, 128);
	      this.Shape3.mirror = true;
	      this.setRotation(this.Shape3, 0.0F, 0.0F, 1.466077F);
	      this.Shape4 = new ModelRenderer(this, 0, 0);
	      this.Shape4.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape4.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape4.setTextureSize(256, 128);
	      this.Shape4.mirror = true;
	      this.setRotation(this.Shape4, 0.0F, 0.0F, 1.954769F);
	      this.Shape5 = new ModelRenderer(this, 0, 0);
	      this.Shape5.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape5.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape5.setTextureSize(256, 128);
	      this.Shape5.mirror = true;
	      this.setRotation(this.Shape5, 0.0F, 0.0F, -0.4886922F);
	      this.Shape6 = new ModelRenderer(this, 0, 0);
	      this.Shape6.addBox(-1.0F, -4.0F, 0.0F, 2, 8, 34);
	      this.Shape6.setRotationPoint(0.0F, 0.0F, 0.0F);
	      this.Shape6.setTextureSize(256, 128);
	      this.Shape6.mirror = true;
	      this.setRotation(this.Shape6, 0.0F, 0.0F, -0.837758F);
	      this.Shape7 = new ModelRenderer(this, 75, 0);
	      this.Shape7.addBox(-1.0F, -4.0F, 0.0F, 10, 10, 20);
	      this.Shape7.setRotationPoint(-4.0F, -1.0F, 13.0F);
	      this.Shape7.setTextureSize(256, 128);
	      this.Shape7.mirror = true;
	      this.setRotation(this.Shape7, 0.0F, 0.0F, 0.0F);
	      this.Shape8 = new ModelRenderer(this, 75, 0);
	      this.Shape8.addBox(-1.0F, -4.0F, 0.0F, 2, 2, 20);
	      this.Shape8.setRotationPoint(-4.0F, -3.0F, 13.0F);
	      this.Shape8.setTextureSize(256, 128);
	      this.Shape8.mirror = true;
	      this.setRotation(this.Shape8, 0.0F, 0.0F, 0.0F);
	      this.Shape9 = new ModelRenderer(this, 75, 0);
	      this.Shape9.addBox(-1.0F, -4.0F, 0.0F, 2, 2, 20);
	      this.Shape9.setRotationPoint(4.0F, -3.0F, 13.0F);
	      this.Shape9.setTextureSize(256, 128);
	      this.Shape9.mirror = true;
	      this.setRotation(this.Shape9, 0.0F, 0.0F, 0.0F);
	      this.Shape10 = new ModelRenderer(this, 75, 0);
	      this.Shape10.addBox(-1.0F, -4.0F, 0.0F, 10, 2, 1);
	      this.Shape10.setRotationPoint(-4.0F, -3.0F, 14.0F);
	      this.Shape10.setTextureSize(256, 128);
	      this.Shape10.mirror = true;
	      this.setRotation(this.Shape10, 0.0F, 0.0F, 0.0F);
	      this.Shape11 = new ModelRenderer(this, 75, 0);
	      this.Shape11.addBox(-1.0F, 0.0F, 0.0F, 1, 2, 20);
	      this.Shape11.setRotationPoint(5.0F, -5.0F, 13.0F);
	      this.Shape11.setTextureSize(256, 128);
	      this.Shape11.mirror = true;
	      this.setRotation(this.Shape11, 0.0F, 0.0F, -0.4712389F);
	      this.Shape12 = new ModelRenderer(this, 75, 0);
	      this.Shape12.addBox(-1.0F, 0.0F, 0.0F, 1, 2, 20);
	      this.Shape12.setRotationPoint(-4.0F, -5.0F, 13.0F);
	      this.Shape12.setTextureSize(256, 128);
	      this.Shape12.mirror = true;
	      this.setRotation(this.Shape12, 0.0F, 0.0F, 0.5235988F);
	      this.Shape13 = new ModelRenderer(this, 75, 0);
	      this.Shape13.addBox(-1.0F, 0.0F, 0.0F, 1, 8, 20);
	      this.Shape13.setRotationPoint(-5.0F, -4.0F, 13.0F);
	      this.Shape13.setTextureSize(256, 128);
	      this.Shape13.mirror = true;
	      this.setRotation(this.Shape13, 0.0F, 0.0F, 0.0F);
	      this.Shape14 = new ModelRenderer(this, 75, 0);
	      this.Shape14.addBox(-1.0F, 0.0F, 0.0F, 1, 8, 20);
	      this.Shape14.setRotationPoint(6.0F, -4.0F, 13.0F);
	      this.Shape14.setTextureSize(256, 128);
	      this.Shape14.mirror = true;
	      this.setRotation(this.Shape14, 0.0F, 0.0F, 0.0F);
	      this.Shape15 = new ModelRenderer(this, 140, 0);
	      this.Shape15.addBox(-1.0F, 0.0F, 0.0F, 13, 1, 1);
	      this.Shape15.setRotationPoint(-5.5F, -3.0F, 14.0F);
	      this.Shape15.setTextureSize(256, 128);
	      this.Shape15.mirror = true;
	      this.setRotation(this.Shape15, 0.0F, 0.0F, 0.0F);
	      this.Shape16 = new ModelRenderer(this, 140, 0);
	      this.Shape16.addBox(-1.0F, 0.0F, 0.0F, 13, 1, 1);
	      this.Shape16.setRotationPoint(-5.5F, -3.0F, 31.0F);
	      this.Shape16.setTextureSize(256, 128);
	      this.Shape16.mirror = true;
	      this.setRotation(this.Shape16, 0.0F, 0.0F, 0.0F);
	      this.Shape17 = new ModelRenderer(this, 140, 0);
	      this.Shape17.addBox(-1.0F, 0.0F, 0.0F, 13, 1, 1);
	      this.Shape17.setRotationPoint(-5.5F, 2.0F, 14.0F);
	      this.Shape17.setTextureSize(256, 128);
	      this.Shape17.mirror = true;
	      this.setRotation(this.Shape17, 0.0F, 0.0F, 0.0F);
	      this.Shape18 = new ModelRenderer(this, 140, 0);
	      this.Shape18.addBox(-1.0F, 0.0F, 0.0F, 13, 1, 1);
	      this.Shape18.setRotationPoint(-5.5F, 2.0F, 31.0F);
	      this.Shape18.setTextureSize(256, 128);
	      this.Shape18.mirror = true;
	      this.setRotation(this.Shape18, 0.0F, 0.0F, 0.0F);
	      this.laser = new ModelRenderer(this, 0, 50);
	      this.laser.addBox(-0.5F, -0.5F, 0.0F, 1, 1, 34);
	      this.laser.setRotationPoint(0.0F, 0.0F, -34.0F);
	      this.laser.setTextureSize(256, 128);
	      this.laser.mirror = true;
	      this.setRotation(this.laser, 0.0F, 0.0F, 0.0F);
	   }

	   public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
	      super.render(entity, f, f1, f2, f3, f4, f5);
	      this.setRotationAngles(f, f1, f2, f3, f4, f5);
	      this.Shape0.render(f5);
	      this.Shape1.render(f5);
	      this.Shape2.render(f5);
	      this.Shape3.render(f5);
	      this.Shape4.render(f5);
	      this.Shape5.render(f5);
	      this.Shape6.render(f5);
	      this.Shape7.render(f5);
	      this.Shape8.render(f5);
	      this.Shape9.render(f5);
	      this.Shape10.render(f5);
	      this.Shape11.render(f5);
	      this.Shape12.render(f5);
	      this.Shape13.render(f5);
	      this.Shape14.render(f5);
	      this.Shape15.render(f5);
	      this.Shape16.render(f5);
	      this.Shape17.render(f5);
	      this.Shape18.render(f5);
	      this.laser.render(f5);
	   }

	   public void render(float f5) {
	      this.Shape0.render(f5);
	      this.Shape1.render(f5);
	      this.Shape2.render(f5);
	      this.Shape3.render(f5);
	      this.Shape4.render(f5);
	      this.Shape5.render(f5);
	      this.Shape6.render(f5);
	      this.Shape7.render(f5);
	      this.Shape8.render(f5);
	      this.Shape9.render(f5);
	      this.Shape10.render(f5);
	      this.Shape11.render(f5);
	      this.Shape12.render(f5);
	      this.Shape13.render(f5);
	      this.Shape14.render(f5);
	      this.Shape15.render(f5);
	      this.Shape16.render(f5);
	      this.Shape17.render(f5);
	      this.Shape18.render(f5);
	   }

	   public void renderLaser(float f5, float f6) {
	      GL11.glScalef(1.0F, 1.0F, f6);
	      this.laser.render(f5);
	   }

	   private void setRotation(ModelRenderer model, float x, float y, float z) {
	      model.rotateAngleX = x;
	      model.rotateAngleY = y;
	      model.rotateAngleZ = z;
	   }

	   public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
	      super.setRotationAngles(f, f1, f2, f3, f4, f5, (Entity)null);
	   }
	}
