package ru.xlv.gun.gui;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.opengl.GL11;
import ru.xlv.gun.network.PacketHandler;
import ru.xlv.gun.network.packets.PacketOpenInv;

@SideOnly(Side.CLIENT)
public class GameOverlay {

	private static final ResourceLocation damageLoc = new ResourceLocation("batmod:textures/DamageLocation.png");
	public static final ResourceLocation gui = new ResourceLocation("batmod:textures/gui/ramka.png");
	private static final ResourceLocation traderequire = new ResourceLocation("batmod:textures/gui/yesno.png");
	private Minecraft mc = Minecraft.getMinecraft();
	public static float hitMakerTime = 0F;
	public static boolean isHeadShot = false;
	public static float damageLocation = 0F;
	public static String damageLocat = null;
	public static String tradeSender = null;

	public GameOverlay() {
	}

	@SubscribeEvent
	public void drawDamageLoc(RenderGameOverlayEvent.Post event) {
		if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
			if (damageLocation > 0) {
				GL11.glPushMatrix();
				mc.entityRenderer.setupOverlayRendering();
				GL11.glEnable(3042 /* GL_BLEND */);
				GL11.glDisable(2929 /* GL_DEPTH_TEST */);
				GL11.glDepthMask(false);
				GL11.glBlendFunc(770, 771);
				GL11.glRotatef(0, 0, 0, 1);
				GL11.glColor4f(1F, 1F, 1F, damageLocation);
				GL11.glDisable(3008 /* GL_ALPHA_TEST */);
				ru.xlv.core.util.Utils.bindTexture(damageLoc);
				Tessellator tessellator = Tessellator.instance;
				tessellator.startDrawingQuads();

				if (damageLocat.equals("NORTH")) {
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 3f, event.resolution.getScaledHeight(), -90D, 0.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 1.5F, event.resolution.getScaledHeight(), -90D, 1.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 1.5F, 0.0D, -90D, 1.0D, 0.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 3f, 0.0D, -90D, 0.0D, 0.0D);
				}
				if (damageLocat.equals("WEST")) { // слева
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 1.5F, event.resolution.getScaledHeight(), -90D, 0.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 1.5F, 0, -90D, 1.0D, 1.0D);
					tessellator.addVertexWithUV(0, 0, -90D, 1.0D, 0.0D);
					tessellator.addVertexWithUV(0, event.resolution.getScaledHeight(), -90D, 0.0D, 0.0D);
				}
				if (damageLocat.equals("EAST")) { //справа
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 3f, 0, -90D, 0.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 3f, event.resolution.getScaledHeight(), -90D, 1.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth(), event.resolution.getScaledHeight(), -90D, 1.0D, 0.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth(), 0, -90D, 0.0D, 0.0D);
				}
				if (damageLocat.equals("SOUTH")) {
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 1.5F, 0, -90D, 0.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 3f, 0, -90D, 1.0D, 1.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 3f, event.resolution.getScaledHeight(), -90D, 1.0D, 0.0D);
					tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 1.5F, event.resolution.getScaledHeight(), -90D, 0.0D, 0.0D);
				}
				tessellator.draw();

				GL11.glDepthMask(true);
				GL11.glEnable(2929 /* GL_DEPTH_TEST */);
				GL11.glEnable(3008 /* GL_ALPHA_TEST */);
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				damageLocation -= 0.08F;
				GL11.glPopMatrix();
			}
		}
	}

	@SubscribeEvent
	public void drawTradeRequirest(RenderGameOverlayEvent.Post event) {
		if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
			if (tradeSender != null) {
				int w = event.resolution.getScaledWidth();
				int h = event.resolution.getScaledHeight();
				float scale = 1.3F;

				GL11.glPushMatrix();
				mc.entityRenderer.setupOverlayRendering();

				GL11.glEnable(3042 /* GL_BLEND */);
				GL11.glDisable(2929 /* GL_DEPTH_TEST */);
				GL11.glDepthMask(false);

				GL11.glBlendFunc(770, 771);
				GL11.glColor4f(1F, 1F, 1F, 1.0F);
				GL11.glDisable(3008 /* GL_ALPHA_TEST */);

				ru.xlv.core.util.Utils.bindTexture(traderequire);

				Tessellator tessellator = Tessellator.instance;
				tessellator.startDrawingQuads();
				tessellator.addVertexWithUV(0, event.resolution.getScaledHeight() / 2f, 90D, 0.0D, 1.0D);
				tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 4f, event.resolution.getScaledHeight() / 2f, 90D, 1.0D, 1.0D);
				tessellator.addVertexWithUV(event.resolution.getScaledWidth() / 4f, 0.0D, 90D, 1.0D, 0.0D);
				tessellator.addVertexWithUV(0, 0.0D, 90D, 0.0D, 0.0D);
				tessellator.draw();
				GL11.glScalef(scale, scale, scale);
				mc.fontRenderer.drawString(tradeSender, (int) (w / scale / 100), (int) (h / scale / 17), 16777215);
				GL11.glDepthMask(true);
				GL11.glEnable(2929 /* GL_DEPTH_TEST */);
				GL11.glEnable(3008 /* GL_ALPHA_TEST */);
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				GL11.glPopMatrix();
			}
		}
	}

//	@SubscribeEvent
//	public void drawHealth(RenderGameOverlayEvent.Post event) {
//		if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
//			GL11.glPushMatrix();
//			ResourceLocation res = new ResourceLocation("batmod:textures/gui/damagescreen.png");
//			float hp = mc.thePlayer.getHealth();
//			mc.entityRenderer.setupOverlayRendering();
//			if (hp > 90) {
//				GL11.glColor4f(1F, 1F, 1F, 0F);
//			} else {
//				GL11.glColor4f(1F, 1F, 1F, 10F / hp);
//			}
//			GL11.glEnable(3042 /* GL_BLEND */);
//			GL11.glDisable(2929 /* GL_DEPTH_TEST */);
//			GL11.glDepthMask(false);
//			GL11.glBlendFunc(770, 771);
//			GL11.glDisable(3008 /* GL_ALPHA_TEST */);
//			ru.xlv.core.util.Utils.bindTexture(res);
//			Tessellator tessellator = Tessellator.instance;
//			tessellator.startDrawingQuads();
//			tessellator.addVertexWithUV(0, event.resolution.getScaledHeight(), -90D, 0.0D, 1.0D);
//			tessellator.addVertexWithUV(event.resolution.getScaledWidth(), event.resolution.getScaledHeight(), -90D, 1.0D, 1.0D);
//			tessellator.addVertexWithUV(event.resolution.getScaledWidth(), 0.0D, -90D, 1.0D, 0.0D);
//			tessellator.addVertexWithUV(0, 0.0D, -90D, 0.0D, 0.0D);
//			tessellator.draw();
//			GL11.glDepthMask(true);
//			GL11.glEnable(2929 /* GL_DEPTH_TEST */);
//			GL11.glEnable(3008 /* GL_ALPHA_TEST */);
//			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//			GL11.glPopMatrix();
//		}
//	}

	@SubscribeEvent
	public void onGuiOpen(GuiOpenEvent event) {
		if (event.gui instanceof GuiInventory) {
			EntityPlayer player = Minecraft.getMinecraft().thePlayer;
			if (!player.capabilities.isCreativeMode) {
				event.setCanceled(true);
				PacketHandler.INSTANCE.sendToServer(new PacketOpenInv());
			}
		}
	}
}
