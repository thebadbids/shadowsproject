package ru.xlv.gun.gui;

import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import ru.xlv.gun.container.ContainerGunModTable;
import ru.xlv.gun.item.ItemWeapon;

import java.util.ArrayList;
import java.util.List;

public class GuiGunModTable extends GuiContainer {

    private static final ResourceLocation texture = new ResourceLocation("batmod", "textures/gui/attach.png");

    public GuiGunModTable(InventoryPlayer inv, World w) {
        super(new ContainerGunModTable(inv, w));
        ySize = 256;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int x, int y) {
        fontRendererObj.drawString("Инвентарь", 8, (ySize - 94) + 2, 0x404040);
        fontRendererObj.drawString("Модификация Оружия", 8, 6, 0x404040);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        GL11.glEnable(3042);
        GL11.glBlendFunc(770, 771);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.6F);

        ru.xlv.core.util.Utils.bindTexture(texture);

        int xOrigin = (width - xSize) / 2;
        int yOrigin = (height - ySize) / 2;
        drawTexturedModalRect(xOrigin, yOrigin, 0, 0, xSize, ySize);

        for (int z = 1; z < 13; z++)
            inventorySlots.getSlot(z).yDisplayPosition = -1000;

        ItemStack gunStack = inventorySlots.getSlot(0).getStack();
        if (gunStack != null && gunStack.getItem() instanceof ItemWeapon) {
            drawTexturedModalRect(xOrigin + 51, yOrigin + 107, 176, 122, 22, 22);
            inventorySlots.getSlot(1).yDisplayPosition = 110;
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.6F);
            drawTexturedModalRect(xOrigin + 77, yOrigin + 81, 202, 96, 22, 22);
            inventorySlots.getSlot(2).yDisplayPosition = 84;
            drawTexturedModalRect(xOrigin + 103, yOrigin + 107, 228, 122, 22, 22);
            inventorySlots.getSlot(3).yDisplayPosition = 110;
            drawTexturedModalRect(xOrigin + 77, yOrigin + 133, 202, 148, 22, 22);
            inventorySlots.getSlot(4).yDisplayPosition = 136;
            this.drawPlayerModel(guiLeft - 40, guiTop + 5, 10, guiLeft + 51 - 0, guiTop + 25 - 0, mc.thePlayer);

            GL11.glDisable(3042);
        }
        if (gunStack != null && gunStack.getItem() instanceof ItemWeapon) {
            ArrayList<String> attach = new ArrayList<String>();
            attach.add(EnumChatFormatting.BOLD + "Можно установить:");
            for (int ix = 0; ix < ((ItemWeapon) gunStack.getItem()).attachments.length; ix++) {
                String xyeta = ((ItemWeapon) gunStack.getItem()).attachments[ix].getUnlocalizedName();
                attach.add(EnumChatFormatting.GREEN + " - " + xyeta.substring(5));
            }
            drawHoveringTextA(attach, this.guiLeft + 169, this.guiTop + 16, fontRendererObj); // makes all that nice default tool tip box from vanilla minecraft
        }


    }

    private void drawHoveringTextA(List p_146283_1_, int p_146283_2_, int p_146283_3_, FontRenderer font) {
        if (!p_146283_1_.isEmpty()) {
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            int k = 0;

            for (Object o : p_146283_1_) {
                String s = (String) o;
                int l = font.getStringWidth(s);

                if (l > k) {
                    k = l;
                }
            }

            int j2 = p_146283_2_ + 12;
            int k2 = p_146283_3_ - 12;
            int i1 = 8;

            if (p_146283_1_.size() > 1) {
                i1 += 2 + (p_146283_1_.size() - 1) * 10;
            }

            if (j2 + k > this.width) {
                j2 -= 28 + k;
            }

            if (k2 + i1 + 6 > this.height) {
                k2 = this.height - i1 - 6;
            }

            this.zLevel = 300.0F;
            itemRender.zLevel = 300.0F;
            this.drawGradientRectA(j2 - 3, k2 - 4, j2 + k + 3, k2 - 3);
            this.drawGradientRectA(j2 - 3, k2 + i1 + 3, j2 + k + 3, k2 + i1 + 4);
            this.drawGradientRectA(j2 - 3, k2 - 3, j2 + k + 3, k2 + i1 + 3);
            this.drawGradientRectA(j2 - 4, k2 - 3, j2 - 3, k2 + i1 + 3);
            this.drawGradientRectA(j2 + k + 3, k2 - 3, j2 + k + 4, k2 + i1 + 3);
            this.drawGradientRectA(j2 - 3, k2 - 3 + 1, j2 - 3 + 1, k2 + i1 + 3 - 1);
            this.drawGradientRectA(j2 + k + 2, k2 - 3 + 1, j2 + k + 3, k2 + i1 + 3 - 1);
            this.drawGradientRectA(j2 - 3, k2 - 3, j2 + k + 3, k2 - 3 + 1);
            this.drawGradientRectA(j2 - 3, k2 + i1 + 2, j2 + k + 3, k2 + i1 + 3);

            for (int i2 = 0; i2 < p_146283_1_.size(); ++i2) {
                String s1 = (String) p_146283_1_.get(i2);
                font.drawStringWithShadow(s1, j2, k2, -1);
                if (i2 == 0) {
                    k2 += 2;
                }
                k2 += 10;
            }

            this.zLevel = 0.0F;
            itemRender.zLevel = 0.0F;
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            RenderHelper.enableStandardItemLighting();
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        }
    }

    private void drawGradientRectA(int p_73733_1_, int p_73733_2_, int p_73733_3_, int p_73733_4_) {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(0F, 0F, 0F, 0.2F);
        tessellator.addVertex((double) p_73733_3_, (double) p_73733_2_, (double) this.zLevel);
        tessellator.addVertex((double) p_73733_1_, (double) p_73733_2_, (double) this.zLevel);
        tessellator.setColorRGBA_F(0F, 0F, 0F, 0.4F);
        tessellator.addVertex((double) p_73733_1_, (double) p_73733_4_, (double) this.zLevel);
        tessellator.addVertex((double) p_73733_3_, (double) p_73733_4_, (double) this.zLevel);
        tessellator.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    private void drawPlayerModel(int x, int y, int scale, int yaw, int pitch, EntityClientPlayerMP thePlayer) {
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef(x + 145, y + 38.0F, 550.0F);
        int scale2 = 30;
        GL11.glScalef(-scale - scale2, scale + scale2, scale + scale2);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glRotatef(30.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(20.0F, 0.0F, 0.0F, 1.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glTranslatef(0.0F, 0, 0.0F);
        RenderManager.instance.itemRenderer.renderItem(this.mc.thePlayer, inventorySlots.getSlot(0).getStack(), 0);
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }
}
