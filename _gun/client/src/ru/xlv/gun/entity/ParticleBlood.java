package ru.xlv.gun.entity;

import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ParticleBlood extends EntityFX {

    public ParticleBlood(World world) {
        super(world, 1, 1, 1, 1, 1, 1);
    }

    public ParticleBlood(World p_i1227_1_, double p_i1227_2_, double p_i1227_4_, double p_i1227_6_, double p_i1227_8_, double p_i1227_10_, double p_i1227_12_) {
        super(p_i1227_1_, p_i1227_2_, p_i1227_4_, p_i1227_6_, p_i1227_8_, p_i1227_10_, p_i1227_12_);
        particleGreen = 0.5f;
        particleRed = 0.6f;
        particleBlue = 0.5f;
        particleGravity = 0.85f;
        particleAlpha = 0.9f;
        this.particleScale = 0.45f;
    }

    public void renderParticle(Tessellator p_70539_1_, float p_70539_2_, float p_70539_3_, float p_70539_4_, float p_70539_5_, float p_70539_6_, float p_70539_7_) {
        ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/red.png"));
        super.renderParticle(p_70539_1_, p_70539_2_, p_70539_3_, p_70539_4_, p_70539_5_, p_70539_6_, p_70539_7_);
    }

    public int getFXLayer() {
        return 2;
    }
}
