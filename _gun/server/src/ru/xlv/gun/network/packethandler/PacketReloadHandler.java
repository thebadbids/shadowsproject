package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import ru.xlv.gun.network.packets.PacketReload;
import ru.xlv.gun.util.ExtendedPlayer;

public class PacketReloadHandler implements IMessageHandler<PacketReload, IMessage> {

	@Override
	public IMessage onMessage(PacketReload message, MessageContext ctx) {
		EntityPlayerMP serverPlayer = ctx.getServerHandler().playerEntity;
		ExtendedPlayer ep = ExtendedPlayer.get(serverPlayer);
		ep.setReloadKeyPressed();
		return null;
	}
}
