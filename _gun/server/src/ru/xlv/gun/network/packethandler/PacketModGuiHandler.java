package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import ru.xlv.gun.XlvsMainMod;
import ru.xlv.gun.network.packets.PacketModGui;

public class PacketModGuiHandler implements IMessageHandler<PacketModGui, IMessage> {

    @Override
    public IMessage onMessage(PacketModGui message, MessageContext ctx) {
        EntityPlayerMP playerEntity = ctx.getServerHandler().playerEntity;
        playerEntity.openGui(XlvsMainMod.instance, 0, playerEntity.worldObj, (int) playerEntity.posX, (int) playerEntity.posY, (int) playerEntity.posZ);
        return null;
    }
}