package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.network.packets.PacketUnreloadMag;
import ru.xlv.gun.util.ExtendedPlayer;

public class PacketUnreloadMagHandler implements IMessageHandler<PacketUnreloadMag, IMessage> {

	@Override
	public IMessage onMessage(PacketUnreloadMag message, MessageContext ctx) {
		EntityPlayerMP serverPlayer = ctx.getServerHandler().playerEntity;
		ExtendedPlayer ep = ExtendedPlayer.get(serverPlayer);
		ep.setReloadKeyUnpressed();
		if (serverPlayer.getHeldItem() != null) {
			if (serverPlayer.getHeldItem().getItem() instanceof ItemWeapon) {
				if (serverPlayer.getHeldItem().getTagCompound().getFloat("reloadTime") == 0) {
					NBTTagCompound nbt = serverPlayer.getHeldItem().getTagCompound();
					nbt.setInteger("unreload", 1);
				}
			}
		}
		return null;
	}
}
