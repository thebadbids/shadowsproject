package ru.xlv.gun.network;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import ru.xlv.gun.network.packethandler.*;
import ru.xlv.gun.network.packets.*;

public class PacketHandler {

    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel("mymodid");

    public static void registryPackets(){
        INSTANCE.registerMessage(PacketShootHandler.class, PacketShoot.class, 0, Side.SERVER);
        INSTANCE.registerMessage(PacketAttachHandler.class, PacketAttachButton.class, 1, Side.SERVER);
        INSTANCE.registerMessage(PacketModGuiHandler.class, PacketModGui.class, 2, Side.SERVER);
        INSTANCE.registerMessage(PacketReloadHandler.class, PacketReload.class, 3, Side.SERVER);
        INSTANCE.registerMessage(PacketOpenInvHandler.class, PacketOpenInv.class, 5, Side.SERVER);
        INSTANCE.registerMessage(PacketDetachHandler.class, PacketDetach.class, 7, Side.SERVER);
        INSTANCE.registerMessage(PacketShSoundHandler.class, PacketShSound.class, 8, Side.SERVER);
        INSTANCE.registerMessage(PacketUnreloadHandler.class, PacketUnreload.class, 9, Side.SERVER);
        INSTANCE.registerMessage(PacketUnreloadMagHandler.class, PacketUnreloadMag.class, 17, Side.SERVER);
        INSTANCE.registerMessage(MessageGunModTable.Handler.class, MessageGunModTable.class, 20, Side.SERVER);
        INSTANCE.registerMessage(Message_CloseItems.Handler.class, Message_CloseItems.class, 21, Side.SERVER);
    }
}
