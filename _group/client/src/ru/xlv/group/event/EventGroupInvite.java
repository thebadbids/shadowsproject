package ru.xlv.group.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EventGroupInvite extends Event {

    private final String userName;
}
