package ru.xlv.group.handle;

import cpw.mods.fml.common.FMLCommonHandler;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementType;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.service.INotificationService;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.common.GroupInvite;
import ru.xlv.group.handle.result.GroupChangeLeaderResult;
import ru.xlv.group.handle.result.GroupInviteAcceptResult;
import ru.xlv.group.handle.result.GroupInviteResult;
import ru.xlv.group.handle.result.GroupKickResult;
import ru.xlv.group.network.PacketGroupInviteNew;
import ru.xlv.group.network.PacketGroupSync;
import ru.xlv.group.util.GroupLocalization;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GroupHandler {

    private static final int GROUP_PLAYERS_LIMIT = 5;

    private final List<GroupServer> groupList = new ArrayList<>();

    private final Random random = new Random();

    public void init() {
        EventListener eventListener = new EventListener();
        FMLCommonHandler.instance().bus().register(eventListener);
        XlvsCoreCommon.EVENT_BUS.register(eventListener);
    }

    public GroupInviteResult handleInvite(String initiatorName, String targetName) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(initiatorName);
        if (serverPlayer == null) {
            return GroupInviteResult.UNKNOWN;
        }
        ServerPlayer targetPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(targetName);
        if(targetPlayer == null) {
            return GroupInviteResult.PLAYER_NOT_FOUND;
        }
        if(getPlayerGroup(targetPlayer) != null) {
            return GroupInviteResult.PLAYER_ALREADY_GROUPED;
        }
        GroupServer group = getPlayerGroup(serverPlayer);
        if (group != null) {
            if(group.getPlayers().size() >= GROUP_PLAYERS_LIMIT) {
                return GroupInviteResult.GROUP_IS_FULL;
            } else {
                for (GroupInvite<ServerPlayer> invite : group.getInvites()) {
                    if(invite.getTarget().equals(targetName)) {
                        return GroupInviteResult.PLAYER_ALREADY_INVITED;
                    }
                }
            }
        } else {
            synchronized (groupList) {
                groupList.add(group = new GroupServer(serverPlayer));
            }
        }
        invitePlayer(initiatorName, group, targetPlayer);
        return GroupInviteResult.SUCCESS;
    }

    public GroupInviteAcceptResult handleInviteAccept(String initiatorName, String targetName) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(initiatorName);
        if (serverPlayer == null || !serverPlayer.isOnline()) {
            return GroupInviteAcceptResult.INVITER_NOT_FOUND_OR_OFFLINE;
        }
        ServerPlayer targetPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(targetName);
        if (targetName == null) {
            return GroupInviteAcceptResult.UNKNOWN_ERROR;
        }
        GroupServer group = getPlayerGroup(serverPlayer);
        if (group == null) {
            return GroupInviteAcceptResult.UNKNOWN_ERROR;
        }
        if(group.getPlayers().size() >= GROUP_PLAYERS_LIMIT) {
            return GroupInviteAcceptResult.GROUP_IS_FULL;
        }
        acceptInvite(group, targetPlayer);
        return GroupInviteAcceptResult.SUCCESS;
    }

    public void handleLeave(ServerPlayer serverPlayer) {
        GroupServer playerGroup = getPlayerGroup(serverPlayer);
        if (playerGroup != null) {
            kickPlayer(playerGroup, serverPlayer, true);
        }
    }

    public GroupKickResult handleKick(String initiatorName, String targetName) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(initiatorName);
        if (serverPlayer == null) {
            return GroupKickResult.UNKNOWN;
        }
        ServerPlayer targetPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(targetName);
        if(targetPlayer == null) {
            return GroupKickResult.PLAYER_NOT_FOUND;
        }
        GroupServer group = getPlayerGroup(serverPlayer);
        if(getPlayerGroup(targetPlayer) != group) {
            return GroupKickResult.UNKNOWN;
        }
        kickPlayer(group, targetPlayer, false);
        return GroupKickResult.SUCCESS;
    }

    public GroupChangeLeaderResult handleChangeLeader(String initiatorName, String targetName) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(initiatorName);
        if (serverPlayer == null) {
            return GroupChangeLeaderResult.UNKNOWN;
        }
        ServerPlayer targetPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(targetName);
        if(targetPlayer == null) {
            return GroupChangeLeaderResult.PLAYER_NOT_FOUND;
        }
        GroupServer group = getPlayerGroup(serverPlayer);
        if(getPlayerGroup(targetPlayer) != group) {
            return GroupChangeLeaderResult.UNKNOWN;
        }
        changeLeader(group, targetPlayer);
        return GroupChangeLeaderResult.SUCCESS;
    }

    private void changeLeader(GroupServer group, ServerPlayer serverPlayer) {
        group.setLeader(serverPlayer);
        if (getNotificationService() != null) {
            getNotificationService().sendNotification(serverPlayer, getLocalization().changeLeaderNotificationMessage);
            for (ServerPlayer player : group.getPlayers()) {
                getNotificationService().sendNotification(player, getLocalization().getFormatted(getLocalization().changeLeaderNotificationGroupMessage, serverPlayer.getPlayerName()));
            }
        }
    }

    private void kickPlayer(GroupServer group, ServerPlayer serverPlayer, boolean doSilently) {
        if (group.getPlayers().removeIf(player -> serverPlayer.getPlayerName().equals(player.getPlayerName()))) {
            boolean flag = group.getPlayers().isEmpty() || group.getPlayers().size() == 1;
            if(group.getLeader().getPlayerName().equals(serverPlayer.getPlayerName())) {
                if(!group.getPlayers().isEmpty()) {
                    group.setLeader(group.getPlayers().get(random.nextInt(group.getPlayers().size())));
                }
            }
            if(flag) {
                synchronized (groupList) {
                    groupList.remove(group);
                }
            }
            group.getPlayers().forEach(serverPlayer1 -> XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer1.getEntityPlayer(), new PacketGroupSync(group)));
            if(!doSilently) {
                if (getNotificationService() != null) {
                    getNotificationService().sendNotification(serverPlayer, getLocalization().kickNotificationMessage);
                    for (ServerPlayer player : group.getPlayers()) {
                        getNotificationService().sendNotification(player, getLocalization().getFormatted(getLocalization().kickNotificationGroupMessage, serverPlayer.getPlayerName()));
                    }
                }
            }
        }
    }

    private void invitePlayer(String initiatorName, GroupServer group, ServerPlayer serverPlayer) {
        synchronized (group.getInvites()) {
            group.getInvites().add(new GroupInvite<>(group, serverPlayer.getPlayerName(), System.currentTimeMillis()));
        }
        XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(serverPlayer, AchievementType.FIRST_GROUP);
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketGroupInviteNew(initiatorName));
        if (getNotificationService() != null) {
            getNotificationService().sendNotification(serverPlayer, getLocalization().getFormatted(getLocalization().inviteNotificationMessage, group.getLeader().getPlayerName()));
            for (ServerPlayer player : group.getPlayers()) {
                getNotificationService().sendNotification(player, getLocalization().getFormatted(getLocalization().inviteNotificationGroupMessage, serverPlayer.getPlayerName()));
            }
        }
    }

    private void acceptInvite(GroupServer group, ServerPlayer serverPlayer) {
        synchronized (group.getInvites()) {
            group.getInvites().removeIf(groupInvite -> groupInvite.getTarget().equals(serverPlayer.getPlayerName()));
        }
        group.getPlayers().add(serverPlayer);
        XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(serverPlayer, AchievementType.FIRST_GROUP);
        if (getNotificationService() != null) {
            getNotificationService().sendNotification(serverPlayer, getLocalization().inviteAcceptNotificationMessage);
            for (ServerPlayer player : group.getPlayers()) {
                getNotificationService().sendNotification(player, getLocalization().getFormatted(getLocalization().inviteAcceptNotificationGroupMessage, serverPlayer.getPlayerName()));
            }
        }
    }

    public GroupServer getPlayerGroup(ServerPlayer player) {
        synchronized (groupList) {
            for (GroupServer group : groupList) {
                for (ServerPlayer serverPlayer : group.getPlayers()) {
                    if(serverPlayer.getPlayerName().equals(player.getPlayerName())) {
                        return group;
                    }
                }
            }
        }
        return null;
    }

    public GroupServer getPlayerGroup(String username) {
        synchronized (groupList) {
            for (GroupServer group : groupList) {
                for (ServerPlayer serverPlayer : group.getPlayers()) {
                    if(serverPlayer.getPlayerName().equals(username)) {
                        return group;
                    }
                }
            }
        }
        return null;
    }

    public synchronized List<GroupServer> getGroupList() {
        return groupList;
    }

    private GroupLocalization getLocalization() {
        return XlvsGroupMod.INSTANCE.getLocalization();
    }

    private INotificationService getNotificationService() {
        return XlvsCore.INSTANCE.getNotificationService();
    }
}
