package ru.xlv.group.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.group.XlvsGroupMod;

@Getter
@RequiredArgsConstructor
public enum GroupKickResult {

    SUCCESS(XlvsGroupMod.INSTANCE.getLocalization().responseKickSuccessMessage),
    PLAYER_NOT_FOUND(XlvsGroupMod.INSTANCE.getLocalization().responseKickPlayerNotFoundMessage),
    UNKNOWN(XlvsGroupMod.INSTANCE.getLocalization().responseKickUnknownMessage);

    private final String responseMessage;
}
