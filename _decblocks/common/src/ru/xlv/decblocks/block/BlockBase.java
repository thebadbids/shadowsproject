package ru.xlv.decblocks.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;

public class BlockBase extends Block {

    public BlockBase(String name, Material p_i45394_1_) {
        super(p_i45394_1_);
        setBlockName(name);
        setBlockTextureName(name);
        setCreativeTab(CreativeTabs.tabBlock);
    }

    @Override
    public void registerBlockIcons(IIconRegister p_149651_1_) {}
}
