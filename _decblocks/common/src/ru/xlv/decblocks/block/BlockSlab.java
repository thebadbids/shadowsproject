package ru.xlv.decblocks.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import java.util.Random;

public class BlockSlab extends net.minecraft.block.BlockSlab {

//    public static final String[] field_150005_b = new String[]{"oak", "spruce", "birch", "jungle", "acacia", "big_oak"};
    private final Block block;
    private BlockSlab slab;

    public BlockSlab(String name, boolean p_i45437_1_, Block block, BlockSlab slab) {
        super(p_i45437_1_, Material.wood);
        this.block = block;
        this.slab = slab;
        setBlockName(name);
        setBlockTextureName(name);
        this.setCreativeTab(CreativeTabs.tabBlock);
        if(slab == null) {
            this.slab = this;
        }
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int p_149691_2_) {
        return block.getIcon(p_149691_1_, p_149691_2_ & 7);
    }

    public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_) {
        return Item.getItemFromBlock(slab);
    }

    protected ItemStack createStackedBlock(int p_149644_1_) {
        return new ItemStack(Item.getItemFromBlock(slab), 2, p_149644_1_ & 7);
    }

    public String func_150002_b(int p_150002_1_) {
//        if (p_150002_1_ < 0 || p_150002_1_ >= field_150005_b.length) {
//            p_150002_1_ = 0;
//        }
//
//        return super.getUnlocalizedName() + "." + field_150005_b[p_150002_1_];
        return super.getUnlocalizedName();
    }

//    @SideOnly(Side.CLIENT)
//    public void getSubBlocks(Item p_149666_1_, CreativeTabs p_149666_2_, List p_149666_3_) {
//        if (p_149666_1_ != Item.getItemFromBlock(Blocks.double_wooden_slab)) {
//            for (int i = 0; i < field_150005_b.length; ++i) {
//                p_149666_3_.add(new ItemStack(p_149666_1_, 1, i));
//            }
//        }
//    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister p_149651_1_) {
    }
}
