package ru.xlv.itemgen.util;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class LootSpawnConfig implements IConfigGson {

    @Getter(AccessLevel.PRIVATE)
    private final List<LootSpawnModel> elements = new ArrayList<>();

    private transient final File configFile = new File("config/item_gen/config.json");

    public LootSpawnModel getLootSpawnConfig(int id) {
        return id < elements.size() ? elements.get(id) : null;
    }

    @ToString
    @Getter
    @Configurable
    @RequiredArgsConstructor
    public static class LootSpawnModel {
        private final int id;
        private final int spawnPeriodSecs;
        private final List<ItemStackModel> items = new ArrayList<>();
    }

    @ToString
    @Getter
    @Configurable
    @RequiredArgsConstructor
    public static class ItemStackModel {
        private final String unlocalizedName;
        private final int amount;
        private final int metadata;
        private final String nbtJson;
        private final float chance;
    }
}
