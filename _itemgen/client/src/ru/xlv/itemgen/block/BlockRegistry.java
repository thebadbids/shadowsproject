package ru.xlv.itemgen.block;

import cpw.mods.fml.common.registry.GameRegistry;
import lombok.SneakyThrows;
import net.minecraft.block.Block;

import java.lang.reflect.Field;

public class BlockRegistry {

    public static final BlockLootSpawn LOOT_SPAWN = new BlockLootSpawn("lootSpawn");
    public static final BlockLootSpawn LOOT_SPAWN1 = new BlockLootSpawn("lootSpawn1");
    public static final BlockLootSpawn LOOT_SPAWN2 = new BlockLootSpawn("lootSpawn2");
    public static final BlockLootSpawn LOOT_SPAWN3 = new BlockLootSpawn("lootSpawn3");
    public static final BlockLootSpawn LOOT_SPAWN4 = new BlockLootSpawn("lootSpawn4");
    public static final BlockLootSpawn LOOT_SPAWN5 = new BlockLootSpawn("lootSpawn5");
    public static final BlockLootSpawn LOOT_SPAWN6 = new BlockLootSpawn("lootSpawn6");
    public static final BlockLootSpawn LOOT_SPAWN7 = new BlockLootSpawn("lootSpawn7");
    public static final BlockLootSpawn LOOT_SPAWN8 = new BlockLootSpawn("lootSpawn8");
    public static final BlockLootSpawn LOOT_SPAWN9 = new BlockLootSpawn("lootSpawn9");
    public static final BlockLootSpawn LOOT_SPAWN10 = new BlockLootSpawn("lootSpawn10");
    public static final BlockLootSpawn LOOT_SPAWN11 = new BlockLootSpawn("lootSpawn11");
    public static final BlockLootSpawn LOOT_SPAWN12 = new BlockLootSpawn("lootSpawn12");
    public static final BlockLootSpawn LOOT_SPAWN13 = new BlockLootSpawn("lootSpawn13");
    public static final BlockLootSpawn LOOT_SPAWN14 = new BlockLootSpawn("lootSpawn14");
    public static final BlockLootSpawn LOOT_SPAWN15 = new BlockLootSpawn("lootSpawn15");
    public static final BlockLootSpawn LOOT_SPAWN16 = new BlockLootSpawn("lootSpawn16");
    public static final BlockLootSpawn LOOT_SPAWN17 = new BlockLootSpawn("lootSpawn17");
    public static final BlockLootSpawn LOOT_SPAWN18 = new BlockLootSpawn("lootSpawn18");
    public static final BlockLootSpawn LOOT_SPAWN19 = new BlockLootSpawn("lootSpawn19");
    public static final BlockLootSpawn LOOT_SPAWN20 = new BlockLootSpawn("lootSpawn20");
    public static final BlockLootSpawn LOOT_SPAWN21 = new BlockLootSpawn("lootSpawn21");
    public static final BlockLootSpawn LOOT_SPAWN22 = new BlockLootSpawn("lootSpawn22");
    public static final BlockLootSpawn LOOT_SPAWN23 = new BlockLootSpawn("lootSpawn23");
    public static final BlockLootSpawn LOOT_SPAWN24 = new BlockLootSpawn("lootSpawn24");
    public static final BlockLootSpawn LOOT_SPAWN25 = new BlockLootSpawn("lootSpawn25");
    public static final BlockLootSpawn LOOT_SPAWN26 = new BlockLootSpawn("lootSpawn26");
    public static final BlockLootSpawn LOOT_SPAWN27 = new BlockLootSpawn("lootSpawn27");
    public static final BlockLootSpawn LOOT_SPAWN28 = new BlockLootSpawn("lootSpawn28");
    public static final BlockLootSpawn LOOT_SPAWN29 = new BlockLootSpawn("lootSpawn29");
    public static final BlockLootSpawn LOOT_SPAWN30 = new BlockLootSpawn("lootSpawn30");
    public static final BlockLootSpawn LOOT_SPAWN31 = new BlockLootSpawn("lootSpawn31");
    public static final BlockLootSpawn LOOT_SPAWN32 = new BlockLootSpawn("lootSpawn32");
    public static final BlockLootSpawn LOOT_SPAWN33 = new BlockLootSpawn("lootSpawn33");
    public static final BlockLootSpawn LOOT_SPAWN34 = new BlockLootSpawn("lootSpawn34");
    public static final BlockLootSpawn LOOT_SPAWN35 = new BlockLootSpawn("lootSpawn35");
    public static final BlockLootSpawn LOOT_SPAWN36 = new BlockLootSpawn("lootSpawn36");
    public static final BlockLootSpawn LOOT_SPAWN37 = new BlockLootSpawn("lootSpawn37");
    public static final BlockLootSpawn LOOT_SPAWN38 = new BlockLootSpawn("lootSpawn38");
    public static final BlockLootSpawn LOOT_SPAWN39 = new BlockLootSpawn("lootSpawn39");
    public static final BlockLootSpawn LOOT_SPAWN40 = new BlockLootSpawn("lootSpawn40");
    public static final BlockLootSpawn LOOT_SPAWN41 = new BlockLootSpawn("lootSpawn41");
    public static final BlockLootSpawn LOOT_SPAWN42 = new BlockLootSpawn("lootSpawn42");
    public static final BlockLootSpawn LOOT_SPAWN43 = new BlockLootSpawn("lootSpawn43");
    public static final BlockLootSpawn LOOT_SPAWN44 = new BlockLootSpawn("lootSpawn44");
    public static final BlockLootSpawn LOOT_SPAWN45 = new BlockLootSpawn("lootSpawn45");
    public static final BlockLootSpawn LOOT_SPAWN46 = new BlockLootSpawn("lootSpawn46");
    public static final BlockLootSpawn LOOT_SPAWN47 = new BlockLootSpawn("lootSpawn47");
    public static final BlockLootSpawn LOOT_SPAWN48 = new BlockLootSpawn("lootSpawn48");
    public static final BlockLootSpawn LOOT_SPAWN49 = new BlockLootSpawn("lootSpawn49");

    @SneakyThrows
    public static void register() {
        for (Field declaredField : BlockRegistry.class.getDeclaredFields()) {
            Object o = declaredField.get(null);
            register((Block) o);
        }
    }

    private static void register(Block block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
    }
}
