package ru.xlv.itemgen;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import lombok.Getter;
import ru.xlv.itemgen.block.BlockRegistry;
import ru.xlv.itemgen.tile.TileEntityLootSpawn;

import static ru.xlv.itemgen.XlvsItemGenMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsItemGenMod {

    static final String MODID = "itemgen";

    @Mod.Instance(MODID)
    public static XlvsItemGenMod INSTANCE;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        BlockRegistry.register();
        GameRegistry.registerTileEntity(TileEntityLootSpawn.class, "TileEntityLootSpawn");
    }
}
