package ru.xlv.cases.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.cases.XlvsCasesMod;
import ru.xlv.cases.handle.Case;
import ru.xlv.cases.handle.CaseItem;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketCaseListSync implements IPacketCallbackOnServer {

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {}

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(XlvsCasesMod.INSTANCE.getCaseHandler().getCaseManager().getCaseList().size());
        for (Case aCase : XlvsCasesMod.INSTANCE.getCaseHandler().getCaseManager().getCaseList()) {
            bbos.writeUTF(aCase.getName());
            bbos.writeInt(aCase.getPrice());
            bbos.writeUTF(aCase.getImageUrl());
            bbos.writeInt(aCase.getItems().size());
            for (CaseItem caseItem : aCase.getItems()) {
                ByteBufUtils.writeItemStack(bbos.buffer(), caseItem.getItemStack());
                bbos.writeInt(caseItem.getChance());
            }
        }
    }
}
