package ru.xlv.cases.pojo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.ItemStack;

@Getter
@RequiredArgsConstructor
public class CaseItem {

    private final ItemStack itemStack;
    private final int rarity = 0;
    private final int chance;
}
